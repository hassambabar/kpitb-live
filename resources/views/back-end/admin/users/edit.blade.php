@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="govt-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Edit {{$user->first_name}} {{$user->last_name}} </h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open([ 'url' => url('admin/profile/update/'.$user->id.''), 'class' =>'wt-formtheme wt-formprojectinfo
                            wt-formcategory', 'id' => 'locations' ]) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <label>Roles</label> 
                                        <!-- <select name="roles"> -->
                                        @if($user_role->id == 6)
                                            <input type="radio" id="3" name="role" value="3" checked> Committee Member </label>
                                        @else
                                            @foreach($roles as $role)
                                                @if($user_role->id == $role->id)
                                                    <input type="radio" id="option{{$role->id}}" name="role" value="{{$role->id}}" checked> {{$role->name}}  </label>
                                                @else
                                                    <input type="radio" id="option{{$role->id}}" name="role" value="{{$role->id}}"> {{$role->name}}  </label>
                                                @endif  
                                            @endforeach
                                        @endif
                                        <!-- </select> -->
                                    </div>
                                    <div class="form-group">
                                    @if($user_role->id == '2') 
                                        <label>Government Department</label>
                                        <select name="government_department">
                                                <option value="0" @if($user->government_department == 0) selected @endif >No</option>
                                                <option value="1" @if($user->government_department == 1) selected @endif >Yes</option>

                                        </select>
                                    @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text( 'first_name', e($user['first_name']), ['class' =>'form-control', 'placeholder' => trans('First Name' )] ) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text( 'last_name', e($user['last_name']), ['class' =>'form-control', 'placeholder' => trans('Last Name' )] ) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text( 'contact_number', e($user['contact_number']), ['class' =>'form-control', 'placeholder' => trans('Contact Number' )] ) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text( 'email', e($user['email']), ['class' =>'form-control','readonly'] ) !!}
                                    </div>
                                   
                                    <div class="form-group">
                                        <label>Password <span class="note">(Must contain 6 or more characters)</span></label>

                                        {!! Form::text( 'password',NULL, ['class' =>'form-control','placeholder' => trans('Password' ),'pattern' => '.{6,}'] ) !!}
                                        <!-- <input type="password" id="password" placeholder="Password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required> -->
                                    </div>
                                   
                                    
                                    <div class="form-group wt-btnarea">
                                        <input type="submit" value="Update User" class="wt-btn">
                                    </div>
                                </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
<style>
    input[type=text], input[type=password],input[type=email]{
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
  }
  input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
  }
    
  .note{
      color: #d8550c;
      font-size: 12px;
  }
   .signupbtn {
    /* float: left; */
   
    display: flex;
      justify-content:center;
  }
</style>
