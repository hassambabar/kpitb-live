@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="orgs-listing" id="org-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Search</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open(['url' => url('admin/users/skills'),
                            'method' => 'get', 'class' => 'wt-formtheme wt-formsearch'])
                        !!}
                            <fieldset>

                                <div class="form-group">
                                    <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                    class="form-control" placeholder="Title">
                                  
                                </div>

                                <!-- <div class="form-group">
                                    <input type="text" name="from_date" value="{{{ !empty($_GET['from_date']) ? $_GET['from_date'] : '' }}}"
                                    class="form-control" placeholder="From Date">
                                  
                                </div>

                                <div class="form-group">
                                    <input type="text" name="to_date" value="{{{ !empty($_GET['to_date']) ? $_GET['to_date'] : '' }}}"
                                    class="form-control" placeholder="To Date">
                                  
                                </div> -->
                                <div class="form-group">

                                    <select name="user_type" style="width: 100%">
                                        <option value="">-- Select User Type --</option>
                                        <option value="employer"  <?php if(isset($_GET['user_type'])) { if($_GET['user_type'] == 'employer'){ echo "selected";}}?> >Employer</option>
                                        <option value="freelancer"  <?php if(isset($_GET['user_type'])) { if($_GET['user_type'] == 'freelancer'){ echo "selected";}}?> >Freelancer</option>
                                    </select>
                                 
                                </div>

                                <div class="form-group"  >
                                    
                                    <select name="gender" style="width: 100%">
                                        <option value="">-- Gender --</option>
                                            <option value="male"  <?php if(isset($_GET['gender'])) { if($_GET['gender'] == 'male'){ echo "selected";}}?> >Male</option>
                                            <option value="female"  <?php if(isset($_GET['gender'])) { if($_GET['gender'] == 'female'){ echo "selected";}}?> >Female</option>
                                    </select>
                                </div>
                                
                                <div class="form-group"  >
                                    
                                    <select name="skill_id" style="width: 100%">
                                        <option value="">-- Select Skill --</option>
                                        @foreach($skills as $skill)
                                            <option value="{{$skill->id}}"  <?php if(isset($_GET['skill_id'])) { if($skill->id == $_GET['skill_id']){ echo "selected";}}?> >{{$skill->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group"  >
                                    
                                    <select name="location" style="width: 100%">
                                        <option value="">-- Select Location --</option>
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}"  <?php if(isset($_GET['location'])) { if($location->id == $_GET['location']){ echo "selected";}}?> >{{$location->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">

                                    <select name="freelaner_type" style="width: 100%">
                                        <option value="">-- Select Freelancer Type --</option>
                                        @foreach (Helper::getFreelancerLevelList() as $key => $freelancer_level)

                                        <option value="{{$key}}"  <?php if(isset($_GET['freelaner_type'])) { if($key == $_GET['freelaner_type']){ echo "selected";}}?> >{{$freelancer_level}}</option>
                                        
                                        @endforeach
                                    </select>

                                </div>
                                
                                <div class="form-group"  >
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="is_disabled" value="1" id="is_disabled"  <?php if(isset($_GET['is_disabled'])) {  echo "checked";}?> >
                                        <label class="form-check-label" for="is_disabled">
                                          Disabled
                                        </label>
                                      </div> 
                                    </div> 
                                <div class="form-group wt-btnarea">
                                    <button name="btn_submit" type="submit" value="Search" class="wt-btn" style="width: 100% !important" >Search</button>

                                    {{-- <input type="submit" value="Search" name="submit" class="wt-btn"> --}}
                                </div>
                                <div class="form-group wt-btnarea">
                                    <button name="btn_submit" type="submit" value="Download" class="wt-btn"  style="width: 100% !important">Download</button>

                                    {{-- <input type="submit" value="Download" name="submit"  class="wt-btn"> --}}
                                </div>
                            </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Users</h2>
                            {{-- {!! Form::open(['url' => url('admin/organizationtype/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="Organization Type">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!} --}}
                           
                        </div>
                      
                    @if ($Users->count() > 0)
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                        <table class="wt-tablecategories" id="checked-val">
                            <thead>
                                <tr>
                                  
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Total Earnings</th>
                                    <th>Skills</th>

                                    <th>Created Date</th>

                                    <th>{{{ trans('lang.action') }}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $counter = 0; @endphp
                                @foreach ($Users as $user)
                                    <tr class="del-{{{ $user->id }}}">
                                     
                                        <td><a href="{{{ url('profile/'.$user->slug) }}}">{{{ Helper::getUserName($user->id) }}}</a><br>
                                           <small> @if($user->profile->gender) {{$user->profile->gender}}, @endif  @if($user->location) {{$user->location->title}} , @endif  @if($user->profile->gender) {{$user->profile->freelancer_type}},  @endif 
                                        @if ($user->is_disabled == 'true')
                                        Disabled
                                        @else
                                           Enabled 
                                        @endif
                                        </small>
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td><?php  $totalEarning = Helper::getProposalsBalance($user->id, 'completed'); ?> @if ($totalEarning >0 )
                                          
                                               {{$english_format_number = number_format($totalEarning)}}
                                           
                                        @endif</td>
                                        <td> @if (!empty($user->skills))
                                          
                                            
                                                  
                                                   @foreach($user->skills as $skill)
                                                      <a href="{{{url('search-results?type=job&skills%5B%5D='.$skill->slug)}}}">{{{ $skill->title }}}</a>,
                                                   @endforeach
                                           @endif</td>
                                        <td>{{{ $user->created_at }}}</td>

                                        <td>
                                            <div class="wt-actionbtn">
                                                <a href="{{{ url('profile/'.$user->slug) }}}" class="wt-addinfo wt-skillsaddinfo">
                                                    <i class="lnr lnr-eye"></i>
                                                </a>
                                                {{-- <a href="{{{ url('admin/organizationtype/edit-orgs') }}}/{{{ $org->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                    <i class="lnr lnr-pencil"></i>
                                                </a>
                                                <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$org->id}}'" :message="'{{trans("lang.ph_lang_delete_message")}}'" :url="'{{url('admin/organizationtype/delete-orgs')}}'"></delete> --}}
                                            </div>
                                        </td>
                                    </tr>
                                    @php $counter++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @if( method_exists($Users,'links') ) {{ $Users->links('pagination.custom') }} @endif
                    </div>
                @else
                    @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                        @include('extend.errors.no-record')
                    @else 
                        @include('errors.no-record')
                    @endif
                @endif
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
