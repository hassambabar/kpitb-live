@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
 
<div class="wt-haslayout wt-main-section">
    <div class="container">
        <div class="row justify-content-md-center">
                <div class="col-xs-12 col-sm-12 col-md-10 push-md-1 col-lg-10 push-lg-2" id="registration"> 
                    <div class="wt-registerformhold">
                        <div class="wt-registerformmain">
                            <div class="wt-joinforms"> 
                                <div class="col-md-12">
                                    <br />
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                            </ul>
                                        </div><br />
                                            @endif
                                        </div>
                <form method="POST" action="{{url('add_usermanage_process')}}"
                    accept-charset="UTF-8" class="">
                    @csrf
    
                            <div class="wt-haslayout" v-if="step === 1" v-cloak>
                                <div class="wt-registerhead">
                                    <div class="wt-title">
                                        <h3>Create a New User</h3>
                                    </div>
                                </div>
                                    <div class="container">
                                        <label for="first_name" ><b>Full Name</b></label>
                                        <input type="text" placeholder="First Name" name="first_name" required>
                                        <label for="last_name"><b>Last Name</b></label>
                                        <input type="text" placeholder="Last Name" name="last_name" required>
                                        <label for="email"><b>Email</b></label>
                                        <input type="email" autocomplete="off" id="user_email" placeholder="email@example.com" name="email" class="form-group" required>
                                        <label for="email"><b>Choose Role </b></label>
                                        <div class="form-group form-group-halfform">
                                        <select onchange="check()" id="roles" name="role_id" required>
                                                    <option value="">Choose User</option>  
                                                    @foreach($roles as $role)
                                                @if($role->id==1 || $role->id==2 || $role->id==3 || $role->id ==4 || $role->id==5)
                                                <option value="{{$role->id}}">{{$role->name}}
                                                            @endif
                                                    </option> 
                                                            @endforeach
                                                        </select>
                                                        <select disabled id="roles_emp" name="role_emp" required>
                                                        <option value="">Choose Employer</option>  
                                                        <option value="govt_emp">Government Employer</option> 
                                                        <option value="emp">Employer</option> 
                                                    </select> 
                                    </div>
                                        <div class="form-group">
                                        <label for="password"><b>Password</b><span class="note">  (Must contain 6 or more characters)</span></label>
                                        <input type="password" id="password" placeholder="Password" name="password" pattern=".{6,}" required>
                                        <label for="password_confirmation"><b>Repeat Password</b></label>
                                        <input type="password" id="password_confirmation" placeholder="Repeat Password" name="password_confirmation" required>
                                        <span id='message'></span> 

                                        </div>
                                   
                                </div>
                               
                                        <div class="form-group signupbtn">
                                            <input type="submit" id="submit_button" value="Save" class="wt-btn ">
                                        </div>
                                   
                </form>
                        </div>
                    </div>
                </div> 
             </div>
        </div>
    </div>
</div>
    
    @endsection
<style>
    input[type=text], input[type=password],input[type=email]{
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
  }
  input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
  }
    
  .note{
      color: #d8550c;
      font-size: 12px;
  }
   .signupbtn {
    /* float: left; */
   
    display: flex;
      justify-content:center;
  }
</style>
@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script type="text/javascript">
    $('#password, #password_confirmation').on('keyup', function () {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("password_confirmation").value;
        if (password != confirmPassword) {
            $('#message').html('<p id="length" class="invalid">Password does not match</p>').css('color', 'red');
            $("#submit_button").css("display","none");
        return false;
        }
        if (password == confirmPassword) {
            $('#message').html('Password Matched').css('color', 'green');
            $("#submit_button").css("display","block");
                        // console.log("bbb");
        return false;
        }
    });
    $("body").on("change", "#locationsDropDown", function () {
    var locId = $(this).val();
    $(".preloader-outer").show();
        $.ajax({
            type: 'POST',
            url: 'getAjaxDistrics',
            cache: false,
            dataType: 'html',
            data: {
                locId: locId,
                _token: '{{csrf_token()}}'
            },
            success: function (res) {
                $("#distDropDown").html(res);
                $(".preloader-outer").hide();
            }
        });
    });

    function check(){
  if(document.getElementById('roles').value!='2')
    document.getElementById('roles_emp').disabled=true;
  else
    document.getElementById('roles_emp').disabled=false;
}
   
    </script>
   
 




@stop