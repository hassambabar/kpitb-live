@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace" id="profile_settings">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right">
                @if (Session::has('message'))
                    <div class="flash_msg">
                        <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                    </div>
                @endif
                <div>
                    
                    
                </div>
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>{{{ trans('lang.manage_users') }}}</h2>
                        <form class="wt-formtheme wt-formsearch">
                        <a style="text-decoration: none;" href="/create-user" class="wt-btn button button-create w3-round-xlarge">Add New User</a>

                            <fieldset>
                                
                                <div class="form-group">
                                    <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                        class="form-control" placeholder="{{{ trans('lang.ph_search_users') }}}">
                                    <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i>
                                </button> 
                                </div>
                            </fieldset>
                        </form>
                        
                    </div><br>
                 
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                        @if ($users->count() > 0)
                        <div class="alert alert-secondary score-bad2" role="alert">
                                * Click on the status to change it.
                        </div>
                            <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                        <th>{{{ trans('lang.user_name') }}}</th>
                                        <th>{{{ trans('lang.ph_email') }}}</th>
                                        <th>{{{ trans('lang.role') }}}</th>
                                        <th>{{{ trans('lang.verification_status') }}} *</th>

                                        <th>Status *</th>
                                        <th>{{{ trans('Government Department') }}} *</th>
                                        <th>{{{ trans('lang.action') }}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $key => $user_data)
                                        @php $user = \App\User::find($user_data['id']); @endphp
                                        @if ($user->getRoleNames()->first() != 'admin')
                                            <tr class="del-user-{{ $user->id }}">
                                                <td>{{{ ucwords(\App\Helper::getUserName($user->id)) }}}</td>
                                                <td>{{{ $user->email }}}</td>
                                                <td>
                                                    @if($user->user_type == 2)
                                                        Committee Member
                                                    @else
                                                        {{$user->getRoleNames()->first()}}
                                                    @endif
                                                </td>
                                                <td id="verify_user-{{$user->id}}">
                                                    @if ($user->user_verified == 1)
                                                        <a href="javascript:;" class="" v-on:click.prevent="verifiedUserEmail('verify_user-{{$user->id}}', '{{$user->id}}', 'not_verify')">{{ trans('lang.verified') }}</a>
                                                    @else
                                                        <a href="javascript:;" class="" v-on:click.prevent="verifiedUserEmail('verify_user-{{$user->id}}', '{{$user->id}}', 'verify')">{{ trans('lang.not_verified') }}</a>
                                                    @endif
                                                </td>
                                                <td id="verify_user-{{$user->id}}">
                                                    @if ($user->is_disabled == 'true')
                                                        <a href="javascript:;" class="" v-on:click.prevent="UserDisable('verify_user-{{$user->id}}', '{{$user->id}}', 'false')">Disabled User</a>
                                                    @else
                                                        <a href="javascript:;" class="" v-on:click.prevent="UserDisable('verify_user-{{$user->id}}', '{{$user->id}}', 'true')">Enabled User</a>
                                                    @endif
                                                </td>
                                                <td id="government_user-{{$user->id}}">
                                                @if($user->getRoleNames()->first() == 'employer')
                                                    @if ($user->government_department == 1)
                                                        <a href="javascript:;" class="" v-on:click.prevent="govtUserEmail('government_user-{{$user->id}}', '{{$user->id}}', 'not_govt')">{{ trans('Government Department') }}</a>
                                                    @else
                                                        <a href="javascript:;" class="" v-on:click.prevent="govtUserEmail('government_user-{{$user->id}}', '{{$user->id}}', 'govt')">{{ trans('Non-Government Department') }}</a>
                                                    @endif
                                                @else
                                                    <a href="javascript:;" class="">-</a>
                                                @endif
                                                </td>
                                                <td>   
                                                    <div class="wt-actionbtn">
                                                        <a href="{{ url('admin/profile/edit/'.$user->slug) }}"  class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-edit"></i></a>
                                                        <a href="{{ url('admin/contactmessage/'.$user->id) }}" class="wt-addinfo wt-skillsaddinfo"><i class="ti-comment-alt"></i></a>
                                                        @php
                                                            $role = $user->getRoleNames()->first();
                                                        @endphp
                                                        @if($role == 'employer')
                                                            @php
                                                                $employer_exist = DB::table('committee_members_employers')->where('employer_id', $user->id)->exists();
                                                                $proposals = \App\Proposal::where('freelancer_id', $user->id)->get();
                                                            @endphp
                                                            @if($employer_exist !== 1 && $proposals->isEmpty())

                                                                @php 
                                                                    $jobs = \App\Job::where('user_id', $user->id)->get();
                                                                @endphp
                                                                @if ($jobs->isEmpty())
                                                                    <a href="javascript:void()" v-on:click.prevent="deleteUser({{$user->id}})" class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-trash"></i></a> 
                                                                    <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>
                                                                @else    
                                                                    <a  class="wt-deleteinfo wt-skillsaddinfo bg-dark" popup-open="popup-1" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                                        <div class="popup" popup-name="popup-1">
                                                                            <div class="popup-content">
                                                                                <p class="wt-title">User has performed activities on the portal so can not be deleted.</p>
                                                                                <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                            </div>
                                                                        </div>
                                                                    <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>  
                                                                @endif
                                                                    
                                                            @else
                                                            <a  class="wt-deleteinfo wt-skillsaddinfo bg-dark" popup-open="popup-1" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                                        <div class="popup" popup-name="popup-1">
                                                                            <div class="popup-content">
                                                                                <p class="wt-title">User has performed activities on the portal so can not be deleted.</p>
                                                                                <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                            </div>
                                                                        </div>
                                                                    <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>  
                                                            @endif
                                                        @elseif($user->user_type == 2)
                                                            @php 
                                                                $committee_member_exist = DB::table('committee_members_employers')->where('committee_member_id', $user->id)->exists();
                                                            @endphp
                                                            @if ($committee_member_exist == 1)
                                                                <a  class="wt-deleteinfo wt-skillsaddinfo bg-dark" popup-open="popup-1" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                                    <div class="popup" popup-name="popup-1">
                                                                        <div class="popup-content">
                                                                            <p class="wt-title">User has performed activities on the portal so can not be deleted.</p>
                                                                            <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                        </div>
                                                                    </div>
                                                                <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>  
                                                            @else  
                                                                <a href="javascript:void()" v-on:click.prevent="deleteUser({{$user->id}})" class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-trash"></i></a> 
                                                                <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>  
                                                            @endif
                                                        @elseif ($role == 'freelancer')
                                                            @php 
                                                                $employer_exist = DB::table('committee_members_employers')->where('employer_id', $user->id)->exists();
                                                                $proposals = \App\Proposal::where('freelancer_id', $user->id)->get();
                                                            @endphp
                                                            @if ($proposals->isEmpty() && $employer_exist != 1)
                                                                <a href="javascript:void()" v-on:click.prevent="deleteUser({{$user->id}})" class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-trash"></i></a>
                                                                <a href="{{ url('profile/'.$user->slug) }}" class="wt-eye wt-addinfo wt-skillsaddinfo"><i class="lnr lnr-eye"></i></a>
                                                            @else   
                                                                <a  class="wt-deleteinfo wt-skillsaddinfo bg-dark" popup-open="popup-1" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                                    <div class="popup" popup-name="popup-1">
                                                                        <div class="popup-content">
                                                                            <p class="wt-title">User has performed activities on the portal so can not be deleted.</p>
                                                                            <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                        </div>
                                                                    </div> 
                                                                <a href="{{ url('profile/'.$user->slug) }}" class="wt-addinfo wt-skillsaddinfo wt-eye"><i class="lnr lnr-eye"></i></a>
                                                            @endif
                                                        @else
                                                                <a href="javascript:void()" v-on:click.prevent="deleteUser({{$user->id}})" class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-trash"></i></a> 
                                                                <a href="{{ url('profile/'.$user->slug) }}" class="wt-addinfo wt-skillsaddinfo wt-eye"><i class="lnr lnr-eye"></i></a>
                                                        @endif

                                                                <!-- <a href="javascript:void()" v-on:click.prevent="deleteUser({{$user->id}})" class="wt-deleteinfo wt-skillsaddinfo"><i class="fa fa-trash"></i></a> -->
                                                            
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php')))
                                @include('extend.errors.no-record')
                            @else
                                @include('errors.no-record')
                            @endif
                        @endif
                        @if ( method_exists($users,'links') )
                            {{ $users->links('pagination.custom') }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
       $(function() {
    // Open Popup
    $('[popup-open]').on('click', function() {
        var popup_name = $(this).attr('popup-open');
 $('[popup-name="' + popup_name + '"]').fadeIn(300);
    });
 
    // Close Popup
    $('[popup-close]').on('click', function() {
 var popup_name = $(this).attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    });
 
    // Close Popup When Click Outside
    $('.popup').on('click', function() {
 var popup_name = $(this).find('[popup-close]').attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    }).children().click(function() {
 return false;
    });
 
});
</script>
@endsection
