@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="pages-listing" id="pages-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
        <div class="flash_msg">
            <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
        </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>{{{ trans('lang.add_page') }}}</h2>
                            <div class="wt-rightarea">
                                <a href="{{{ url('admin/create/skilltype') }}}" class="wt-btn">Create New Skill By Type</a>
                            </div>
                            <a href="javascript:void(0);" v-if="this.is_show" @click="deleteChecked('{{ trans('lang.ph_delete_confirm_title') }}', '{{ trans('lang.ph_page_delete_message') }}')" class="wt-skilldel">
                                <i class="lnr lnr-trash"></i>
                                <span>{{ trans('lang.del_select_rec') }}</span>
                            </a>
                        </div>
                        @if ($skilltypes->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="wt-checkbox">
                                                    <input name="pages[]" id="wt-pages" @click="selectAll" type="checkbox" name="head">
                                                    <label for="wt-pages"></label>
                                                </span>
                                            </th>
                                            <th>{{{ trans('lang.name') }}}</th>
                                            <th>{{{ trans('lang.type') }}}</th>
                                            <th>{{{ trans('lang.action') }}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($skilltypes as $skilltype)
                                            <tr class="del-{{{ $skilltype->id }}}" v-bind:id="pageID">
                                                <td>
                                                    <span class="wt-checkbox">
                                                        <input name="pages[]" @click="selectRecord" value="{{{ $skilltype->id }}}" id="wt-check-{{{ $counter }}}" type="checkbox" name="head">
                                                        <label for="wt-check-{{{ $counter }}}"></label>
                                                    </span>
                                                </td>
                                                <td>{{{ $skilltype->skill_name }}}</td>
                                                @if($skilltype->skill_type == 'budget')
                                                <td>By Budget</td>
                                                @elseif($skilltype->skill_type == 'skill')
                                                <td>By Skill</td>
                                                @elseif($skilltype->skill_type == 'category')
                                                <td>By Category</td>
                                                @else
                                                <td>By Location</td>
                                                @endif
                                                <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/edit-skill-type') }}}/{{{ $skilltype->id }}}" class="wt-addinfo wt-pages">
                                                            <i class="lnr lnr-pencil"></i>
                                                        </a>
                                                        <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$skilltype->id}}'" :message="'{{trans("lang.ph_page_delete_message")}}'" :url="'{{url('admin/delete-skill-type')}}'"></delete>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($skilltypes,'links') ) {{ $skilltypes->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php')))
                                @include('extend.errors.no-record')
                            @else
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
