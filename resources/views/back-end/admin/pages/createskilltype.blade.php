@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@push('sliderStyle')
    <link href="{{ asset('css/antd.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 float-left">
  <div class="wt-dashboardbox">
    <div class="wt-dashboardboxtitle">
      <h2>Add New Skill</h2>
    </div>
    <div class="wt-dashboardboxcontent">
      <form method="POST" action="{{url('admin/store-skill-type')}}" accept-charset="UTF-8" id="skills" class="wt-formtheme wt-formprojectinfo wt-formcategory">
        {{ csrf_field() }}
         <fieldset>
           <div class="form-group">
             <input placeholder="Skill Title / Location" name="skill_title" type="text" class="form-control">
           </div>
           <div class="form-group">
             <select name="skill_type" class="form-control">
               <option value="budget">By Budget</option>
               <option value="skill">By Skill</option>
               <option value="category">By Category</option>
               <option value="location">By Location</option>
             </select>

           </div>
           <div class="form-group wt-btnarea">
             <input type="submit" value="Add New Skill Type" class="wt-btn">
           </div>
         </fieldset>
       </form>
     </div>
   </div>
 </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>

@endpush
