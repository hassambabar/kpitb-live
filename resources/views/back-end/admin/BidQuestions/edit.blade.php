@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="govt-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Edit Bid CriteriaQ uestion</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open([ 'url' => url('admin/bid-questions-update/'.$BidCriteriaQuestion->id.''), 'class' =>'wt-formtheme wt-formprojectinfo
                            wt-formcategory', 'id' => 'locations' ]) !!}
                                <fieldset>
                                    <div class="form-group">
                                    
                                        <select name="question_type">
                                                <option value="0" @if($BidCriteriaQuestion->question_type == 0) selected @endif >Technical</option>
                                                <option value="1" @if($BidCriteriaQuestion->question_type == 1) selected @endif >Financial</option>
                                                <option value="2" @if($BidCriteriaQuestion->question_type == 2) selected @endif >General</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea( 'question', e($BidCriteriaQuestion['question']), ['class' =>'form-control'.($errors->has('question') ? ' is-invalid' : '')] ) !!}
                                        
                                        @if ($errors->has('question'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('question') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group wt-btnarea">
                                        <input type="submit" value="Update Question" class="wt-btn">
                                    </div>
                                </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
