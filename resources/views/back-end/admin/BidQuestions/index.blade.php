@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="dist-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Add Bid Criteria Question</h2>
                        </div>

                        <div class="wt-dashboardboxcontent">
                            @if ($errors->any())
                            <div class="alert alert-danger col-md-12">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                            {!! Form::open(['url' => route('bidquestions.store'), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory','id' =>
                            'BidCriteriaQuestion']) !!}
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    
                                    <select name="question_type">
                                        <option value="">--Question Type--</option>
                                            <option value="0">Technical</option>
                                            <option value="1">Financial</option>
                                            <option value="2">General</option>
                                    </select>
                                    @if ($errors->has('question_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('question_type') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    
                                    {!! Form::textarea( 'question', null, ['class' =>'form-control'.($errors->has('question') ? ' is-invalid' : ''),
                                    'placeholder' => 'Question'] ) !!}
                                    
                                </div>
                                <div class="form-group wt-btnarea">
                                    <input type="submit" value="Add Question" class="wt-btn">
                                </div>
                            </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Bid Criteria Questions</h2>
                            {!! Form::open(['url' => url('admin/bid-questions/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="Question Title">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!}
                            
                        </div>
                        @if ($BidCriteriaQuestion->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">

                                @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                           
                                            <th>Title</th>
                                            <th>Type</th>
                                            <th>{{{ trans('lang.action') }}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($BidCriteriaQuestion as $singleQuestion)
                                            <tr class="del-{{{ $singleQuestion->id }}}" v-bind:id="districtID">
                                              
                                                <td>
                                                   {{$singleQuestion->question}}
                                                </td>
                                                <td>
                                                    @if($singleQuestion->question_type == 0)
                                                    Technical
                                                    @elseif($singleQuestion->question_type == 1)
                                                    Financial
                                                    @elseif($singleQuestion->question_type == 2)
                                                    General
                                                    @endif
                                                   
                                                </td>
                                                <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/bid-questions-edit') }}}/{{{ $singleQuestion->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                            <i class="lnr lnr-pencil"></i>
                                                        </a>
                                                        @php
                                                            $bid_question = DB::table('bid_questions')->where('title', $singleQuestion->question)->get();
                                                        @endphp
                                                        @if ($bid_question->count() > 0)
                                                            <a class="wt-addinfo wt-skillsaddinfo bg-dark open-button" popup-open="popup-1" href="javascript:void(0)">
                                                                <i class="lnr lnr-trash"></i>
                                                            </a>
                                                            <div class="popup" popup-name="popup-1">
                                                                <div class="popup-content">
                                                                    <p class="wt-title">Question is added in a job so can not be deleted.</p>
                                                                    <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                </div>
                                                            </div>  
                                                        @else
                                                            <form action="{{ route('bidquestions.destroy', $singleQuestion->id)}}" method="post" class="formDelete wt-skillsaddinfo">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('Are you sure you want to delete this item?');"class="wt-deleteinfo" type="submit"><i class="lnr lnr-trash"></i></button>
                                                            </form>
                                                        @endif
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($singleQuestion,'links') ) {{ $singleQuestion->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
<script>
       $(function() {
    // Open Popup
    $('[popup-open]').on('click', function() {
        var popup_name = $(this).attr('popup-open');
 $('[popup-name="' + popup_name + '"]').fadeIn(300);
    });
 
    // Close Popup
    $('[popup-close]').on('click', function() {
 var popup_name = $(this).attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    });
 
    // Close Popup When Click Outside
    $('.popup').on('click', function() {
 var popup_name = $(this).find('[popup-close]').attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    }).children().click(function() {
 return false;
    });
 
});
</script>
@endsection
