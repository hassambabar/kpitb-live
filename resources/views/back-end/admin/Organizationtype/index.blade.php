@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="orgs-listing" id="org-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Add New Organization Type</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open(['url' => url('admin/store-organizationtype'), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory','id' =>
                            'organizationtype']) !!}
                            <fieldset>
                                <div class="form-group">
                                    {!! Form::text( 'organization_name', null, ['class' =>'form-control'.($errors->has('organization_name') ? ' is-invalid' : ''),
                                    'placeholder' => 'Organization Type'] ) !!}
                                    
                                    @if ($errors->has('organization_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('organization_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group wt-btnarea">
                                    <input type="submit" value="Add New Organization Type" class="wt-btn">
                                </div>
                            </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Organization Types</h2>
                            {!! Form::open(['url' => url('admin/organizationtype/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="Organization Type">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!}
                            <a href="javascript:void(0);" v-if="this.is_show" @click="deleteChecked('{{ trans('lang.ph_delete_confirm_title') }}', '{{ trans('lang.ph_lang_delete_message') }}')" class="wt-skilldel">
                                <i class="lnr lnr-trash"></i>
                                <span>{{ trans('lang.del_select_rec') }}</span>
                            </a>
                        </div>
                        @if ($orgs->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="wt-checkbox">
                                                    <input name="orgs[]" id="wt-langs" @click="selectAll" type="checkbox" name="head">
                                                    <label for="wt-langs"></label>
                                                </span>
                                            </th>
                                            <th>Name</th>
                                            <th>{{{ trans('lang.action') }}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($orgs as $org)
                                            <tr class="del-{{{ $org->id }}}" v-bind:id="orgID">
                                                <td>
                                                    <span class="wt-checkbox">
                                                        <input name="orgs[]" @click="selectRecord" value="{{{ $org->id }}}" id="wt-check-{{{ $counter }}}" type="checkbox" name="head">
                                                        <label for="wt-check-{{{ $counter }}}"></label>
                                                    </span>
                                                </td>
                                                <td>{{{ $org->organization_name }}}</td>
                                                <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/organizationtype/edit-orgs') }}}/{{{ $org->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                            <i class="lnr lnr-pencil"></i>
                                                        </a>
                                                        @php
                                                            $user_org = DB::table('users')->where('org_type_id', $org->id)->get();
                                                            $govt_dept_org = DB::table('govt_departments')->where('org_type_id', $org->id)->get();
                                                        @endphp
                                                        @if ($user_org->count() > 0 || $govt_dept_org->count() > 0)
                                                            <a class="wt-addinfo wt-skillsaddinfo bg-dark open-button" popup-open="popup-1" href="javascript:void(0)">
                                                                <i class="lnr lnr-trash"></i>
                                                            </a>
                                                            <div class="popup" popup-name="popup-1">
                                                                <div class="popup-content">
                                                                    <p class="wt-title">Government Department has data so can not be deleted.</p>
                                                                    <a class="close-button" popup-close="popup-1" href="javascript:void(0)">x</a>
                                                                </div>
                                                            </div>  
                                                        @else
                                                            <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$org->id}}'" :message="'{{trans("lang.ph_lang_delete_message")}}'" :url="'{{url('admin/organizationtype/delete-orgs')}}'"></delete>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($orgs,'links') ) {{ $orgs->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
<script>
       $(function() {
    // Open Popup
    $('[popup-open]').on('click', function() {
        var popup_name = $(this).attr('popup-open');
 $('[popup-name="' + popup_name + '"]').fadeIn(300);
    });
 
    // Close Popup
    $('[popup-close]').on('click', function() {
 var popup_name = $(this).attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    });
 
    // Close Popup When Click Outside
    $('.popup').on('click', function() {
 var popup_name = $(this).find('[popup-close]').attr('popup-close');
 $('[popup-name="' + popup_name + '"]').fadeOut(300);
    }).children().click(function() {
 return false;
    });
 
});
</script>
@endsection
