@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="dpts-listing" id="reviews">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-review-holder">
            <div class="row">
            
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-xl-12 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Reviews</h2>
                            {{-- <a href="javascript:void(0);" v-if="this.is_show" @click="deleteChecked('{{ trans('lang.ph_delete_confirm_title') }}', '{{ trans('lang.ph_review_delete_message') }}')" class="wt-skilldel">
                                <i class="lnr lnr-trash"></i>
                                <span>{{ trans('lang.del_select_rec') }}</span>
                            </a> --}}
                        </div>
                        <div class="col-md-12">
                            {!! Form::open(['url' => url('admin/review'),
                            'method' => 'post'])
                        !!}
                        @csrf
                            <div class="form-row">
                            <div class="col-md-5">
                              <label for="mobile2">From</label>
            
                                <input id="mobile2" type="text" class="form-control" name="from" value="<?php if(isset($_POST['from'])){ echo $_POST['from']; }?>">
                            </div>
            
                            <div class="col-md-5">
                             <label for="email2" data-error="Please enter a proper e-mail format (e.g. abc@xyz.com)" data-success="right">To</label>
            
                                <input id="email2" type="email" class="form-control" name="to" value="<?php if(isset($_POST['to'])){ echo $_POST['to']; }?>">
                            </div>
                            <div class="col-md-2 wt-btnarea">
                                <label for="email2" data-error="Please enter a proper e-mail format (e.g. abc@xyz.com)" data-success="right">-</label>
                                <button class="btn wt-btn" type="submit" id="submit"><i class="icon-lock"></i> Search</button>
                               </div>
                        </div><br />
                        {!! Form::close(); !!}
                        <hr>
                        </div>
                        @if ($Review->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                          
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Job</th>
                                            <th>avg rating</th>
                                            <th>Created At</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($Review as $option)
                                            <tr class="del-{{{ $option->id }}}" v-bind:id="optionID">
                                              
                                              
                                                
                                                <td>{{ $option->ReviewFrom->email }}</td>
                                                
                                                <td>{{ $option->Reviewto->email }}</td>
                                                <td>{{ $option->job->title }}</td>
                                                <td>{{ $option->avg_rating }}</td>
                                                <td>{{ $option->created_at }}</td>
                                                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal_{{ $option->id }}">
                                                    View
                                                  </button>
                                                
                                                    <div class="modal fade" id="exampleModal_{{ $option->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                          <div class="modal-content">
                                                            
                                                            <div class="modal-body">
                                                                {!! $option->feedback !!}
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                
                                                </td>
                                                {{-- <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/review-options/edit-review-options') }}}/{{{ $option->id }}}" class="wt-addinfo wt-dpts">
                                                            <i class="lnr lnr-pencil"></i>
                                                        </a>
                                                        <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$option->id}}'" :message="'{{trans("lang.ph_review_delete_message")}}'" :url="'{{url('admin/review-options/delete-review-options')}}'"></delete>
                                                    </div>
                                                </td> --}}
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($Review,'links') ) {{ $Review->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
