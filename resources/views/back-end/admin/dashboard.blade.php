@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')

<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
  <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css" type="text/css" rel="stylesheet">
  <link href="https://cdn.anychart.com/releases/v8/fonts/css/anychart-font.min.css" type="text/css" rel="stylesheet">


    <section class="wt-haslayout wt-dbsectionspace wt-insightuser" id="dashboard">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
            @php session()->forget('message');  @endphp
        @endif
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="wt-insightsitemholder wt-employer-dashboard">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>&lowast; Total Jobs</h3>
                                        <h2>{{$count_total_jobs}}</h2>
                                    </div>
                                </div>                            

                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Completed Jobs</h3>
                                       <h2>{{$count_completed_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Current Jobs</h3>
                                       <h2>{{$count_ongoing_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Cancelled Jobs</h3>
                                       <h2>{{$count_cancelled_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Total Users</h3>
                                       <h2>{{$count_total_users}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Freelancers</h3>
                                       <h2>{{$cout_user_free}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Procurers</h3>
                                       <h2>{{$cout_user_emp}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Skills</h3>
                                       <h2>{{$count_Skill}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Feedback/Rating</h3>
                                       <h2>{{$count_Review}}</h2>
                                       <a href="{{{ url('admin/review') }}}">{{ trans('lang.click_view') }}</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 mt-4">
                            <p>*Total Jobs count is equal to the posted jobs, expired jobs, completed jobs, ongoing jobs, cancelled jobs.</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>New Jobs</h3>
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                          
                                            <th>Title</th>
                                            <th>Skills</th>
                                            <th>Posted By</th>
                                            <th>Posted Date</th>
                                        </tr>
                                    </thead>                            <tbody>

                                    @forelse ($ongoing_jobs_5 as $jonbSingle)
                                    <tr>
                                    <td><a href="{{{ url('job/'.$jonbSingle->slug) }}}">{{ $jonbSingle->title }}</a></td>
                                    <td> @forelse ($jonbSingle->skills as $item)
                                        {{$item->title}} ,
                                    @empty
                                        -
                                    @endforelse  </td>
                                    <td><?php echo $user_name = !empty($jonbSingle->employer->id) ? Helper::getUserName($jonbSingle->employer->id) : ''; ?></td>
                                    <td>{{{ $jonbSingle->created_at }}}</td>
                                    </tr>
                                    @empty
                                        
                                    @endforelse
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>New Procurers</h3>
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                          
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Location</th>
                                            <th>Posted Date</th>
                                        </tr>
                                    </thead>                            <tbody>

                                    @forelse ($user_by_role_employer as $single_employer)
                                    <tr>
                                    <td><a href="{{{ url('profile/'.$single_employer->slug) }}}">{{{ Helper::getUserName($single_employer->id) }}}</a></td>
                                    <td>{{$single_employer->email}}</td>
                                    <td>{{isset($single_employer->location['title']) ? $single_employer->location['title'] : ''}}</td>
                                    <td>{{{isset($single_employer->created_at) ? $single_employer->created_at : ''}}}</td>
                                    </tr>
                                    @empty
                                        
                                    @endforelse
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>New Freelancers</h3>
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                          
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Skills</th>
                                            <th>Ratings</th>
                                            <th>Posted Date</th>
                                        </tr>
                                    </thead>                            
                                    <tbody>

                                    @forelse ($user_by_role_freelancer as $single_freelancer)
                                    @if($single_freelancer->user_type != '2')
                                    <tr>
                                    <td><a href="{{{ url('profile/'.$single_freelancer->slug) }}}">{{{ Helper::getUserName($single_freelancer->id) }}}</a></td>
                                    <td>{{$single_freelancer->email}}</td>
                                    <td> @forelse ($single_freelancer->skills as $item)
                                        {{$item->title}} ,
                                    @empty
                                        -
                                    @endforelse</td>
                                    @php
                                        $feedbacks = \App\Review::select('feedback')->where('receiver_id', $single_freelancer->id)->count();
                                        $reviews = \App\Review::where('receiver_id', $single_freelancer->id)->get();
                                        $stars  = $reviews->sum('avg_rating') != 0 ? (($reviews->sum('avg_rating')/$feedbacks)/5)*100 : 0;
                                    @endphp
                                    <td><span class="wt-stars"><span style="width: {{ $stars }}%;"></span></span></td>
                                    <td>{{{ $single_freelancer->created_at }}}</td>
                                    </tr>
                                    @endif  
                                    @empty
                                    
                                    @endforelse
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>Freelancer by Location</h3>
                                    <script>
                                        anychart.onDocumentReady(function () {
                                          var data = [
                                              {!! $chart_user_freelancer_location !!}
                                          ];
                                          // sort data by alphabet order
                                          data.sort(function (itemFirst, itemSecond) {
                                            return itemSecond[1] - itemFirst[1];
                                          });
                                          // create bar chart
                                          var chart = anychart.bar();
                                    
                                          // turn on chart animation
                                          chart
                                            .animation(true)
                                            .padding([10, 40, 5, 20])
                                            // set chart title text settings
                                            .title('');
                                    
                                          // create area series with passed data
                                          var series = chart.bar(data);
                                          // set tooltip formatter
                                          series
                                            .tooltip()
                                            .position('right')
                                            .anchor('left-center')
                                            .offsetX(5)
                                            .offsetY(0)
                                            .format('{%Value}{groupsSeparator: }');
                                    
                                          // set titles for axises
                                          chart.xAxis().title('Location');
                                          chart.yAxis().title('Number of Users');
                                          chart.interactivity().hoverMode('by-x');
                                          chart.tooltip().positionMode('point');
                                          // set scale minimum
                                          chart.yScale().minimum(0);
                                    
                                          // set container id for the chart
                                          chart.container('container');
                                          // initiate chart drawing
                                          chart.draw();
                                        });
                                      
                                    </script>
                                    <div id="container" style="height: 350px; width:100%"></div>

                            
                                    
                            </div>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>Freelancer by Skills</h3>
                                    <script>
                                        anychart.onDocumentReady(function () {
                                          var data = [
                                              {!! $chart_user_freelancer_skills !!}
                                          ];
                                          // sort data by alphabet order
                                          data.sort(function (itemFirst, itemSecond) {
                                            return itemSecond[1] - itemFirst[1];
                                          });
                                          // create bar chart
                                          var chart = anychart.bar();
                                    
                                          // turn on chart animation
                                          chart
                                            .animation(true)
                                            .padding([10, 40, 5, 20])
                                            // set chart title text settings
                                            .title('');
                                    
                                          // create area series with passed data
                                          var series = chart.bar(data);
                                          // set tooltip formatter
                                          series
                                            .tooltip()
                                            .position('right')
                                            .anchor('left-center')
                                            .offsetX(5)
                                            .offsetY(0)
                                            .format('{%Value}{groupsSeparator: }');
                                    
                                          // set titles for axises
                                          chart.xAxis().title('Skills');
                                          chart.yAxis().title('Number of Users');
                                          chart.interactivity().hoverMode('by-x');
                                          chart.tooltip().positionMode('point');
                                          // set scale minimum
                                          chart.yScale().minimum(0);
                                    
                                          // set container id for the chart
                                          chart.container('container2');
                                          // initiate chart drawing
                                          chart.draw();
                                        });
                                      
                                    </script>
                                    <div id="container2" style="height: 350px; width:100%"></div>

                            
                                    
                            </div>
                        </div>

                      
                    </div>
                </div>
            </div>
        </div>
      
    </section>
@endsection
