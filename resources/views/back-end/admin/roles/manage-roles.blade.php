@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace" id="profile_settings">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right">
                @if (Session::has('message'))
                    <div class="flash_msg">
                        <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                    </div>
                @endif
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>{{{ trans('Manage Roles') }}}</h2>
                        <form class="wt-formtheme wt-formsearch">
                            <!-- <fieldset>
                                <div class="form-group">
                                    <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                        class="form-control" placeholder="{{{ trans('lang.ph_search_users') }}}">
                                    <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                </div>
                            </fieldset> -->
                        </form>
                        <!-- <button class="wt-btn float-right">Create New Role</button> -->
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                        @if ($roles->count() > 0)
                            <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                        <th>{{{ trans('Title') }}}</th>
                                        <th>{{{ trans('Role Type') }}}</th>
                                        <th>{{{ trans('Description') }}}</th>
                                        <!-- <th>{{{ trans('lang.ph_email') }}}</th>
                                        <th>{{{ trans('lang.role') }}}</th>
                                        <th>{{{ trans('lang.verification_status') }}}</th>

                                        <th>Status </th>
                                        <th>{{{ trans('Government Department') }}}</th>
                                        <th>{{{ trans('lang.action') }}}</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $key => $role_data)
                                            <tr class="">
                                                <td>{{{ $role_data->name }}}</td>
                                                <td>{{{ $role_data->role_type }}}</td>
                                                <td>{{{ $role_data->description }}}</td>
                                            </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php')))
                                @include('extend.errors.no-record')
                            @else
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
