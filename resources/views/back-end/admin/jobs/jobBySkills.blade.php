@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="orgs-listing" id="org-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Search</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open(['url' => url('admin/jobs/skills'),
                            'method' => 'get', 'class' => 'wt-formtheme wt-formsearch'])
                        !!}
                            <fieldset>

                                <div class="form-group">
                                    <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                    class="form-control" placeholder="Title">
                                  
                                </div>
                                <div class="form-group"  >
                                    
                                    <select name="skill_id" style="width: 100%">
                                        <option value="">-- Select Skill --</option>
                                        @foreach($skills as $skill)
                                            <option value="{{$skill->id}}"  <?php if(isset($_GET['skill_id'])) { if($skill->id == $_GET['skill_id']){ echo "selected";}}?> >{{$skill->title}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">

                                    <select name="category_id" style="width: 100%">
                                        <option value="">-- Select Category --</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"  <?php if(isset($_GET['category_id'])) { if($category->id == $_GET['category_id']){ echo "selected";}}?> >{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                 
                                </div>
                                <div class="form-group">

                                    <select name="status" style="width: 100%">
                                        <option value="">-- Select Status --</option>
                                        <option value="posted"  <?php if(isset($_GET['status'])) { if($_GET['status'] == 'posted'){ echo "selected";}}?> > Posted </option>
                                        <option value="hired" <?php if(isset($_GET['status'])) { if($_GET['status'] == 'hired'){ echo "selected";}}?>> Hired </option>
                                        <option value="completed" <?php if(isset($_GET['status'])) { if($_GET['status'] == 'completed'){ echo "selected";}}?>> Completed </option>
                                        <option value="cancelled" <?php if(isset($_GET['status'])) { if($_GET['status'] == 'cancelled'){ echo "selected";}}?>> Cancelled </option>

                                    </select>
                                 
                                </div>

                                <div class="form-group">
                                    <input type="number" name="minprice" value="{{{ !empty($_GET['minprice']) ? $_GET['minprice'] : '' }}}"
                                    class="form-control" placeholder="Min Price">
                                  
                                </div>

                                <div class="form-group">
                                    <input type="number" name="maxprice" value="{{{ !empty($_GET['maxprice']) ? $_GET['maxprice'] : '' }}}"
                                    class="form-control" placeholder="Max Price">
                                  
                                </div>
                                <div class="form-group wt-btnarea">
                                    <button name="btn_submit" type="submit" value="Search" class="wt-btn" style="width: 100% !important" >Search</button>

                                    {{-- <input type="submit" value="Search" name="submit" class="wt-btn"> --}}
                                </div>
                                <div class="form-group wt-btnarea">
                                    <button name="btn_submit" type="submit" value="Download" class="wt-btn" style="width: 100% !important" >Download</button>

                                    {{-- <input type="submit" value="Download" name="submit"  class="wt-btn"> --}}
                                </div>
                            </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Jobs</h2>
                            {{-- {!! Form::open(['url' => url('admin/organizationtype/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="Organization Type">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!} --}}
                           
                        </div>
                      
                    @if ($jobs->count() > 0)
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                        <table class="wt-tablecategories" id="checked-val">
                            <thead>
                                <tr>
                                  
                                    <th>Title</th>
                                    <th>Skills</th>
                                    <th>Category</th>
                                    <th>Posted By</th>
                                    <th>Status</th>
                                    <th>Posted Date</th>

                                    <th>{{{ trans('lang.action') }}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $counter = 0; @endphp
                                @foreach ($jobs as $job)
                                    <tr class="del-{{{ $job->id }}}">
                                     
                                        <td><a href="{{{ url('job/'.$job->slug) }}}">{{ $job->title }}</a>  <br> PKR: @if (!empty($job->price))
                                           {{{ $job->price }}}
                                        @endif</td>
                                        <td> @forelse ($job->skills as $item)
                                            {{$item->title}} ,
                                        @empty
                                            
                                        @endforelse  </td>
                                        <td> @if ($job->categories)
                                            @php
                                                $category_job = \App\Category::where('id', $job->categories)->first();
                                            @endphp
                                            {{$category_job->title}} 
                                        @endif   </td>
                                        <td><?php echo $user_name = !empty($job->employer->id) ? Helper::getUserName($job->employer->id) : ''; ?></td>
                                        <td>{{{ $job->status }}}</td>
                                        <td>{{{ $job->created_at }}}</td>

                                        <td>
                                            <div class="wt-actionbtn">
                                                <a href="{{{ url('job/'.$job->slug) }}}" class="wt-addinfo wt-skillsaddinfo">
                                                    <i class="lnr lnr-eye"></i>
                                                </a>
                                                {{-- <a href="{{{ url('admin/organizationtype/edit-orgs') }}}/{{{ $org->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                    <i class="lnr lnr-pencil"></i>
                                                </a>
                                                <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$org->id}}'" :message="'{{trans("lang.ph_lang_delete_message")}}'" :url="'{{url('admin/organizationtype/delete-orgs')}}'"></delete> --}}
                                            </div>
                                        </td>
                                    </tr>
                                    @php $counter++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @if( method_exists($jobs,'links') ) {{ $jobs->links('pagination.custom') }} @endif
                    </div>
                @else
                    @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                        @include('extend.errors.no-record')
                    @else 
                        @include('errors.no-record')
                    @endif
                @endif
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
