@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="dist-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Add New District</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open(['url' => url('admin/store-district'), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory','id' =>
                            'district']) !!}
                            <fieldset>
                                <div class="form-group">
                                    
                                    <select name="loc_id">
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    
                                    {!! Form::text( 'district_name', null, ['class' =>'form-control'.($errors->has('district_name') ? ' is-invalid' : ''),
                                    'placeholder' => 'District Name'] ) !!}
                                    
                                    @if ($errors->has('district_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('district_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group wt-btnarea">
                                    <input type="submit" value="Add New District" class="wt-btn">
                                </div>
                            </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Districts</h2>
                            {!! Form::open(['url' => url('admin/district/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="District Name">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!}
                            <a href="javascript:void(0);" v-if="this.is_show" @click="deleteChecked('{{ trans('lang.ph_delete_confirm_title') }}', '{{ trans('lang.ph_lang_delete_message') }}')" class="wt-skilldel">
                                <i class="lnr lnr-trash"></i>
                                <span>{{ trans('lang.del_select_rec') }}</span>
                            </a>
                        </div>
                        @if ($district->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="wt-checkbox">
                                                    <input name="district[]" id="wt-langs" @click="selectAll" type="checkbox" name="head">
                                                    <label for="wt-langs"></label>
                                                </span>
                                            </th>
                                            <th>Province</th>
                                            <th>District Name</th>
                                            <th>{{{ trans('lang.action') }}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($district as $dist)
                                            <tr class="del-{{{ $dist->id }}}" v-bind:id="districtID">
                                                <td>
                                                    <span class="wt-checkbox">
                                                        <input name="district[]" @click="selectRecord" value="{{{ $dist->id }}}" id="wt-check-{{{ $counter }}}" type="checkbox" name="head">
                                                        <label for="wt-check-{{{ $counter }}}"></label>
                                                    </span>
                                                </td>
                                                <td>
                                                    @foreach($locations as $location)
                                                        @if($location->id == $dist->loc_id)
                                                            {{$location->title}}
                                                        @endif
                                                    @endforeach
                                                </td>
                                                <td>{{{ $dist->district_name }}}</td>
                                                <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/district/edit-district') }}}/{{{ $dist->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                            <i class="lnr lnr-pencil"></i>
                                                        </a>
                                                        <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$dist->id}}'" :message="'{{trans("lang.ph_lang_delete_message")}}'" :url="'{{url('admin/district/delete-district')}}'"></delete>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($district,'links') ) {{ $district->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
