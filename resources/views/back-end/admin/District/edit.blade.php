@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="govt-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Edit District</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            {!! Form::open([ 'url' => url('admin/district/update-district/'.$district->id.''), 'class' =>'wt-formtheme wt-formprojectinfo
                            wt-formcategory', 'id' => 'locations' ]) !!}
                                <fieldset>
                                    <div class="form-group">
                                    
                                        <select name="loc_id">
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}" @if($location->id == $district['loc_id']) selected @endif >{{$location->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text( 'district_name', e($district['district_name']), ['class' =>'form-control'.($errors->has('district_name') ? ' is-invalid' : '')] ) !!}
                                        
                                        @if ($errors->has('district_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('district_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group wt-btnarea">
                                        <input type="submit" value="Update District" class="wt-btn">
                                    </div>
                                </fieldset>
                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
