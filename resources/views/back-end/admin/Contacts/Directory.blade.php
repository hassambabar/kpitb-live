@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="dist-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
            
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Contact User:</h2>
                            <div class="wt-dashboardboxcontent">
                                <div class="form-group">
                                    
                                       <strong>Name:</strong> {{$user->first_name}} {{$user->last_name}}
                                    </div>
                                    @php
                                      $role = $user->getRoleNames()->first();
                                    @endphp
                                    <div class="form-group">
                                        
                                        <strong>Role:</strong> {{$role}}

                                    </div>
                                    <div class="form-group">
                                        
                                        <strong>Email Address:</strong>  {{$user->email}} 

                                    </div>
                                    <div class="form-group">
                                        
                                        <strong>Message History:</strong> 
                                        @foreach($user_messages as $user_message)
                                            <ul>
                                                <li>{{$user_message->message}}</li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="wt-dashboardboxcontent">
                                    <strong>New Message:</strong> 
                                    <form action="{{route('contactmessageuser')}}" method="post">
                                            @csrf
                                                <input type="hidden" class="form-control" id="id" name="id" value="{{$user->id}}"></input>
                                                <textarea required class="form-control" name="message" placeholder="Send a message" style="height:150px;"></textarea><br />
                                                <br />
                                                <input class="wt-btn" type="submit" value="Send" /><br /><br />
                                    </form>
                                
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection