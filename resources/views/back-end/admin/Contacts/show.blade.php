@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="govt-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Contact</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                         <div class="form-group">
                                    
                                       <strong>From:</strong> {{$Contact->from}}
                                    </div>
                                    <div class="form-group">
                                        
                                        <strong>Subject:</strong> {{$Contact->subject}}

                                    </div>
                                    <div class="form-group">
                                        
                                        <strong>Email Address:</strong>    {{$Contact->email}}

                                    </div>
                                    <div class="form-group">
                                        
                                        <strong>Message:</strong>  {{$Contact->body}}

                                    </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6 float-left">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Reply</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                                @if(!empty($Contact->reply))
                                    <div class="form-group">
                                        <strong>Reply:</strong>  {{$Contact->reply}}

                                    </div>
                                @else
                                    <form action="{{route('contactreply')}}" method="post">
                                            @csrf
                                                <input type="hidden" class="form-control" id="id" name="id" value="{{$Contact->id}}"></input>
                                                <textarea required class="form-control" name="reply" placeholder="Reply to the query" style="height:150px;"></textarea><br />
                                                <br />
                                                <input class="wt-btn" type="submit" value="Send" /><br /><br />
                                    </form>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
