@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="govts-listing" id="dist-list">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
            <div class="row">
            
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-right">
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle wt-titlewithsearch">
                            <h2>Contact Us</h2>
                            {!! Form::open(['url' => url('admin/contacts/search'), 'method' => 'get', 'class' =>'wt-formtheme wt-formsearch']) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="{{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}}"
                                            class="form-control" placeholder="from">
                                        <button type="submit" class="wt-searchgbtn"><i class="lnr lnr-magnifier"></i></button>
                                    </div>
                                </fieldset>
                            {!! Form::close() !!}
                            
                        </div>
                        @if ($Contacts->count() > 0)
                            <div class="wt-dashboardboxcontent wt-categoriescontentholder">

                                @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                           
                                            <th>From</th>
                                            <th>Email</th>
                                            <th>Subject</th>
                                            <th>Read Status</th>
                                            <th>{{{ trans('lang.action') }}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $counter = 0; @endphp
                                        @foreach ($Contacts as $Contact)
                                            <tr class="del-{{{ $Contact->id }}}" v-bind:id="districtID">
                                              
                                                <td>
                                                   {{$Contact->from}}
                                                </td>
                                                <td>
                                                  {{$Contact->email}}
                                                   
                                                </td>
                                                <td>
                                                    {{$Contact->subject}}
                                                     
                                                  </td>
                                                  <td>
                                                    @if ($Contact->status == 0)
                                                        <button type="button" class="btn btn-warning">Unread</button>

                                                    @else
                                                    <button type="button" class="btn btn-success">Read</button>
                                                    
                                                    @endif
                                                     
                                                  </td>
                                                <td>
                                                    <div class="wt-actionbtn">
                                                        <a href="{{{ url('admin/contactview') }}}/{{{ $Contact->id }}}" class="wt-addinfo wt-skillsaddinfo">
                                                            <i class="lnr lnr-eye"></i>
                                                        </a>
                                                        <form action="{{ route('contact.destroy', $Contact->id)}}" method="post" class="formDelete">
                                                            @csrf
                                                            @method('DELETE')
                                                <button onclick="return confirm('Are you sure you want to delete this item?');"class="wt-deleteinfo" type="submit"><i class="lnr lnr-trash"></i></button>
                                              </form>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $counter++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                @if( method_exists($Contact,'links') ) {{ $Contact->links('pagination.custom') }} @endif
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
