@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')

<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-ui.min.js"></script>
  <script src="https://cdn.anychart.com/releases/v8/js/anychart-exports.min.js"></script>
  <link href="https://cdn.anychart.com/releases/v8/css/anychart-ui.min.css" type="text/css" rel="stylesheet">
  <link href="https://cdn.anychart.com/releases/v8/fonts/css/anychart-font.min.css" type="text/css" rel="stylesheet">


    <section class="wt-haslayout wt-dbsectionspace wt-insightuser" id="dashboard">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
            @php session()->forget('message');  @endphp
        @endif
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="wt-insightsitemholder wt-employer-dashboard">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Total Jobs</h3>
                                       <h2>{{$count_total_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Completed Jobs</h3>
                                       <h2>{{$count_completed_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Current Jobs</h3>
                                       <h2>{{$count_ongoing_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <figure class="wt-userlistingimg">
                                </figure>
                                <div class="wt-insightdetails">
                                    <div class="wt-title">
                                        <h3>Cancelled Jobs</h3>
                                       <h2>{{$count_cancelled_jobs}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12">
                            <div class="wt-insightsitem wt-dashboardbox ">
                                <h3>New Jobs</h3>
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                          
                                            <th>Title</th>
                                            <th>Skills</th>
                                            <th>PostedBy</th>
                                            <th>Posted Date</th>
                                        </tr>
                                    </thead>                            <tbody>

                                    @forelse ($ongoing_jobs_5 as $jonbSingle)
                                    <tr>
                                    <td><a href="{{{ url('job/'.$jonbSingle->slug) }}}">{{ $jonbSingle->title }}</a></td>
                                    <td> @forelse ($jonbSingle->skills as $item)
                                        {{$item->title}} ,
                                    @empty
                                        -
                                    @endforelse  </td>
                                    <td><?php echo $user_name = !empty($jonbSingle->employer->id) ? Helper::getUserName($jonbSingle->employer->id) : ''; ?></td>
                                    <td>{{{ $jonbSingle->created_at }}}</td>
                                    </tr>
                                    @empty
                                        
                                    @endforelse
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
      
    </section>
@endsection
