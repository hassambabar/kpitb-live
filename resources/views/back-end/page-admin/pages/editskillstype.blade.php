@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@push('sliderStyle')
    <link href="{{ asset('css/antd.css') }}" rel="stylesheet">
@endpush
@section('content')
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 float-left">
  <div class="wt-dashboardbox">
    <div class="wt-dashboardboxtitle">
      <h2>Edit Skill Type</h2>
    </div>
    <div class="wt-dashboardboxcontent">
      <form method="POST" action="{{url('admin/edit-skill-type')}}" accept-charset="UTF-8" id="skills" class="wt-formtheme wt-formprojectinfo wt-formcategory">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{$id}}" />
         <fieldset>
           <div class="form-group">
             <input placeholder="Skill Title / Location" value="{{$skills['skill_name']}}" name="skill_title" type="text" class="form-control">
           </div>
           <div class="form-group">
             <select name="skill_type" class="form-control">
               <option @if($skills['skill_type'] == 'budget') selected="selected" @endif value="budget">By Budget</option>
               <option @if($skills['skill_type'] == 'skill') selected="selected" @endif value="skill">By Skill</option>
               <option @if($skills['skill_type'] == 'category') selected="selected" @endif value="category">By Category</option>
               <option @if($skills['skill_type'] == 'location') selected="selected" @endif value="location">By Location</option>
             </select>

           </div>
           <div class="form-group wt-btnarea">
             <input type="submit" value="Edit Skill Type" class="wt-btn">
           </div>
         </fieldset>
       </form>
     </div>
   </div>
 </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>

@endpush
