@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    @php
        $verified_user = \App\User::select('user_verified')
        ->where('id', $job->employer->id)->pluck('user_verified')->first();
    @endphp
    <section class="wt-haslayout wt-dbsectionspace la-dbproposal" id="jobs">
        @if (Session::has('message'))
        <div class="flash_msg">
            <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
        </div>
        @php session()->forget('message') @endphp;
    @elseif (Session::has('error'))
        <div class="flash_msg">
            <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ str_replace("'s", " ", Session::get('error')) }}}'" v-cloak></flash_messages>
        </div>
        @php session()->forget('error'); @endphp
    @endif

    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @if ($proposal->status == "cancelled" && !empty($cancel_reason))
                <div class="wt-jobalertsholder">
                    <ul class="wt-jobalerts">
                        <li class="alert alert-danger alert-dismissible fade show">
                            <span><em>{{ trans('lang.sorry') }}</em> {{ trans('lang.job_cancelled') }}</span>
                            <a href="javascript:void(0)" class="wt-alertbtn danger" v-on:click.prevent="viewReason('{{$cancel_reason->description}}')" >{{ trans('lang.reason') }}</a>
                            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                </div>
            @endif
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>{{ trans('lang.job_dtl') }}</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-jobdetailsholder">
                        <div class="wt-freelancerholder wt-tabsinfo">
                            <div class="wt-jobdetailscontent">

                                {{-- Show dispute message --}}
                                @if ($proposal->status == "cancelled" && !empty($cancel_reason))
                                <div class="wt-canceljobholder">
                                <div class="alert alert-danger alert-dismissible fade show wt-userlistinghold">
                                    <h4>Job Cancelled</h4>
                                {{$cancel_reason->description}}<br>
                                    <a href='{{url("freelancer/dispute/$job->slug")}}' class="wt-btn">Raise a dispute</a>
                                </div>
                                <div class="clearfix">
                                </div>
                                </div>
                                @endif
                                <div class="wt-userlistinghold wt-featured wt-userlistingvtwo">
                                    <!-- @if (!empty($job->is_featured) && $job->is_featured === 'true')
                                        <span class="wt-featuredtag">
                                            <img src="{{{ asset('images/featured.png') }}}" alt="{{ trans('lang.is_featured') }}"
                                                data-tipso="Plus Member" class="template-content tipso_style">
                                        </span>
                                    @endif -->
                                    <div class="wt-userlistingcontent">
                                        <div class="wt-contenthead">
                                            @if (!empty($employer_name) || !empty($job->title) )
                                                <div class="wt-title">
                                                    @if (!empty($employer_name))
                                                        <a href="{{{ url('profile/'.$job->employer->slug) }}}">
                                                            @if($verified_user === 1)
                                                                <i class="fa fa-check-circle"></i>&nbsp;
                                                            @endif
                                                            {{{ $employer_name }}}
                                                        </a>
                                                    @endif
                                                    @if (!empty($job->title))
                                                        <h2>{{{ $job->title }}}</h2>
                                                    @endif
                                                </div>
                                            @endif
                                            <ul class="wt-userlisting-breadcrumb">
                                                @if (!empty($job->price))
                                                    <li><span><i class="far fa-money-bill-alt"></i> {{ !empty($symbol) ? $symbol['symbol'] : 'PKR' }} {{{ $english_format_number = number_format($job->price) }}}</span></li>
                                                @endif
                                                @if (!empty($job->location->title))
                                                    <li>
                                                        <span>
                                                             {{{ $job->location->title }}}
                                                        </span>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="wt-rightarea">
                                            <div class="wt-hireduserstatus">
                                                <figure><img src="{{{ asset($employer_image) }}}" alt="{{ trans('lang.profie_img') }}"></figure>
                                                <span> 
                                               
                                                {{{ $employer_name }}}
                                                </span>
                                            </div>
                                        </div>
                                        @if($proposal->status == "hired")
                                            <div class="form-group">
                                                <div class="row">
                                                    @php
                                                        $id = Auth::user()->id;
                                                        $request_status_proposal = \App\Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
                                                        $request_status = $request_status_proposal->request_to_close;
                                                        $request_cancel_proposal = \App\Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
                                                        $request_cancel = $request_cancel_proposal->request_to_cancel;
                                                        @endphp
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    {!! Form::open(['url' => url('freelancer/jobs_request/'.$job->slug.''), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory', 'id' => 'request_to_close'] ) !!}
                                                        @if($request_status === 0 && $request_cancel === 0)  
                                                        <!-- <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"> -->
                                                            <div class="wt-btnarea">
                                                                {!! Form::submit(trans('Request to close the project'), ['class' => 'wt-btn']) !!}
                                                            </div>
                                                        <!-- </div> -->
                                                        @elseif($request_status === 1)
                                                            <div class="wt-btnarea" >
                                                            <p class="text-danger">You have requested the employer to close the project. Kindly, wait for their response.</p> 
                                                            </div>
                                                        @endif
                                                    {!! Form::close(); !!}
                                                    </div>
                                                    @if($request_status === 0)
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    {!! Form::open(['url' => url('freelancer/jobs_cancel_request/'.$job->slug.''), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory', 'id' => 'request_to_cancel'] ) !!}
                                                        @if($request_cancel === 0)                                                   
                                                         
                                                            <div class="wt-btnarea">
                                                                {!! Form::submit(trans('Request to cancel the project'), ['class' => 'wt-btn']) !!}
                                                            </div>
                                                        
                                                        @elseif($request_cancel === 1)
                                                            <div class="wt-btnarea" >
                                                            <p class="text-danger">You have requested the employer to cancel the project. Kindly, wait for their response. Keep in mind that you requested to cancel the project so the employer is not obliged to pay the invoice to you.</p> 
                                                            </div>
                                                        @endif
                                                    {!! Form::close(); !!}
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($proposal->status == "hired")
                        @if($Milestones != '')
                        @if (count($Milestones) > 0)
                        <div class="wt-projecthistory">
                            <div class="wt-tabscontenttitle">
                                <h2>Milestones</h2>
                            </div>
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                           
                                            <th >Title</th>
                                            <th>Start Date / End Date</th>
                                            <th>Percentage</th>
                                            <th>Status</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($Milestones as $item => $value)
                                        <tr>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->start_date}} - {{$value->end_date}}</td>
                                        <td>{{$value->percentage}}%</td>
                                        <td><a href="{{url("freelancer/milestone/check/$value->id/$job->slug")}}">
                                            @if($value->milestone_release === 1)
                                                Requested
                                            @elseif($value->status == 0) 
                                                Pending 
                                            @else 
                                                Completed  
                                            @endif
                                        </a></td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                        </div>
                        @endif
                        @endif
                        @endif
                        <div class="clearfix">
                        </div>
                        @if($Invoice != '')
                        @if (count($Invoice) > 0)
                    
                        <div class="wt-projecthistory">
                            <div class="wt-tabscontenttitle">
                                <h2>Job Invoice</h2>
                            </div>
                            <table class="wt-tablecategories" >
                                <thead>
                                    <tr>
                                       
                                        <th >Title</th>
                                        <th>Price</th>
                                        <th>Created At</th>
                                        <th>Status</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($Invoice as $item => $value)
                                    <tr>
                                    <td>{{$value->title}}</td>
                                    <td>{{$value->price}} </td>
                                    <td>{{{ \Carbon\Carbon::parse($value->created_at)->format('M d, Y') }}}</td>
                                    <td>@if($value->paid == 0) Pending @else Completed  @endif</td>
                                    <td><a href="{{url("freelancer/job/check/$value->id/$job->slug")}}">View </a> </td>
                                    </tr>
                                    @endforeach
                                   
                                </tbody>
                            </table>

                            <div class="invoiceList">
                                <ul>
                                        @foreach ($Invoice as $item => $value)
                                        <li><strong>Title</strong>: {{$value->title}}</li>
                                        <li><strong>Price</strong>: {{$value->price}} - PKR</li>
                                        <li><strong>Tax Deduction</strong>: {{$value->sales_tax}} - PKR</li>
                                        <li><strong>Created At</strong>: {{{ \Carbon\Carbon::parse($value->created_at)->format('M d, Y') }}}</li>

                                        <li><strong>Detail</strong>: {{$value->detail}}</li>
                                        <li><strong>Attachment</strong>: 
                                            @if(!empty($value->transection_doc))
                                            <?php 
                                            
                                            $test = unserialize($value->transection_doc);
        
        ?>
                                            @forelse ($test as $key => $value2)
                                            <br> <a href=" {{{ asset("/storage/app/uploads/users/$job->user_id/$value2") }}}" >View Attachment </a>
                                           
                                            @empty
                                                
                                            @endforelse
        
                                            @endif
                                        </li>
                                        <li>
                                            @if($value->paid == 0)
                                            <a href="{{url("freelancer/job/status/order/$value->id/accept/$value->job_id")}}" class="btn btn-success">Accept</a>
                                            @if($JobDispute == '')
                                            
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</a>
                                            @endif

                                            @endif
                                            {{-- @if(empty($value->milestonDispute))
        
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</a>
                                            @endif --}}
        
                                        </li>
                                        <div class="modal" id="myModal">
                                            <div class="modal-dialog">
                                              <div class="modal-content">
                                          
                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                  <h4 class="modal-title">Please Provide Reason</h4>
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                          
                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                    {!! Form::open(['url' => url('freelancer/job/submit-disputes'), 'files'=>'true']) !!}
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="reason">Reason:</label>
                                                                    <input type="text" class="form-control"  name="reason" placeholder="Reason" id="reason" required>
                                                                  </div>
                                                                  <div class="form-group">
                                                                    <label for="reason">Description:</label>
                                                                    <textarea class="form-control" name="freelancer_description" required ></textarea>
                                                                  </div>
                                                                  <div class="form-group">
                                                                    <button type="submit" class="btn btn-danger" >Submit</button>
                                        
                                                                  </div>
                                        
                                                                  <input type="hidden" name="job_id" value="{{$job->id}}"  readonly/>
                                                                  <input type="hidden" name="invoice_id" value="{{$value->id}}" readonly />
                                        
                                                                {!! Form::close(); !!}
                                        
                                                </div>
                                          
                                                <!-- Modal footer -->
                                            
                                          
                                              </div>
                                            </div>
                                          </div>
                                    
                                    
                                        @endforeach
                                </ul>
                            </div>   
                        </div>
                        <div class="clearfix">
                        </div>
                        @endif
                        @endif
                        <div class="wt-projecthistory">
                            <div class="wt-tabscontenttitle">
                                <h2>{{ trans('lang.project_history') }}</h2>
                            </div>
                            <div class="wt-historycontent">
                                <private-message :ph_job_dtl="'{{ trans('lang.ph_job_dtl') }}'" :upload_tmp_url="'{{url('proposal/upload-temp-image')}}'" :id="'{{$proposal->id}}'" :recipent_id="'{{$job->user_id}}'"></private-message>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
            </div>
        </div>
    </section>

      @endsection
