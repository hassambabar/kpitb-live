@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
                @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div><br />
                          @endif

                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Job Feedback</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder" >
                        @if (Session::has('message'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('message') @endphp;
                    @elseif (Session::has('error'))
                    
                        <div class="flash_msg">
                            <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ str_replace("'s", " ", Session::get('error')) }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('error'); @endphp
                    @endif
                         
                            <form class="wt-formtheme wt-formfeedback"  action="{{url('freelancer/submit-job/experience')}}" method="POST">
                                @csrf
                                <fieldset>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="{{ trans('lang.add_your_feedback') }}" name="feedback"></textarea>
                                    </div>
                                   
                                    @if(!empty($review_options))
                                        @foreach ($review_options as $key => $option)
                                            <div class="form-group wt-ratingholder">
                                                <div class="wt-ratepoints">
                                                    <vue-stars
                                                        :name="'rating[{{$key}}][rate]'"
                                                        :active-color="'#fecb02'"
                                                        :inactive-color="'#999999'"
                                                        :shadow-color="'#ffff00'"
                                                        :hover-color="'#dddd00'"
                                                        :max="5"
                                                        :value="0"
                                                        :readonly="false"
                                                        :char="'★'"
                                                        id="rating-{{$key}}"
                                                    />
                                                    <div class="counter wt-pointscounter"></div>
                                                </div>
                                                <input type="hidden" name="rating[{{$key}}][reason]" value="{{{$option->id}}}">
                                                <span class="wt-ratingdescription">{{{$option->title}}}</span>
                                            </div>
                                        @endforeach
                                    @endif
                                    <input type="hidden" name="receiver_id" value="{{{$job->user_id}}}">
                                    <input type="hidden" name="job_id" value="{{{$job->id}}}">

                                    <input type="hidden" name="proposal_id" value="{{{$accepted_proposal->id}}}">
                                    <div class="form-group wt-btnarea">
                                        <button class="wt-btn" type="submit" >{{ trans('lang.btn_send_feedback') }}</button>
                                    </div>
                                </fieldset>
                            </form>

                        


                    </div>
                </div>
            </div>
        </div>
        <style>
            .milstonePaid{
                background-color: #b8d5c1;
            }
            .milstonedispute{
                background-color: #b8d5c1;
            }
            .milesonelist ul li{
                list-style: none;
                padding: 10px 20px;
            }
            .milesonelist ul li span{
                font-weight: bold;
                padding-right: 10px;
            }
            </style>
    </section>
@endsection
