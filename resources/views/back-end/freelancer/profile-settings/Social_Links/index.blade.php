@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="wt-dbsectionspace wt-haslayout la-ps-freelancer">
        <div class="freelancer-profile" id="user_profile">
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10"> 
                    <div class="wt-dashboardbox wt-dashboardtabsholder">
                        @if (file_exists(resource_path('views/extend/back-end/freelancer/profile-settings/tabs.blade.php'))) 
                            @include('extend.back-end.freelancer.profile-settings.tabs')
                        @else 
                            @include('back-end.freelancer.profile-settings.tabs')
                        @endif
                        <div class="wt-tabscontent tab-content">
                            @if (Session::has('message'))
                                <div class="flash_msg">
                                    <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                                </div>
                            @endif
                            @if ($errors->any())
                                <ul class="wt-jobalerts">
                                    @foreach ($errors->all() as $error)
                                        <div class="flash_msg">
                                            <flash_messages :message_class="'danger'" :time ='10' :message="'{{{ $error }}}'" v-cloak></flash_messages>
                                        </div>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="wt-personalskillshold tab-pane active fade show" id="wt-skills">
                            <div class="wt-tabscontenttitle">
                                <h2>{{{ trans('Add your Social Profiles') }}}</h2>
                            </div>
                            {!! Form::open([  'url' => 'freelancer/store-social-links',  'class' =>'wt-userform', 'id' => 'social_links']) !!}
                        
                                @if(!empty($social_links) )
                                <!-- <div class="wt-tabscontenttitle">
                                <h2>{{{ trans('Already Added Profiles') }}}</h2>
                                </div> -->
                                <table class="wt-tablecategories" id="checked-val">
                                    <thead>
                                        <tr>
                                        
                                           
                                            <th>Type</th>
                                            <th>Social Profile</th>
                                            <th>Action</th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                  
                                        @foreach($social_links as $social)
                                            <tr>
                                            <td>
                                            <p>{{ $social->type }}</p> 
                                            </td>
                                            <td>
                                            <p><a href="{{ $social->social_links }}" class="">View Profile</a></p>
                                            </td>
                                            <td class="social_link_delete_icon">
                                            <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$social->social_links}}'" :message="'{{trans("Social Link Deleted")}}'" :url="'{{url('freelancer/profile/delete-social_links')}}'"></delete>
                                            </td></tr>
                                                     
                                           
                                        @endforeach
                                        </tbody>
                                    </table>

                                <div class="form-group form-group-half">

                               </div>
                               <div class="wt-tabscontenttitle">
                                <h2>{{{ trans('Add More') }}}</h2>
                                </div>
                                <div class="form-group">
                               @php echo Form::select('type', array(
                                    'type' => array('Linkedin' => 'Linkedin','Upwork' => 'Upwork','Fiverr' => 'Fiverr','Freelancer' => 'Freelancer'),
                                )); @endphp
                                {{ Form::text('social_links_id', '',  array('multiple'=>true,'class' => 'social_links_id') ) }}
                              
                                </div>
                                @else

                                <div class="form-group">
                               @php echo Form::select('type', array(
                                    'type' => array('Linkedin' => 'Linkedin','Upwork' => 'Upwork','Fiverr' => 'Fiverr','Freelancer' => 'Freelancer'),
                                )); @endphp
                                {{ Form::text('social_links_id', '',  array('multiple'=>true,'class' => 'social_links_id') ) }}
                              
                                </div>
                                                        
                                @endif
                                  
                                    <div class="wt-updatall">
                                        <i class="ti-announcement"></i>
                                        <span>{{{ trans('lang.save_changes_note') }}}</span>
                                        <!-- <button type="submit" class="btn btn-primary">Save and Submit</button> -->
                                        
                                        {!! Form::submit(trans('lang.btn_save_update'), ['class' => 'wt-btn', 'id'=>'submit-social_links']) !!}
                                    </div>
                                {!! form::close(); !!}
                            </div>
                        </div>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
