<div class="wt-tabscontenttitle">
    <h2>{{{ trans('lang.your_details') }}}</h2>
</div>
<div class="wt-formtheme">
    <fieldset>
        <div class="form-group form-group-half">
            <span class="wt-select">
                {!! Form::select( 'gender', ['male' => 'Male', 'female' => 'Female'], e($gender), ['placeholder' => trans('lang.ph_select_gender')] ) !!}
            </span>
        </div>
        <div class="form-group form-group-half">
            {!! Form::text( 'first_name', e(Auth::user()->first_name), ['class' =>'form-control', 'placeholder' => trans('lang.ph_first_name')] ) !!}
        </div>
        <div class="form-group form-group-half">
            {!! Form::text( 'last_name', e(Auth::user()->last_name), ['class' =>'form-control', 'placeholder' => trans('lang.ph_last_name')] ) !!}
        </div>
        <div class="form-group form-group-half">
            {!! Form::number( 'hourly_rate', e($hourly_rate), ['class' =>'form-control', 'placeholder' => 'Service Hourly Rate (PKR)'] ) !!}
        </div>
        <div class="form-group">
            {!! Form::text( 'tagline', e($tagline), ['class' =>'form-control', 'placeholder' => trans('lang.ph_add_tagline')] ) !!}
        </div>

        <div class="form-group">
            <select id="freelancer_type" name="freelancer_type" class="form-control" >
                                                    
                <option selected="selected" value="">Select Type</option>
                @foreach (Helper::getFreelancerLevelList() as $key => $freelancer_level)
    
    <option value="{{$key}}"  <?php if(isset($freelancer_type)){ if($freelancer_type == $key){ echo "selected"; } } ?> >{{$freelancer_level}}</option>
    
    @endforeach
               
            </select>        </div>

      
        <div class="form-group">
            {!! Form::textarea( 'description', e($description), ['class' =>'form-control', 'placeholder' => trans('lang.ph_desc')] ) !!}
        </div>
        <div class="form-group">
        @if(!empty($attachments))
        <div class="wt-tabscontenttitle">
            <h2 for="attachments">Attached Resume</h2> 
        </div>  
        
        <div class="form-group delete-file">                                         
        <span><a href="{{URL::asset('/uploads/'.$profile->attachments)}}" taget="_blank">{{{$attachments}}}</a> | 
        <a download href="{{URL::asset('/uploads/'.$profile->attachments)}}"><i class="fas fa-download"></i></a> | 
        <delete :title="'{{trans("lang.ph_delete_confirm_title")}}'" :id="'{{$profile->attachments}}'" :message="'{{trans("Resume Deleted")}}'" :url="'{{url('freelancer/profile/delete-file')}}'"></delete>

        </div>
        <!-- <a href="#"><i class="lnr lnr-cross"></i></a> -->
        <div class="form-group">
            <h5>Re-Upload your Resume</h5>
            <input type="file" name="attachments" id="attachments" class="form-control-file" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
        </div>
                                        
      
                                       
        @else
        <div class="wt-tabscontenttitle">
            <h2 for="attachments">Attach a resume</h2>
        </div>
        <div class="form-group">
            
            <input type="file" name="attachments" id="attachments" class="form-control-file" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
        </div>
       
        @endif
        
      
                                   
    </fieldset>
 
</div> 
