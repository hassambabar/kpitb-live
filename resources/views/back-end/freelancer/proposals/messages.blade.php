@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="proposals">
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>{{ trans('lang.all_proposals') }}</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-jobdetailsholder">
                        <div class="wt-completejobholder">
                            @if (!empty($proposal))
                                <div class="wt-managejobcontent">
                                   
                                        @php
                                            $freelancer_proposal = \App\Proposal::find($proposal->id);
                                            $duration = Helper::getJobDurationList($proposal->job->duration);
                                            $status_btn = $proposal->status == 'cancelled' ? trans('lang.view_reason') : trans('lang.view_detail');
                                            $detail_link = $proposal->status == 'hired' ? url('freelancer/job/'.$proposal->job->slug) : 'javascript:void(0);';
                                            $user_name = Helper::getUserName($proposal->job->employer->id);
                                        @endphp
                                        <div class="wt-userlistinghold wt-featured wt-userlistingvtwo">
                                            <!-- @if (!empty($proposal->job->is_featured) && $proposal->job->is_featured === 'true')
                                                <span class="wt-featuredtag"><img src="{{{ asset('images/featured.png') }}}" alt="{{ trans('lang.is_featured') }}" data-tipso="Plus Member" class="template-content tipso_style"></span>
                                            @endif -->
                                            <div class="wt-userlistingcontent wt-userlistingcontentvtwo">
                                                <div class="wt-contenthead">
                                                    @if (!empty($user_name) || !empty($proposal->job->title) )
                                                        <div class="wt-title">
                                                            @if (!empty($user_name))
                                                            <a href="{{{ url('profile/'.$proposal->job->employer->slug) }}}">
                                                                @if ($proposal->job->employer->user_verified === 1)
                                                                    <i class="fa fa-check-circle"></i>
                                                                @endif
                                                                &nbsp;{{{ $user_name }}}</a>
                                                            @endif
                                                            @if (!empty($proposal->job->title))
                                                                <h2>{{{ $proposal->job->title }}}</h2>
                                                            @endif
                                                        </div>
                                                    @endif
                                                    @if (!empty($proposal->job->price) ||
                                                        !empty($location['title'])  ||
                                                        !empty($proposal->job->project_type) ||
                                                        !empty($proposal->job->duration)
                                                        )
                                                        <ul class="wt-saveitem-breadcrumb wt-userlisting-breadcrumb">
                                                            @if (!empty($proposal->job->price))
                                                                <li><span class="wt-dashboraddoller"><i>{{ !empty($symbol) ? $symbol['symbol'] : 'PKR' }}</i> {{{ $english_format_number = number_format($proposal->job->price) }}}</span></li>
                                                            @endif
                                                            @if (!empty($proposal->job->location->title))
                                                                <li><span><img src="{{{asset(Helper::getLocationFlag($proposal->job->location->flag))}}}" alt="{{{ trans('lang.locations') }}}"> {{{ $proposal->job->location->title }}}</span></li>
                                                            @endif
                                                            @if (!empty($proposal->job->project_type))
                                                                <li><a href="javascript:void(0);" class="wt-clicksavefolder"><i class="far fa-folder"></i> {{ trans('lang.type') }} {{{ $proposal->job->project_type }}}</a></li>
                                                            @endif
                                                            @if (!empty($proposal->job->duration) && !is_array($duration))
                                                                <li><span class="wt-dashboradclock"><i class="far fa-clock"></i> {{ trans('lang.duration') }} {{{ $duration }}}</span></li>
                                                            @endif
                                                        </ul>
                                                    @endif
                                                    
                                                </div>
                                                <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">    
                                                        <br />
                                                        <div class="col-md-6 col-xl-12">
                                                            <h4>Display Messages</h4> 
                                                            @if(!empty($messages))
                                                                <div class="wt-userlistingcontent wt-userlistingcontentvtwo">
                                                                    @foreach($messages as $message)
                                                                        <div class="display-comment" >
                                                                            <div class="col-md-12">
                                                                                @php
                                                                                    $receiver = \App\User::find($message->receiver_id);
                                                                                    $sender = \App\User::find($message->sender_id);
                                                                                @endphp
                                                                                <strong>{{ $sender->first_name }}  {{ $sender->last_name }}</strong>
                                                                                    <p>Message: {{ $message->message }} <br>  <strong> {{ $message->created_at }}</strong></p>
                                                                                <a href="" id="reply"></a>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                                <form method="post" action="{{route('sendProposalMessage')}}">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <textarea required class="form-control" id="message" name="message" ></textarea>
                                                                        <input type="hidden" name="sender_id" value="{{Auth::user()->id}}" />
                                                                        <input type="hidden" name="receiver_id" value="{{$proposal->job->user_id}}" />
                                                                        <input type="hidden" name="proposal_id" value="{{$proposal->id}}" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="submit" class="btn btn-success" value="Send Message" />
                                                                    </div>
                                                                </form>
                                                    </div>
                                                </div>
                                             

                                            </div>
                                        </div>
                                </div>
                            @else
                                @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                    @include('extend.errors.no-record')
                                @else 
                                    @include('errors.no-record')
                                @endif
                            @endif
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<script>
   $(".message")[0].reset();
</script>
@endsection
