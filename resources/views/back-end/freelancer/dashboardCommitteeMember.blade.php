@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace wt-insightuser" id="dashboard">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
            @php session()->forget('message');  @endphp
        @endif
       
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-left">
                <div class="wt-dashboardbox wt-ongoingproject la-ongoing-projects">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>{{ trans('lang.ongoing_project') }}</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-hiredfreelance">
                            <table class="wt-tablecategories wt-freelancer-table">
                                <thead>
                                    <tr>
                                        <th>Project Title</th>
                                        <th>Procurer</th>
                                        <th>Project Created At</th>
                                        <th>Project Level</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                             @foreach ($CommitteeMember as $key => $value )
                                    @php
                                        $job_user = \App\user::where('id', $value->jobDetail->user_id)->first();
                                    @endphp
                             <tr>
                                <td>{{$value->jobDetail->title}}</td>
                                <td>{{$job_user->first_name}} {{$job_user->last_name}}</td>
                                <td>{{$value->jobDetail->created_at}}</td>
                                <td>{{$value->jobDetail->project_level}}</td>
                                <td><a href="{{url('comments-to-project',$value->jobDetail->id)}}">Add Comments</a> </td>
                             </tr>
                               
                             @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
@endsection
