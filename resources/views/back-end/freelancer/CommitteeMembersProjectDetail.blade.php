@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content') 
    <section class="wt-haslayout wt-dbsectionspace">

        <div class="manage-proposals float-left">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="jobs">
                    @if (Session::has('error'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'danger'" :time='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
                        </div> 
                    @endif
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>Job Details</h2>
                        </div>
                        @if (!empty($proposals))
                            @php
                                $user = \App\User::find($job->user_id);
                                $user_name = $user->first_name.' '.$user->last_name;
                                $verified_user = \App\User::select('user_verified')->where('id', $job->employer->id)->pluck('user_verified')->first();
                                $count = 0;
                                $received_proposal_count = 0;
                                $feature_class = !empty($job->is_featured) ? 'wt-featured' : '';
                            @endphp
                            
                            <div class="wt-dashboardboxcontent wt-rcvproposala">
                                <div class="wt-userlistinghold wt-userlistingvtwo {{ $feature_class }}">
                                    <!-- @if (!empty($job->is_featured) && $job->is_featured === 'true')
                                        <span class="wt-featuredtag"><img src="{{{ asset('images/featured.png') }}}" alt="{{ trans('lang.is_featured') }}" data-tipso="Plus Member" class="template-content tipso_style"></span>
                                    @endif -->
                                    <div class="wt-userlistingcontent">
                                        <div class="wt-contenthead">
                                            @if (!empty($user_name) || !empty($job->title) )
                                                <div class="wt-title">
                                                    @if (!empty($user_name))
                                                        <a href="{{{ url('profile/'.$job->employer->slug) }}}">@if($verified_user === 1)<i class="fa fa-check-circle"></i>@endif&nbsp;{{{ $user_name }}}</a>
                                                    @endif
                                                    @if (!empty($job->title))
                                                        <h2>{{{ $job->title }}}</h2>
                                                    @endif
                                                </div>
                                            @endif
                                            @if (!empty($job->professional_level) ||
                                                !empty($location['title'])  ||
                                                !empty($job->price) ||
                                                !empty($job->duration)
                                                )
                                                <ul class="wt-saveitem-breadcrumb wt-userlisting-breadcrumb">
                                                    @if (!empty($job->price))
                                                        <li><span class="wt-dashboraddoller"><i>{{ !empty($symbol) ? $symbol['symbol'] : 'PKR' }}</i> {{{ $english_format_number = number_format($job->price) }}}</span></li>
                                                    @endif
                                                    @if (!empty($job->location->title))
                                                        <li><span><img src="{{{asset(App\Helper::getLocationFlag($job->location->flag))}}}" alt="{{ trans('lang.img') }}"> {{{ $job->location->title }}}</span></li>
                                                    @endif
                                                    @if (!empty($job->project_type))
                                                        <li><a href="javascript:void(0);" class="wt-clicksavefolder"><i class="far fa-folder"></i> {{ trans('lang.type') }} {{{ $job->project_type }}}</a></li>
                                                    @endif
                                                    @if (!empty($job->duration) && !is_array($duration))
                                                        <li><span class="wt-dashboradclock"><i class="far fa-clock"></i> {{ trans('lang.duration') }} {{{ $duration }}}</span></li>
                                                    @endif

                                                    @if (!empty($job->financial_score) && !is_array($job->financial_score))
                                                        <li><span class="wt-dashboradclock"> Financial Score {{{ $job->financial_score }}}</span></li>
                                                    @endif

                                                    @if (!empty($job->teachniqal_score) && !is_array($job->teachniqal_score))
                                                        <li><span class="wt-dashboradclock"> Technical Score {{{ $job->teachniqal_score }}}</span></li>
                                                    @endif

                                                    @if (!empty($job->selection_procurement_system) && !is_array($job->selection_procurement_system))
                                                        <li><span class="wt-dashboradclock">Procurement System: <?php if($job->selection_procurement_system == 1){ echo "Quality Based Selection"; }
                                                        elseif($job->selection_procurement_system == 2){ echo "Quality & Cost Based Selection";} 
                                                        elseif($job->selection_procurement_system == 3){ echo "Least Cost";} 
                                                        elseif($job->selection_procurement_system == 4){ echo "Fixed Budget";} 
                                                        elseif($job->selection_procurement_system == 5){ echo "Single source or direct selection";} 
                                                        ?> </span></li>
                                                    @endif
                                                    

                                                </ul>
                                            @endif
                                        </div>
                                        <div class="wt-rightarea">
                                        {{-- {!!$jobDetails->description!!} --}}
                                    </div>
                                        <div class="wt-rightarea">
                                            <div class="wt-hireduserstatus">
                                                <h4>{{{ $proposals->count() }}}</h4><span>{{ trans('lang.proposals') }}</span>
                                                @if ($proposals->count() > 0)
                                                    <ul class="wt-hireduserimgs">
                                                        @foreach ($proposals as $proposal)
                                                            @php
                                                                $profile = \App\User::find($proposal->freelancer_id)->profile;
                                                                $user_image = !empty($profile) ? $profile->avater : '';
                                                                $profile_image = !empty($user_image) ? '/uploads/users/'.$proposal->freelancer_id.'/'.$user_image : 'images/user-login.png';
                                                            @endphp
                                                            <li><figure><img src="{{{ asset($profile_image) }}}" alt="{{ trans('lang.img') }}" class="mCS_img_loaded"></figure></li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                {{-- {{ dd($jobDetails->selection_procurement_system) }} --}}
                                @if (!empty($accepted_proposal))
                                    <div class="wt-freelancerholder wt-rcvproposalholder la-free-proposal">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.hired_freelancers') }}</h2>
                                    </div>
                                    <div class="wt-managejobcontent">
                                        @php
                                            $user = \App\User::find($accepted_proposal->freelancer_id);
                                            $profile = \App\User::find($accepted_proposal->freelancer_id)->profile;
                                            $user_image = !empty($profile) ? $profile->avater : '';
                                            $profile_image = !empty($user_image) ? '/uploads/users/'.$accepted_proposal->freelancer_id.'/'.$user_image : 'images/user-login.png';
                                            $user_name = Helper::getUserName($user->id);
                                            $feedbacks = \App\Review::select('feedback')->where('receiver_id', $user->id)->count();
                                            $avg_rating = App\Review::where('receiver_id', $user->id)->sum('avg_rating');
                                            $rating  = $avg_rating != 0 ? round($avg_rating/\App\Review::count()) : 0;
                                            $reviews = \App\Review::where('receiver_id', $user->id)->get();
                                            $stars  = $reviews->sum('avg_rating') != 0 ? (($reviews->sum('avg_rating')/$feedbacks)/5)*100 : 0;
                                            $average_rating_count = !empty($feedbacks) ? $reviews->sum('avg_rating')/$feedbacks : 0;
                                            $completion_time = !empty($accepted_proposal->completion_time) ? \App\Helper::getJobDurationList($accepted_proposal->completion_time) : '';
                                            $p_attachments = !empty($accepted_proposal->attachments) ? unserialize($accepted_proposal->attachments) : '';
                                            $badge = Helper::getUserBadge($user->id);
                                            if (!empty($enable_package) && $enable_package === 'true') {
                                                $feature_class = !empty($badge) ? 'wt-featured' : '';
                                                $badge_color = !empty($badge) ? $badge->color : '';
                                                $badge_img  = !empty($badge) ? $badge->image : '';
                                            } else {
                                                    $feature_class = '';
                                                    $badge_color = '';
                                                    $badge_img    = '';
                                            }
                                            @endphp
                                            <div class="wt-userlistinghold wt-proposalitem {{ $feature_class }}">
                                                @if(!empty($enable_package) && $enable_package === 'true')     
                                                    @if (!empty($badge))
                                                        <span class="wt-featuredtag" style="border-top: 40px solid {{ $badge_color }};">
                                                            <img src="{{{ asset(Helper::getBadgeImage($badge_img)) }}}" alt="{{ trans('lang.hired_freelancers') }}" data-tipso="Plus Member" class="template-content tipso_style">
                                                        </span>
                                                    @endif
                                                @endif
                                                <figure class="wt-userlistingimg">
                                                    <img src="{{{ asset($profile_image) }}}" alt="{{ trans('lang.profile_img') }}" class="mCS_img_loaded">
                                                </figure>
                                                <div class="wt-proposaldetails">
                                                    @if (!empty($user_name))
                                                        <div class="wt-contenthead">
                                                            <div class="wt-title">
                                                                <a href="{{ url('profile/'.$user->slug) }}">{{{ $user_name }}}</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="wt-proposalfeedback">
                                                        <span class="wt-stars"><span style="width: {{ $stars }}%;"></span></span>
                                                        <span class="wt-starcontent">{{{ round($average_rating_count) }}}<sub>{{ trans('lang.5') }}</sub> <em>({{{ $feedbacks }}} {{ trans('lang.feedbacks') }})</em></span>
                                                    </div>
                                                </div>
                                                <div class="wt-rightarea">
                                                    <div class="wt-btnarea">
                                                        <a href="javascript:void(0);" class="wt-btn" style="pointer-events:none;">{{ trans('lang.hired') }}</a>
                                                        <a href="{{{ url('proposal/'.$job->slug.'/'.$job->status) }}}"  class="wt-btn">{{ trans('lang.view_detail') }}</a>
                                                    </div>
                                                    <div class="wt-hireduserstatus">
                                                        <h5>{{ !empty($symbol) ? $symbol['symbol'] : '$' }}{{{ $accepted_proposal->amount }}}</h5>
                                                        @if(!empty($completion_time))
                                                            <span>{{{ $completion_time }}}</span>
                                                        @endif
                                                    </div>
                                                    <div class="wt-hireduserstatus">
                                                        <i class="far fa-envelope"></i>
                                                        <a href="javascript:void(0);" v-on:click.prevent="showCoverLetter('{{ $accepted_proposal->id }}')" ><span>{{ trans('lang.cover_letter') }}</span></a>
                                                    </div>
                                                    <div class="wt-hireduserstatus">
                                                        <i class="fa fa-paperclip"></i>
                                                        @if (!empty($p_attachments))
                                                            {!! Form::open(['url' => url('proposal/download-attachments'), 'class' =>'post-job-form wt-haslayout', 'id' => 'accepted-download-attachments-form-'.$accepted_proposal->id]) !!}
                                                                @foreach ($p_attachments as $attachments)
                                                                    @if (Storage::disk('local')->exists('uploads/proposals/'.$accepted_proposal->freelancer_id.'/'.$attachments))
                                                                        {!! Form::hidden('attachments['.$count.']', $attachments, []) !!}
                                                                        @php $count++; @endphp
                                                                    @endif
                                                                @endforeach
                                                                {!! Form::hidden('freelancer_id', $accepted_proposal->freelancer_id, []) !!}
                                                            {!! form::close(); !!}
                                                            <a href="javascript:void(0);"  v-on:click.prevent="downloadAttachments('{{'accepted-download-attachments-form-'.$accepted_proposal->id}}')" ><span>{{{ $count }}} {{ trans('lang.files_attached') }}</span></a>
                                                        @else
                                                            <span>{{{ $count }}} {{ trans('lang.files_attached') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <b-modal ref="myModalRef-{{ $accepted_proposal->id }}" hide-footer title="Cover Letter" v-cloak>
                                        <div class="d-block text-center">
                                                {{{$accepted_proposal->content}}}
                                            </div>
                                    </b-modal>
                                @endif 
                                <div class="wt-freelancerholder wt-rcvproposalholder">
                                        <div class="wt-tabscontenttitle">
                                            <h2>{{ trans('lang.received_proposals') }}</h2>
                                        </div>

                                         {{-- 
                                    1= Quality Based Selection;
                                    2= Quality & Cost Based Selection
                                    3= Least Cost
                                    4= Fixed Budget
                                    5=
                                    --}}
                                    
                                        @if (!empty($proposals))
                                            <div class="wt-managejobcontent">
                                                @foreach ($scoreArray as $proposalsinge)
                                                {{-- {{ dd($proposal['proposals_id']) }} --}}
                                                    @php
                                                     
                                                     $proposal = \App\Proposal::find($proposalsinge['proposals_id']);

                                                        $user = \App\User::find($proposal->freelancer_id);
                                                        $profile = \App\User::find($proposal->freelancer_id)->profile;
                                                        $user_image = !empty($profile) ? $profile->avater : '';
                                                        $profile_image = !empty($user_image) ? '/uploads/users/'.$proposal->freelancer_id.'/'.$user_image : 'images/user-login.png';
                                                        $user_name = $user->first_name.' '.$user->last_name;
                                                        $feedbacks = \App\Review::select('feedback')->where('receiver_id', $proposal->freelancer_id)->count();
                                                        $avg_rating = App\Review::where('receiver_id', $proposal->freelancer_id)->sum('avg_rating');
                                                        $rating  = $avg_rating != 0 ? round($avg_rating/\App\Review::count()) : 0;
                                                        $reviews = \App\Review::where('receiver_id', $proposal->freelancer_id)->get();
                                                        $stars  = $reviews->sum('avg_rating') != 0 ? (($reviews->sum('avg_rating')/$feedbacks)/5)*100 : 0;
                                                        $average_rating_count = !empty($feedbacks) ? $reviews->sum('avg_rating')/$feedbacks : 0;
                                                        $completion_time = !empty($proposal->completion_time) ? \App\Helper::getJobDurationList($proposal->completion_time) : '';
                                                        $attachments = !empty($proposal->attachments) ? unserialize($proposal->attachments) : '';
                                                        $attachments_count = 0;
                                                        if (!empty($attachments)){
                                                            $attachments_count = count($attachments);
                                                        }
                                                        $reviews = \App\Review::where('receiver_id', $user->id)->count();
                                                        $badge = Helper::getUserBadge($user->id);
                                                        if (!empty($enable_package) && $enable_package === 'true') {
                                                            $feature_class = !empty($badge) ? 'wt-featured' : '';
                                                            $badge_color = !empty($badge) ? $badge->color : '';
                                                            $badge_img  = !empty($badge) ? $badge->image : '';
                                                        } else {
                                                            $feature_class = '';
                                                            $badge_color = '';
                                                            $badge_img    = '';
                                                        }
                                                    @endphp
                                                    <div class="wt-userlistinghold wt-proposalitem {{ $feature_class }}">
                                                        @if(!empty($enable_package) && $enable_package === 'true')        
                                                            @if (!empty($badge))
                                                                <span class="wt-featuredtag" style="border-top: 40px solid {{ $badge_color }};">
                                                                    <img src="{{{ asset(Helper::getBadgeImage($badge_img)) }}}" alt="{{ trans('lang.is_featured') }}" data-tipso="Plus Member" class="template-content tipso_style">
                                                                </span>
                                                            @endif
                                                        @endif    
                                                        <figure class="wt-userlistingimg">
                                                            <img src="{{{ asset($profile_image) }}}" alt="{{ trans('lang.profile_img') }}" class="mCS_img_loaded">
                                                        </figure>
                                                        <div class="wt-proposaldetails">
                                                            @if (!empty($user_name))
                                                                <div class="wt-contenthead">
                                                                    <div class="wt-title">
                                                                        <a href="{{ url('profile/'.$user->slug) }}">{{{ $user_name }}}</a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="wt-proposalfeedback">
                                                                <span class="wt-stars"><span style="width: {{ $stars }}%;"></span></span>
                                                                <span class="wt-starcontent">{{{ round($average_rating_count) }}}<sub>{{ trans('lang.5') }}</sub> <em>({{{ $feedbacks }}} {{ trans('lang.feedbacks') }})</em></span>
                                                            </div>
                                                        </div>
                                                        <div class="wt-rightarea mb-3">
                                                           
                                                            <div class="wt-hireduserstatus">
                                                                <h5>{{ !empty($symbol) ? $symbol['symbol'] : '$' }}{{{$proposal->amount}}}</h5>
                                                                @if(!empty($completion_time))
                                                                    <span>{{{ $completion_time }}}</span>
                                                                @endif
                                                            </div>

                                                            <div class="wt-hireduserstatus">
                                                                    <h6>Technical </h6>
                                                                    <span>{{$proposalsinge['TechnicalQuestionsScore']}} / {{$TechnicalQuestionsTotal}}</span>
                                                                    <h6>Financial </h6>
                                                                    <span>{{$proposalsinge['FinancialQuestionsScore']}} / {{$FinancialQuestionsTotal}}</span>
                                                               
                                                            </div>
                                                          
                                                            <div class="wt-hireduserstatus">
                                                               
                                                                    <span><a href='{{url("add-score/{$proposal->id}/{$jobDetails->id}")}}'>Add / Update Score</a></span>
                                                               
                                                            </div>
                                                            <div class="wt-hireduserstatus">
                                                                <i class="far fa-envelope"></i>
                                                                <a href='{{url("add-score/{$proposal->id}/{$jobDetails->id}")}}'>Detail</a>
                                                            </div>

                                                            
                                                            <div class="wt-hireduserstatus">
                                                                <i class="fa fa-paperclip"></i>
                                                                @if (!empty($attachments))
                                                                    {!! Form::open(['url' => url('proposal/download-attachments'), 'class' =>'post-job-form wt-haslayout', 'id' => 'download-attachments-form-'.$proposal->id]) !!}
                                                                        @foreach ($attachments as $attachment)
                                                                            @if (Storage::disk('local')->exists('uploads/proposals/'.$proposal->freelancer_id.'/'.$attachment))
                                                                                {!! Form::hidden('attachments['.$received_proposal_count.']', $attachment, []) !!}
                                                                                @php $received_proposal_count++; @endphp
                                                                            @endif
                                                                        @endforeach
                                                                        {!! Form::hidden('freelancer_id', $proposal->freelancer_id, []) !!}
                                                                    {!! form::close(); !!}
                                                                    <a href="javascript:void(0);"  v-on:click.prevent="downloadAttachments('{{'download-attachments-form-'.$proposal->id}}')" ><span>{{{ $received_proposal_count }}} {{ trans('lang.files_attached') }}</span></a>
                                                                @else
                                                                    <span>{{{ $attachments_count }}} {{ trans('lang.files_attached') }}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if(count($proposalsinge['UserScores']) > 0)
                                                        <div class="mx-auto small" align="center">
                                                            <table class='table table-responsive table-striped'>
                                                                <thead>
                                                                    <th>Member Name</th>
                                                                    <th>Total Technical Score</th>
                                                                    <th>Total Financial Score</th>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($proposalsinge['UserScores'] as $scoringUser)
                                                                    <tr>
                                                                        <td>{{ $scoringUser['name'] }}</td>
                                                                        <td>{{ $scoringUser['total_technical_score'] . ' / ' . $TechnicalQuestionsTotal}}</td>
                                                                        <td>{{ $scoringUser['total_financial_score'] . ' / ' . $FinancialQuestionsTotal}}</td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                                @include('extend.errors.no-record')
                                            @else 
                                                @include('errors.no-record')
                                            @endif
                                        @endif
                                    </div>
                                  
                                </div>
                                @if ( method_exists($proposals,'links') )
                                    {{ $proposals->links('pagination.custom') }}
                                @endif
                        @endif

                        <div class="wt-dashboardboxtitle">
                            <h2>Bid Question</h2>
                        </div>
                        <table class="wt-tablecategories">
                            <thead>
                                        <tr>
                                          
                                            <th>Question Title</th>
                                            <th>Question Score</th>
                                            <th>Question Type</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($BidQuestion as $Question)
                                        <tr class="del-1">
                                      
                                            <td><span class="bt-content">
                                                {{ $Question->title }}</td>
                                            <td>{{ $Question->score }}</td>
                                            <td><?php if($Question->type == 0){ echo "Technical";}else{ echo "Financial"; } ?></td>
                                    
                                           
                                        </tr>
                                        @endforeach
                                   
                                    </tbody>
                                </table>


                        <div class="wt-dashboardboxtitle">
                            <h2>Meeting Minutes</h2>
                        </div>
                        <table class="wt-tablecategories">
                            <thead>
                                        <tr>
                                          
                                            <th>Meeting Title</th>
                                            <th>Meeting Date</th>
                                            <th>Meeting Location</th>
                                            <th>Approved</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($MinutesOfMeetings as $Minutes)
                                        @php
                                            $userId = Auth::id();           
                                            $Accepted_meeting_status = \App\MeetingAccept::where('meeting_minutes_id',$Minutes->id)->where('user_id', $userId)->first();
                                        @endphp
                                        <tr class="del-1">
                                      
                                            <td><span class="bt-content">
                                                {{ $Minutes->title }}</td>
                                            <td>{{ $Minutes->date }}</td>
                                            <td>{{ $Minutes->location }}</td>
                                            @if($Accepted_meeting_status)
                                                @if($Accepted_meeting_status->status == '1')
                                                    <td class="text text-success">Approved</td>
                                                @else
                                                    <td class="text text-danger">Disapproved</td>
                                                @endif
                                            @else
                                                <td class="text text-primary">Pending</td>
                                            @endif
                                            @if($Minutes->status == '0')
                                                <td class="text text-danger">Unpublished</td> 
                                            @else
                                                <td class="text text-success">Published</td> 
                                            @endif
                                            <td><a href="{{url('minutes-of-meeting',$Minutes->id)}}">View Detail</a></td>
                                           
                                        </tr>
                                        @endforeach
                                   
                                    </tbody>
                                </table>
                                <div class="wt-dashboardboxtitle">
                                    <h2>Comments</h2>
                                </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right" id="invoice_list">
                             
                                {{-- {{dd($jobDetails)}} --}}
                                <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                                    
                                    <br />
                                    <div class="col-md-6 col-xl-12">
                                    {{-- <h4>Display Comments</h4> --}}
                                    @include('back-end.employer.jobs.commentsDisplay', ['comments' => $jobDetails->comments, 'post_id' => $jobDetails->id])
            
                                    <form method="post" action="{{ route('comments.store'   ) }}">
                                        @csrf
                                        <div class="form-group">
                                            <textarea class="form-control" name="body"></textarea>
                                            <input type="hidden" name="job_id" value="{{ $jobDetails->id }}" />
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success" value="Add Comment" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                    </div>

                    
                </div>

                
            </div>

            
        </div>
     
            
          
     
        
    </section>
@endsection
@section('scripts')
<script type="text/javascript">

$(".addMember").change(function(){

var MemberID = $(this).val();
var JobID = $('#JobID').val();

if($(this).is(":checked")) {
    var CheckStatus = 'checked';
        }else{

            var CheckStatus = 'unchecked';

        }
$.ajax({
    type: 'POST',
    url: '{{url('committee-member-to-project')}}',

    cache: false,
    dataType: 'html',
    data: {
        MemberID: MemberID,
        CheckStatus:CheckStatus,
        JobID:JobID,
        _token: '{{csrf_token()}}'
    },
    success: function (res) {
    //    $("#distDropDown").html(res);
        //$(".preloader-outer").hide();
    }
    });
});

</script>
    @endsection
