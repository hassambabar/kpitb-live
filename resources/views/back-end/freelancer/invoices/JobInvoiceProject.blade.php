@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">

                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif


                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Invoice</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder milesonelist" id="printable_area">

                        <ul>
                                <li><span>Project: </span>{{$job->title}}
                                </li>
                                <li><span>Invoice: </span>{{$Invoice->title}}
                                </li>
                                <li><span>Invoice Amount: </span>{{$Invoice->price}}
                                </li>   
                                <li><span>Invoice Created Date:</span>{{$Invoice->created_at}}
                                </li>
                                <li><strong>Attachment</strong>: 
                                    @if(!empty($Invoice->transection_doc))
                                    <?php 
                                    
                                    $test = unserialize($Invoice->transection_doc);

?>
                                    @forelse ($test as $key => $value2)
                                    <br> <a href=" {{{ asset("/storage/app/uploads/users/$job->user_id/$value2") }}}" >View Attachment </a>
                                   
                                    @empty
                                        
                                    @endforelse

                                    @endif
                                </li>
                                <li><span>Description: </span>{{$Invoice->detail}}
                                </li>
                                <li>
                                    @if($Invoice->paid == 0)
                                    <a href="{{url("freelancer/job/status/order/$Invoice->id/accept/$Invoice->job_id")}}" class="btn btn-success">Accept</a>
                                    @if($JobDispute == '')
                                    
                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</a>
                                    @endif

                                    @endif
                                    {{-- @if(empty($value->milestonDispute))

                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</a>
                                    @endif --}}

                                </li>
                                
                              
                            </ul>
                            <?php //echo "<pre>"; print_r($Milestone->invoice_id); exit;?>
                          


                                @if(!empty($JobDispute))
                                <div class="milstonePaids alert-warning">
                                  <div class="wt-borderheading wt-borderheadingvtwo">
                                      <h3>Dispute</h3> 
                                  </div>
                                  <ul>
                                      <li><strong>Reason:</strong> {{$JobDispute->freelancer_reason}}  </li>
                                      <li><strong>Freelance Detial:</strong> {{$JobDispute->freelancer_description}}</li>
                                      <li><strong>Freelance Status:</strong> @if($JobDispute->freelancer_status == 1) Accepted @else Rejected @endif</li>
                              
                                      @if(!empty($JobDispute->pe_description))
                                      <li><strong>Response:</strong> {!! strip_tags($JobDispute->pe_description) !!}</li>
                                      <li><strong>Procurer Status:</strong> @if($JobDispute->pe_status == 1) Accepted @else Rejected @endif</li>
                              
                                      @if($JobDispute->freelancer_status != '')
                                      <li>
                                          <?php 
                                             // dd($Milestone->milestonDispute);
                                              $disputeId = $JobDispute->id;
                                              $status =  $JobDispute->status;
                                              ?> 
                                              @if($status != 1)
                                            <a href="{{url("freelance/milestone-reject/$disputeId")}}" class="btn btn-danger" >Reject</a></li>
                                            @endif
                                      @else
                                      <li><strong>Your Status:</strong> @if($JobDispute->freelancer_status == 1) Accepted @else Rejected @endif</li>
                              
                              
                                            @endif
                                      @endif
                                  </ul>
                                </div>
                              
                                <div class="milstonePaids alert-danger">
                                  <div class="wt-borderheading wt-borderheadingvtwo">
                                      <h3>Grievance Process</h3> 
                                  </div>
                                  <ul>
                                   <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                         Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                         Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                          sunt in culpa qui officia deserunt mollit anim id est laborum</p></li>
                                   </ul>
                                </div>
                                @endif



                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
          
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title">Please Provide Reason</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
          
                <!-- Modal body -->
                <div class="modal-body">
                    {!! Form::open(['url' => url('freelancer/job/submit-disputes'), 'files'=>'true']) !!}
                                @csrf
                                <div class="form-group">
                                    <label for="reason">Reason:</label>
                                    <input type="text" class="form-control"  name="reason" placeholder="Reason" id="reason" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="reason">Description:</label>
                                    <textarea class="form-control" name="freelancer_description" required ></textarea>
                                  </div>
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-danger" >Submit</button>
        
                                  </div>
        
                                  <input type="hidden" name="job_id" value="{{$job->id}}"  readonly/>
                                  <input type="hidden" name="invoice_id" value="{{$Invoice->id}}" readonly />
        
                                {!! Form::close(); !!}
        
                </div>
          
                <!-- Modal footer -->
            
          
              </div>
            </div>
          </div>
        <style>
            .milstonePaid{
                background-color: #b8d5c1;
            }
            .milesonelist ul li{
                list-style: none;
                padding: 10px 20px;
            }
            .milesonelist ul li span{
                font-weight: bold;
                padding-right: 10px;
            }
            </style>
    </section>

    @endsection
