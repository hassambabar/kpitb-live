@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">

                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif


                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Complete Milestone</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder milesonelist" id="printable_area">

                        <ul>
                                <li><span>Project: </span>{{$job->title}}
                                </li>
                                <li><span>Milestone: </span>{{$Milestone->title}}
                                </li>
                                <li><span>Milestone Amount: </span>{{$Milestone->amount}}
                                </li> 
                                <li><span>Milestone Percentage: </span>{{$Milestone->percentage}}%
                                </li>  
                                <li><span>Milestone Start Date: </span>{{$Milestone->start_date}}
                                </li>
                                <li><span>Milestone End Date: </span>{{$Milestone->end_date}}
                                </li>
                                <li><span>Description: </span>{{$Milestone->body}}
                                </li>
                                <li>
                                        {!! Form::open(['url' => url('freelancer/milestone_release_request/'.$Milestone->id.'/'.$job->slug.''), 'class' =>'wt-formtheme wt-formprojectinfo wt-formcategory', 'id' => 'request_to_close'] ) !!}
                                            @if($Milestone->milestone_release === 0)
                                                <div class="form-group wt-btnarea">
                                                    {!! Form::submit(trans('Request to release the payment'), ['class' => 'wt-btn']) !!}
                                                </div>
                                            @elseif($Milestone->milestone_release === 1)
                                                <div class=" form-group wt-btnarea" >
                                                   <p class="text-danger">You have requested the employer to release the payment. Kindly, wait for their response.</p> 
                                                </div>
                                            @endif
                                        {!! Form::close(); !!}
                                </li>
                                
                              
                            </ul>
                            <?php //echo "<pre>"; print_r($Milestone->invoice_id); exit;?>
                            @if($Milestone->invoice_id != '')

                            <div class="milstonePaid">
                                <div class="wt-borderheading wt-borderheadingvtwo">
                                    {{-- <h3>Milestone Complete</h3> --}}
                                    {{-- <a class="print-window" href="javascript:void(0);" @click="print()">
                                        <i class="fa fa-print"></i>
                                        {{{trans('lang.print')}}}
                                    </a> --}}
                                </div>
                            <ul>
                                <li>{{$Milestone->milestonInvoice->title}}
                                </li>
                                <li>Description: {!! $Milestone->milestonInvoice->detail !!}
                                </li>
                                <li>Tax Deducted ({{ $job_tax }}%): {!! $job_tax_amount !!}
                                </li>
                                <li><b>Amount Paid: {!! $Milestone->milestonInvoice->price !!}</b>
                                </li>
                                <li>Attachment: 
                                    @if(!empty($Milestone->milestonInvoice->attachments))
                                    <?php 
                                    
                                    $test = unserialize($Milestone->milestonInvoice->attachments);
                                    //echo "<pre>"; print_r( $test);

?>
                                    @forelse ($test as $key => $value)
                                    <br> <a href="{{url("/uploads/jobs/$value")}}" >View Attachment </a>
                                    @empty
                                        
                                    @endforelse

                                    @endif
                                </li>
                                <li>Date: {{$Milestone->milestonInvoice->created_at}}
                                </li>

                                <li>
                                    @if($Milestone->status != 1)
                                        <a href="{{url("freelancer/milestone/status/$Milestone->id/accept/$job->slug")}}" class="btn btn-success">Accept</a>
                                        @if(empty($Milestone->milestonDispute))
                                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Reject</a>
                                        @endif
                                    @endif

                                </li>
                            </ul>
                            </div>

                            @endif
  @if(!empty($Milestone->milestonDispute))
  <div class="milstonePaids alert-warning">
    <div class="wt-borderheading wt-borderheadingvtwo">
        <h3>Dispute</h3> 
    </div>
    <ul>
        <li><strong>Reason:</strong> {{$Milestone->milestonDispute->freelancer_reason}}  </li>
        <li><strong>Freelance Detial:</strong> {{$Milestone->milestonDispute->freelancer_description}}</li>
        <li><strong>Freelance Status:</strong> @if($Milestone->milestonDispute->freelancer_status == 1) Accepted @else Rejected @endif</li>

        @if(!empty($Milestone->milestonDispute->pe_description))
        <li><strong>Response:</strong> {!! strip_tags($Milestone->milestonDispute->pe_description) !!}</li>
        <li><strong>Procurer Status:</strong> @if($Milestone->milestonDispute->pe_status == 1) Accepted @else Rejected @endif</li>

        @if($Milestone->milestonDispute->freelancer_status != '')
        <li>
            <?php 
               // dd($Milestone->milestonDispute);
                $disputeId = $Milestone->milestonDispute->id;
                $status =  $Milestone->milestonDispute->status;
                ?> 
                @if($status != 1)
              <a href="{{url("freelance/milestone-reject/$disputeId")}}" class="btn btn-danger" >Reject</a></li>
              @endif
        @else
        <li><strong>Your Status:</strong> @if($Milestone->milestonDispute->freelancer_status == 1) Accepted @else Rejected @endif</li>


              @endif
        @endif
    </ul>
  </div>

  <div class="milstonePaids alert-danger">
    <div class="wt-borderheading wt-borderheadingvtwo">
        <h3>Grievance Process</h3> 
    </div>
    <ul>
     <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
           Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
           Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
            sunt in culpa qui officia deserunt mollit anim id est laborum</p></li>
     </ul>
  </div>
  @endif



                    </div>
                </div>
            </div>
        </div>
        @if($Milestone->invoice_id != '')
        <!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Please Provide Reason</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
            {!! Form::open(['url' => url('freelancer/submit-disputes'), 'files'=>'true']) !!}
                        @csrf
                        <div class="form-group">
                            <label for="reason">Reason:</label>
                            <input type="text" class="form-control"  name="reason" placeholder="Reason" id="reason" required>
                          </div>
                          <div class="form-group">
                            <label for="reason">Description:</label>
                            <textarea class="form-control" name="freelancer_description" required ></textarea>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-danger" >Submit</button>

                          </div>

                          <input type="hidden" name="job_id" value="{{$job->id}}" />
                          <input type="hidden" name="milestone_id" value="{{$Milestone->id}}" />
                          <input type="hidden" name="milestone_invoice_id" value="{{$Milestone->milestonInvoice->id}}" />

                        {!! Form::close(); !!}

        </div>
  
        <!-- Modal footer -->
    
  
      </div>
    </div>
  </div>
  @endif

        <style>
            .milstonePaid{
                background-color: #b8d5c1;
            }
            .milesonelist ul li{
                list-style: none;
                padding: 10px 20px;
            }
            .milesonelist ul li span{
                font-weight: bold;
                padding-right: 10px;
            }
            </style>
    </section>

    @endsection
