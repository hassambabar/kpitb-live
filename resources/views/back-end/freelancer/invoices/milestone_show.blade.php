@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@push('stylesheets')
    <link href="{{ asset('css/print.css') }}" rel="stylesheet" media="print" type="text/css">
@endpush
@section('content')
    <div class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="invoice_list">
                <div class="wt-transactionhold">
                    <div class="wt-borderheading wt-borderheadingvtwo">
                        <h3>{{{trans('lang.transaction_detl')}}}</h3>
                        <a class="print-window" href="javascript:void(0);" @click="print()">
                            <i class="fa fa-print"></i>
                            {{{trans('lang.print')}}}
                        </a>
                    </div>
                    @php
                        $sender = \App\User::where('id',$invoice_info->sender_id)->first();
                        $reciver = \App\User::where('id',$invoice_info->reciver_id)->first();
                        $job = \App\Job::where('id',$invoice_info->job_id)->first();
                    @endphp
                    <div class="wt-transactioncontent" id="printable_area">
                        <ul class="wt-transactiondetails">
                            <li>
                                <span><em>{{{trans('lang.pay_rec')}}}</em> {{{trans('lang.from')}}} {{{$sender->first_name}}} {{{$sender->last_name}}}</span>
                                <span class="wt-grossamount">{{{trans('lang.gross_amnt')}}}</span>
                            </li>
                            <li>
                                <span>
                                    {{{ Carbon\Carbon::parse($invoice_info->created_at)->diffForHumans()}}} on {{{Carbon\Carbon::parse($invoice_info->created_at)->format('l \\a\\t H:i:s')}}}
                                </span>

                                <span class="wt-transactionid">
                                    {{{trans('Order ID')}}}:&nbsp;{{{$invoice_info->order_id}}}
                                </span>
                                @if (!empty($reciver->id))
                                    <span class="wt-transactionid">
                                        {{{trans('lang.customer_id')}}}:&nbsp;{{{$reciver->id}}}
                                    </span>
                                @endif
                                <span class="wt-grossamount wt-grossamountusd">{{{ $symbol }}}{{{$english_format_number = number_format($invoice_info->price)}}}&nbsp;{{{ strtoupper($currency_code) }}}</span>
                            </li>
                            @if (!empty($invoice_info->reciver_status))
                                <li>
                                    <span>{{{trans('lang.pay_status')}}}&nbsp;&colon;</span>
                                    @if($invoice_info->reciver_status === 1)
                                        <span class="wt-paymentstatus">Paid</span>
                                    @else
                                        <span class="wt-paymentstatus">Unpaid</span>
                                    @endif
                                </li>
                            @endif
                        </ul>
                        
                        <div class="wt-createtransactionhold wt-createtransactionholdvtwo">
                            <div class="wt-createtransactionheading">
                                <span></span>
                            </div>
                            <div class="wt-refundscontent">
                                <ul class="wt-refundsdetails">
                                    <li>
                                        <strong>{{{trans('Milestone Invoice ID')}}}</strong>
                                        <div class="wt-rightarea"><span>{{{$invoice_info->id}}}</span></div>
                                    </li>
                                    <li>
                                        <strong>{{{trans('lang.paid_by')}}}</strong>
                                        <div class="wt-rightarea">
                                            <span>{{{$sender->first_name}}} {{{$sender->last_name}}}</span>
                                            <span>{{{trans('lang.pay_sender_note')}}}</span>
                                            <span>{{{$sender->email}}}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <strong><span>{{{trans('lang.need_help')}}}</span></strong>
                                        <span class="wt-refundsinfo">{{{trans('lang.paypal_note')}}}</span>
                                    </li>
                                    <li><span class="wt-refundsinfo">{{{trans('lang.paypal_warning_note')}}}</span></li>
                                    <li>
                                        <strong>{{{trans('Job')}}}</strong>
                                        <div class="wt-rightarea"><span>{{{$job->title}}}</span></div>
                                    </li>
                                    <li>
                                        <strong>{{{trans('lang.memo')}}}</strong>
                                        <div class="wt-rightarea"><span>{{{$invoice_info->title}}}</span></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
