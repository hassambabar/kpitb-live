@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="wt-haslayout wt-email-notification-settings wt-dbsectionspace" id="profile_settings">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-10">
                <div class="wt-dashboardbox wt-dashboardtabsholder wt-accountsettingholder">
                    @if (file_exists(resource_path('views/extend/back-end/settings/tabs.blade.php'))) 
                        @include('extend.back-end.settings.tabs')
                    @else 
                        @include('back-end.settings.tabs')
                    @endif
                    <div class="wt-tabscontent tab-content">
                        <div class="wt-emailnotiholder" id="wt-emailnoti">
                            <div class="wt-emailnoti">
                                <div class="wt-tabscontenttitle">
                                    <h2>KPRA Information</h2>
                                </div>
                                <div class="wt-settingscontent">

                                    <div class="wt-description">
                                        <p>Please Provide Your Valid KPRA Number</p>
                                    </div>
                                    <div class="row">
                                    <div class="col-sm-12">

                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div><br />
                                        @endif
                                    </div>
                                    </div>
                                    {!! Form::open(['url' => url('profile/settings/save-kpra-settings'),'class' =>'wt-formtheme wt-userform', 'id'=>'notifications'])!!}
                                        <fieldset>
                                            @if(Helper::getKpraInfo(Auth::user()->id) == '')

                                                    <div class="form-group">
                                                        <input type="text" name="kntn" class="form-control" placeholder="" >

                                            @else 
                                            <div class="form-group form-disabeld">

                                                <input type="email" name="kpraInfo" class="form-control" placeholder="{{{ $kntn }}}" disabled="">

                                            @endif
                                                {{-- <input type="email" name="kpraInfo" class="form-control" placeholder="{{{ $kpraInfo }}}" disabled=""> --}}

                                                {{-- <input type="text" name="kntn" class="form-control" placeholder="{{{ $kntn }}}" > --}}
                                                &nbsp;<br>
                                                @if(Helper::getKpraInfo(Auth::user()->id) == '')
                                                <input class="wt-btn" type="submit">
                                                @endif
                                            </div>
                                        </fieldset>
                                        <hr>
                                    {!! Form::close() !!}
                                    @if(!empty($kntn) && !empty($kpraInfo))
                                    {!! Form::open([ 'class' =>'wt-formtheme wt-userform', 'id'=>'notifications'])!!}
                                        <fieldset>
                                            <div class="form-group form-disabeld kpra-form">
                                                
                                                <div class="form-group form-group-half">
                                                    <label for="termsconditions"><span>NTN:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->NTN }}}" disabled="" ><br />
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <label for="termsconditions"><span>Name:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->Business_Name }}}" disabled="" >
                                                </div>   
                                                <div class="form-group form-group-half">
                                                    <label for="termsconditions"><span>Enrollment Date:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->Enrollment_Date }}}" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <label for="termsconditions"><span>Principal Activity:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->Principal_Activity }}}" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                <label for="termsconditions"><span>Address:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->Addresse }}}" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                <label for="termsconditions"><span>City:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">                                            
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->City_Name }}}" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                <label for="termsconditions"><span>District:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">                                           
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->District_Name }}}" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                <label for="termsconditions"><span>Tax Payer:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">                               
                                                    <input type="email" name="kpraInfo" class="form-control" value=" @if($kpraInfo->Activetaxpayer==1) Active @else Inactive @endif" disabled="" >
                                                </div>
                                                <div class="form-group form-group-half">
                                                <label for="termsconditions"><span>Compliance Level:</span></label>
                                                </div>
                                                <div class="form-group form-group-half">                                            
                                                    <input type="email" name="kpraInfo" class="form-control" value="{{{ $kpraInfo->Compliance_Level }}}" disabled="" >
                                                </div>

                                            </div>
                                        </fieldset>
                                    {!! Form::close() !!}

                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
