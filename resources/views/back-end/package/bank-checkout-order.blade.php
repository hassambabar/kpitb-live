@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class=" col-12 col-xl-8" id="packages">
                <div class="preloader-section" v-if="loading" v-cloak>
                    <div class="preloader-holder">
                        <div class="loader"></div>
                    </div>
                </div>
                <div class="wt-dashboardbox">
                    @if (Session::has('message'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('message') @endphp;
                    @elseif (Session::has('error'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ str_replace("'s", " ", Session::get('error')) }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('error'); @endphp
                    @endif
                    <div class="sj-checkoutjournal">
                        <div class="wt-dashboardboxtitle">
                            <h2>{{{trans('lang.order')}}}</h2>
                        </div> 
                     
                        <div class="wt-dashboardboxcontent wt-oderholder">
                            <table class="sj-checkouttable wt-ordertable">
                                <thead>
                                    <tr>
                                        <th>{{ trans('lang.item_title') }}</th>
                                    <th>{{trans('lang.details')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="sj-producttitle">
                                                <div class="sj-checkpaydetails">
                                                    <h4>{{{$title}}}</h4>
                                                    @if (!empty($subtitle))
                                                        <span>{{{$subtitle}}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                        <td>PKR {{{$english_format_number = number_format($cost)}}}</td>
                                    </tr>
                                    @if (!empty($options))
                                        <tr>
                                            <td>{{ trans('lang.duration') }}</td>
                                            <td>{{{Helper::getPackageDurationList($options['duration'])}}}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>{{ trans('lang.tax_deduction') }} ({{ $job_tax }}%)</td>
                                        <td>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : 'PKR' }} {{{$english_format_number = number_format($job_tax_amount)}}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('lang.total') }}</td>
                                        <td>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : 'PKR' }} {{{$english_format_number = number_format($newAmount)}}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('lang.status') }}</td>
                                        <td>{{ trans('lang.pending') }}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            <div class="wt-tabscontenttitle">
                                {{-- <h2>Approved Work Order</h2> --}}
                            </div>
                            <div class="wt-transection-holder">
                                {{-- {!! Form::open(['url' => '', 'class' =>'wt-formtheme wt-userform sj-checkouttable', '@submit.prevent' => 'submitTransection("'.$product_id.'")', 'id' => 'trans_form' ])!!} --}}
                                <form method="POST" action="{{url('freelancer/approved-job-submit/')}}" class="wt-formtheme wt-userform sj-checkouttable">
                                    @csrf
                                    <input type="hidden" value="{{$product_id}}" name="proposal_id">
                                    <input type="hidden" value="{{$product_id}}" name="product_id">
                                    <input type="hidden" value="{{$title}}" name="product_title">

                                    
                                    {{-- <input type="hidden" value="{{$order}}" name="order_id">
                                    <input type="hidden" value="{{$cost}}" name="product_price">
                                    <input type="hidden" value="{{$type}}" name="type"> --}}

                                    <fieldset>
                                      
                                        <div class="form-group form-group-half wt-btnarea">
                                            {!! Form::submit('Approve Work Order Now', ['class' => 'wt-btn']) !!}
                                        </div>
                                    </fieldset>
                                </form>
                                {{-- {!! Form::close() !!} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         
        </div>
    </section>
@endsection
