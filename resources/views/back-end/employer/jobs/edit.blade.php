@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 float-left" id="post_job">
            @if (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time='5' :message="'{{{ Session::get('error') }}}'" v-cloak>
                </flash_messages>
            </div>
            @endif
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="wt-haslayout wt-post-job-wrap">
                {!! Form::open(['url' => '', 'class' =>'post-job-form wt-haslayout', 'id' => 'job_edit_form',
                '@submit.prevent'=>'updateJob("'.$job->id.'")']) !!}

                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>{{ trans('lang.edit_job') }}</h2>
                    </div>
                    <div class="wt-dashboardboxcontent">
                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>{{ trans('lang.job_desc') }}</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group">
                                        {!! Form::text('title', $job->title, array('class' => 'form-control',
                                        'placeholder' => trans('lang.job_title'))) !!}
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('project_levels', $project_levels ,
                                            e($job->project_levels)) !!}
                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('job_duration', $job_duration , e($job->duration)) !!}
                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('service_type', $service_type , e($job->service_type)) !!}
                                        </span>
                                    </div>


                                    {{-- <div class="form-group form-group-half wt-formwithlabel">
                                                <span class="wt-select">
                                                    {!! Form::select('freelancer_type', $freelancer_level_list, e($job->freelancer_type)) !!}
                                                </span>
                                            </div> --}}
                                    {{-- <div class="form-group form-group-half wt-formwithlabel">
                                                <span class="wt-select">
                                                    {!! Form::select('english_level', $english_levels, e($job->english_level)) !!}
                                                </span>
                                            </div> --}}
                                    @if(Auth::user()->government_department == 1)
                                    <div class="form-group form-group-half wt-formwithlabel">

                                        <!-- {!! Form::number('project_cost', null, array('class' => '', 'placeholder' => trans('lang.project_cost'))) !!} -->
                                        <select name="project_cost" style="width:100%;">
                                            <option @if($job->price == "0-200k PKR") selected @endif value="0-200k
                                                PKR">0-200k PKR</option>
                                            <option @if($job->price == "200k-500k PKR") selected @endif value="200k-500k
                                                PKR">200k-500k PKR</option>
                                            <option @if($job->price == "500k PKR and above") selected @endif value="500k
                                                PKR and above">500k PKR and above</option>
                                        </select>
                                    </div>
                                    @else
                                    <div class="form-group form-group-half wt-formwithlabel job-cost-input">

                                        {!! Form::text('project_cost', $job->price, array('class' => 'form-control',
                                        'placeholder' => trans('lang.project_cost'))) !!}

                                    </div>
                                    @endif
                                    <job-expiry :db_expiry_date="'{{$job->expiry_date}}'"
                                        :ph_expiry_date="'{{trans('lang.project_expiry')}}'"
                                        :weekdays="'{{json_encode($weekdays)}}'" :months="'{{json_encode($months)}}'">
                                    </job-expiry>
                                </fieldset>
                            </div>
                        </div>
                        @if(Auth::user()->government_department == 1)


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Selection Procurement System </h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('selection_procurement_system',
                                            $selection_procurement_system, e($job->selection_procurement_system),
                                            array('class' => '', 'id'=> 'selection_procurement_system')) !!}


                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('single_source',
                                            $getSelectionProcurementSystemSingleSource, e($job->single_source),
                                            array('class' => '', 'disabled' => true,'id'=> 'single_source',
                                            'placeholder' => 'Selection single source')) !!}
                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('project_budget', $project_budget, e($job->project_budget),
                                            array('class' => '', 'placeholder' => 'Project budget')) !!}
                                        </span>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel job-cost-input">

                                        {!! Form::number('project_cost', e($job->price), array('class' => '',
                                        'placeholder' => trans('lang.project_cost'))) !!}

                                    </div>



                                </fieldset>

                            </div>
                        </div>
                        @endif



                        @if(Auth::user()->government_department == 1)


                        <div class="wt-jobskills wt-jobskills-holder wt-tabsinfo single_source_box"
                            style="display: none">
                            <div class="wt-tabscontenttitle">
                                <h2>Single Source Account</h2>
                            </div>
                            <div>
                                <div class="wt-formtheme wt-skillsform">
                                    <!---->
                                    <fieldset>

                                        <div class="form-group">

                                            <div class="form-group-holder">
                                                <input type="text" name="single_source_email"
                                                    value="{{$job->single_source_email}}" id="add_single_source"
                                                    class="form-control" placeholder="Single Source Email">

                                            </div>
                                        </div>
                                        {{-- <div class="form-group wt-btnarea"><a href="javascript:void(0);" id="check_single_email" class="wt-btn">Verify</a></div> --}}
                                    </fieldset>
                                </div>
                                <div class="wt-myskills">
                                    <ul id="verify_account" class="sortable list"> </ul>
                                    <div class="wt-description">
                                        <p id="freelancer_account_message"></p>
                                    </div>
                                </div>



                            </div>
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Bid Score Weightage</h2><small>Totle Score Must Be Equal 100 Points</small>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <div class="wt-description">
                                            <p>Technical Score</p>
                                        </div>
                                        <div class="form-group">

                                            <input type="number" id="teachniqal_score"
                                                value="{{$job->teachniqal_score}}" name="teachniqal_score"
                                                style="width:100%;" />
                                        </div>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <div class="wt-description">
                                            <p>Financial Score</p>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" min="1" max="100" id="financial_score"
                                                value="{{$job->financial_score}}" name="financial_score"
                                                style="width:100%;" placeholder="Financial Score" />
                                        </div>
                                    </div>

                                </fieldset>




                            </div>
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Bid Evaluation Criteria</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div id="field">

                                        <?php    $questions = count($BidQuestion);

                                        if($questions > 0){
                                            foreach ($BidQuestion as $key => $value) {
                                               // echo "<pre>"; print_r($value); exit;
                                                ?>
                                        <div id="field0">
                                            <div class="form-group-half wt-formwithlabel">
                                                <div class="form-group">
                                                    <input id="action_id" name="question_title[{{$value->id}}]"
                                                        type="text" value="{{$value->title}}" placeholder="Question"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group-half wt-formwithlabel">
                                                <div class="form-group">
                                                    <input id="action_name" name="question_score[{{$value->id}}]"
                                                        value="{{$value->score}}" type="text" placeholder="Score"
                                                        class="form-control">

                                                </div>
                                            </div>
                                            <div class="form-group form-group-half wt-formwithlabel">
                                                <span class="wt-select">
                                                    <select name="question_type[{{$value->id}}]">
                                                        <option selected="selected" value="0"
                                                            <?php if($value->type == 0){ echo "Selected"; } ?>>
                                                            Technical</option>
                                                        <option value="1"
                                                            <?php if($value->type == 1){ echo "Selected"; } ?>>Financial
                                                        </option>

                                                    </select></span>
                                            </div>

                                        </div>

                                        <?php } }      ?>

                                    </div>
                                    {{-- <div class="form-group form-group-half wt-formwithlabel">
                                    <div class="col-md-12">
                                    <div class="form-group wt-btnarea"><a href="javascript:void(0);" id="add-more" class="btn btn-primary">Add More</a></div>
                                  </div>
                                </div> --}}




                                </fieldset>




                            </div>
                        </div>



                        @endif

                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>{{ trans('lang.job_cats') }}</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('categories_parent', $categories, $categories_parent->id,
                                            array('class' => '','id' => 'parent_category', 'placeholder' =>
                                            trans('lang.select_job_cats'))) !!}
                                        </span>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('categories', $categories_child, $job->categories,
                                            array('class' => '','id' => 'child_category', 'placeholder' =>
                                            trans('lang.select_job_cats'))) !!}
                                        </span>
                                    </div>

                                </fieldset>
                            </div>
                        </div>


                        {{-- <div class="wt-jobskills wt-tabsinfo">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.job_cats') }}</h2>
                    </div>
                    <div class="wt-divtheme wt-userform wt-userformvtwo">
                        <div class="form-group">
                            <span class="wt-select">
                                {!! Form::select('categories[]', $categories, $job->categories, array('class' =>
                                'chosen-select', 'multiple', 'data-placeholder' => trans('lang.select_job_cats'))) !!}
                            </span>
                        </div>
                    </div>
                </div> --}}
                {{-- <div class="wt-jobskills wt-tabsinfo">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.langs') }}</h2>
            </div>
            <div class="wt-divtheme wt-userform wt-userformvtwo">
                <div class="form-group">
                    <span class="wt-select">
                        {!! Form::select('languages[]', $languages, $job->languages, array('class' => 'chosen-select',
                        'multiple', 'data-placeholder' => trans('lang.select_lang'))) !!}
                    </span>
                </div>
            </div>
        </div> --}}
        <div class="wt-jobdetails wt-tabsinfo">
            <div class="wt-tabscontenttitle">
                <h2>{{ trans('lang.job_dtl') }}</h2>
            </div>
            <div class="wt-formtheme wt-userform wt-userformvtwo">
                {!! Form::textarea('description', $job->description, ['class' => 'wt-tinymceeditor', 'id' =>
                'wt-tinymceeditor', 'placeholder'
                => trans('lang.job_dtl_note')]) !!}
            </div>
        </div>
        <div class="wt-jobskills wt-tabsinfo la-jobedit">
            <div class="wt-tabscontenttitle">
                <h2>{{ trans('lang.skills_req') }}</h2>
            </div>
            <div class="la-jobedit-content">
                <job_skills :placeholder="'select skills'"></job_skills>
            </div>
        </div>



        <div class="wt-joblocation wt-tabsinfo">
            <div class="wt-tabscontenttitle">
                <h2>{{ trans('lang.your_loc') }}</h2>
            </div>
            <div class="wt-formtheme wt-userform">
                <fieldset>
                    <div class="form-group form-group-half">
                        <span class="wt-select">
                            {!! Form::select('locations', $locations, $job->location_id, array('class' =>
                            'skill-dynamic-field', 'id' => 'locationProvice','placeholder' => 'Province')) !!}
                        </span>
                    </div>
                    <!-- <div class="form-group form-group-half">
                                                <span class="wt-select">

                                                    {!! Form::select('district', $District, $job->district, array('class' => 'skill-dynamic-field', 'id' => 'distDropDown')) !!}

                                                {{-- <select name="district" id="distDropDown"  >
                                                    <option value="">District</option>
                                                </select> --}}
                                                <span class="help-block" >
                                                </span>
                                            </span>
                                            </div> -->
                    <div class="form-group form-group-half">
                        {!! Form::text( 'address', $job->address, ['id'=>"pac-input", 'class' =>'form-control',
                        'placeholder' => trans('lang.your_address')] ) !!}
                    </div>

                </fieldset>
            </div>
        </div>


        {{-- <div class="wt-featuredholder wt-tabsinfo">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.is_featured') }}</h2>
        <div class="wt-rightarea">
            <div class="wt-on-off float-right">
                <switch_button v-model="is_featured">{{{ trans('lang.make_job_featured') }}}</switch_button>
                <input type="hidden" :value="is_featured" name="is_featured">
            </div>
        </div>
    </div>
</div> --}}
<div class="wt-attachmentsholder">
    <div class="lara-attachment-files">
        <div class="wt-tabscontenttitle">
            <h2>{{ trans('lang.attachments') }}</h2>
            <div class="wt-rightarea">
                <div class="wt-on-off float-right">
                    <switch_button v-model="show_attachments">{{{ trans('lang.attachments_note') }}}</switch_button>
                    <input type="hidden" :value="show_attachments" name="show_attachments">
                </div>
            </div>
        </div>
        <job_attachments :temp_url="'{{url('job/upload-temp-image')}}'"></job_attachments>
        <div class="form-group input-preview">
            <ul class="wt-attachfile dropzone-previews">

            </ul>
        </div>
        @if (!empty($attachments))
        @php $count = 0; @endphp
        <div class="form-group input-preview">
            <ul class="wt-attachfile">
                @foreach ($attachments as $key => $attachment)
                <li id="attachment-item-{{$key}}">
                    <span>{{{Helper::formateFileName($attachment)}}}</span>
                    <em>
                        @if (Storage::disk('local')->exists('uploads/jobs/'.$job->user_id.'/'.$attachment))
                        {{ trans('lang.file_size') }}
                        {{{Helper::bytesToHuman(Storage::size('uploads/jobs/'.$job->user_id.'/'.$attachment))}}}
                        @endif
                        <a
                            href="{{{route('getfile', ['type'=>'jobs','attachment'=>$attachment,'id'=>$job->user_id])}}}"><i
                                class="lnr lnr-download"></i></a>
                        <a href="#" v-on:click.prevent="deleteAttachment('attachment-item-{{$key}}')"><i
                                class="lnr lnr-cross"></i></a>
                    </em>
                    <input type="hidden" value="{{{$attachment}}}" class="" name="attachments[{{$key}}]">
                </li>
                @php $count++; @endphp
                @endforeach
                <div class="dropzone-previews"></div>
            </ul>
        </div>
        @endif
    </div>
</div>
</div>
</div>
<div class="wt-updatall">
    <i class="ti-announcement"></i>
    <span>{{{ trans('lang.save_changes_note') }}}</span> {!! Form::submit(trans('lang.btn_save_update'), ['class' =>
    'wt-btn',
    'id'=>'submit-profile']) !!}
</div>
{!! form::close(); !!}
</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#parent_category').select2({
        placeholder: "Select Your Job's Category"
    }).on('change', function(e) {
        var data = $("#parent_category option:selected").text();
  });
  $('#child_category').select2({
        placeholder: "Please Select Sub Category"
    }).on('change', function(e) {
        var data = $("#child_category option:selected").text();
  });
   $('#locationProvices').select2({
        placeholder: "Your Location"
    }).on('change', function(e) {
        var data = $("#locationProvices option:selected").text();
  });
  $('#freelancer_skill').select2({
        placeholder: "Your Location"
    }).on('change', function(e) {
        var data = $("#freelancer_skill option:selected").text();
  });
    $("#parent_category").change(function(){

$("#child_category").prop("disabled", false);
    $.ajax({
        url: "{{ route('categories.get_by_parent') }}?parent_id=" + $(this).val(),
        method: 'GET',
        success: function(data) {
            $('#child_category').html(data.html);
        }
    });
    $.ajax({
        url: "{{ route('categories.skills_by_category') }}?parent_id=" + $(this).val(),
        method: 'GET',
        success: function(data) {
            $('#freelancer_skill').html(data.html);
            $('#skill_list').html('');

        }
    });


});
$("#selection_procurement_system").change(function(){
    if (this.value != 5) {
        $(".single_source_box").hide();
        $("#single_source").val('');
    } else {
        $(".single_source_box").show();
    }

if(this.value == 5){
    $("#single_source").prop("disabled", false);

}else{
    $("#single_source").prop("disabled", true);

}
if(this.value == 1){
    $("#financial_score").val("40");
    $("#teachniqal_score").val("60");
    $(".single_source_box").hide();

}else if(this.value == 2){
    $("#financial_score").val("50");
    $("#teachniqal_score").val("50");
    $(".single_source_box").hide();

}else if(this.value == 3){
    $("#financial_score").val("60");
    $("#teachniqal_score").val("40");
    $(".single_source_box").hide();

}else if(this.value == 4){
    $("#financial_score").val("80");
    $("#teachniqal_score").val("20");
    $(".single_source_box").hide();

}else if(this.value == 5){
    $("#financial_score").val("0");
    $("#teachniqal_score").val("0");
    $(".single_source_box").show();

}





});


$("#single_source").change(function(){

if(this.value == 5){
$(".single_source_box").hide();

}else{
$(".single_source_box").show();

}

});




$( document ).ready(function() {
    var selection_procurement_system = $("#selection_procurement_system").val();
    if (selection_procurement_system != 5) {
        $(".single_source_box").hide();
    } else {
        $(".single_source_box").show();
    }
    if(selection_procurement_system == 5){
    $("#single_source").prop("disabled", false);

}else{
    $("#single_source").prop("disabled", true);

}



var single_source = $("#single_source").val();

if(selection_procurement_system == 5 && single_source == 5){
    $(".single_source_box").hide();

}else if (selection_procurement_system == 5 && single_source != 5){
    $(".single_source_box").show();

}


});

$("#locationProvice").change(function(){

var locId = $(this).val();
$.ajax({
    type: 'POST',
    url: '{{url('getAjaxDistrics')}}',
    cache: false,
    dataType: 'html',
    data: {
        locId: locId,
        _token: '{{csrf_token()}}'
    },
    success: function (res) {
        $("#distDropDown").html(res);
        //$(".preloader-outer").hide();
    }
    });
});


</script>
@endsection
