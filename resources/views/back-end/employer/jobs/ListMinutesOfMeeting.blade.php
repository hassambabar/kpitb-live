@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right" id="invoice_list">
                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Meeting Minutes</h2>
                        <a href="{{ url('meeting-minutes-add', $job_id) }}" class="btn btn-primary" style="float: right;     margin: 5px;">Add Meeting Minutes</a>&nbsp; &nbsp; 
                        <a href="{{ url('employer/dashboard/manage-jobs') }}" class="btn btn-primary" style="float: right;     margin: 5px;">Back To Job</a>

                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                        {{-- @if (!empty($invoices) && $invoices->count() > 0) --}}

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div class="alert alert-white score-bad2" role="alert">
                            <p class="text text-success">* Committee Members who have accepted the minutes of meeting.</p>
                            <p class="text text-primary">* Committee Members who's approval is pending.</p>
                            <p class="text text-danger">* Committee Members who have rejected the minutes of meeting.</p>
                        </div>
                        <!-- <div class="alert alert-warning score-bad2" role="alert">
                            Note: Committee Members who's approval is pending.
                        </div>
                        <div class="alert alert-danger score-bad2" role="alert">
                            Note: Committee Members who have rejected the minutes of meeting.
                        </div> -->

                    <table class="wt-tablecategories">
                        <thead>
                                    <tr>
                                      
                                        <th>Title</th>
                                        <th>Date</th>
                                        <th>Location</th>
                                        <th>Accepted By:</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($MinutesOfMeetings as $Minutes)
                                    <tr class="del-1">
                                   
                                        <td><span class="bt-content">
                                            <a href="{{url('minutes-of-meeting',$Minutes->id)}}">   {{ $Minutes->title }} </a></td>
                                        <td>{{ $Minutes->date }}</td>
                                        <td>{{ $Minutes->location }}</td>
                                        <td>
                                            @if(!empty($CommitteeMembers))
                                                @php
                                                    $Approved_by_all = 0;
                                                    $AcceptanceList = \App\MeetingAccept::where('meeting_minutes_id',$Minutes->id)->get();
                                                    foreach($CommitteeMembers as $CommitteeMember){
                                                        $MeetingAccept = \App\MeetingAccept::where('user_id', $CommitteeMember->member_id)->where('meeting_minutes_id', $Minutes->id)->first();
                                                        if($MeetingAccept){
                                                            if($MeetingAccept->status == '1'){
                                                                $Approved_by_all = 1;
                                                            }
                                                            else{
                                                                $Approved_by_all = 0;
                                                            }
                                                        }
                                                        else{
                                                            $Approved_by_all = 0;
                                                        }
                                                    }
                                                    $rejected = 0;
                                                @endphp
                                            <ul>
                                                @foreach ($CommitteeMembers as $committeemember)
                                                    @php
                                                        $MeetingAccept = \App\MeetingAccept::where('user_id', $committeemember->member_id)->where('meeting_minutes_id', $Minutes->id)->first();
                                                        $committeemember_info = \App\User::where('id', $committeemember->member_id)->first();
                                                        
                                                    @endphp
                                                    @if(!empty($MeetingAccept))
                                                        @if($MeetingAccept->status == '1')
                                                            <li class="text text-success">{{$committeemember_info->first_name}} {{$committeemember_info->last_name}}</li>
                                                        @else
                                                            @php $rejected = 1; @endphp
                                                            <li class="text text-danger">{{$committeemember_info->first_name}} {{$committeemember_info->last_name}}</li>
                                                        @endif
                                                    @else
                                                        <li class="text text-primary">{{$committeemember_info->first_name}} {{$committeemember_info->last_name}}</li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>  
                                            @php
                                            if ($Approved_by_all) {
                                                if ($Approved_by_all == 1 && $rejected == 0) {
                                                    if ($Minutes->status == 1) {
                                                        $label = 'Publish';
                                                        $titleAdd = 'Click to UnPublish';
                                                        $titleAddClass = 'btn-success';
                                                    } else {
                                                        $label = 'Draft';
                                                        $titleAdd = 'Click to Publish';
                                                        $titleAddClass = 'btn-warning';
                                                    }
                                                } else {
                                                    $Minutes_status = \App\MinutesOfMeeting::where('id', $Minutes->id)->update(['status' => 0]);
                                                    $label = 'Draft';
                                                    $titleAdd = 'Can not publish the meeting unless all members approve it';
                                                    $titleAddClass = 'btn-danger';
                                                }
                                            }
                                            else{
                                                $Minutes_status = \App\MinutesOfMeeting::where('id', $Minutes->id)->update(['status' => 0]);
                                                    $label = 'Draft';
                                                    $titleAdd = 'Can not publish the meeting unless all members approve it';
                                                    $titleAddClass = 'btn-dark';
                                            }
                                        @endphp
                                            @if($titleAddClass == 'btn-success' || $titleAddClass == 'btn-warning')
                                                <form action="{{ url('meeting-minutes/'.$Minutes->id)}}" method="post">
                                                    @csrf
                                                    @method('put')
                                                    <button class="btn btn-sm {{ $titleAddClass }}" title = '{{ $titleAdd }}' type="submit">{{ $label }}</button>
                                                </form>
                                            @else
                                                <form action="" method="post">
                                                    @csrf
                                                    @method('put')
                                                    <button class="btn btn-sm {{ $titleAddClass }}" title = '{{ $titleAdd }}' disabled>{{ $label }}</button>
                                                </form>
                                            @endif
                                            
                                           
                                        </td>
                                     

                                        <td>  <div class="wt-actionbtn">  
                                         <?php if ($Minutes->status == 0) { ?>
                                          <a href="{{route('minutes-of-meeting.edit',$Minutes->id)}}" class="wt-addinfo wt-skillsaddinfo"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                          &nbsp;   &nbsp; <form action="{{ route('minutes-of-meeting.destroy', $Minutes->id)}}" method="post">
                                          @csrf
                                          @method('DELETE')
                                          {{-- <a href="#" id="1" class="wt-deleteinfo"><i class="lnr lnr-trash"></i></a> --}}

                                         <button class="btn btn-sm wt-deleteinfo btn-delete-meeting" onclick="return confirm('Are you sure you want to delete this item?');" type="submit"><i class="lnr lnr-trash"></i></button>
                            </form>
                            {{-- &nbsp;   &nbsp;   <a href="#" id="1" class="wt-deleteinfo"><i class="lnr lnr-trash"></i></a> --}}
                                                 
                                                 <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                               
                                </tbody>
                            </table>
                           

                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endsection
    