@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')

@section('content')

<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-left" >

            <div class="wt-haslayout wt-post-job-wrap">

                {!! Form::open(['url' => route('minutes-of-meeting.store'), 'class' =>'post-job-form wt-haslayout', 'files' => true , '@submit.prevent'=>'submitJob']) !!}
                <div class="wt-dashboardbox">

                    
                    <div class="wt-dashboardboxtitle">
                        <h2>Add Meeting Minutes</h2>
                    </div>

                    <div class="wt-dashboardboxcontent">

                        <div class="col-sm-12">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Meeting Minutes Details</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') }}">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="location" class="form-control" placeholder="Location" value="{{ old('location') }}" >
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="date" data-provider="datepicker" class="form-control" id="meeting_date" placeholder="Date" value="{{ old('date') }}" >
                                    </div>

                                  

                                </fieldset>
                            </div>
                        </div>
                            <div class="wt-jobdetails wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Detail</h2>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo">
                                    {!! Form::textarea('body', null, ['class' => 'wt-tinymceeditor', 'id' => 'wt-tinymceeditor']) !!}
                                </div>
                            </div>

                            <div class="wt-attachmentsholder">
                                <div class="lara-attachment-files">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.attachments') }}</h2>
                                      
                                    </div>
                                   
                                    <div class="form-group">
                                       
                                        <div class="col-sm-7">
                                            <input type="file" class="form-control" id="attachments" name="attachments[]"   multiple  />
                                        </div>
                                    </div>
                                </div>
                            </div>

                             
                        
                   
                </div>

                
                </div>

                <input type="hidden" name="job_id" value="{{$job_id}}">
                <div class="wt-updatall">
                    <i class="ti-announcement"></i>
                    <span>Save all the latest changes made by you</span>
                    {!! Form::submit('ADD Minutes ', ['class' => 'wt-btn']) !!}
                </div>
                {!! form::close(); !!}

            </div>

        </div>
    
    </div>
</div>
   
@section('bootstrap_script')



<script>
    $( function() {
      $( "#meeting_date" ).datepicker({
        changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'dd MM yy',
      });
    } );
    </script> 

<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

@stop
@endsection
