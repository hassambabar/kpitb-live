@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-left" id="post_job">
            @if (Session::has('payment_message'))
                @php $response = Session::get('payment_message') @endphp
                <div class="flash_msg">
                    <flash_messages :message_class="'{{{$response['code']}}}'" :time ='5' :message="'{{{ $response['message'] }}}'" v-cloak></flash_messages>
                </div>
            @endif
            @if (session()->has('type'))
                @php session()->forget('type'); @endphp
            @endif
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="wt-haslayout wt-post-job-wrap">
                {!! Form::open(['url' => url('job/post-job'), 'class' =>'post-job-form wt-haslayout', 'id' => 'post_job_form',  '@submit.prevent'=>'submitJob']) !!}
                    <div class="wt-dashboardbox">
                        <div class="wt-dashboardboxtitle">
                            <h2>{{ trans('lang.post_job') }}</h2>
                        </div>
                        <div class="wt-dashboardboxcontent">
                            <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                             <h2>Write a headline for your job post</h2> <small>This helps your job post stand out to the right candidates. It’s the first thing they’ll see, so make it count!</small>
                            </div>
                            
                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" name="title" class="form-control" placeholder="Graphic designer needed to design ad creative for multiple campaigns" v-model="title">
                                           
                                        </div>
                                    </fieldset> 
                                </div>
                            </div>   
                            <div class="wt-jobdescription wt-tabsinfo">
                                        <div class="wt-tabscontenttitle">
                                             <h2>Estimate the scope of your work</h2><small>Consider the size of your project and the time it will take.</small>
                                        </div>
                                        
                                        <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">

                                        <fieldset>

                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('project_levels', $project_levels, null, array('class' => '', 'placeholder' => 'Job Scale', 'v-model'=>'project_level')) !!}
                                            </span>
                                        </div>
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('job_duration', $job_duration, null, array('class' => '', 'placeholder' => trans('lang.select_job_duration'), 'v-model'=>'job_duration')) !!}
                                            </span>
                                        </div>

                                      
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('service_type', $service_type, null, array('class' => '', 'placeholder' => 'Service type','v-model'=>'service_type')) !!}
                                            </span>
                                        </div>
                                        
                                        {{-- <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('freelancer_type', $freelancer_level, null, array('placeholder' => trans('lang.select_freelancer_level'), 'class' => '', 'v-model'=>'freelancer_level')) !!}
                                            </span>
                                        </div> --}}
                                        <!-- <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('english_level', $english_levels, null, array('class' => '', 'placeholder' => trans('lang.select_english_level'), 'v-model'=>'english_level')) !!}
                                            </span>
                                        </div> -->
                                        {{-- {{Auth::user()->roles[0]->name}} --}}

                                        @if (Auth::user()->org_type_id == 1)
                                        @else
                                        <div class="form-group form-group-half wt-formwithlabel job-cost-input">
                                            
                                            {!! Form::number('project_cost', null, array('class' => '', 'placeholder' => 'Job Cost')) !!}
                                            
                                        </div>
                                        @endif
                                        <job-expiry 
                                            :ph_expiry_date="'Job Expiry Date'"
                                            :weekdays="'{{json_encode($weekdays)}}'"
                                            :months="'{{json_encode($months)}}'">
                                        </job-expiry>
                                    </fieldset>
                                </div>
                            </div>


                            @if (Auth::user()->org_type_id == 1)
                            

                            <div class="wt-jobdescription wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Selection Procurement System </h2>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                    <fieldset>
                                       
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('selection_procurement_system', $selection_procurement_system, null, array('class' => '', 'id'=> 'selection_procurement_system','placeholder' => 'Selection Procurement System','v-model'=>'selection_procurement_system')) !!}
                                            </span>
                                        </div>

                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('single_source', $getSelectionProcurementSystemSingleSource, null, array('class' => '',  'disabled' => true,'id'=> 'single_source', 'placeholder' => 'Selection single source')) !!}
                                            </span>
                                        </div>

                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('project_budget', $project_budget, null, array('class' => '', 'placeholder' => 'Project budget','v-model'=>'project_budget')) !!}
                                            </span>
                                        </div>
                                        <div class="form-group form-group-half wt-formwithlabel job-cost-input">
                                            
                                            {!! Form::number('project_cost', null, array('class' => '', 'placeholder' => trans('lang.project_cost'))) !!}
                                            
                                        </div>
                                  
                                        
                                      
                                    </fieldset>
                                 
                                </div>
                            </div>

                            @endif


                            @if (Auth::user()->org_type_id == 1)
                            

                            <div class="wt-jobskills wt-jobskills-holder wt-tabsinfo single_source_box"  style="display: none">
                                <div class="wt-tabscontenttitle">
                                    <h2>Single Source Account</h2>
                                </div> 
                                <div>
                                    <div class="wt-formtheme wt-skillsform">
                                        <!----> <fieldset>

                                            <div class="form-group">

                                                <div class="form-group-holder">
                                                        <input type="text" name="single_source_email" id="add_single_source" class="form-control" placeholder="Single Source Email" >
                                                </div>
                                            </div> 
                                            {{-- <div class="form-group wt-btnarea"><a href="javascript:void(0);" id="check_single_email" class="wt-btn">Verify</a></div> --}}
                                        </fieldset>
                                        </div> <div class="wt-myskills"><ul id="verify_account" class="sortable list"> </ul> <div class="wt-description"><p id="freelancer_account_message"></p></div></div>
                                       


                                    </div>
                                    </div>


                            <div class="wt-jobdescription wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Bid Score Weightage</h2><small>Total Score Must Be Equal 100 Points</small>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                    <fieldset>
                                
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            {{-- <div class="wt-description"><p>Technical Score</p></div> --}}
                                            <div class="form-group">
                                                
                                                <input type="number" min="1" max="100" id="teachniqal_score" name="teachniqal_score" style="width:100%;"  placeholder="Technical Score" />
                                            </div>
                                        </div>
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            {{-- <div class="wt-description"><p>Financial Score</p></div> --}}
                                            <div class="form-group">
                                                <input type="number" min="1" max="100" id="financial_score" name="financial_score" style="width:100%;" placeholder="Financial Score" />
                                            </div>
                                        </div>
                                      
                                    </fieldset>


                                    
                                   
                                </div>
                            </div>


                            <div class="wt-jobdescription wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Pre-defined Bid Evaluation Criteria</h2><small>You can select form already define questions</small>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                    <fieldset>
                                
                                           <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            <select class="js-example-basic-multiple" name="multiplebidquestions[]" id="multiplebidquestions" multiple="multiple">
                                              @foreach ($BidCriteriaQuestion as $item => $value)
                                              <option value="{{$item}}">{{$value}}</option>
 
                                              @endforeach
                                              
                                              </select>
                                        </span>

                                        </div>
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            {{-- <div class="wt-description"><p>Financial Score</p></div> --}}
                                            <div class="form-group">
                                                <div class="form-group wt-btnarea"><a href="javascript:void(0);" id="add-Bid-Evaluation-Criteria" class="btn btn-primary">Add Bid Evaluation Criteria</a></div>
                                            </div>
                                        </div>
                                      
                                    </fieldset>


                                    
                                   
                                </div>
                            </div>

                            <div class="wt-jobdescription wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Bid Evaluation Criteria</h2>
                                </div>
                                {{-- <div style="display: none">
                                <select class="BidCriteriaQuestionPredefine" name="">
                                    @foreach ($BidCriteriaQuestion as $item => $value)
                                    <option value="{{$item}}">{{$value}}</option>
                                    @endforeach
                                    </select> </div> --}}

                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>

                                    {{-- <div class="form-group form-group-half wt-formwithlabel" style="border-bottom:1px solid #ccc ">
                                        <div class="wt-description">Financial Score</div>
                                        <span class="wt-select">
                                            <select class="js-example-basic-multiple" name="states[]" multiple="multiple">
                                              @foreach ($BidCriteriaQuestion as $item => $value)
                                              <option value="{{$item}}">{{$value}}</option>
 
                                              @endforeach
                                              
                                              </select>
                                        </span>

                                        </div> --}}

                                    <div class="fieldbidc" id="field">
                                       <div id="field0">

                                        {{-- <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">                                              
                                                {!! Form::select('BidCriteriaQuestion', $BidCriteriaQuestion, null, array('class' => 'BidCriteriaQuestion skill-dynamic-field', 'id' => 'locationProvices','placeholder' => 'Bid Criteria Question')) !!}
                                            </span>
                                            </div> --}}
                                        
                                        <div class="form-group-half wt-formwithlabel">
                                                <div class="form-group">
                                                  <input id="action_id" name="question_title[]" type="text" placeholder="Question" class="form-control">                                   
                                  </div>
                                </div>
                                <div class="form-group-half wt-formwithlabel">
                                    <div class="form-group">
                                        <input id="action_name" name="question_score[]" type="text" placeholder="Score" class="form-control">
                                    
                                  </div>
                                </div>    
                                <div class="form-group form-group-half wt-formwithlabel">
                                    <span class="wt-select">
                                        <select name="question_type[]">
                                            <option selected="selected" value="0">Technical</option>
                                            <option value="1">Financial</option>
                                   
                                        </select></span>
                                    </div>

                                </div>
                                </div>
                                <div class="form-group form-group-half wt-formwithlabel">
                                    <div class="col-md-12">
                                    <div class="form-group wt-btnarea"><a href="javascript:void(0);" id="add-more" class="btn btn-primary">Add More</a></div>
                                  </div>
                                </div>
                                              
                                    
                                
                                      
                                    </fieldset>


                                    
                                   
                                </div>
                            </div>



                            @endif
                            <div class="wt-jobdescription wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Select Your Job's Category</h2>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                    <fieldset>
                                        
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                            {!! Form::select('categories_parent', $categories, null, array('class' => '','id' => 'parent_category', 'placeholder' => trans('lang.select_job_cats'))) !!}
                                            </span>
                                        </div>
                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                            {!! Form::select('categories', $categories_child, null, array('class' => '','id' => 'child_category',  'disabled' => true, 'placeholder' => trans('lang.select_job_cats'))) !!}
                                            </span>
                                        </div>
                                        
                                   
                                        
                                    </fieldset>
                                </div>
                            </div>


                      
                                

                           

                            
                          
                            <div class="wt-jobdetails wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Give Your Job's Details</h2>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo">
                                    {!! Form::textarea('description', null, ['class' => 'wt-tinymceeditor', 'id' => 'wt-tinymceeditor', 'placeholder' => trans('lang.job_dtl_note')]) !!}
                                </div>
                            </div>
                            <div class="wt-jobskills wt-jobskills-holder wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Choose the Skills Required</h2>
                                </div>
                                <job_skills :placeholder="'skills already selected'"></job_skills>
                            </div>
                            <div class="wt-joblocation wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>{{ trans('lang.your_loc') }}</h2>
                                </div>
                                <!-- @if (!empty($district))
                                            <fieldset class="wt-registerformgroup">
                                             <div class="form-group">
                                                <div class="form-group">
                                                <p>Select your district:</p>
                                                </div>
                                                <div class="form-group">
                                                    <span class="wt-select">
                                                            <select name="distDropDown" id="distDropDown" class="form-control" v-bind:class = '{ "is-invalid": form_step2.is_distDropDowns_error}'>
                                                            <option value="">Select District</option>
                                                            <?php
                                                                foreach($district as $k=>$v):
                                                                    $district_name[$k] = $v['district_name'];
                                                                    $loc_id[$k] = $v['loc_id'];
                                                                    $id[$k] = $v['id'];
                                                                    ?>
                                                                    <option value="<?php echo $id[$k];?>"><?php echo $district_name[$k];?></option>
                                                                    <?php
                                                                endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block disctic_block" style="display:none">
                                                            <strong>The District field is required.</strong>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                            </fieldset>
                                        @endif -->
                                <div class="wt-formtheme wt-userform">
                                    <fieldset>
                                        <div class="form-group form-group-half">
                                            <span class="wt-select">
                                                {!! Form::select('locations', $locations, null, array('class' => 'skill-dynamic-field', 'id' => 'locationProvices','placeholder' => 'District')) !!}
                                            </span>
                                        </div>
                                        {{-- <div class="form-group form-group-half">
                                            <span class="wt-select">
                                            <select name="district" id="distDropDown"  >
                                                <option value="">District</option>
                                            </select>
                                            <span class="help-block" >
                                            </span>
                                        </span>
                                        </div> --}}
                                        <div class="form-group form-group-half">
                                            {!! Form::text( 'address', null, ['id'=>"pac-input", 'class' =>'form-control', 'placeholder' => trans('lang.your_address')] ) !!}
                                        </div>
                                       
                                    </fieldset>
                                </div>
                            </div>
                            <!-- <div class="wt-featuredholder wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>{{ trans('lang.is_featured') }}</h2>
                                    <div class="wt-rightarea">
                                        <div class="wt-on-off float-right">
                                            <switch_button v-model="is_featured">{{{ trans('lang.is_featured') }}}</switch_button>
                                            <input type="hidden" :value="is_featured" name="is_featured">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="wt-attachmentsholder">
                                <div class="lara-attachment-files">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.attachments') }}</h2>
                                        <div class="wt-rightarea">
                                            <div class="wt-on-off float-right">
                                                <switch_button v-model="show_attachments">{{{ trans('lang.attachments_note') }}}</switch_button>
                                                <input type="hidden" :value="show_attachments" name="show_attachments">
                                            </div>
                                        </div>
                                    </div>
                                    <job_attachments :temp_url="'{{url('job/upload-temp-image')}}'"></job_attachments>
                                    <div class="form-group input-preview">
                                        <ul class="wt-attachfile dropzone-previews">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wt-updatall">
                        <i class="ti-announcement"></i>
                        <span>{{{ trans('lang.save_changes_note') }}}</span>
                        {!! Form::submit(trans('lang.post_job'), ['class' => 'wt-btn', 'id'=>'submit-profile']) !!}
                    </div>
                {!! form::close(); !!}
            </div>
        </div>
    </div>
</div>



@section('scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">


$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});

        $("#parent_category").change(function(){

            $("#child_category").prop("disabled", false);  
            $.ajax({
                url: "{{ route('categories.get_by_parent') }}?parent_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#child_category').html(data.html);
                }
            });


            $.ajax({
                url: "{{ route('categories.skills_by_category') }}?parent_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#freelancer_skill').html(data.html);
                    $('#skill_list').html('');
                   
                }
            });


        });

        $("#selection_procurement_system").change(function(){
if(this.value == 5){
    $("#single_source").prop("disabled", false);  

}else{
    $("#single_source").prop("disabled", true);  
   
}
if(this.value == 1){
    $("#financial_score").val("40");
    $("#teachniqal_score").val("60");


}else if(this.value == 2){
    $("#financial_score").val("50");
    $("#teachniqal_score").val("50");
}else if(this.value == 3){
    $("#financial_score").val("60");
    $("#teachniqal_score").val("40");
}else if(this.value == 4){
    $("#financial_score").val("80");
    $("#teachniqal_score").val("20");
}else if(this.value == 5){
    $("#financial_score").val("0");
    $("#teachniqal_score").val("0");
}



 

});
       


$("#single_source").change(function(){

    if(this.value == 5){
    $(".single_source_box").hide();  

}else{
    $(".single_source_box").show();  
   
}

});

$(".BidCriteriaQuestion").change(function(){

    var bidquestion = $(this).val();
               alert('in');
//$("#child_category").prop("disabled", false);  

$.ajax({
    url: "{{ url('bidquestion') }}/" + $(this).val(),
    method: 'GET',
    success: function(data) {
        console.log(data);
        var v = $('.BidCriteriaQuestion').closest("div.fieldbidc").find("input[name='question_title[]']").val(data.question);
        var vt = $('.BidCriteriaQuestion').closest("div.fieldbidc").find("select[name='question_type[]']").val(data.question_type);

   
       // $('#child_category').html(data.html);
    }
});


// $.ajax({
//     url: "{{ route('categories.skills_by_category') }}?parent_id=" + $(this).val(),
//     method: 'GET',
//     success: function(data) {
//         $('#freelancer_skill').html(data.html);
//         $('#skill_list').html('');
       
//     }
// });


});

$( "#check_single_email" ).click(function() {
  valSingeleEmail = $("#add_single_source").val();


var formData = new FormData();
formData.append('single_source_email',valSingeleEmail);
$.ajax({
            type: "POST",
            url: "{{ url('check-single-source') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){

       

                console.log(data.message.first_name);
                if(data.type == "sucess"){

                    $('#freelancer_account_message').html("<br><div class='alert alert-success' role='alert'>User Account Found <strong>"+  data.message.first_name+' '+ data.message.last_name +"</strong></div>");
                }else{
                    $('#freelancer_account_message').html("<br><div class='alert alert-danger' role='alert'>"+ data.message +"</div>");
                }
            }
        });



      


});

$("#add-Bid-Evaluation-Criteria").click(function(e){ 
    e.preventDefault();
  valmultiplebidquestions = $("#multiplebidquestions").val();

  var locId = $(this).val();
    $.ajax({
        type: 'POST',
        url: '{{url('getbidquestionlist')}}',
        cache: false,
        dataType: 'html',
        data: {
            valmultiplebidquestions: valmultiplebidquestions,
            _token: '{{csrf_token()}}'
        },
        success: function (res) {
          // $("#distDropDown").html(res);
            //$(".preloader-outer").hide();
            console.log(res)
            var next = 0;
            var addto = "#field" + next;
            var newInput = $(res);
            $(addto).after(newInput);
        }
        });



});

var next = 0;
    $("#add-more").click(function(e){ 

        e.preventDefault();

       // var BidCriteriaQuestionPredefine =$('.BidCriteriaQuestionPredefine').html();

        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        // var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><div class="form-group form-group-half wt-formwithlabel"><span class="wt-select"><select class="BidCriteriaQuestion" name="question_type[]">'+BidCriteriaQuestionPredefine+'</select></span></div><div class="form-group-half wt-formwithlabel"><div class="form-group"><input id="action_id" name="question_title[]" type="text" placeholder="Question" class="form-control"> </div></div><div class="form-group-half wt-formwithlabel"> <div class="form-group"> <input id="action_name" name="question_score[]" type="number" placeholder="Score" class="form-control"></div> </div><div class="form-group form-group-half wt-formwithlabel">'+
        //  "<span class='wt-select'><select name='question_type[]'><option selected='selected' value='0'>Technical</option><option value='1'>Financial</option></select></span></div></div></div>";
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><div class="form-group-half wt-formwithlabel"><div class="form-group"><input id="action_id" name="question_title[]" type="text" placeholder="Question" class="form-control"> </div></div><div class="form-group-half wt-formwithlabel"> <div class="form-group"> <input id="action_name" name="question_score[]" type="number" placeholder="Score" class="form-control"></div> </div><div class="form-group form-group-half wt-formwithlabel">'+
         "<span class='wt-select'><select name='question_type[]'><option selected='selected' value='0'>Technical</option><option value='1'>Financial</option></select></span></div></div></div>";
        var newInput = $(newIn);
        var removeBtn = '<div style="clear: both;"><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >Remove</button></div></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });

 $("#locationProvice").change(function(){

    var locId = $(this).val();
    $.ajax({
        type: 'POST',
        url: '{{url('getAjaxDistrics')}}',
        cache: false,
        dataType: 'html',
        data: {
            locId: locId,
            _token: '{{csrf_token()}}'
        },
        success: function (res) {
            $("#distDropDown").html(res);
            //$(".preloader-outer").hide();
        }
        });
    });

    </script>
    @endsection
    @section('bootstrap_script')
    <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
   
@stop
@endsection

