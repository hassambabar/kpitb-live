@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="wt-dashboardbox">
                <div class="wt-dashboardboxtitle">
                    <h2>{{ trans('lang.cancelled_jobs') }}</h2>
                </div>
                <div class="wt-dashboardboxcontent wt-jobdetailsholder la-projectc-completed">
                    <div class="wt-freelancerholder">
                        @if(!empty($cancelled_jobs) && $cancelled_jobs->count() > 0)
                            <div class="wt-managejobcontent wt-verticalscrollbar mCustomScrollbar _mCS_1">
                                @foreach ($cancelled_jobs as $job)
                                    @php
                                        $accepted_proposal = \App\Job::find($job->id)->proposals()->where('status', 'cancelled')->first();
                                        $request_to_cancel = \App\Proposal::where('job_id', $job->id)->first();
                                        $request_cancel = $request_to_cancel->request_to_cancel;
                                        $verified_user = \App\User::select('user_verified')->where('id', $job->employer->id)->pluck('user_verified')->first();
                                        $project_type  = Helper::getProjectTypeList($job->project_type);
                                    @endphp
                                    <div class="wt-userlistinghold wt-featured wt-userlistingvtwo wt-cancelled">
                                        <!-- @if (!empty($job->is_featured) && $job->is_featured === 'true')
                                            <span class="wt-featuredtag"><img src="{{{ asset('images/featured.png') }}}" alt="{{ trans('lang.is_featured') }}" data-tipso="Plus Member" class="template-content tipso_style"></span>
                                        @endif -->
                                        <div class="wt-userlistingcontent wt-userlistingcontentvtwo">
                                            <div class="wt-contenthead">
                                                <div class="wt-title">
                                                    <a href="{{{ url('profile/'.$job->employer->slug) }}}">
                                                        @if ($verified_user === 1)
                                                            <i class="fa fa-check-circle"></i>
                                                        @endif
                                                        &nbsp;{{{ Helper::getUserName($job->employer->id) }}}
                                                    </a>
                                                    @if (!empty($job->title))
                                                        <h2>{{{ $job->title }}}</h2>
                                                    @endif
                                                </div>
                                                <ul class="wt-saveitem-breadcrumb wt-userlisting-breadcrumb">
                                                    @if (!empty($job->price))
                                                        <li><span class="wt-dashboraddoller"><i>{{ !empty($symbol) ? $symbol['symbol'] : 'PKR' }}</i> {{{ $english_format_number = number_format($job->price) }}}</span></li>
                                                    @endif
                                                    @if (!empty($job->location->title))
                                                        <li><span><img src="{{{asset(Helper::getLocationFlag($job->location->flag))}}}" alt="{{{ trans('lang.locations') }}}"> {{{ $job->location->title }}}</span></li>
                                                    @endif
                                                    @if (!empty($job->project_type))
                                                        <li><a href="javascript:void(0);" class="wt-clicksavefolder"><img class="wt-job-icon" src="{{asset('images/job-icons/job-type.png')}}"> {{{ trans('lang.type') }}} {{{ $project_type }}}</a></li>
                                                    @endif
                                                    @if (!empty($job->duration) && !is_array(Helper::getJobDurationList($job->duration)))
                                                        <li><span class="wt-dashboradclock"><img class="wt-job-icon" src="{{asset('images/job-icons/job-duration.png')}}"> {{ trans('lang.duration') }} {{{ Helper::getJobDurationList($job->duration)}}}</span></li>
                                                    @endif
                                                </ul>
                                            </div>
                                            @if($request_cancel === 0)
                                            <div class="wt-rightarea">
                                                <div class="wt-btnarea">
                                                    <span> {{ trans('Project Cancelled') }}</span>
                                                    <a href="{{{ url('proposal/'.$job->slug.'/'.$job->status) }}}" class="wt-btn">{{ trans('lang.view_detail') }}</a>
                                                    <a href="{{url('dispute/details/'.$job->slug)}}" class="wt-btn">View a dispute</a>
                                                </div>
                                            </div>
                                            @else
                                            <div class="wt-rightarea">
                                                <div class="wt-btnarea">
                                                    <span> {{ trans('Project Cancelled') }}</span>
                                                    <a href="{{{ url('proposal/'.$job->slug.'/'.$job->status) }}}" class="wt-btn">{{ trans('lang.view_detail') }}</a>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            @if (file_exists(resource_path('views/extend/errors/no-record.blade.php'))) 
                                @include('extend.errors.no-record')
                            @else 
                                @include('errors.no-record')
                            @endif
                        @endif
                    </div>
                </div>
                @if ( method_exists($cancelled_jobs,'links') )
                    {{ $cancelled_jobs->links('pagination.custom') }}
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
