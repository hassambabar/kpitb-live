@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
                @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div><br />
                          @endif

                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Job Dispute</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder milesonelist" id="printable_area">
                        <table class="wt-tablecategories" id="checked-val">
                            <thead>
                                <tr>
                                   
                                    <th style="width: 40%">Job Title</th>
                                    <th>Job Price</th>
                                    <th>Job Expiry Date</th>
                                    <th>Job Code</th>
                                </tr>
                            </thead>
                            <tbody>
                                <td  style="width: 40%">{{$job->title}}</td>
                                <td>{{$job->price}}</td>
                                <td>{{$job->expiry_date}}</td>
                                <td>{{$job->code}}</td>
                       </table>
                       <hr>
                        <ul>
                               
                                <li><span>Proposal: </span>{{$Proposal->content}}
                                </li>
                                <li><span>Proposal Amount: </span>PKR {{$Proposal->amount}}
                                </li>   
                                <li><span>Status: </span>{{$Proposal->status}}
                                </li>
                                <li><strong>FreeLancer Project Status:</strong> @if($Proposal->status_hired == 1) Accepted @else Rejected @endif</li>
                                @if(!empty($Report))
                                <li><span>Project Cancel Reason: </span>{{$Report->reason}}
                                </li>
                                <li><span>Reason Detail:</span> {!! $Report->description !!}
                                </li>
                                @endif
                                @if(!empty($Disputes))
                                <li><span>Dispute Count: {{ count($Disputes) }}</span></li>
                                @php
                                    $count = 1;
                                @endphp
                                @foreach($Disputes as $Dispute)
                                <hr>
                                <li>
                                    <span>Dispute {{ $count }}</span> 
                                </li>
                                <hr>
                                <li><span>Freelancer Reason:</span> {{$Dispute->reason}}
                                </li>
                                <li><span>Freelancer Response:</span> {!! $Dispute->description !!}
                                </li>
                                @if($Dispute->attachment)
                                <li><span>Freelancer Attachment:</span> <a href="{{ public_path('app/public/'.$Dispute->attachment) }}">View/Download</a>
                                </li>
                                @endif
                                @php
                                    $count++
                                @endphp
                                @endforeach
                                @endif
                                
                            </ul>
                         
                           

                            <div class="milstonePaids alert-danger">
                                <div class="wt-borderheading wt-borderheadingvtwo">
                                    <h3>Grievance Process</h3> 
                                </div>
                                <ul>
                                 <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum</p></li>
                                 </ul>
                              </div>


                    </div>
                </div>
            </div>
        </div>
        <style>
            .milstonePaid{
                background-color: #b8d5c1;
            }
            .milstonedispute{
                background-color: #b8d5c1;
            }
            .milesonelist ul li{
                list-style: none;
                padding: 10px 20px;
            }
            .milesonelist ul li span{
                font-weight: bold;
                padding-right: 10px;
            }
            </style>
    </section>
@endsection
