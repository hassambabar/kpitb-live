@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')

@section('content')

<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-left" >

            <div class="wt-haslayout wt-post-job-wrap">

                <div class="wt-dashboardbox">

                    
                    <div class="wt-dashboardboxtitle">
                        <h2>Meeting Minutes</h2>
                    </div>

                    <div class="wt-dashboardboxcontent">

                       


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Meeting Minutes Details</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group">
                                        {{$MinutesOfMeeting->title}}
                                    </div>

                                    <div class="form-group">
                                        <strong>Location:</strong> {{$MinutesOfMeeting->location}}  
                                    </div>
                                    <div class="form-group">
                                        <strong>Date:</strong> {{$MinutesOfMeeting->date}}    
                                    </div>

                                  

                                </fieldset>
                            </div>
                        </div>
                        @if(Auth::id() != $MinutesOfMeeting->user_id)
                        <div class="wt-jobdetails wt-tabsinfo acceptDiv">
                            <div class="wt-tabscontenttitle">
                                <h2>Accept / Reject</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo">
                               <form>
                                   <?php 
                                    if(!empty($MeetingAccept)){
                                        if($MeetingAccept->status == 0 || $MeetingAccept->status == 1){ 
                                            if($MeetingAccept->status == 0){
                                                $classDisableAccept = 'btn-seconday';
                                                $classDisableReject = '';

                                            }else{
                                                $classDisableAccept = '';
                                                $classDisableReject = 'btn-seconday';
                                            }
                                            ?>
                                            
                                            <input type="button" data-vale="Accept" class="btn btn-success btn-group-sm col-md-3 <?php if($MeetingAccept->status == 0){ echo "btn-dark"; } ?>" value="Accept" disabled>
                                            <input type="button" data-vale="Reject" class="btn btn-danger btn-group-sm col-md-3 <?php if($MeetingAccept->status == 1){ echo "btn-dark"; } ?>" value="Reject"disabled>
                                       <?php }
                                    }else{
                                    ?>
                                   <input type="button" data-vale="Accept" class="btnaccept btnActionMOM btn btn-success btn-group-sm col-md-3" value="Accept" >
                                   <input type="button" data-vale="Reject" class="btnreject btnActionMOM btn btn-danger btn-group-sm col-md-3" value="Reject">
                                   <?php } ?>
                               </form>

                               
                            </div>
                        </div>
                        @endif
                        @if($AcceptanceList->count() > 0)
                        <div class="wt-jobdetails wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Minutes Acceptance List</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo">
                                <table class="wt-tablecategories wt-freelancer-table">
                                    <thead>
                                        <tr>
                                            <th>Member Name</th>
                                             <th>Date</th>
                                              <th>Status</th>
                                            </tr>
                                        </thead>
                                         <tbody>
                                             @forelse ($AcceptanceList as $key => $value)
                                              
                                             <tr>
                                                <td>{{$value->user->first_name}} {{$value->user->last_name}} </td> 
                                                <td>{{$value->created_at}}</td>
                                                 <td>@if ($value->status == 0)
                                                    Rejected
                                                 @else
                                                 Accepted
                                                 @endif</td> 
                                                 
                                               </tr>

                                             @empty
                                                 
                                             @endforelse
                                             
                                            </tbody>
                                        </table>  
                            </div>
                        </div>
                        @endif


                            <div class="wt-jobdetails wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>Detail</h2>
                                </div>
                                <div class="wt-formtheme wt-userform wt-userformvtwo">
                                   {!!$MinutesOfMeeting->body!!}    
                                </div>
                            </div>

                            <div class="wt-attachmentsholder">
                                <div class="lara-attachment-files">
                                    <div class="wt-tabscontenttitle">
                                        <h2>{{ trans('lang.attachments') }}</h2>
                                      
                                    </div>
                                   
                                    <div class="form-group">
                                       
                                        <div class="col-sm-7">

                                            @if (!empty($attachments))
                                            @foreach ($attachments as $key => $attachment)
                                           <span id="attachment_id_{{$key}}">
                                            <a href="{{ url('uploads/jobs/' . $attachment) }}">{{$attachment}}</a><br>                                           
                                           </span>
                                           
                                            @endforeach
    
                                            @endif

                                            

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                                    
                                <br />
                                <div class="col-md-6 col-xl-12">
                                {{-- <h4>Display Comments</h4> --}}
                                @include('back-end.employer.jobs.commentsMinutesOfMeetingDisplay', ['comments' => $MinutesOfMeeting->comments, 'post_id' => $MinutesOfMeeting->id])
        
                                <form method="post" action="{{ route('commentsmeeting.store'   ) }}">
                                    @csrf
                                    <div class="form-group">
                                        <textarea class="form-control" name="body"></textarea>
                                        <input type="hidden" name="post_id" value="{{ $MinutesOfMeeting->id }}" />
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-success" value="Add Comment" />
                                    </div>
                                </form>
                            </div>
                        </div>

                             
                        
                   
                </div>

                
                </div>

            

            </div>

        </div>
    
    </div>
</div>
   
@section('bootstrap_script')



<script>


  
$('.btnActionMOM').click(function (event) {

    var acceptvalue = $(this).val();

    if (confirm('Are you sure you want to '+ acceptvalue + ' minutes of meeting?')) {
        $("input.btnaccept").attr("disabled", true);
        $("input.btnreject").attr("disabled", true);

        $.ajax({
            url: '{{url('accept-meeting')}}',
            type: "POST",
            dataType: "json",
            data: {
                acceptvalue: acceptvalue,
                meeting_minutes_id: {{ $MinutesOfMeeting->id }},
            _token: '{{csrf_token()}}'
            },
            success: function (data) {
                if(data.status == 'success'){
                    if(acceptvalue == 'Accept'){
                        $( ".btnreject" ).addClass( "btn-light" );
                    }else{
                        $( ".btnaccept" ).addClass( "btn-light" );

                    }
    }else if(data.status == 'error'){
        alert("Error on query!");
    }
            }
        });
    }
});

    </script>

<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

@stop
@endsection
