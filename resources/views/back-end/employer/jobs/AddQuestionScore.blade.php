@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')

@section('content')

<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 float-left" >

            <div class="wt-haslayout wt-post-job-wrap">

                <div class="wt-dashboardbox">

                    
                    <div class="wt-dashboardboxtitle">
                        <h2>Questions Score</h2>
                        <a href="{{url('comments-to-project',$job->id)}}" class="btn btn-primary" style="float: right">Back To Project</a>

                    </div>

                    <div class="wt-dashboardboxcontent">

                        <div class="col-sm-12">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif

                            @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session('success'))<div class="alert alert-success">
    {{session('success')}}</div>
@endif
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Proposal Details</h2>

                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group">
                                       {!!$proposal->content!!}
                                    </div>
                                    <br><br>
                                    <div class="wt-managejobcontent">
                                            @php

                                                $user = \App\User::find($proposal->freelancer_id);
                                                $profile = \App\User::find($proposal->freelancer_id)->profile;
                                                $user_image = !empty($profile) ? $profile->avater : '';
                                                $profile_image = !empty($user_image) ? '/uploads/users/'.$proposal->freelancer_id.'/'.$user_image : 'images/user-login.png';
                                                $user_name = $user->first_name.' '.$user->last_name;
                                                $feedbacks = \App\Review::select('feedback')->where('receiver_id', $proposal->freelancer_id)->count();
                                                $avg_rating = App\Review::where('receiver_id', $proposal->freelancer_id)->sum('avg_rating');
                                                $rating  = $avg_rating != 0 ? round($avg_rating/\App\Review::count()) : 0;
                                                $reviews = \App\Review::where('receiver_id', $proposal->freelancer_id)->get();
                                                $stars  = $reviews->sum('avg_rating') != 0 ? (($reviews->sum('avg_rating')/$feedbacks)/5)*100 : 0;
                                                $average_rating_count = !empty($feedbacks) ? $reviews->sum('avg_rating')/$feedbacks : 0;
                                                $completion_time = !empty($proposal->completion_time) ? \App\Helper::getJobDurationList($proposal->completion_time) : '';
                                                $attachments = !empty($proposal->attachments) ? unserialize($proposal->attachments) : '';
                                                $attachments_count = 0;
                                                $received_proposal_count = 0;
                                                if (!empty($attachments)){
                                                    $attachments_count = count($attachments);
                                                }
                                                $reviews = \App\Review::where('receiver_id', $user->id)->count();
                                                $badge = Helper::getUserBadge($user->id);
                                                if (!empty($enable_package) && $enable_package === 'true') {
                                                    $feature_class = !empty($badge) ? 'wt-featured' : '';
                                                    $badge_color = !empty($badge) ? $badge->color : '';
                                                    $badge_img  = !empty($badge) ? $badge->image : '';
                                                } else {
                                                    $feature_class = '';
                                                    $badge_color = '';
                                                    $badge_img    = '';
                                                }
                                            @endphp
                                            <div class="wt-userlistinghold wt-proposalitem {{ $feature_class }}">
                                                @if(!empty($enable_package) && $enable_package === 'true')        
                                                    @if (!empty($badge))
                                                        <span class="wt-featuredtag" style="border-top: 40px solid {{ $badge_color }};">
                                                            <img src="{{{ asset(Helper::getBadgeImage($badge_img)) }}}" alt="{{ trans('lang.is_featured') }}" data-tipso="Plus Member" class="template-content tipso_style">
                                                        </span>
                                                    @endif
                                                @endif    
                                                <figure class="wt-userlistingimg">
                                                    <img src="{{{ asset($profile_image) }}}" alt="{{ trans('lang.profile_img') }}" class="mCS_img_loaded">
                                                </figure>
                                                <div class="wt-proposaldetails">
                                                    @if (!empty($user_name))
                                                        <div class="wt-contenthead">
                                                            <div class="wt-title">
                                                                <a href="{{ url('profile/'.$user->slug) }}">{{{ $user_name }}}</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="wt-proposalfeedback">
                                                        <span class="wt-stars"><span style="width: {{ $stars }}%;"></span></span>
                                                        <span class="wt-starcontent">{{{ round($average_rating_count) }}}<sub>{{ trans('lang.5') }}</sub> <em>({{{ $feedbacks }}} {{ trans('lang.feedbacks') }})</em></span>
                                                    </div>
                                                </div>
                                                <div class="wt-rightarea">
                                                   
                                                    <div class="wt-hireduserstatus">
                                                        <h5>{{ !empty($symbol) ? $symbol['symbol'] : 'PKR' }}{{{$proposal->amount}}}</h5>
                                                        @if(!empty($completion_time))
                                                            <span>{{{ $completion_time }}}</span>
                                                        @endif
                                                    </div>

                                                  
                                                  
                                                    
                                                    <div class="wt-hireduserstatus">
                                                        <i class="fa fa-paperclip"></i>
                                                        @if (!empty($attachments))
                                                            {!! Form::open(['url' => url('proposal/download-attachments'), 'class' =>'post-job-form wt-haslayout', 'id' => 'download-attachments-form-'.$proposal->id]) !!}
                                                                @foreach ($attachments as $attachment)
                                                                    @if (Storage::disk('local')->exists('uploads/proposals/'.$proposal->freelancer_id.'/'.$attachment))
                                                                        {!! Form::hidden('attachments['.$received_proposal_count.']', $attachment, []) !!}
                                                                        @php $received_proposal_count++; @endphp
                                                                    @endif
                                                                @endforeach
                                                                {!! Form::hidden('freelancer_id', $proposal->freelancer_id, []) !!}
                                                                <button type="submit"><span>Download {{{ $received_proposal_count }}} {{ trans('lang.files_attached') }}</span></button>
                                                           
                                                            {!! form::close(); !!}
                                                            @else
                                                            <span>{{{ $attachments_count }}} {{ trans('lang.files_attached') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                  

                                </fieldset>
                            </div>
                        </div>
                     
@if ($BidScore > 0)

    <form action="{{ route('questionscore.update',$proposal->id) }}" method="post">
        @method('PATCH') 
        @csrf



    @else
{!! Form::open(['url' => route('questionscore.store'), 'class' =>'post-job-form wt-haslayout', '@submit.prevent'=>'submitJob']) !!}
@endif



                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Technical Questions</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    @foreach ($TechnicalQuestions as $TechnicalQuestion)
                                    <div class="form-group col-md-6">
                                        <label>{{$TechnicalQuestion->title}} - Max Score: {{$TechnicalQuestion->score}} </label>

                                        <input type="number" name="answer[{{$TechnicalQuestion->id}}]" value="{{$TechnicalQuestion->BidScore}}" max="{{$TechnicalQuestion->score}}" class="form-control" placeholder="Score">
                                    </div>
                                    @endforeach

                                </fieldset>
                            </div>
                        </div>

                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Financial Questions</h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                        @foreach ($FinancialQuestions as $FinancialQuestion)
                                        <div class="form-group col-md-6">
                                            <label>{{$FinancialQuestion->title}} - Max Score: {{$FinancialQuestion->score}} </label>

                                            <input type="number" name="answer[{{$FinancialQuestion->id}}]" value="{{$FinancialQuestion->BidScore}}" max='{{$FinancialQuestion->score}}' class="form-control" placeholder="Score">
                                        </div>
                                        @endforeach

                                  

                                </fieldset>
                            </div>
                        </div>
                </div>

                
                </div>

                <div class="wt-updatall">
                    <i class="ti-announcement"></i>
                    <span>Save all the latest changes made by you</span>
                    <input type="hidden" name="proposal_id" value="{{$proposal->id}}" >
                    @if($FinancialQuestion)
                    <input type="hidden" name="max_financial_score" value="{{$FinancialQuestion->score}}" >
                    @endif
                    @if($TechnicalQuestion)
                    <input type="hidden" name="max_technical_score" value="{{$TechnicalQuestion->score}}" >
                    @endif
                    <input type="hidden" name="proposal_id" value="{{$proposal->id}}" >
                    {!! Form::submit('ADD Score ', ['class' => 'wt-btn']) !!}
                </div>
                {!! form::close(); !!}

            </div>

        </div>
    
    </div>
</div>
   
@section('bootstrap_script')


{{-- 
<script>
    $( function() {
      $( "#meeting_date" ).datepicker({
        changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM yy',
      });
    } );
    </script> --}}

<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

@stop
@endsection

@section('scripts')
<script type="text/javascript">

$(".downloadAttachments").click(function(){

// var MemberID = $(this).val();
// var JobID = $('#JobID').val();

// if($(this).is(":checked")) {
//     var CheckStatus = 'checked';
//         }else{

//             var CheckStatus = 'unchecked';

//         }


// $.ajax({
//     type: 'POST',
//     url: '/committee-member-to-project',
//     cache: false,
//     dataType: 'html',
//     data: {
//         MemberID: MemberID,
//         CheckStatus:CheckStatus,
//         JobID:JobID,
//         _token: '{{csrf_token()}}'
//     },
//     success: function (res) {
//     //    $("#distDropDown").html(res);
//         //$(".preloader-outer").hide();
//     }
//     });
});

</script>
    @endsection
