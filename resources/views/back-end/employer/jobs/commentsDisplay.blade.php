@foreach($comments as $comment)
    <div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <div class="col-md-12">
        <strong>{{ $comment->user->first_name }}  {{ $comment->user->last_name }}</strong>
        <p>{{ $comment->body }} <br>  <strong> {{ $comment->created_at }}</strong></p>
        <a href="" id="reply"></a>
        <form method="post" action="{{ route('comments.store') }}">
            @csrf
           
            <div class="form-group">
                <input type="text" name="body" class="form-control" />
                <input type="hidden" name="job_id" value="{{ $comment->job_id }}" />
                <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Reply" />
            </div>
        </form>
        @include('back-end.employer.jobs.commentsDisplay', ['comments' => $comment->replies])
    </div>
</div>
@endforeach