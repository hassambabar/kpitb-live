@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')

@section('content')
<div class="wt-haslayout wt-dbsectionspace">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 float-left" id="post_job">
            @if (Session::has('payment_message'))
            @php $response = Session::get('payment_message') @endphp
            <div class="flash_msg">
                <flash_messages :message_class="'{{{$response['code']}}}'" :time='5'
                    :message="'{{{ $response['message'] }}}'" v-cloak></flash_messages>
            </div>
            @endif
            @if (session()->has('type'))
            @php session()->forget('type'); @endphp
            @endif
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="wt-haslayout wt-post-job-wrap">
                {!! Form::open(['url' => url('job/post-job'), 'class' =>'post-job-form wt-haslayout', 'id' =>
                'post_job_form', '@submit.prevent'=>'submitJob']) !!}
                <input type="hidden" id="_token" value="{{ csrf_token() }}">
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>{{ trans('lang.post_job') }} @if(Helper::getKpraInfo(Auth::user()->id) == '')
                            <small>
                                <div class="alert alert-danger" role="alert">
                                    Please add valid KPRA Number
                                </div>
                                @endif
                            </small>
                        </h2>
                    </div>
                    <div class="wt-dashboardboxcontent">
                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Write a headline for your job post <span class="requiredfield">*</span></h2>
                                <small>This helps your job post stand out to the right candidates. It’s the first thing
                                    they’ll see, so make it count!</small>
                            </div>

                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group">
                                        <input type="text" name="title" class="form-control" maxlength='50'
                                            placeholder="Job Title" v-model="title" required>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Estimate the scope of your work <span class="requiredfield">*</span></h2>
                                <small>Consider the size of your project and the time it will take.</small>
                            </div>

                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">

                                <fieldset>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('project_levels', $project_levels, null, array('class' =>
                                            'project_levels', 'placeholder' => 'Job Scale', 'required', 'v-model'=>'project_level'))
                                            !!}
                                        </span>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('job_duration', $job_duration, null, array('class' => '',
                                            'placeholder' => trans('lang.select_job_duration'), 'required',
                                            'v-model'=>'job_duration')) !!}
                                        </span>
                                    </div>


                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('service_type', $service_type, null, array('class' => '',
                                            'placeholder' => 'Service type','v-model'=>'service_type', 'required')) !!}
                                        </span>
                                    </div>



                                    {{-- <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('freelancer_type', $freelancer_level, null,
                                            array('placeholder' => trans('lang.select_freelancer_level'), 'class' => '',
                                            'v-model'=>'freelancer_level')) !!}
                                        </span>
                                    </div> --}}
                                    <!-- <div class="form-group form-group-half wt-formwithlabel">
                                            <span class="wt-select">
                                                {!! Form::select('english_level', $english_levels, null, array('class' => '', 'placeholder' => trans('lang.select_english_level'), 'v-model'=>'english_level')) !!}
                                            </span>
                                        </div> -->
                                    {{-- {{Auth::user()->roles[0]->name}} --}}

                                    @if(Auth::user()->government_department == 0)

                                    <div class="form-group form-group-half wt-formwithlabel job-cost-input">

                                        {!! Form::number('project_cost', null, array('class' => '', 'required', 'placeholder' =>
                                        'Job Cost')) !!}

                                    </div>
                                    @endif

                                    <div class="form-group form-group-half wt-formwithlabel">

                                        {!! Form::text( 'expiry_date', null, ['class' =>'datepicker
                                        form-control'.($errors->has('start_date') ? ' is-invalid' : ''),
                                         'placeholder' => 'Job Expiry Date', 'required'] ) !!}
                                        <input type="hidden" name="exp_range_error" />
                                    </div>

                                    {{-- <job-expiry :ph_expiry_date="'Job Expiry Date'"
                                        :weekdays="'{{json_encode($weekdays)}}'" :months="'{{json_encode($months)}}'"
                                    :min-date='new Date()'>
                                    </job-expiry> --}}

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            <select name="payment_system" id="payment_system" required>
                                                <option value="">Payment System</option>
                                                <option value="lumpsum">Lump sum Payment</option>
                                                <option value="milestone">Milestone Payment</option>

                                            </select>
                                        </span>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="wt-jobdescription wt-tabsinfo payment_system_box" style="display: none">
                            <div class="wt-tabscontenttitle">
                                <h2>Milestones</h2><small>Every Milestone Payment that is created will have its own
                                    invoice that can be viewed and downloaded from the project page</small>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div id="another-add-primary_contact1">

                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <div class="form-group">

                                                <input type="text" name="milestones_title[]" style="width:100%;"
                                                    placeholder="Title" />
                                            </div>
                                        </div>

                                        <div class="form-group form-group-half wt-formwithlabel">
                                            <div class="form-group">

                                                <input class="percent-mile" type="number" min="1" step="0.01" max="100"
                                                    name="milestones_percentage[]" style="width:15%" placeholder="%" />
                                                <p>Percentage</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary" id="add-primary_contact">+ Add
                                            more</button>
                                    </div>
                                </fieldset>




                            </div>
                        </div>


                        @if(Auth::user()->government_department == 1)


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Selection of Procurement System </h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('selection_procurement_system',
                                            $selection_procurement_system, null, array('class' => '', 'id'=>
                                            'selection_procurement_system','placeholder' => 'Select Procurement
                                            System','v-model'=>'selection_procurement_system')) !!}
                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('single_source',
                                            $getSelectionProcurementSystemSingleSource, null, array('class' => '',
                                            'disabled' => true,'id'=> 'single_source', 'placeholder' => 'Select single
                                            source')) !!}
                                        </span>
                                    </div>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {{-- {!! Form::select('project_budget', $project_budget, null, array('class'
                                            => 'project_budget', 'placeholder' => 'Project
                                            budget','v-model'=>'project_budget')) !!} --}}
                                            {!! Form::select('project_budget', $project_budget, null, array('class' =>
                                            'project_budget', 'placeholder' => 'Project budget')) !!}

                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel job-cost-input">

                                        {!! Form::number('project_cost', null, array('class' => '', 'placeholder' =>
                                        trans('lang.project_cost'), 'required')) !!}

                                    </div>



                                </fieldset>

                            </div>
                        </div>

                        <div class="wt-jobskills wt-jobskills-holder wt-tabsinfo single_source_box"
                            style="display: none">
                            <div class="wt-tabscontenttitle">
                                <h2>Single Source Account</h2>
                            </div>
                            <div>
                                <div class="wt-formtheme wt-skillsform">
                                    <fieldset>

                                        <div class="form-group">

                                            <div class="form-group-holder">
                                                <input type="text" name="single_source_email" id="add_single_source"
                                                    class="form-control" placeholder="Single Source Email">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="wt-myskills">
                                    <ul id="verify_account" class="sortable list"> </ul>
                                    <div class="wt-description">
                                        <p id="freelancer_account_message"></p>
                                    </div>
                                </div>



                            </div>
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Bid Score Weightage</h2><small>Total Score Must Be Equal to 100 Points</small>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="alert alert-danger score-bad" role="alert" style="display:none;">
                                        Make sure the Technical & Financial Scores equals to <b>100</b>!
                                    </div>
                                    <div class="alert alert-success score-good" role="alert" style="display:none;">
                                        Looks Good! Technical & Financial Scores equal <b>100</b>!
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <div class="wt-description">
                                            <p>Technical Score</p>
                                        </div>
                                        <div class="form-group">

                                            <input type="number" min="1" max="100" id="teachniqal_score"
                                                name="teachniqal_score" style="width:100%;"
                                                placeholder="Technical Score" />
                                        </div>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <div class="wt-description">
                                            <p>Financial Score</p>
                                        </div>
                                        <div class="form-group">
                                            <input type="number" min="1" max="100" id="financial_score"
                                                name="financial_score" style="width:100%;"
                                                placeholder="Financial Score" />
                                        </div>
                                    </div>

                                </fieldset>




                            </div>
                        </div>


                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Pre-defined Bid Evaluation Criteria</h2><small>You can select form already defined
                                    questions</small>
                            </div>


                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            <select class="js-example-basic-multiple" name="multiplebidquestions[]"
                                                id="multiplebidquestions" multiple="multiple">
                                                @foreach ($BidCriteriaQuestion as $item => $value)
                                                <option value="{{$item}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </span>

                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        {{-- <div class="wt-description">
                                            <p>Financial Score</p>
                                        </div> --}}
                                        <div class="form-group">
                                            <div class="form-group wt-btnarea"><a href="javascript:void(0);"
                                                    id="add-Bid-Evaluation-Criteria" class="btn btn-primary">Add Bid
                                                    Evaluation Criteria</a> <a href="javascript:void(0);" id="add-more"
                                                    class="btn btn-primary">Add More</a></div>

                                            {{-- <div class="form-group wt-btnarea"><a href="javascript:void(0);"
                                                    id="add-more" class="btn btn-primary">Add More</a></div> --}}

                                            {{-- <div class="form-group form-group-half wt-formwithlabel">
                                                <div class="col-md-12">
                                                    <div class="form-group wt-btnarea"><a href="javascript:void(0);"
                                                            id="add-more" class="btn btn-primary">Add More</a></div>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>

                                </fieldset>




                            </div>
                        </div>

                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Bid Evaluation Criteria <span class="requiredfield">*</span></h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>
                                    <div class="alert alert-warning score-bad2" role="alert">
                                        Make sure the total Technical Scores equals <b><span class="ts"></span></b> and/or Total Financial Scores equals <b><span class="fs"></span></b>, the values defined above!
                                    </div>
                                    <div class="fieldbidc" id="field">
                                        <div id="field0">
                                            <div class="form-group col-md-6  wt-formwithlabel">
                                                <input id="action_id" name="question_title[]" type="text"
                                                    placeholder="Question" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6 wt-formwithlabel row">
                                                <span class="col-md-4">
                                                    <input id="action_name" name="question_score[]" class="form-control qs"
                                                        type="number" placeholder="weight">
                                                </span>
                                                <span class="col-md-4">
                                                    <select name="question_type[]">
                                                        <option selected="selected" value="0">Technical</option>
                                                        <option value="1">Financial</option>
                                                    </select></span>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        @endif
                        <div class="wt-jobdescription wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Select Your Job's Category <span class="requiredfield">*</span></h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo la-job-details-form">
                                <fieldset>

                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            {!! Form::select('categories_parent', $categories, null, array('class' =>
                                            '','id' => 'parent_category', 'placeholder' => trans('Select Job
                                            Category'), 'required')) !!}
                                        </span>
                                    </div>
                                    <div class="form-group form-group-half wt-formwithlabel">
                                        <span class="wt-select">
                                            <!-- <p></p> -->
                                            {!! Form::select('categories', $categories_child, null, array('class' =>
                                            '','id' => 'child_category', 'disabled' => true, 'placeholder' =>
                                            trans('Select Sub Category'), 'required')) !!}
                                        </span>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="wt-jobdetails wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Give Your Job's Details <span class="requiredfield">*</span></h2>
                            </div>
                            <div class="wt-formtheme wt-userform wt-userformvtwo">
                                {!! Form::textarea('description', null, ['class' => 'wt-tinymceeditor', 'id' =>
                                'wt-tinymceeditor',  'placeholder' => trans('lang.job_dtl_note')]) !!}
                            </div>
                        </div>
                        <div class="wt-jobskills wt-jobskills-holder wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>Choose the Skills Required <span class="requiredfield">*</span></h2>
                            </div>
                            <job_skills :placeholder="'skills already selected'" required></job_skills>
                        </div>
                        <div class="wt-joblocation wt-tabsinfo">
                            <div class="wt-tabscontenttitle">
                                <h2>{{ trans('lang.your_loc') }} <span class="requiredfield">*</span></h2>
                            </div>
                            <!-- @if (!empty($district))
                                            <fieldset class="wt-registerformgroup">
                                             <div class="form-group">
                                                <div class="form-group">
                                                <p>Select your district:</p>
                                                </div>
                                                <div class="form-group">
                                                    <span class="wt-select">
                                                            <select name="distDropDown" id="distDropDown" class="form-control" v-bind:class = '{ "is-invalid": form_step2.is_distDropDowns_error}'>
                                                            <option value="">Select District</option>
                                                            <?php
                                                                foreach($district as $k=>$v):
                                                                    $district_name[$k] = $v['district_name'];
                                                                    $loc_id[$k] = $v['loc_id'];
                                                                    $id[$k] = $v['id'];
                                                                    ?>
                                                                    <option value="<?php echo $id[$k];?>"><?php echo $district_name[$k];?></option>
                                                                    <?php
                                                                endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block disctic_block" style="display:none">
                                                            <strong>The District field is required.</strong>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                            </fieldset>
                                        @endif -->
                            <div class="wt-formtheme wt-userform">
                                <fieldset>
                                    <div class="form-group form-group-half">
                                        <span class="wt-select">
                                            {!! Form::select('locations', $locations, null, array('class' =>
                                            'skill-dynamic-field', 'id' => 'locationProvices','required', 'placeholder' =>
                                            'District')) !!}
                                        </span>
                                    </div>
                                    {{-- <div class="form-group form-group-half">
                                        <span class="wt-select">
                                            <select name="district" id="distDropDown">
                                                <option value="">District</option>
                                            </select>
                                            <span class="help-block">
                                            </span>
                                        </span>
                                    </div> --}}
                                    {{-- <div class="form-group form-group-half">
                                        {!! Form::text( 'address', null, ['id'=>"pac-input", 'class' =>'form-control',
                                        'placeholder' => trans('lang.your_address')] ) !!}
                                    </div> --}}

                                </fieldset>
                            </div>
                        </div>
                        <!-- <div class="wt-featuredholder wt-tabsinfo">
                                <div class="wt-tabscontenttitle">
                                    <h2>{{ trans('lang.is_featured') }}</h2>
                                    <div class="wt-rightarea">
                                        <div class="wt-on-off float-right">
                                            <switch_button v-model="is_featured">{{{ trans('lang.is_featured') }}}</switch_button>
                                            <input type="hidden" :value="is_featured" name="is_featured">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        <div class="wt-attachmentsholder">
                            <div class="lara-attachment-files">
                                <div class="wt-tabscontenttitle">
                                    <h2>{{ trans('lang.attachments') }}</h2>
                                    <div class="wt-rightarea">
                                        <div class="wt-on-off float-right">
                                            <switch_button v-model="show_attachments">{{{ trans('lang.attachments_note')
                                                }}}</switch_button>
                                            <input type="hidden" :value="show_attachments" name="show_attachments">
                                        </div>
                                    </div>
                                </div>
                                <job_attachments :temp_url="'{{url('job/upload-temp-image')}}'"></job_attachments>
                                <div class="form-group input-preview">
                                    <ul class="wt-attachfile dropzone-previews">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(Helper::getKpraInfo(Auth::user()->id) != '')
                <div class="wt-updatall">
                    <i class="ti-announcement"></i>
                    <span>{{{ trans('lang.save_changes_note') }}}</span>
                    {!! Form::submit(trans('lang.post_job'), ['class' => 'wt-btn', 'id'=>'submit-profile']) !!}
                </div>
                @else

                @endif


                {!! form::close(); !!}
            </div>
        </div>
    </div>
</div>

<style>
.wt-description{
    padding-bottom: unset;
}
</style>

@section('scripts')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $('.percent-mile').keyup(function() {
    var totalper = 0;
    var values = $("input[name='milestones_percentage[]']")
              .map(function(){
                totalper += Number($(this).val());
                //   return $(this).val();
                }).get();

            //   if(totalper >= 100){
            //       alert('Percentage can not exceed 100% ');
            //       exit(0);
            //   }
});


$(document).ready(function() {
    $('#parent_category').select2({
        placeholder: "Select Your Job's Category"
    }).on('change', function(e) {
        var data = $("#parent_category option:selected").text();
  });
  $('#child_category').select2({
        placeholder: "Please Select Sub Category"
    }).on('change', function(e) {
        var data = $("#child_category option:selected").text();
  });
   $('#locationProvices').select2({
        placeholder: "Your Location"
    }).on('change', function(e) {
        var data = $("#locationProvices option:selected").text();
  });
  $('#freelancer_skill').select2({
        placeholder: "Your Location"
    }).on('change', function(e) {
        var data = $("#freelancer_skill option:selected").text();
  });
$('.js-example-basic-multiple').select2();

    $('#teachniqal_score').blur(function(e) {
        if ((parseInt($(this).val()) + parseInt($('#financial_score').val())) !== 100) {
            $('.score-bad').show(500);
            $('.score-good').hide(500);
            $('#submit-profile').prop('disabled', true);
            $('#submit-profile').css('opacity', "0.5");
        } else {
            $('.score-bad').hide(500);
            $('.score-good').show(500);
            $('#submit-profile').prop('disabled', false);
            $('#submit-profile').css('opacity', "1");
            $('.ts').html("'"+parseInt($(this).val())+"'");
            $('.fs').html("'"+parseInt($('#financial_score').val())+"'");
        }
    });

    $('#financial_score').blur(function(e) {
        if ((parseInt($(this).val()) + parseInt($('#teachniqal_score').val())) !== 100) {
            $('.score-bad').show(500);
            $('.score-good').hide(500);
            $('#submit-profile').prop('disabled', true);
            $('#submit-profile').css('opacity', "0.5");
        } else {
            $('.score-bad').hide(500);
            $('.score-good').show(500);
            $('#submit-profile').prop('disabled', false);
            $('#submit-profile').css('opacity', "1");
            $('.fs').html("'"+parseInt($(this).val())+"'");
            $('.ts').html("'"+parseInt($('#teachniqal_score').val())+"'");
        }
    });

});

        $("#parent_category").change(function(){

            $("#child_category").prop("disabled", false);
            $.ajax({
                url: "{{ route('categories.get_by_parent') }}?parent_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#child_category').html(data.html);
                }
            });


            $.ajax({
                url: "{{ route('categories.skills_by_category') }}?parent_id=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#freelancer_skill').html(data.html);
                    $('#skill_list').html('');

                }
            });


        });


        $("#selection_procurement_system").change(function(){
                    if (this.value != 5) {
                        $(".single_source_box").hide();
                        $("#single_source").val('');
                    } else {
                        $(".single_source_box").show();
                    }
                    if(this.value == 5){
                        $("#single_source").prop("disabled", false);

                    }else{
                        $("#single_source").prop("disabled", true);

                    }
                    if(this.value == 1){
                        $("#financial_score").val("40");
                        $("#teachniqal_score").val("60");


                    }else if(this.value == 2){
                        $("#financial_score").val("50");
                        $("#teachniqal_score").val("50");
                    }else if(this.value == 3){
                        $("#financial_score").val("60");
                        $("#teachniqal_score").val("40");
                    }else if(this.value == 4){
                        $("#financial_score").val("80");
                        $("#teachniqal_score").val("20");
                    }else if(this.value == 5){
                        $("#financial_score").val("0");
                        $("#teachniqal_score").val("0");
                    }
        });

    $('.project_levels').on('change', function(e) {
        $('.project_budget option:selected').removeAttr('selected');
                if(this.value == 'basic'){;
                // alert('d');
                $('.project_budget option:selected').removeAttr('selected');
                $(".project_budget option[value=0]").attr('selected', 'selected');
                }else if(this.value == 'medium'){
                    $('.project_budget option:selected').removeAttr('selected');
                    $(".project_budget option[value=200-500]").attr('selected', 'selected');
                }else if(this.value == 'expensive'){
                    $('.project_budget option:selected').removeAttr('selected');
                    $(".project_budget option[value=500]").attr('selected', 'selected');
                }
});
$("#payment_system").change(function(){
    if(this.value == 'milestone'){
        $(".payment_system_box").show();

    }else{
        $(".payment_system_box").hide();
    }
});
$("#add-primary_contact").click(function(){
    var totalper = 0;
    var values = $("input[name='milestones_percentage[]']")
            .map(function(){
            totalper += Number($(this).val());
            //   return $(this).val();
            }).get();
        // get the last DIV which ID starts with ^= "another-participant"
        var $div = $('div[id^="another-add-primary_contact"]:last');

        // Read the Number from that DIV's ID (i.e: 1 from "another-participant1")
        // And increment that number by 1
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;

        // Clone it and assign the new ID (i.e: from num 4 to ID "another-participant4")
        var $klon = $div.clone().prop('id', 'another-add-primary_contact'+num );

        // for each of the inputs inside the dive, clear it's value and
        // increment the number in the 'name' attribute by 1
        $klon.find('input').each(function() {
            this.value= "";
            let name_number = this.name.match(/\d+/);
            name_number++;
            this.name = this.name.replace(/\[[0-9]\]+/, '['+name_number+']')
        });
        // Finally insert $klon after the last div
        $div.after( $klon );


        $('.percent-mile').keyup(function() {
            var totalper = 0;
var values = $("input[name='milestones_percentage[]']")
            .map(function(){
            totalper += Number($(this).val());
            //   return $(this).val();
            }).get();
});
                              });

$("#single_source").change(function(){
    if (this.value == 5) {
        $(".single_source_box").hide();
    }else{
        $(".single_source_box").show();
    }
});

$(".BidCriteriaQuestion").change(function(){

    var bidquestion = $(this).val();

$.ajax({
    url: "{{ url('bidquestion') }}/" + $(this).val(),
    method: 'GET',
    success: function(data) {
      //  console.log(data);
        var v = $('.BidCriteriaQuestion').closest("div.fieldbidc").find("input[name='question_title[]']").val(data.question);
        var vt = $('.BidCriteriaQuestion').closest("div.fieldbidc").find("select[name='question_type[]']").val(data.question_type);


       // $('#child_category').html(data.html);
    }
});


// $.ajax({
//     url: "{{ route('categories.skills_by_category') }}?parent_id=" + $(this).val(),
//     method: 'GET',
//     success: function(data) {
//         $('#freelancer_skill').html(data.html);
//         $('#skill_list').html('');

//     }
// });


});

$( "#check_single_email" ).click(function() {
  valSingeleEmail = $("#add_single_source").val();


var formData = new FormData();
formData.append('single_source_email',valSingeleEmail);
$.ajax({
            type: "POST",
            url: "{{ url('check-single-source') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){



                console.log(data.message.first_name);
                if(data.type == "sucess"){

                    $('#freelancer_account_message').html("<br><div class='alert alert-success' role='alert'>User Account Found <strong>"+  data.message.first_name+' '+ data.message.last_name +"</strong></div>");
                }else{
                    $('#freelancer_account_message').html("<br><div class='alert alert-danger' role='alert'>"+ data.message +"</div>");
                }
            }
        });






});

$("#add-Bid-Evaluation-Criteria").click(function(e){
    e.preventDefault();
  valmultiplebidquestions = $("#multiplebidquestions").val();

  var locId = $(this).val();
    $.ajax({
        type: 'POST',
        url: '{{url('getbidquestionlist')}}',
        cache: false,
        dataType: 'html',
        data: {
            valmultiplebidquestions: valmultiplebidquestions,
            _token: '{{csrf_token()}}'
        },
        success: function (res) {
          // $("#distDropDown").html(res);
            //$(".preloader-outer").hide();
           // console.log(res)
            var next = 0;
            var addto = "#field" + next;
            var newInput = $(res);
            $(addto).after(newInput);
        }
        });



});

var next = 0;
    $("#add-more").click(function(e){

        e.preventDefault();

       // var BidCriteriaQuestionPredefine =$('.BidCriteriaQuestionPredefine').html();

        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;

        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"><div class="form-group col-md-6  wt-formwithlabel"><input id="action_id" name="question_title[]" type="text" placeholder="Question" class="form-control"></div><div class="form-group col-md-6 wt-formwithlabel row"> <span class="col-md-4"> <input id="action_name" name="question_score[]" type="number" placeholder="Score" class="form-control qs"></span> <span class="col-md-4">'+
         '<select name="question_type[]"><option selected="selected" value="0">Technical</option><option value="1">Financial</option></select></span><span class="col-md-4"><button data-id='+next+' remove'+(next - 1)+'" class="btn btn-danger remove-me" >Remove</button></span</div></div>';
        var newInput = $(newIn);
        var removeBtn = '<div style="clear: both;"><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >Remove</button></div></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
       // $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);

            $('.remove-me').click(function(e){

                e.preventDefault();

                var fieldNum = $(this).data("id");
               // var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                //$(this).remove();
              //  alert(fieldNum);
                $(fieldID).remove();
            });
    });

    $(document).on("click", '.remove-me-code', function(e) {
                e.preventDefault();
//                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldNum = $(this).data("id");

               // alert(fieldNum);
                var fieldID = "#fieldcode" + fieldNum;
                $(this).remove();
                $(fieldID).remove();});

             //   $( function() {
//     $( ".datepicker" ).datepicker();
//   } );

  var dateToday = new Date();
$(function() {
    $( ".datepicker" ).datepicker({
        minDate: dateToday,
        dateFormat: 'yy-mm-dd'
    });
});


 $("#locationProvice").change(function(){

    var locId = $(this).val();
    $.ajax({
        type: 'POST',
        url: '{{url('getAjaxDistrics')}}',
        cache: false,
        dataType: 'html',
        data: {
            locId: locId,
            _token: '{{csrf_token()}}'
        },
        success: function (res) {
            $("#distDropDown").html(res);
            //$(".preloader-outer").hide();
        }
        });
    });

</script>
@endsection
@section('bootstrap_script')
<script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>

@stop
@endsection
