@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
                @if ($errors->any())
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div><br />
                          @endif

                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Complete Milestone</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder milesonelist" id="printable_area">
                        {!! Form::open(['url' => url('user/invoic-submit/process'), 'files'=>'true']) !!}
                        @csrf

                        <ul>
                                <li><span>Project: </span>{{$job->title}}
                                </li>
                                <li><span>Milestone: </span>{{$Milestone->title}}
                                </li>
                                <li><span>Milestone Amount: </span>{{$Milestone->amount}}
                                </li>   
                                <li><span>Tax Deduction ({{ $job_tax }}%): </span>{{$job_tax_amount}}
                                </li>
                                <li><span>Total Milestone Payment: </span>{{$newAmount}}
                                </li> 
                                <li><span>Milestone Start Date: </span>{{$Milestone->start_date}}
                                </li>
                                <li><span>Milestone End Date: </span>{{$Milestone->end_date}}
                                </li>
                                <li><span>Description: </span>{{$Milestone->body}}
                                </li>
                                
                                @if(empty($Milestone->invoice_id))

                                <li>
                                        <textarea id="wt-tinymceeditor" name="body" cols="50" rows="10" class="wt-tinymceeditor"></textarea>
                                        <button type="submit" value="Complete Milestone">
                                </li>
                                <li>
                                    Attachment: <input type="file" id="attachments" name="attachments[]" multiple>

                                </li>

                                <li>  
                                    <input type="hidden" name="job_id" value="{{$job->id}}" />
                                    <input type="hidden" name="milestone_id" value="{{$Milestone->id}}" />
                                    <input type="hidden" name="amount" value="{{$newAmount}}" />
                                    <div class="form-group wt-btnarea">
                                    
                                    <input type="submit" value="Complete Milestone" class="wt-btn">
                                   
                                </div>
                                </li>
                                @endif
                            </ul>
                            {!! Form::close(); !!}
                            @if(!empty($Milestone->invoice_id))

                            <div class="milstonePaid">
                                <div class="wt-borderheading wt-borderheadingvtwo">
                                    <h3>Milestone Complete</h3>
                                    {{-- <a class="print-window" href="javascript:void(0);" @click="print()">
                                        <i class="fa fa-print"></i>
                                        {{{trans('lang.print')}}}
                                    </a> --}}
                                </div>
                            <ul>
                                <li>Paid Amount: {{$Milestone->milestonInvoice->title}}
                                </li>
                                <li>Description: {!! $Milestone->milestonInvoice->detail !!}
                                </li>
                                    <li><strong>Freelancer Status:</strong> @if($Milestone->reciver_status == 1) Accepted @else Pending @endif</li>
                                </li>
                                <li>Attachment: 
                                    @if(!empty($Milestone->milestonInvoice->attachments))
                                    <?php 
                                    
                                    $test = unserialize($Milestone->milestonInvoice->attachments);
                                    //echo "<pre>"; print_r( $test);

?>
                                    @forelse ($test as $key => $value)
                                    <br> <a href="{{url("/uploads/jobs/$value")}}" >View Attachment </a>
                                    @empty
                                        
                                    @endforelse

                                    @endif
                                </li>
                                <li>Date: {{$Milestone->milestonInvoice->created_at}}
                                </li>
                            </ul>
                            </div>
                            @endif
                            @if(!empty($Milestone->milestonDispute))
                            <div class="milstonePaids alert-warning">
                              <div class="wt-borderheading wt-borderheadingvtwo">
                                  <h3>Dispute</h3> 
                                 
                                
                              </div>
                              
                              {!! Form::open(['url' => url('employer/milestone-dispute-update'), 'files'=>'true']) !!}

                              <ul>
                                  <li><strong>Reason:</strong> {{$Milestone->milestonDispute->freelancer_reason}}  </li>
                                  <li><strong>Freelance Detial:</strong> {{$Milestone->milestonDispute->freelancer_description}}</li>
                                  @if(!empty($Milestone->milestonDispute->pe_description))
                                  <li><strong>Your Response:</strong> {!! strip_tags($Milestone->milestonDispute->pe_description) !!}</li>
                                  <li><strong>Your Status:</strong> @if($Milestone->milestonDispute->pe_status == 1) Accepted @else Rejected @endif</li>
                                  @endif
                                @if(empty($Milestone->milestonDispute->pe_description))
                                  <li>Accept / Reject: <select name="disputeOption">
                                      <option value="">-- Please Select --</option>
                                      <option value="1">-- Accept --</option>
                                      <option value="0">-- Reject --</option>
                                </select></li>
                                  <li><textarea id="wt-tinymceeditor" name="reason" cols="50" rows="10" class="wt-tinymceeditor">{{old('reason')}}</textarea>
                                <input type="hidden" value="{{$Milestone->milestonDispute->id}}" name="dispute_id" />
                                </li>
                                  <li> <div class="form-group wt-btnarea">
                                    
                                    <input type="submit" value="Dispute Response" class="wt-btn">
                                   
                                </div>
                            </li>
                            @endif
                              </ul>

                              {!! Form::close(); !!}

                            </div>

                            <div class="milstonePaids alert-danger">
                                <div class="wt-borderheading wt-borderheadingvtwo">
                                    <h3>Grievance Process</h3> 
                                </div>
                                <ul>
                                 <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum</p></li>
                                 </ul>
                              </div>

                            @endif

                    </div>
                </div>
            </div>
        </div>
        <style>
            .milstonePaid{
                background-color: #b8d5c1;
            }
            .milstonedispute{
                background-color: #b8d5c1;
            }
            .milesonelist ul li{
                list-style: none;
                padding: 10px 20px;
            }
            .milesonelist ul li span{
                font-weight: bold;
                padding-right: 10px;
            }
            </style>
    </section>
@endsection
