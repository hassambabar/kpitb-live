@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class=" col-12 col-xl-8" id="packages">
                <div class="preloader-section" v-if="loading" v-cloak>
                    <div class="preloader-holder">
                        <div class="loader"></div>
                    </div>
                </div>
                <div class="wt-dashboardbox">
                    
                    @if (Session::has('message'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('message') @endphp
                    @elseif (Session::has('error'))
                        <div class="flash_msg">
                            <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ str_replace("'s", " ", Session::get('error')) }}}'" v-cloak></flash_messages>
                        </div>
                        @php session()->forget('error'); @endphp
                    @endif
                    <div class="sj-checkoutjournal">
                        <div class="wt-dashboardboxtitle">
                            <h2>{{{trans('lang.order')}}}</h2>
                        </div>
                        @php
                            session()->put(['product_id' => e($product_id)]);
                            session()->put(['product_title' => e($title)]);
                            // session()->put(['product_price' => e($cost)]);
                            session()->put(['product_price' => e($newTotalCost)]);
                            session()->put(['price_tax' => e($job_tax_amount)]);
                            session()->put(['type' => $type]);
                            session()->put(['order' => $order]);
                        @endphp
                        <div class="wt-dashboardboxcontent wt-oderholder">
                            <table class="sj-checkouttable wt-ordertable">
                                <thead>
                                    <tr>
                                        <th>{{ trans('lang.item_title') }}</th>
                                    <th>{{trans('lang.details')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="sj-producttitle">
                                                <div class="sj-checkpaydetails">
                                                    <h4>{{{$title}}}</h4>
                                                    @if (!empty($subtitle))
                                                        <span>{{{$subtitle}}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : 'PKR' }} {{{$english_format_number = number_format($cost)}}}</td>
                                    </tr>
                                    @if (!empty($options))
                                        <tr>
                                            <td>{{ trans('lang.duration') }}</td>
                                            <td>{{{Helper::getPackageDurationList($options['duration'])}}}</td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td>{{ trans('lang.tax_deduction')}} ({{ $job_tax }}%)</td>
                                        <td>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : 'PKR' }} {{{$english_format_number = number_format($job_tax_amount)}}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('lang.total') }}</td>
                                        <td>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : 'PKR' }} {{{$english_format_number = number_format($newTotalCost)}}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                       
                                        <td> @if(!empty($Invoice)) @if($Invoice->paid == 0) Pending @else Complete @endif @else Pending @endif</td>
                                       
                                    </tr>
                                   
                                </tbody>
                            </table>
                            @if(!empty($Invoice))
                            <div style="margin-top: 10px">
                                <strong>Detail</strong>
                                <p>{!!$Invoice->detail!!}</p>
                            </div>
                            @endif
                            @if(empty($Invoice))

                            <div class="wt-tabscontenttitle">
                                <h2>{{{trans('lang.submit_trans')}}}</h2>
                            </div>
                            <div class="wt-transection-holder">

                                {{-- {!! Form::open(['url' => '', 'class' =>'wt-formtheme wt-userform sj-checkouttable', '@submit.prevent' => 'submitTransection("'.$product_id.'")', 'id' => 'trans_form' ])!!} --}}
                                <form method="POST" action="{{url('user/job-post/transection')}}" class="wt-formtheme wt-userform sj-checkouttable">
                                    @csrf
                                    <input type="hidden" value="{{$product_id}}" name="proposal_id">
                                    <input type="hidden" value="{{$product_id}}" name="product_id">
                                    <input type="hidden" value="{{$title}}" name="product_title">

                                    
                                    <input type="hidden" value="{{$order}}" name="order_id">
                                    <input type="hidden" value="{{$newTotalCost}}" name="product_price">
                                    <input type="hidden" value="{{$job_tax_amount}}" name="price_tax">
                                    <input type="hidden" value="{{$type}}" name="type">   
                                <fieldset>
                                        <div class="form-group">
                                            <textarea name="trans_detail" id="transection_detail" class="form-control" placeholder="{{{ trans('lang.trans_detail') }}}"></textarea>
                                        </div>
                                        <div class="wt-attachmentsholder">
                                            <job_attachments :max_file="1" :temp_url="'{{url('user/upload-temp-image/file')}}'"></job_attachments>
                                            <div class="form-group input-preview">
                                                <ul class="wt-attachfile dropzone-previews">

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-half wt-btnarea">
                                            {!! Form::submit(trans('lang.btn_submit'), ['class' => 'wt-btn']) !!}
                                        </div>
                                    </fieldset>
                                {!! Form::close() !!}
                              
                            </div>
                            @endif
                        </div>
                    </div>

                    
        
                    @if(!empty($JobDispute))

                    <div class=" col-12 col-xl-12 milesonelist" >
                       
                        <div class="wt-dashboardbox ">
                    <div class="milstonePaids alert-warning">
                      <div class="wt-borderheading wt-borderheadingvtwo">
                          <h3>Dispute</h3> 
                         
                        
                      </div>
                      
                      {!! Form::open(['url' => url('employer/job-dispute-update'), 'files'=>'true']) !!}
        
                      <ul>
                          <li><strong>Reason:</strong> {{$JobDispute->freelancer_reason}}  </li>
                          <li><strong>Freelance Detial:</strong> {{$JobDispute->freelancer_description}}</li>
                          @if(!empty($JobDispute->pe_description))
                          <li><strong>Your Response:</strong> {!! strip_tags($JobDispute->pe_description) !!}</li>
                          <li><strong>Your Status:</strong> @if($JobDispute->pe_status == 1) Accepted @else Rejected @endif</li>
                          @endif
                          @if(empty($JobDispute->pe_description))
                          <li>Accept / Reject: <select name="disputeOption">
                              <option value="">-- Please Select --</option>
                              <option value="1">-- Accept --</option>
                              <option value="0">-- Reject --</option>
                        </select></li>
                          <li><textarea id="wt-tinymceeditor" name="reason" cols="50" rows="10" class="wt-tinymceeditor">{{old('reason')}}</textarea>
                        <input type="hidden" value="{{$JobDispute->id}}" name="dispute_id" />
                        </li>
                          <li> <div class="form-group wt-btnarea">
                            
                            <input type="submit" value="Dispute Response" class="wt-btn">
                           
                        </div>
                    </li>
                    @endif
                      </ul>
        
                      {!! Form::close(); !!}
        
                    </div>
        
                    <div class="milstonePaids alert-danger">
                        <div class="wt-borderheading wt-borderheadingvtwo">
                            <h3>Grievance Process</h3> 
                        </div>
                        <ul>
                         <li> <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                               Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                               Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                sunt in culpa qui officia deserunt mollit anim id est laborum</p></li>
                         </ul>
                      </div>
                    </div> </div>
                    @endif
      
                </div>

                
            </div>
           
           

        </div>
    </section>

    <style>
        .milstonePaid{
            background-color: #b8d5c1;
        }
        .milstonedispute{
            background-color: #b8d5c1;
        }
        .milesonelist ul li{
            list-style: none;
            padding: 10px 20px;
        }
        .milesonelist ul li span{
            font-weight: bold;
            padding-right: 10px;
        }
        </style>
@endsection
