@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
<div class="govts-listing" id="govt-list">


    @if (Session::has('message'))
    <div class="flash_msg">
        <flash_messages :message_class="'success'" :time='5' :message="'{{{ Session::get('message') }}}'" v-cloak>
        </flash_messages>
    </div>
    @elseif (Session::has('error'))
    <div class="flash_msg">
        <flash_messages :message_class="'danger'" :time='5' :message="'{{{ Session::get('error') }}}'" v-cloak>
        </flash_messages>
    </div>
    @endif
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-10 float-left">
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>Edit Milestone</h2>
                    </div>
                    <div class="wt-dashboardboxcontent">

                        @if ($errors->any())
                        <div class="alert alert-danger col-md-12">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif


                        {!! Form::open([ 'url' => url('employer/milestone-update/'.$Milestone->id.''), 'class'
                        =>'wt-formtheme wt-formprojectinfo
                        wt-formcategory', 'id' => 'locations' ]) !!}
                        <fieldset>

                            <div class="form-group">

                                {!! Form::text( 'title', e($Milestone['title']), ['class'
                                =>'form-control'.($errors->has('title') ? ' is-invalid' : ''),
                                'placeholder' => 'Title'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::text( 'start_date', e($Milestone['start_date']), ['class' =>'datepicker
                                form-control'.($errors->has('start_date') ? ' is-invalid' : ''),
                                'placeholder' => 'Start date'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::text( 'end_date', e($Milestone['end_date']), ['class' =>'datepicker
                                form-control'.($errors->has('end_date') ? ' is-invalid' : ''),
                                'placeholder' => 'End date'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::number( 'amount', e($Milestone['amount']), ['class'
                                =>'form-control'.($errors->has('amount') ? ' is-invalid' : ''),
                                'placeholder' => 'Amount'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::number( 'percentage', e($Milestone['percentage']), ['class'
                                =>'form-control'.($errors->has('percentage') ? ' is-invalid' : ''),
                                'placeholder' => 'Percentage', 'step' => '0.01'] ) !!}

                            </div>

                            <div class="form-group">

                                {!! Form::textarea( 'body', e($Milestone['body']), ['class'
                                =>'form-control'.($errors->has('body') ? ' is-invalid' : ''),
                                'placeholder' => 'Description'] ) !!}

                            </div>




                            <div class="form-group wt-btnarea">
                                <input type="submit" value="Update Milestone" class="wt-btn">
                            </div>
                        </fieldset>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    $( function() {
    $( ".datepicker" ).datepicker();
  } );

  $(document).ready(function(){ 
    $("input").attr("autocomplete", "off"); 
});

</script>
@endsection
