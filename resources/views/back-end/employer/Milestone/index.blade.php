@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
<div class="govts-listing" id="dist-list">
    @if (Session::has('message'))
    <div class="flash_msg">
        <flash_messages :message_class="'success'" :time='5' :message="'{{{ Session::get('message') }}}'" v-cloak>
        </flash_messages>
    </div>
    @elseif (Session::has('error'))
    <div class="flash_msg">
        <flash_messages :message_class="'danger'" :time='5' :message="'{{{ Session::get('error') }}}'" v-cloak>
        </flash_messages>
    </div>
    @endif

    {{-- {{dd($Milestones)}} --}}
    <section class="wt-haslayout wt-dbsectionspace la-admin-languages">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 float-left">
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>Add Milestones</h2>
                    </div>
                    @php
                    $job_info = \App\Job::where('id', $jobID)->first();
                    @endphp
                    <div class="wt-dashboardboxcontent">
                        @if ($errors->any())
                        <div class="alert alert-danger col-md-12">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div><br />
                        @endif
                        {!! Form::open(['url' => route('employermilestone.store'), 'class' =>'wt-formtheme
                        wt-formprojectinfo wt-formcategory','id' =>
                        'BidCriteriaQuestion']) !!}
                        @csrf
                        <fieldset>
                            <div class="form-group">

                                {!! Form::text( 'title', null, ['class' =>'form-control'.($errors->has('title') ? '
                                is-invalid' : ''),
                                'placeholder' => 'Title'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::text( 'start_date', null, ['class' =>'datepicker
                                form-control'.($errors->has('start_date') ? ' is-invalid' : ''),
                                'placeholder' => 'Start date'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::text( 'end_date', null, ['class' =>'datepicker
                                form-control'.($errors->has('end_date') ? ' is-invalid' : ''),
                                'placeholder' => 'End date'] ) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::number( 'amount', null, ['class' =>'form-control'.($errors->has('amount') ? '
                                is-invalid' : ''),
                                'placeholder' => 'Amount'] ) !!}

                            </div>

                            <div class="form-group">

                                {!! Form::number( 'percentage', null, ['class'
                                =>'form-control'.($errors->has('percentage') ? ' is-invalid' : ''),
                                'placeholder' => 'Percentage', 'step' => '0.01'] ) !!}

                            </div>

                            <div class="form-group">

                                {!! Form::textarea( 'body', null, ['class' =>'form-control'.($errors->has('body') ? '
                                is-invalid' : ''),
                                'placeholder' => 'Description'] ) !!}

                            </div>
                            <input type="hidden" name="job_id" value="{{$jobID}}" readonly>
                            @if($job_info->status === 'completed' || $job_info->status === 'cancelled')
                            <div class="form-group wt-btnarea">
                                <button type="submit" value="Add Milestones" class="wt-btn bg-secondary text-white"
                                    disabled>Add Milestones</button>
                            </div>
                            @else
                            <div class="form-group wt-btnarea">
                                <input type="submit" value="Add Milestones" class="wt-btn">
                            </div>
                            @endif
                        </fieldset>
                        {!! Form::close(); !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 float-right">
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Milestones</h2>


                    </div>
                    @if ($Milestones->count() > 0)
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder">

                        @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                        @endif
                        <table class="wt-tablecategories" id="checked-val">
                            <thead>
                                <tr>

                                    <th>Title</th>
                                    <th>Start Date / End Date</th>
                                    <th>Amount</th>
                                    <th>Percentage</th>
                                    <th>Status</th>

                                    <th>{{{ trans('lang.action') }}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $counter = 0; @endphp
                                @foreach ($Milestones as $Milestone)
                                <tr class="del-{{{ $Milestone->id }}}" v-bind:id="districtID">

                                    <td>
                                        {{$Milestone->title}}
                                    </td>
                                    <td>
                                        {{$Milestone->start_date}} / {{$Milestone->end_date}}
                                    </td>
                                    <td>
                                        {{$Milestone->amount}}
                                    </td>
                                    <td>
                                        {{$Milestone->percentage}}%
                                    </td>
                                    <td>
                                        @if($Milestone->status == 1)
                                        Completed
                                        @elseif($job_info->status === 'cancelled')
                                        Cancelled
                                        @else
                                        Pending</a>

                                        @endif
                                    </td>
                                    <td>
                                        @if($job_info->status === 'completed' || $job_info->status === 'cancelled')
                                        <div class="wt-actionbtn wt-milestones-actionbtn">
                                            <form action="{{ route('employermilestone.destroy', $Milestone->id)}}"
                                                method="post" class="formDelete">
                                                @csrf
                                                @method('DELETE')
                                                <button
                                                    onclick="return confirm('Are you sure you want to delete this item?');"
                                                    class="wt-deleteinfo bg-secondary text-white" type="submit"
                                                    disabled><i class="lnr lnr-trash"></i></button>
                                            </form>
                                            <a class="wt-addinfo wt-skillsaddinfo bg-secondary text-white" disabled>
                                                <i class="lnr lnr-pencil" disbaled></i>
                                            </a>
                                        </div>
                                        @else
                                        <div class="wt-actionbtn wt-milestones-actionbtn">
                                            <form action="{{ route('employermilestone.destroy', $Milestone->id)}}"
                                                method="post" class="formDelete">
                                                @csrf
                                                @method('DELETE')
                                                <button
                                                    onclick="return confirm('Are you sure you want to delete this item?');"
                                                    class="wt-deleteinfo" type="submit"><i
                                                        class="lnr lnr-trash"></i></button>
                                            </form>
                                            <a href="{{{ url('employer/milestone-edit') }}}/{{{ $Milestone->id }}}"
                                                class="wt-addinfo wt-skillsaddinfo">
                                                <i class="lnr lnr-pencil"></i>
                                            </a>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @php $counter++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        @if( method_exists($Milestone,'links') ) {{ $Milestone->links('pagination.custom') }} @endif
                    </div>
                    @else
                    @if (file_exists(resource_path('views/extend/errors/no-record.blade.php')))
                    @include('extend.errors.no-record')
                    @else
                    @include('errors.no-record')
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
<script>
    $( function() {
    $( ".datepicker" ).datepicker();
  } );

  $(document).ready(function(){ 
    $("input").attr("autocomplete", "off"); 
});

</script>
@endsection
