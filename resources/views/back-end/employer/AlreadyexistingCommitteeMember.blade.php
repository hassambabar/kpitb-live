@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
@if(Session::has('errormessage'))
    <div class="alert alert-danger"> {{ Session::get('errormessage') }}</div>
  @endif
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right" id="invoice_list">
                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Add Member</h2>
                    </div> 
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder">

                        <div class="col-md-12">
                            <br />
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
            
                         </div>
                         <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                   
                                            <tr>
                                                <input type="hidden" value="{{ $user_info->id }}" id="userid" name="user_id">
                                                <td>{{{ $user_info->first_name }}} {{{ $user_info->last_name }}}</td>
                                                <td>{{{ $user_info->email }}} </td>
                                                <td>
                                                    <a href="{{{ url('add_existing_committeemember/'.$user_info->id) }}}"class="wt-btn">Add</a>
                                                </td>
                                            </tr>
                                  
                               
                                </tbody>
                            </table> 

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
