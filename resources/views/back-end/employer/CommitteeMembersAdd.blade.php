@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right" id="invoice_list">
                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Add Member</h2>
                        <a href="{{ url('committee-members') }}" class="btn btn-primary" style="float: right">Members</a>
                    </div>

                  

                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder">

                        <div class="col-md-12">
                            <br />
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
            
                         </div>
                            <form method="POST" action="{{url('committee-store')}}"
                                accept-charset="UTF-8" class="wt-formtheme wt-userform">
                                @csrf

                                <div class="wt-securitysettings wt-tabsinfo wt-haslayout">
                                    <div class="wt-tabscontenttitle">
                                        <h2>Basic Information</h2>
                                    </div>
                                    <div class="wt-settingscontent">
                                        <div class="wt-description">
                                            <p>Please Add Basic information for Committee Member</p>
                                        </div>
                                       
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">

                                                <div class="form-group form-group-half"><input type="text" name="first_name" placeholder="First Name" class="form-control"> <!----></div>

                                               
                                            </div>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                                <div class="form-group form-group-half"><input type="text" name="last_name" placeholder="Last Name" class="form-control"> <!----></div>
                                            </div>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                                <div class="form-group form-group-half "><input id="user_email" type="email" name="email" placeholder="Email" value="" class="form-control"> <!----></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="wt-securitysettings wt-tabsinfo wt-haslayout">
                                    <div class="wt-tabscontenttitle">
                                        <h2>Account Password</h2>
                                    </div>
                                    <div class="wt-settingscontent">
                                        <div class="wt-description">
                                        </div>
                                       
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div> <span class="note">  (Must contain 6 or more characters)</span>
                                            <div class="wt-formtheme wt-userform">
                                           
                                                <div class="form-group form-group-half"><input id="password" type="password" name="password" placeholder="Password" pattern=".{6,}" class="form-control"> <!----></div>
                                               
                                            </div>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                                <div class="form-group form-group-half"><input id="password_confirmation" type="password" name="password_confirmation" placeholder="Retype Password" class="form-control"> <!----></div>
                                                <span id='message'></span>                                             </div>
                                        </div>
                                        
                                    </div>
                                </div>
                              
                                <div class="wt-settingscontent">
                                    <div class="wt-settingscontent">
                                <div class="form-group form-group-half wt-btnarea"><input type="submit" value="Save"
                                        class="wt-btn"></div>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script type="text/javascript">
    $('#password, #password_confirmation').on('keyup', function () {
        var password = document.getElementById("password").value;
        var confirmPassword = document.getElementById("password_confirmation").value;
        if (password != confirmPassword) {
            $('#message').html('<p id="length" class="invalid">Password does not matched</p>').css('color', 'red');
            $("#submit_button").css("display","none");
        return false;
        }
        if (password == confirmPassword) {
            $('#message').html('Password Matched').css('color', 'green');
            $("#submit_button").css("display","block");
                        // console.log("bbb");
        return false;
        }
    });
    $("body").on("change", "#locationsDropDown", function () {
    var locId = $(this).val();
    $(".preloader-outer").show();
        $.ajax({
            type: 'POST',
            url: 'getAjaxDistrics',
            cache: false,
            dataType: 'html',
            data: {
                locId: locId,
                _token: '{{csrf_token()}}'
            },
            success: function (res) {
                $("#distDropDown").html(res);
                $(".preloader-outer").hide();
            }
        });
    });

    function check(){
  if(document.getElementById('roles').value!='2')
    document.getElementById('roles_emp').disabled=true;
  else
    document.getElementById('roles_emp').disabled=false;
}
   
    </script>
@stop