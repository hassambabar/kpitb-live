@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' :
'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 float-right" id="invoice_list">
                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Edit Member</h2>
                        <a href="{{ url('committee-members') }}" class="btn btn-primary" style="float: right">Members</a>
                    </div>

                  

                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder">

                        <div class="col-md-12">
                            <br />
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
            
                         </div>


                        {{-- @if (!empty($invoices) && $invoices->count() > 0) --}}
                            <form method="POST" action="{{url('committee-update',$user->id)}}"
                                accept-charset="UTF-8" class="wt-formtheme wt-userform">
                                @method('POST') 
                                @csrf
                                <div class="wt-securitysettings wt-tabsinfo wt-haslayout">
                                    <div class="wt-tabscontenttitle">
                                        <h2>Basic Information</h2>
                                    </div>
                                    <div class="wt-settingscontent">
                                        <div class="wt-description">
                                            <p>Please Add Basic information for Committee Member</p>
                                        </div>
                                       
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">

                                                <div class="form-group form-group-half"><input type="text" value="{{$user->first_name}}" name="first_name" placeholder="First Name" class="form-control"> <!----></div>

                                               
                                            </div>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                                <div class="form-group form-group-half"><input type="text" value="{{$user->last_name}}" name="last_name" placeholder="Last Name" class="form-control"> <!----></div>
                                            </div>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                                <div class="form-group form-group-half "><input id="user_email" value="{{$user->email}}" disabled type="email" name="email" placeholder="Email" value="" class="form-control"> <!----></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="wt-securitysettings wt-tabsinfo wt-haslayout">
                                    <div class="wt-tabscontenttitle">
                                        <h2>Account Password</h2>
                                    </div>
                                    <div class="wt-settingscontent">
                                        <div class="wt-description">
                                        </div>
                                       
                                        <div class="wt-settingscontent">
                                            <div class="wt-description">
                                            </div>
                                            <div class="wt-formtheme wt-userform">
                                         
                                                <div class="form-group form-group-half"><input id="password" type="password" name="password"  placeholder="Password" class="form-control"> <!----></div>
                                               
                                            </div>
                                        </div>
                                       
                                        
                                    </div>
                                </div>
                              
                                <div class="wt-settingscontent">
                                    <div class="wt-settingscontent">
                                <div class="form-group form-group-half wt-btnarea"><input type="submit" value="Update"
                                        class="wt-btn"></div>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<style>
    input[type=text], input[type=password],input[type=email]{
    width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    display: inline-block;
    border: none;
    background: #f1f1f1;
  }
  input[type=text]:focus, input[type=password]:focus {
    background-color: #ddd;
    outline: none;
  }
    
  .note{
      color: #d8550c;
      font-size: 12px;
  }
   .signupbtn {
    /* float: left; */
   
    display: flex;
      justify-content:center;
  }
</style>