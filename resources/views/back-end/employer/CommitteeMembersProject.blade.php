@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 float-right" id="invoice_list">
           
                <div class="wt-dashboardbox wt-dashboardinvocies"> 
                    
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Committee Members Already Added To The Project</h2>
                        <a href="{{url('employer/dashboard/manage-jobs')}}" class="btn btn-primary" style="float: right">Back To Project</a>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                        <div>
                        @if(count($CommitteeMembersinfo) > 0)
                            <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                       <th></th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    @foreach ($CommitteeMembersinfo as $CommitteeMember)
                                        @php
                                            $CommitteeMember = App\User::find($CommitteeMember['member_id']);
                                        @endphp
                                        @csrf
                                            <tr>
                                                <input type="hidden" value="{{ $project_id }}" id="JobID" name="job_id">
                                                <td><input type="checkbox" class="addMember" name="addedid" checked value="{{ $CommitteeMember->id }}" disabled>
                                                </td>
                                                <td>{{{ $CommitteeMember->first_name }}} {{{ $CommitteeMember->last_name }}}</td>
                                                <td>{{{ $CommitteeMember->email }}} </td>
                                                <td>
                                                    <a href="{{{url('committee-member-to-project-delete/'.$project_id.'/'.$CommitteeMember->id)}}}">Remove</a>
                                                </td>
                                            </tr>
                                    @endforeach
                               
                                </tbody>
                            </table> 
                            @else
                                    <h3 class="text-center">No Committee Members added.</h3>
                            @endif
                        </div>
                    </div>
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Update Committee Members To Project</h2>
                    </div>
                    <form method="POST" action="{{ url('committee-member-to-project') }}" class="wt-formtheme wt-loginform do-login-form">
                    @csrf
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                        <div>
                        <input type="hidden" value="{{ $project_id }}" id="JobID" name="job_id">
                            <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                       <th></th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $userSingle)
                                        @if(empty($userSingle->memberValue))
                                            <tr>
                                                <td><input type="checkbox" class="addMember" name="ids[]" value="{{ $userSingle->id }}"></td>
                                                <td>{{{ $userSingle->first_name }}} {{{ $userSingle->last_name }}}</td>
                                                <td>{{{ $userSingle->email }}} </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        <button type="submit" class="wt-btn btn btn-primary" style="float: right">Add To The Project</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
<script type="text/javascript">

$(".addMember").change(function(){

var MemberID = $(this).val();
var JobID = $('#JobID').val();

if($(this).is(":checked")) {
    var CheckStatus = 'checked';
        }else{

            var CheckStatus = 'unchecked';

        }


// $.ajax({
//     type: 'POST',
//     url: '{{url('committee-member-to-project')}}',
//     cache: false,
//     dataType: 'html',
//     data: {
//         MemberID: MemberID,
//         CheckStatus:CheckStatus,
//         JobID:JobID,
//         _token: '{{csrf_token()}}'
//     },
//     success: function (res) {
        // actual comments
    //    $("#distDropDown").html(res);
    //     $(".preloader-outer").hide();
    //     end actual comments 
    // }
    // });
});

</script>
@endsection