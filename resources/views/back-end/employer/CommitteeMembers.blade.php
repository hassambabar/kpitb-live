@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  float-right" id="invoice_list">
                <div class="wt-dashboardbox wt-dashboardinvocies">
                    <div class="wt-dashboardboxtitle wt-titlewithsearch">
                        <h2>Committee Members</h2>
                        <a href="{{url('committee-add')}}" class="btn btn-primary" style="float: right">Add Member</a>
                    </div>
                    <div class="wt-dashboardboxcontent wt-categoriescontentholder wt-categoriesholder" id="printable_area">
                        {{-- @if (!empty($invoices) && $invoices->count() > 0) --}}

                        @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif

                            <table class="wt-tablecategories">
                                <thead>
                                    <tr>
                                       
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $userSingle)
                                        <tr>
                                          
                                            <td>{{{ $userSingle->first_name }}} {{{ $userSingle->last_name }}}</td>
                                            <td>{{{ $userSingle->email }}} </td>
                                            <td>{{{ \Carbon\Carbon::parse($userSingle->created_at)->format('M d, Y') }}}</td>
                                            <td> <a href="{{ url('committee-edit', $userSingle->id)}} ">Edit</a> |  <form action="{{ url('committee-delete', $userSingle->id)}}" style="display: inline;"  method="post">
                                                    @csrf
                                                    @method('POST')
                                        <button  onclick="return confirm('Are you sure you want to delete this item?');" type="submit">Delete</button>
                                      </form>
                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                      
                  
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
