@auth
    @php
            $user = !empty(Auth::user()) ? Auth::user() : '';
            $slug = $user->slug;
            $role = !empty($user) ? $user->getRoleNames()->first() : array();
    @endphp
    @if($role !== 'committee-member')
    <div id="wt-sidebarwrapper" class="wt-sidebarwrapper">
        <div id="wt-btnmenutoggle" class="wt-btnmenutoggle">
            <span class="menu-icon">
                <em></em>
                <em></em>
                <em></em>
            </span>
        </div>
        @php
            $user = !empty(Auth::user()) ? Auth::user() : '';
            $slug = $user->slug;
            $role = !empty($user) ? $user->getRoleNames()->first() : array();
            $profile = \App\User::find($user->id)->profile;
            $setting = \App\SiteManagement::getMetaValue('footer_settings');
            $payment_settings = \App\SiteManagement::getMetaValue('commision');
            $payment_module = !empty($payment_settings) && !empty($payment_settings[0]['enable_packages']) ? $payment_settings[0]['enable_packages'] : 'true';
            $employer_payment_module = !empty($payment_settings) && !empty($payment_settings[0]['employer_package']) ? $payment_settings[0]['employer_package'] : 'true';
            $copyright = !empty($setting) ? $setting['copyright'] : 'KPITB All Rights Reserved';
        @endphp
        <div id="wt-verticalscrollbar" class="wt-verticalscrollbar">
            <div class="wt-companysdetails wt-usersidebar wt-verticalscrollbar">
                <figure class="wt-companysimg">
                    <img src="{{{ asset(Helper::getUserProfileBanner($user->id, 'small')) }}}" alt="{{{ trans('lang.profile_banner') }}}">
                </figure>
                <div class="wt-companysinfo">
                    <figure><img src="{{{ asset(Helper::getImageWithSize('uploads/users/'.$user->id, $profile->avater, 'listing')) }}}" alt="{{ trans('lang.profile_photo') }}"></figure>
                    <div class="wt-title">
                        <h2>
                            <a href="{{{ $role != 'admin' ? url($role.'/dashboard') : 'javascript:void()' }}}">
                                {{{ !empty(Auth::user()) ? Helper::getUserName(Auth::user()->id) : 'No Name' }}}
                            </a>
                        </h2>
                        <span>
                            <?php
                                if($role == 'job-administrator') {
                                    echo "Job Administrator";
                                }
                                elseif($role == 'page-management') {
                                    echo "Page Manager";
                                }
                                elseif(!empty(Auth::user()->profile->tagline)) { 
                                    echo str_limit(Auth::user()->profile->tagline, 26, ''); 
                                }
                                else {
                                    if (Auth::user()->user_type != '' || Auth::user()->user_type == 2) {
                                        echo "Committee Member";
                                    } else {
                                        echo Helper::getAuthRoleName();
                                    }
                                }
                            ?> 
                        </span>
                    </div>
                    @if ($role === 'employer')
                    @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'jobs')
                    @if(Helper::getKpraInfo(Auth::user()->id) != '')
                    <div class="wt-btnarea"><a href="{{{ url(route('employerPostJob')) }}}" class="wt-btn">{{{ trans('lang.post_job') }}}</a></div>
                   
                    @else 
                    <div class="wt-btnarea"><a href="#" class="wt-btn" style="background-color: #ccc !important" title="Kindly, enter your KPRA number in the account settings in order to post a job">{{{ trans('lang.post_job') }}}</a></div>
                    @endif
                    @else
                        <div class="wt-btnarea"><a href="{{{ url(route('showUserProfile', ['slug' => Auth::user()->slug])) }}}" class="wt-btn">{{{ trans('lang.view_profile') }}}</a></div>
                    @endif
                    @elseif ($role === 'freelancer')
                        @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'services')
                        @if(Auth::user()->user_type == '' || Auth::user()->user_type != 2 )

                            <div class="wt-btnarea"><a href="{{{ url(route('freelancerPostService')) }}}" class="wt-btn">{{{ trans('lang.post_service') }}}</a></div>
                            @endif
                        @else
                            <div class="wt-btnarea"><a href="{{{ url(route('showUserProfile', ['slug' => Auth::user()->slug])) }}}" class="wt-btn">{{{ trans('lang.view_profile') }}}</a></div>
                        @endif
                    @endif
                </div>
            </div>
            <nav id="wt-navdashboard" class="wt-navdashboard">
                <ul>
                    @if ($role === 'admin')
                        <li>
                            <a href="{{{ route('adminDashboard') }}}">
                                <i class="ti-desktop"></i>
                                <span>{{ trans('lang.dashboard') }}</span>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="{{{ route('manageRoles') }}}">
                                <i class="ti-user"></i>
                                <span>{{ trans('Manage Roles') }}</span>
                            </a>
                        </li> -->
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('lang.manage_users') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('userListing') }}}">{{ trans('Users') }}</a></li>
                                <li><hr><a href="{{{ route('manageRoles') }}}">{{ trans('Manage Roles') }}</a></li>
                                <li><hr><a href="{{{ route('procurerListing') }}}">{{ trans('Procurer List') }}</a></li>
                                <li><hr><a href="{{{ route('userBySkills') }}}">{{ trans('Users Filters') }}</a></li>
                                <!-- <li>
                            <a href="{{{ route('userBySkills') }}}">
                                <i class="ti-briefcase"></i>
                                <span>Users Filters</span>
                            </a>
                        </li> -->
                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('lang.manage_articles') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('articles') }}}">{{ trans('lang.articles') }}</a></li>
                                <li><hr><a href="{{{ route('articleCategories') }}}">{{ trans('lang.categories') }}</a></li>
                            </ul>
                        </li>
                        <!-- {{-- <li>
                            <a href="{{{ route('orderList') }}}">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                <span>{{ trans('lang.orders') }}</span>
                            </a>
                        </li> --}} -->
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('Manage Jobs') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('allJobs') }}}">{{ trans('lang.all_jobs') }}</a></li>
                                <li><hr><a href="{{{ route('allJobsskills') }}}">{{ trans('Jobs Filters') }}</a></li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="{{{ route('allJobs') }}}">
                                <i class="ti-briefcase"></i>
                                <span>{{ trans('lang.all_jobs') }}</span>
                            </a>
                        </li> -->
                        <!-- <li>
                            <a href="{{{ route('userListing') }}}">
                                <i class="ti-user"></i>
                                <span>{{ trans('lang.manage_users') }}</span>
                            </a>
                        </li> -->
                        <!-- <li>
                            <a href="{{{ route('allJobsskills') }}}">
                                <i class="ti-briefcase"></i>
                                <span>Jobs Filters</span>
                            </a>
                        </li> -->
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('Manage Rating/Reviews') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('reviewOptions') }}}">{{ trans('lang.review_options') }}</a></li>
                                <li><hr><a href="{{{ route('review') }}}">{{ trans('User Feedbacks/Ratings') }}</a></li>
                            </ul>
                        </li>
                       <li>
                            <a href="{{{ route('adminPayouts') }}}">
                                <i class="far fa-money-bill-alt"></i>
                                <span>{{ trans('Manage Payouts') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{{ route('emailTemplates') }}}">
                                <i class="ti-email"></i>
                                <span>{{ trans('lang.email_templates') }}</span>
                            </a>
                        </li>
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('Manage Pages') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('pages') }}}">{{ trans('lang.all_pages') }}</a></li>
                                <li><hr><a href="{{{ route('createPage') }}}">{{ trans('lang.add_pages') }}</a></li>

                            </ul>
                        </li>

                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-settings"></i>
                                <span>{{ trans('lang.settings') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('adminProfile') }}}">{{ trans('lang.acc_settings') }}</a></li>
                                <li><hr><a href="{{{ url('admin/settings') }}}">{{ trans('lang.general_settings') }}</a></li>
                                <li><hr><a href="{{{ route('resetPassword') }}}">{{ trans('lang.reset_pass') }}</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{{ route('contacts') }}}">
                                <i class="ti-comment-alt"></i>
                                <span>Contact Us</span>
                            </a>
                        </li>
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('Manage Taxonomies') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('skills') }}}">{{ trans('lang.skills') }}</a></li>
                                <li><hr><a href="{{{ route('categories') }}}">{{ trans('lang.job_cats') }}</a></li>
                                <li><hr><a href="{{{ route('departments') }}}">{{ trans('lang.dpts') }}</a></li>
                                <li><hr><a href="{{{ route('languages') }}}">{{ trans('lang.langs') }}</a></li>
                                <li><hr><a href="{{{ route('locations') }}}">{{ trans('lang.locations') }}</a></li>
                                <!-- <li><hr><a href="{{{ route('badges') }}}">{{ trans('lang.badges') }}</a></li>
                                <li><a href="{{{ route('deliveryTime') }}}">{{ trans('lang.delivery_time') }}</a></li>
                                <li><a href="{{{ route('ResponseTime') }}}">{{ trans('lang.response_time') }}</a></li> -->
                                <li><a href="{{{ route('organizationtype') }}}">Organizations</a></li>
                                <li><a href="{{{ route('govtdept') }}}">Govt Departments</a></li>
                                <!-- <li><a href="{{{ route('district') }}}">Districts</a></li> -->
                                <li><a href="{{ route('bid-questions') }}">Bid Criteria Questions</a></li>


                            </ul>
                        </li>
                        @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'services')
                            <li>
                                <a href="{{{ route('allServices') }}}">
                                    <i class="ti-briefcase"></i>
                                    <span>{{ trans('Manage Services') }}</span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="{{{ route('ServiceOrders') }}}">
                                    <i class="ti-briefcase"></i>
                                    <span>{{ trans('lang.service_orders') }}</span>
                                </a>
                            </li> -->
                        @endif
                        
                        <!-- <li>
                            <a href="{{{ route('reviewOptions') }}}">
                                <i class="ti-check-box"></i>
                                <span>{{ trans('lang.review_options') }}</span>
                            </a>
                        </li> -->
                        <!-- <li>
                            <a href="{{{ route('review') }}}">
                                <i class="ti-check-box"></i>
                                <span>User Feedbacks/Ratings</span>
                            </a>
                        </li> -->
                       
                        
                            <!-- <li>
                                <a href="{{{ route('createPackage') }}}">
                                    <i class="ti-package"></i>
                                    <span>{{ trans('lang.packages') }}</span>
                                </a>
                            </li> -->
                        
                        <!-- <li>
                            <a href="{{{ route('homePageSettings') }}}">
                                <i class="ti-home"></i>
                                <span>{{ trans('lang.home_page_settings') }}</span>
                            </a>
                        </li> -->
                        
                        
                    @endif

                    <!-- job-administrator -->

                    @if ($role === 'job-administrator')
                        <li>
                            <a href="{{{ route('jobadminDashboard') }}}">
                                <i class="ti-desktop"></i>
                                <span>{{ trans('lang.dashboard') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{{ route('alladminJobs') }}}">
                                <i class="ti-briefcase"></i>
                                <span>{{ trans('lang.all_jobs') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{{ route('allJobsadminskills') }}}">
                                <i class="ti-briefcase"></i>
                                <span>Jobs Filters</span>
                            </a>
                        </li>

                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-settings"></i>
                                <span>{{ trans('lang.settings') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('jobadminProfile') }}}">{{ trans('lang.acc_settings') }}</a></li>
                                <li><hr><a href="{{{ route('resetPassword') }}}">{{ trans('lang.reset_pass') }}</a></li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="{{{ route('contacts') }}}">
                                <i class="ti-comment-alt"></i>
                                <span>Contact Us</span>
                            </a>
                        </li> -->
                        <li class="menu-item-has-children">
                    <span class="wt-dropdowarrow"><i class="ti-layers"></i></span>
                    <a href="javascript:void(0)">
                        <i class="ti-layers"></i>
                        <span>{{ trans('lang.taxonomies') }}</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{{ route('categories') }}}">{{ trans('lang.job_cats') }}</a></li>
                        <li><a href="{{{ route('departments') }}}">{{ trans('lang.dpts') }}</a></li>
                        <li><a href="{{{ route('district') }}}">Districts</a></li>
                        <li><a href="{{ route('bid-questions') }}">Bid Criteria Questions</a></li>
                    </ul>
                </li>

                    @endif

                    @if ($role === 'page-management')
                        <li>
                            <a href="{{{ route('pageadminDashboard') }}}">
                                <i class="ti-desktop"></i>
                                <span>{{ trans('lang.dashboard') }}</span>
                            </a>
                        </li>
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-layers"></i>
                                <span>{{ trans('lang.pages') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('pages') }}}">{{ trans('lang.all_pages') }}</a></li>
                                <li><hr><a href="{{{ route('createPage') }}}">{{ trans('lang.add_pages') }}</a></li>

                            </ul>
                        </li>
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-settings"></i>
                                <span>{{ trans('lang.settings') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ route('pageadminProfile') }}}">{{ trans('lang.acc_settings') }}</a></li>
                                <li><hr><a href="{{{ route('resetPassword') }}}">{{ trans('lang.reset_pass') }}</a></li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="{{{ route('contacts') }}}">
                                <i class="ti-comment-alt"></i>
                                <span>Contact Us</span>
                            </a>
                        </li> -->
                        <!-- <li class="menu-item-has-children">
                    <span class="wt-dropdowarrow"><i class="ti-layers"></i></span>
                    <a href="javascript:void(0)">
                        <i class="ti-layers"></i>
                        <span>{{ trans('lang.taxonomies') }}</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{{ route('categories') }}}">{{ trans('lang.job_cats') }}</a></li>
                        <li><a href="{{{ route('departments') }}}">{{ trans('lang.dpts') }}</a></li>
                        <li><a href="{{{ route('district') }}}">Districts</a></li>
                        <li><a href="{{ route('bid-questions') }}">Bid Criteria Questions</a></li>
                    </ul>
                </li> -->

                    @endif

                    @if ($role === 'employer' || $role === 'freelancer' )
                        <li>
                            <a href="{{{ url($role.'/dashboard') }}}">
                                <i class="ti-desktop"></i>
                                <span>{{ trans('lang.dashboard') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{{ url('profile/'.$slug) }}}">
                                <i class="ti-eye"></i>
                                <span>{{ trans('Preview Profile') }}</span>
                            </a>
                        </li>
                        @if(Auth::user()->user_type == '' || Auth::user()->user_type != 2 )

                        <li>
                            <a href="{{{ route('message') }}}">
                                <i class="ti-envelope"></i>
                                <span>{{ trans('lang.msg_center') }}</span>
                            </a>
                        </li>
                        @endif
                        <li class="menu-item-has-children">
                            <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                            <a href="javascript:void(0)">
                                <i class="ti-settings"></i>
                                <span>{{ trans('lang.settings') }}</span>
                            </a>
                            <ul class="sub-menu">
                                <li><hr><a href="{{{ url($role.'/profile') }}}">{{ trans('lang.profile_settings') }}</a></li>
                                <li><hr><a href="{{{ route('manageAccount') }}}">{{ trans('lang.acc_settings') }}</a></li>
                            </ul>
                        </li>
                        @if ($role === 'employer')
                            @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'jobs')
                                <li class="menu-item-has-children">
                                    <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                    <a href="javascript:void(0)">
                                        <i class="ti-announcement"></i>
                                        <span>{{ trans('lang.jobs') }}</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li><hr><a href="{{{ route('employerManageJobs') }}}">{{ trans('lang.manage_job') }}</a></li>
                                        <li><hr><a href="{{{ url('employer/jobs/completed') }}}">{{ trans('lang.completed_jobs') }}</a></li>
                                        <li><hr><a href="{{{ url('employer/jobs/hired') }}}">{{ trans('lang.ongoing_jobs') }}</a></li>
                                        <li><hr><a href="{{{ url('employer/jobs/cancelled') }}}">{{ trans('lang.cancelled_jobs') }}</a></li>
                                    </ul>
                                </li>
                            @endif
                            @if(Auth::user()->government_department == 1)
                            <li>
                            <a href="{{ url('committee-members') }}">
                                <i class="ti-user"></i>
                                <span>{{ trans('Committee Members') }}</span>
                            </a>
                            </li>
                        @endif
                            <!-- @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'services')
                                <li class="menu-item-has-children">
                                    <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                    <a href="javascript:void(0)">
                                        <i class="ti-briefcase"></i>
                                        <span>{{ trans('lang.manage_services') }}</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li><hr><a href="{{{ url('employer/services/hired') }}}">{{ trans('lang.ongoing_services') }}</a></li>
                                        <li><hr><a href="{{{ url('employer/services/completed') }}}">{{ trans('lang.completed_services') }}</a></li>
                                        <li><hr><a href="{{{ url('employer/services/cancelled') }}}">{{ trans('lang.cancelled_services') }}</a></li>
                                    </ul>
                                </li>
                            @endif -->
                            <li>
                                <a href="{{{ route('employerPayoutsSettings') }}}">
                                    <i class="far fa-money-bill-alt"></i>
                                    <span> {{ trans('lang.payouts') }}</span>
                                </a>
                            </li>
                            <li class="menu-item-has-children">
                                <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                <a href="javascript:void(0)">
                                    <i class="ti-file"></i>
                                    <span>{{ trans('lang.invoices') }}</span>
                                </a>
                                <ul class="sub-menu">
                                    @if ($employer_payment_module === 'true' )
                                        <li><hr><a href="{{{ url('employer/package/invoice') }}}">{{ trans('lang.pkg_inv') }}</a></li>
                                    @endif
                                    <li><hr><a href="{{{ url('employer/project/invoice') }}}">{{ trans('lang.project_inv') }}</a></li>
                                    <li><hr><a href="{{{ url('employer/project/milestone_invoice') }}}">{{ trans('Milestones Invoices') }}</a></li>
                                </ul>
                            </li>
                            @if ($employer_payment_module === 'true' )
                                <li>
                                    <a href="{{{ url('dashboard/packages/'.$role) }}}">
                                        <i class="ti-package"></i>
                                        <span>{{ trans('lang.packages') }}</span>
                                    </a>
                                </li>
                            @endif
                        @elseif ($role === 'freelancer')

                        @if(Auth::user()->user_type == '' || Auth::user()->user_type != 2 )

                            <li class="menu-item-has-children">
                                <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                <a href="javascript:void(0)">
                                    <i class="ti-briefcase"></i>
                                    <span>{{ trans('lang.all_projects') }}</span>
                                </a>
                                <ul class="sub-menu">
                                    <li><hr><a href="{{{ url('freelancer/jobs/completed') }}}">{{ trans('lang.completed_projects') }}</a></li>
                                    <li><hr><a href="{{{ url('freelancer/jobs/cancelled') }}}">{{ trans('lang.cancelled_projects') }}</a></li>
                                    <li><hr><a href="{{{ url('freelancer/jobs/hired') }}}">{{ trans('lang.ongoing_projects') }}</a></li>
                                </ul>
                            </li>

                            

                            @if (Helper::getAccessType() == 'both' || Helper::getAccessType() == 'services')
                                <li class="menu-item-has-children">
                                    <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                    <a href="javascript:void(0)">
                                        <i class="ti-briefcase"></i>
                                        <span>{{ trans('lang.manage_services') }}</span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li><hr><a href="{{{ route('ServiceListing', ['status'=>'posted']) }}}">{{ trans('lang.posted_services') }}</a></li>
                               
                                    </ul>
                                </li>
                            @endif
                            
                            <li>
                                <a href="{{{ route('showFreelancerProposals') }}}">
                                    <i class="ti-bookmark-alt"></i>
                                    <span> {{ trans('lang.proposals') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{{ route('FreelancerPayoutsSettings') }}}">
                                    <i class="far fa-money-bill-alt"></i>
                                    <span> {{ trans('lang.payouts') }}</span>
                                </a>
                            </li>@endif
                            
                                <li class="menu-item-has-children">
                                    <span class="wt-dropdowarrow"><i class="lnr lnr-chevron-right"></i></span>
                                    <a href="javascript:void(0)">
                                        <i class="ti-file"></i>
                                        <span>{{ trans('lang.invoices') }}</span>
                                    </a>
                                    <ul class="sub-menu">
                                    @if ($payment_module === 'true' )
                                        <li><hr><a href="{{{ url('freelancer/package/invoice') }}}">{{ trans('lang.pkg_inv') }}</a></li>
                                    @endif
                                    <li><hr><a href="{{{ url('freelancer/project/invoice') }}}">{{ trans('lang.project_inv') }}</a></li>
                                    <li><hr><a href="{{{ url('freelancer/project/milestone_invoice') }}}">{{ trans('Milestones Invoices') }}</a></li>
                                    </ul>
                                </li>
                                @if ($payment_module === 'true' )
                                <li>
                                    <a href="{{{ url('dashboard/packages/'.$role) }}}">
                                        <i class="ti-package"></i>
                                        <span>{{ trans('lang.packages') }}</span>
                                    </a>
                                </li>
                            @endif
                        @endif
                        @if(Auth::user()->user_type == '' || Auth::user()->user_type != 2 )

                        <li>
                            <a href="{{{ url($role.'/saved-items') }}}">
                                <i class="ti-heart"></i>
                                <span>{{ trans('lang.saved_items') }}</span>
                            </a>
                        </li>
                        @endif
                    @endif
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('dashboard-logout-form').submit();">
                            <i class="lnr lnr-exit"></i>{{{trans('lang.logout')}}}
                        </a>
                        <form id="dashboard-logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
    @endif
@endauth
