@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <section class="wt-haslayout wt-dbsectionspace wt-insightuser dashboardcolums" id="dashboard">
        @if (Session::has('message'))
            <div class="flash_msg">
                <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
            </div>
            @php session()->forget('message');  @endphp
        @endif
        <div class="wt-haslayout">
            <div class="container">
                <div class="row">
                    <div class="wt-widget wt-categoriesholder wt-freelancers-sidebar">
                        <div class="wt-widgettitle">
                            <h2>Notifications</h2>
                        </div>
                        <div class="wt-widgetcontent">
                          
                            
                            <ul class="wt-categoriescontent">
                                            @php
                                                $role = Auth::user()->getRoleNames()->first();
                                            @endphp
                                            @foreach($notifications as $notification)  
                                            @php
                                                if (!empty($notification->read_at)) {
                                                   $class = "border border-bottom";
                                                   $role = " ";
                                                } else {
                                                   $class = "alert alert-success";
                                                   $role = "alert";
                                                }
                                            @endphp
                                                        <li class="notifications-list {{$class}}" role="{{$role}}">
                                                                @if($notification->type === "App\\Notifications\\Hired")
                                                                    <a href="{{$notification->data['Url']}}"><span>You have been hired for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\AccountUpdated")
                                                                    <a><span>Your Account has been updated. For further information, check your email.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\AdministrationsMessage")
                                                                    <a><span>Administration has send you a message. For further information, check your email.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\InvoicePaid")
                                                                    <a href="{{url('freelancer/job'.$notification->data['Slug'])}}"><span>{{$notification->data['Employer']}} has paid your invoice of amount Rs.{{$notification->data['Invoice']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\InvoiceAccepted")
                                                                    @php
                                                                        $status = 'completed';
                                                                     @endphp
                                                                    <a href="{{url('proposal/'.$notification->data['Slug'].'/'.$status)}}"><span>{{$notification->data['Freelancer']}} has accepted your invoice of amount Rs.{{$notification->data['Amount']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\DisputeRaised")
                                                                    @php
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                    @endphp
                                                                    <a href="{{url('dispute/details/'.$jobinfo->slug)}}"><span>{{$notification->data['Freelancer']}} has raised dispute against your job: {{$jobinfo->title}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\FreelancerApprovedOrder")
                                                                    @php
                                                                        $user = Auth::user()->id;
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                        $status = 'hired';
                                                                     @endphp
                                                                    <a href="{{url('proposal/'.$jobinfo->slug.'/'.$status)}}"><span>{{$notification->data['Freelancer']}} has approved order of amount Rs.{{$notification->data['Amount']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MilestoneAccepted")
                                                                    @php
                                                                        $status = 'hired';
                                                                    @endphp
                                                                    <a href="{{url('proposal/'.$notification->data['Slug']. '/' .$status)}}"><span>{{$notification->data['Freelancer']}} has accepted the milestone: {{$notification->data['Milestone']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MilestoneCompleted")
                                                                    <a href="{{url('freelancer/job/'.$notification->data['Slug'])}}"><span>Milestone: {{$notification->data['Milestone']}} for the job: {{$notification->data['Job']}} has been completed.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MilestoneReleaseRejected")
                                                                    <a href="{{url('freelancer/job/'.$notification->data['Slug'])}}"><span>{{$notification->data['Employer']}} has rejected release of the milestone: {{$notification->data['Milestone']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MilestoneReleaseRequest")
                                                                    @php
                                                                        $user = Auth::user()->id;
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                        $status = 'hired';
                                                                    @endphp
                                                                    <a href="{{url('proposal/'.$jobinfo->slug.'/'.$status)}}"><span>{{$notification->data['Freelancer']}} has requested to release the milestone: {{$notification->data['Milestone']}} for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\RequestToCancelProject")
                                                                    @php
                                                                        $user = Auth::user()->id;
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                        $status = 'hired';
                                                                    @endphp
                                                                    <a href="{{url('proposal/'.$jobinfo->slug.'/'.$status)}}"><span>{{$notification->data['Freelancer']}} has requested to cancel the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\RequestToCloseProject")
                                                                    @php
                                                                        $status = 'hired';
                                                                    @endphp
                                                                    <a href="{{url('proposal/'.$notification->data['Slug'].'/'.$status)}}"><span>{{$notification->data['Freelancer']}} has requested to close the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ResponseToCancelProject")
                                                                    @php
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                    @endphp
                                                                    <a href="{{url('freelancer/job/'.$jobinfo->slug)}}"><span>{{$notification->data['Employer']}} has rejected your requested to cancel the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ResponseToCancelProjectAccept")
                                                                    @php
                                                                        $status = 'cancelled';
                                                                    @endphp
                                                                    <a href="{{url('freelancer/jobs/'.$status)}}"><span>{{$notification->data['Employer']}} has accepted your requested to cancel the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ResponseToCloseProject")
                                                                    <a href="{{url('freelancer/job/'.$notification->data['Slug'])}}"><span>{{$notification->data['Employer']}} has reject your requested to close the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\CommitteeMemberAdd")   
                                                                    <a href="{{$notification->data['Url']}}"><span>You have been added as a committee member for the job: {{$notification->data['jobDetails']}}.Go to the Dashboard to view the job.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\CommitteeMemberDelete")   
                                                                    <a href="{{$notification->data['Url']}}"><span>You have been removed as a committee member for the job: {{$notification->data['jobDetails']}}.Go to the Dashboard</span>
                                                                @elseif($notification->type === "App\\Notifications\\MinutesMeetingNotification")
                                                                    <a href="{{$notification->data['Url']}}"><span>Minutes of meeting {{$notification->data['Meeting']}} have been created for the job.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\SubmitReport")
                                                                    <a><span>{{$notification->data['user']}} has submitted a report against {{$notification->data['subject']}}. The reason is that {{$notification->data['description']}}</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ProposalNegotiationsEmployer")
                                                                    @php
                                                                        $user = Auth::user()->id;
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                        $proposal = App\Proposal::where('job_id', $jobinfo->id)->where('freelancer_id', $user)->first();

                                                                    @endphp
                                                                    <a href="{{url('freelancer/proposal/'.$proposal->id.'/messages')}}"><span>Procurer: {{$notification->data['Sender']}} has sent you a message on your proposal against job: {{$notification->data['Job']}}. The message says: {{$notification->data['Message']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ProposalNegotiationsFreelancer")
                                                                    @php
                                                                        $jobinfo = App\Job::where('id', $notification->data['Job_id'])->first();
                                                                    @endphp   
                                                                    <a href="{{url('employer/dashboard/job/'.$jobinfo->slug.'/proposals')}}"><span>Bidder: {{$notification->data['Sender']}} has sent you a message on their proposal against job: {{$notification->data['Job']}}. The message says: {{$notification->data['Message']}}</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\AssignNewRole")
                                                                    <a><span>Dear {{$notification->data['Name']}}, KPITB Administration has updated your role. They have updated your role from {{$notification->data['Previous_Role']}} to {{$notification->data['New_Role']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ContactUsQuery")
                                                                    <a href="{{url('admin/contacts')}}"><span>You have received a new message from Contact Us Page.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\RejectedFreelancer")
                                                                    <a href="{{$notification->data['Url']}}"><span>You have been rejected for the job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\ProposalsReceived")
                                                                    <a href="{{url('employer/dashboard/job/' .$notification->data['slug']. '/proposals')}}"><span>You have received a new proposal for the job: {{$notification->data['Job']}} from {{$notification->data['first_name']}} {{$notification->data['second_name']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\JobUnpublished")
                                                                    <a href="{{url('employer/dashboard/manage-jobs')}}"><span>Administration has unpublished your job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\JobPublished")
                                                                    <a href="{{url('employer/dashboard/manage-jobs')}}"><span>Administration has published your job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MinutesOfMeetingAccept")
                                                                    <a href="{{url('add-minutes-of-meeting/' .$notification->data['Job_id'])}}"><span>{{$notification->data['First_name']}} {{$notification->data['Last_name']}} has accepted your minutes of meeting: {{$notification->data['Meeting']}} for your job: {{$notification->data['Job']}}.</span></a>
                                                                @elseif($notification->type === "App\\Notifications\\MinutesOfMeetingReject")
                                                                    <a href="{{url('add-minutes-of-meeting/' .$notification->data['Job_id'])}}"><span>{{$notification->data['First_name']}} {{$notification->data['Last_name']}} has rejected your minutes of meeting: {{$notification->data['Meeting']}} for your job: {{$notification->data['Job']}}.</span></a>
                                                                @endif
                                                                @if(!empty($notification->read_at))
                                                                @else
                                                                <form method="POST"  action="{{ route('markNotification') }}">
                                                                    @csrf
                                                                    <input type="hidden" id="id" name="id" value="{{{$notification->id}}}">
                                                                    <button type="submit" class="float-right mark-as-read">
                                                                        Mark as read
                                                                    </button>
                                                                </form>
                                                                @endif
                                                            </li>
                                            @endforeach                                                                                    
                                        </ul>

                                    </div>
                                </div>
                </div>
            </div> 
        </div>                
                                        
    </section>
    <script>
    function sendMarkRequest(id = null) {
        return $.ajax("{{ route('markNotification') }}", {
            method: 'POST',
            data: {
                _token,
                id
            }
        });
    }
    $(function() {
        $('.mark-as-read').click(function() {
            let request = sendMarkRequest($(this).data('id'));
            request.done(() => {
                $(this).parents('li.alert').remove();
            });
        });
        $('#mark-all').click(function() {
            let request = sendMarkRequest();
            request.done(() => {
                $('li.alert').remove();
            })
        });
    });
    </script>
@endsection
