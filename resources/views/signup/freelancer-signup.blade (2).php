@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ? 'extend.front-end.master' : 'front-end.master')
@section('content')
@php
    $employees      = Helper::getEmployeesList();
    $departments    = App\Department::all();
    $getAllOrgs    = App\OrganizationType::getAllOrgs();


    $roles          = Spatie\Permission\Models\Role::all()->toArray();
    $register_form = App\SiteManagement::getMetaValue('reg_form_settings');
    $reg_one_title = !empty($register_form) && !empty($register_form[0]['step1-title']) ? $register_form[0]['step1-title'] : trans('lang.join_for_good');
    $reg_one_subtitle = !empty($register_form) && !empty($register_form[0]['step1-subtitle']) ? $register_form[0]['step1-subtitle'] : trans('lang.join_for_good_reason');
    $reg_two_title = !empty($register_form) && !empty($register_form[0]['step2-title']) ? $register_form[0]['step2-title'] : trans('lang.pro_info');
    $reg_two_subtitle = !empty($register_form) && !empty($register_form[0]['step2-subtitle']) ? $register_form[0]['step2-subtitle'] : '';
    $term_note = !empty($register_form) && !empty($register_form[0]['step2-term-note']) ? $register_form[0]['step2-term-note'] : trans('lang.agree_terms');
    $reg_three_title = !empty($register_form) && !empty($register_form[0]['step3-title']) ? $register_form[0]['step3-title'] : trans('lang.almost_there');
    $reg_three_subtitle = !empty($register_form) && !empty($register_form[0]['step3-subtitle']) ? $register_form[0]['step3-subtitle'] : trans('lang.acc_almost_created_note');
    $register_image = !empty($register_form) && !empty($register_form[0]['register_image']) ? '/uploads/settings/home/'.$register_form[0]['register_image'] : 'images/work.jpg';
    $reg_page = !empty($register_form) && !empty($register_form[0]['step3-page']) ? $register_form[0]['step3-page'] : '';
    $reg_four_title = !empty($register_form) && !empty($register_form[0]['step4-title']) ? $register_form[0]['step4-title'] : trans('lang.congrats');
    $reg_four_subtitle = !empty($register_form) && !empty($register_form[0]['step4-subtitle']) ? $register_form[0]['step4-subtitle'] : trans('lang.acc_creation_note');
    $show_emplyr_inn_sec = !empty($register_form) && !empty($register_form[0]['show_emplyr_inn_sec']) ? $register_form[0]['show_emplyr_inn_sec'] : 'true';
    $show_reg_form_banner = !empty($register_form) && !empty($register_form[0]['show_reg_form_banner']) ? $register_form[0]['show_reg_form_banner'] : 'true';
    $reg_form_banner = !empty($register_form) && !empty($register_form[0]['reg_form_banner']) ? $register_form[0]['reg_form_banner'] : null;
    $selected_registration_type = !empty($register_form) && !empty($register_form[0]['registration_type']) ? $register_form[0]['registration_type'] : 'multiple';
    $breadcrumbs_settings = \App\SiteManagement::getMetaValue('show_breadcrumb');
    $show_breadcrumbs = !empty($breadcrumbs_settings) ? $breadcrumbs_settings : 'true';
@endphp
@php $breadcrumbs = Breadcrumbs::generate('registerPage'); @endphp
@if (file_exists(resource_path('views/extend/front-end/includes/inner-banner.blade.php')))
    @include('extend.front-end.includes.inner-banner',
        ['title' => trans('lang.join_for_free'), 'inner_banner' => $reg_form_banner, 'path' => 'uploads/settings/home/', 'show_banner' => $show_reg_form_banner ]
    )
@else
    @include('front-end.includes.inner-banner',
        ['title' =>  trans('lang.join_for_free'), 'inner_banner' => $reg_form_banner, 'path' => 'uploads/settings/home/', 'show_banner' => $show_reg_form_banner ]
    )
@endif

<div class="wt-haslayout wt-main-section">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-10 push-md-1 col-lg-8 push-lg-2" id="registration">
                <div class="preloader-section" v-if="loading" v-cloak>
                    <div class="preloader-holder">
                        <div class="loader"></div>
                    </div>
                </div>
                <div class="wt-registerformhold">
                    <div class="wt-registerformmain">
                        <div class="wt-joinforms">
                            <div class="kapra-register">
                                <form method="POST"  class="wt-formtheme wt-formregister demo1" @submit.prevent="checkSingleForm" id="register_form">
                                    @csrf
                                    <fieldset class="wt-registerformgroup">
                                        <div class="wt-haslayout" v-if="step === 1" v-cloak>
                                            <div class="wt-registerhead">
                                                <div class="wt-title">
                                                    <h3>{{{ $reg_one_title }}}</h3>
                                                </div>
                                                <div class="wt-description">
                                                    <p>{{{ $reg_one_subtitle }}}</p>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-half">
                                                <input type="text" name="first_name" class="form-control" placeholder="{{{ trans('lang.ph_first_name') }}}" v-bind:class="{ 'is-invalid': form_step1.is_first_name_error }" v-model="first_name">
                                                <span class="help-block" v-if="form_step1.first_name_error">
                                                    <strong v-cloak>@{{form_step1.first_name_error}}</strong>
                                                </span>
                                            </div>
                                            <div class="form-group form-group-half">
                                                <input type="text" name="last_name" class="form-control" placeholder="{{{ trans('lang.ph_last_name') }}}" v-bind:class="{ 'is-invalid': form_step1.is_last_name_error }" v-model="last_name">
                                                <span class="help-block" v-if="form_step1.last_name_error">
                                                    <strong v-cloak>@{{form_step1.last_name_error}}</strong>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <input id="user_email" type="email" class="form-control" name="email" placeholder="{{{ trans('lang.ph_email') }}}" value="{{ old('email') }}" v-bind:class="{ 'is-invalid': form_step1.is_email_error }" v-model="user_email">
                                                <span class="help-block" v-if="form_step1.email_error">
                                                    <strong v-cloak>@{{form_step1.email_error}}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </fieldset>
                                    {{-- <div class="wt-haslayout"> --}}
                                        <fieldset class="wt-registerformgroup">

                                            <div class="form-group form-group-half">
                                                <input id="password" type="password" class="form-control" name="password" placeholder="{{{ trans('lang.ph_pass') }}}" v-bind:class="{ 'is-invalid': form_step2.is_password_error }">
                                                <span class="help-block" v-if="form_step2.password_error">
                                                    <strong v-cloak>@{{form_step2.password_error}}</strong>
                                                </span>
                                            </div>
                                            <div class="form-group form-group-half">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{{ trans('lang.ph_retry_pass') }}}" v-bind:class="{ 'is-invalid': form_step2.is_password_confirm_error }">
                                                <span class="help-block" v-if="form_step2.password_confirm_error">
                                                    <strong v-cloak>@{{form_step2.password_confirm_error}}</strong>
                                                </span>
                                            </div>
                                        </fieldset>
                                        @if (!empty($locations))
                                            <fieldset class="wt-registerformgroup">
                                                <div class="form-group">
                                                    <span class="wt-select">
                                                        <select name="locations" id="locationsDropDown" class="form-control" v-bind:class = '{ "is-invalid": form_step2.is_locations_error }'>
                                                            <option selected="selected" value="">Select Province</option>
                                                            <?php
                                                                foreach($locations as $k=>$v):
                                                                    $new[$k] = $v['title'];
                                                                    $slug[$k] = $v['slug'];
                                                                    $id[$k] = $v['id'];
                                                                    ?>
                                                                    <option value="<?php echo $id[$k];?>"><?php echo $new[$k];?></option>
                                                                    <?php
                                                                endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block" v-if="form_step2.locations_error">
                                                            <strong v-cloak>@{{form_step2.locations_error}}</strong>
                                                        </span>
                                                    </span>
                                                </div>
                                            </fieldset>
                                        @endif
                                        @if (!empty($district))
                                            <fieldset class="wt-registerformgroup">
                                                <div class="form-group">
                                                    <span class="wt-select">
                                                        <select name="distDropDown" id="distDropDown" v-bind:class="{ 'is-invalid': form_step2.is_distDropDowns_error }">
                                                            <option selected="selected" value="">Select District</option>
                                                            <?php
                                                                foreach($district as $k=>$v):
                                                                    $district_name[$k] = $v['district_name'];
                                                                    $loc_id[$k] = $v['loc_id'];
                                                                    $id[$k] = $v['id'];
                                                                    ?>
                                                                    <option value="<?php echo $id[$k];?>"><?php echo $district_name[$k];?></option>
                                                                    <?php
                                                                endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block" v-if="form_step2.distDropDown_error">
                                                            <strong v-cloak>@{{form_step2.distDropDown_error}}</strong>
                                                        </span>
                                                    </span>
                                                </div>
                                            </fieldset>
                                        @endif
                                        <fieldset class="wt-registerformgroup">
                                            <div class="form-group form-group-half">
                                                <input type="text" id="contact-number" name="contact_number" class="form-control contact-number"  minlength="11" placeholder="Contact Number" autocomplete="off" placeholder="Contact Number" v-bind:class="{ 'is-invalid': form_step2.contact_number_error }">
                                                <span class="help-block" v-if="form_step2.contact_number_error">
                                                    <strong v-cloak>@{{form_step2.contact_number_error}}</strong>
                                                </span>
                                            </div>
                                            <div class="form-group form-group-half">
                                                <input type="text" name="address" class="form-control" placeholder="Address" v-bind:class="{ 'is-invalid': form_step2.is_address_error }">
                                                <span class="help-block" v-if="form_step2.address_error">
                                                    <strong v-cloak>@{{form_step2.address_error}}</strong>
                                                </span>
                                            </div>
                                        </fieldset>
                                        <fieldset class="wt-registerformgroup">
                                            <div class="form-group form-group-half">
                                               <p class="kapra_number_field">Enter KAPRA(KNTN) NO.</p>
                                            </div>
                                            <div class="form-group form-group-half">
                                                <input id="Kepranumber" type="text" name="kpra_no" class="form-control kapra-no" minlength="8" maxlength="9" placeholder="0003456-2"  autocomplete="off" v-bind:class="{ 'is-invalid': form_step2.is_kapra_error }"  />
                                                <span class="help-block" v-if="form_step2.kapra_error">
                                                    <strong v-cloak>@{{form_step2.kapra_error}}</strong>
                                                </span>
                                            </div>
                                        </fieldset>
                                        <fieldset class="wt-registerformgroup">
                                            <div class="form-group">
                                                <span class="wt-select" >
                                                    <select id="type" name="type"  v-bind:class="{ 'is-invalid': form_step2.is_type_error }">
                                                        <option selected="selected" value="">Select Type</option>
                                                        <option value="individual">Individual</option>
                                                        <option value="agency">Agency</option>
                                                    </select>
                                                    <span class="help-block" v-if="form_step2.type_error">
                                                        <strong v-cloak>@{{form_step2.type_error}}</strong>
                                                    </span>
                                                </span>
                                            </div>
                                        </fieldset>
                                        <input id="wt-company-2" type="hidden" name="role" value="freelancer" >
                                        <fieldset class="wt-termsconditions">
                                            <div class="wt-checkboxholder">
                                                <span class="wt-checkbox">
                                                    <input id="termsconditions" type="checkbox" name="termsconditions" checked="">
                                                    <label for="termsconditions"><span>{{{ $term_note }}}</span></label>
                                                    <span class="help-block" v-if="form_step2.termsconditions_error">
                                                        <strong style="color: red;" v-cloak>{{trans('lang.register_termsconditions_error')}}</strong>
                                                    </span>
                                                </span>
                                                <a href="#" @click.prevent="checkSingleForm('{{ trans('lang.email_not_config') }}')" class="wt-btn">{{{ trans('lang.submit') }}}</a>
                                            </div>
                                        </fieldset>
                                    {{-- </div> --}}
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="wt-registerformfooter">
                        <span>{{{ trans('lang.have_account') }}}<a id="wt-lg" href="javascript:void(0);" @click.prevent='scrollTop()'>{{{ trans('lang.btn_login_now') }}}</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('pagespecificscripts')
    <!-- flot charts scripts-->
    <script type="text/javascript">
    $("body").on("change", "#locationsDropDown", function () {
    var locId = $(this).val();
    $(".preloader-outer").show();
        $.ajax({
            type: 'POST',
            url: 'getAjaxDistrics',
            cache: false,
            dataType: 'html',
            data: {
                locId: locId,
                _token: '{{csrf_token()}}'
            },
            success: function (res) {
                $("#distDropDown").html(res);
                $(".preloader-outer").hide();
            }
        });
    });
    </script>
@stop