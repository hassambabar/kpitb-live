@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ? 'extend.front-end.master' : 'front-end.master')
@push('stylesheets')
@endpush
@if (Schema::hasTable('site_managements'))
    @php
    $app_description =  env('APP_DESCRIPTION');
    @endphp
    @section('title'){{ config('app.name') }} @stop
    @section('description', "$app_description")
        @section('content')
        <section class="signup_home home_signup_box">
          <div class="">
          <div class="container text-xs-center">
            <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3 cat-title">
            <div class="wt-sectionhead wt-textcenter">
                                    <div class="wt-sectiontitle">
                                        <h2>Become A Part Of The Community</h2>
                                        <span>Embrace the Freelance Revolution</span>
                                        <!-- <span>{{{ Helper::getHomeSection('cat_sec_subtitle') }}}</span> -->
                                    </div>
            </div>
            </div>
            </div>
          </div>
            <div  class="la-signup-page">
                        <section class="signup_home home_signup_box">
                            <div class="">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="wt-companydetails">
                                    <div class="wt-companycontent" style="padding-left: 130px;">
                                    <div style="padding-top:60px;">
                                        <img src="{{asset('/uploads/settings/general/Procurer.png')}}">
                                    </div>
                                    <div class="wt-companyinfotitle">
                                        <h2>Find A Job</h2>
                                    </div>
                                    <div class="wt-description">Take your First Step to a Propitious Future, Become a Part of the KP's IT Freelancing Community.</div>
                                    <div class="wt-btnarea"><a href="{{URL::to('/become-a-freelancer')}}" class="wt-btn btn1">SIGN UP</a>
                                    </div>
                                    </div>
                                    <div id="wrapper1"></div>
                                    <div class="wt-companycontent" style="padding-right: 130px;">
                                    <div  style="padding-top:60px;">
                                        <img src="{{asset('/uploads/settings/general/Provider.png')}}" style="margin-top: 5px;">
                                    </div>
                                    <div class="wt-companyinfotitle">
                                        <h2>Post A Job</h2>
                                    </div>
                                    <div class="wt-description">
                                    Join our Network to Collaborate with KP's Most Talented Independent Professionals to get your Job Done.
                                    </div>
                                    <div class="wt-btnarea">
                                        <a href="{{URL::to('/become-an-employee')}}" class="wt-btn btn2">SIGN UP</a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    </div>
                        </section>
                    </div>
                    </div>

                </section>
    @endsection
@endif
@push('scripts')
@endpush