<header id="wt-header" class="wt-header wt-haslayout wt-headervtwo">
@if (!empty(Route::getCurrentRoute()) && Route::getCurrentRoute()->uri() != '/' && Route::getCurrentRoute()->uri() != 'home')
    <div class="wt-navigationarea wt-navigationarea1">

        <div class="container-fluid">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    @auth

                        {{ Helper::displayEmailWarning() }}

                    @endauth

                    @if (!empty($logo) || Schema::hasTable('site_managements'))

                        <strong class="wt-logo"><a href="{{{ url('/') }}}"><img src="{{{ asset($logo) }}}" alt="{{{ trans('Logo') }}}"></a></strong>

                    @endif

                    @if (!empty(Route::getCurrentRoute()) && Route::getCurrentRoute()->uri() != '/' && Route::getCurrentRoute()->uri() != 'home')

                        <search-form

                        :placeholder="'{{ trans('lang.looking_for') }}'"

                        :freelancer_placeholder="'{{ trans('lang.search_filter_list.freelancer') }}'"

                        :employer_placeholder="'{{ trans('lang.search_filter_list.employers') }}'"

                        :job_placeholder="'{{ trans('lang.search_filter_list.jobs') }}'"

                        :service_placeholder="'{{ trans('lang.search_filter_list.services') }}'"

                        :no_record_message="'{{ trans('lang.no_record') }}'"

                        >

                        </search-form>

                    @endif

                    <div class="wt-rightarea wt-rightarea1">

                        @if (file_exists(resource_path('views/extend/includes/menu.blade.php')))

                            @include('extend.includes.menu', ['page_order' => $page_order])

                        @else

                            @include('includes.menu', ['page_order' => $page_order])

                        @endif

                        @guest

                            <div class="wt-loginarea">

                                <div class="wt-loginoption">

                                    <a href="javascript:void(0);" id="wt-loginbtn" class="wt-loginbtn">{{{trans('lang.login') }}}</a>

                                    <div class="wt-loginformhold" @if ($errors->has('email') || $errors->has('password')) style="display: block;" @endif>

                                        <div class="wt-loginheader">

                                            <span>{{{ trans('lang.login') }}}</span>

                                            <a href="javascript:;"><i class="fa fa-times"></i></a>

                                        </div>

                                        <form method="POST" action="{{ route('login') }}" class="wt-formtheme wt-loginform do-login-form">

                                            @csrf

                                            <fieldset>

                                                <div class="form-group">

                                                    <input id="email1" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"

                                                        placeholder="Email" required autofocus>

                                                    @if ($errors->has('email'))

                                                    <span class="invalid-feedback" role="alert">

                                                        <strong>{{ $errors->first('email') }}</strong>

                                                    </span>

                                                    @endif

                                                </div>

                                                <div class="form-group">

                                                    <input id="password" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"

                                                        placeholder="Password" required>

                                                    @if ($errors->has('password'))

                                                    <span class="invalid-feedback" role="alert">

                                                        <strong>{{ $errors->first('password') }}</strong>

                                                    </span>

                                                    @endif

                                                </div>

                                                <div class="wt-logininfo">

                                                        <button type="submit" class="wt-btn do-login-button">{{{ trans('lang.login') }}}</button>

                                                    <span class="wt-checkbox">

                                                        <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label for="remember">{{{ trans('lang.remember') }}}</label>

                                                    </span>

                                                </div>

                                            </fieldset>

                                            <div class="wt-loginfooterinfo">

                                                @if (Route::has('password.request'))

                                                    <a href="{{ route('password.request') }}" class="wt-forgot-password">{{{ trans('lang.forget_pass') }}}</a>

                                                @endif

                                                <a href="{{URL::to('/signup')}}">{{{ trans('lang.create_account') }}}</a>

                                            </div>


                                        </form>

                                    </div>

                                </div>

                                <a href="{{{ route('signup') }}}" class="wt-btn">SIGN UP NOW</a>

                            </div>

                        @endguest

                        @auth

                            @php

                                $user = !empty(Auth::user()) ? Auth::user() : '';

                                $role = !empty($user) ? $user->getRoleNames()->first() : array();

                                $profile = \App\User::find(Auth::user()->id)->profile;

                                $user_image = !empty($profile) ? $profile->avater :  'images/user-login.png';

                                $employer_job = \App\Job::select('status')->where('user_id', Auth::user()->id)->first();

                                $profile_image = !empty($user_image) ? '/uploads/users/'.$user->id.'/'.$user_image : 'images/user-login.png';

                                $payment_settings = \App\SiteManagement::getMetaValue('commision');

                                $payment_module = !empty($payment_settings) && !empty($payment_settings[0]['enable_packages']) ? $payment_settings[0]['enable_packages'] : 'true';

                                $employer_payment_module = !empty($payment_settings) && !empty($payment_settings[0]['employer_package']) ? $payment_settings[0]['employer_package'] : 'true';

                                $class=" ";
                                $count = 0;
                                if (Auth::user()) {
                                    $notifications = Auth::user()->notifications;
                                    if (!empty($notifications)) {
                                        foreach ($notifications as $notification) {
                                            if (!empty($notification->read_at)) {
                                                $class = " ";
                                            } else {
                                                $class = "wt-insightnotificon-bell";
                                                $count++;
                                            }
                                        }
                                    } else {
                                        $class = " ";
                                    }
                                }
                                else{
                                    $class = " ";
                                }

                            @endphp
                            <div class="wt-userlogedin">
                                            <a href="{{ route('notifications') }}"><i class="fas fa-bell bell-notification"><span class="{{$class}}">@if($count > 0){{$count}}@endif</span></i></a>
                            </div>

                                <div class="wt-userlogedin">

                                    <figure class="wt-userimg">

                                        <img src="{{{ asset(Helper::getImage('uploads/users/' . Auth::user()->id, $profile->avater, '' , 'user.jpg')) }}}" alt="{{{ trans('lang.user_avatar') }}}">

                                    </figure>

                                    <div class="wt-username">

                                        <h3>{{{ Helper::getUserName(Auth::user()->id) }}}</h3>
                                      
                                       
                                        <span>{{{ $role }}}</span>
                                       
                                    
                                    </div>

                                    @if (file_exists(resource_path('views/extend/back-end/includes/profile-menu.blade.php')))

                                        @include('extend.back-end.includes.profile-menu', ['styling' => $page_header_styling])

                                    @else

                                        @include('back-end.includes.profile-menu', ['styling' => $page_header_styling])

                                    @endif

                                </div>

                        @endauth

                        @if (!empty(Route::getCurrentRoute()) && Route::getCurrentRoute()->uri() != '/' && Route::getCurrentRoute()->uri() != 'home')
                            <div class="wt-respsonsive-search"><a href="javascript:;" class="wt-searchbtn"><i class="fa fa-search"></i></a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- home menu -->
@else
<div class="wt-navigationarea">

<div class="container-fluid">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            @auth

                {{ Helper::displayEmailWarning() }}

            @endauth

            @if (!empty($logo) || Schema::hasTable('site_managements'))

                <strong class="wt-logo"><a href="{{{ url('/') }}}"><img src="{{{ asset($logo) }}}" alt="{{{ trans('Logo') }}}"></a></strong>

            @endif





            <div class="wt-rightarea">

                @if (file_exists(resource_path('views/extend/includes/menu.blade.php')))

                    @include('extend.includes.menu', ['page_order' => $page_order])

                @else

                    @include('includes.menu', ['page_order' => $page_order])

                @endif

                @guest

                    <div class="wt-loginarea">

                        <div class="wt-loginoption">

                            <a href="javascript:void(0);" id="wt-loginbtn" class="wt-loginbtn">{{{trans('lang.login') }}}</a>

                            <div class="wt-loginformhold" @if ($errors->has('email') || $errors->has('password')) style="display: block;" @endif>

                                <div class="wt-loginheader">

                                    <span>{{{ trans('lang.login') }}}</span>

                                    <a href="javascript:;"><i class="fa fa-times"></i></a>

                                </div>

                                <form method="POST" action="{{ route('login') }}" class="wt-formtheme wt-loginform do-login-form">

                                    @csrf

                                    <fieldset>

                                        <div class="form-group">

                                            <input id="email" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"

                                                placeholder="Email" required autofocus>

                                            @if ($errors->has('email'))

                                            <span class="invalid-feedback" role="alert">

                                                <strong>{{ $errors->first('email') }}</strong>

                                            </span>

                                            @endif

                                        </div>

                                        <div class="form-group">

                                            <input id="password" type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"

                                                placeholder="Password" required>

                                            @if ($errors->has('password'))

                                            <span class="invalid-feedback" role="alert">

                                                <strong>{{ $errors->first('password') }}</strong>

                                            </span>

                                            @endif

                                        </div>

                                        <div class="wt-logininfo">

                                                <button type="submit" class="wt-btn do-login-button">{{{ trans('lang.login') }}}</button>

                                            <span class="wt-checkbox">

                                                <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label for="remember">{{{ trans('lang.remember') }}}</label>

                                            </span>

                                        </div>

                                    </fieldset>

                                    <div class="wt-loginfooterinfo">

                                        @if (Route::has('password.request'))

                                            <a href="{{ route('password.request') }}" class="wt-forgot-password">{{{ trans('lang.forget_pass') }}}</a>

                                        @endif

                                        <a href="{{URL::to('/signup')}}">{{{ trans('lang.create_account') }}}</a>

                                    </div>


                                </form>

                            </div>

                        </div>

                        <a href="{{{ route('signup') }}}" class="wt-btn">SIGN UP NOW</a>

                    </div>

                @endguest

                @auth

                    @php

                        $user = !empty(Auth::user()) ? Auth::user() : '';

                        $role = !empty($user) ? $user->getRoleNames()->first() : array();

                        $profile = \App\User::find(Auth::user()->id)->profile;

                        $user_image = !empty($profile) ? $profile->avater : '';

                        $employer_job = \App\Job::select('status')->where('user_id', Auth::user()->id)->first();

                        $profile_image = !empty($user_image) ? '/uploads/users/'.$user->id.'/'.$user_image : 'images/user-login.png';

                        $payment_settings = \App\SiteManagement::getMetaValue('commision');

                        $payment_module = !empty($payment_settings) && !empty($payment_settings[0]['enable_packages']) ? $payment_settings[0]['enable_packages'] : 'true';

                        $employer_payment_module = !empty($payment_settings) && !empty($payment_settings[0]['employer_package']) ? $payment_settings[0]['employer_package'] : 'true';
                        
                        $class=" ";
                                $count = 0;
                                if (Auth::user()) {
                                    $notifications = Auth::user()->notifications;
                                    if (!empty($notifications)) {
                                        foreach ($notifications as $notification) {
                                            if (!empty($notification->read_at)) {
                                                $class = " ";
                                            } else {
                                                $class = "wt-insightnotificon-bell";
                                                $count++;
                                            }
                                        }
                                    } else {
                                        $class = " ";
                                    }
                                }
                                else{
                                    $class = " ";
                                }

                    @endphp
                            <div class="wt-userlogedin">
                                <a href="{{ route('notifications') }}"><i class="fas fa-bell bell-notification "><span class="{{$class}}">@if($count > 0){{$count}}@endif</span></i></a>
                            </div>

                        <div class="wt-userlogedin">

                            <figure class="wt-userimg">

                                <img src="{{{ asset(Helper::getImage('uploads/users/' . Auth::user()->id, $profile->avater, '' , 'user.jpg')) }}}" alt="{{{ trans('lang.user_avatar') }}}">

                            </figure>

                            <div class="wt-username">

                                <h3>{{{ Helper::getUserName(Auth::user()->id) }}}</h3>

                                <span>{{{ $role }}}</span>

                            </div> 
                            

                            @if (file_exists(resource_path('views/extend/back-end/includes/profile-menu.blade.php')))

                                @include('extend.back-end.includes.profile-menu', ['styling' => $page_header_styling])

                            @else

                                @include('back-end.includes.profile-menu', ['styling' => $page_header_styling])

                            @endif

                        </div>

                @endauth

                @if (!empty(Route::getCurrentRoute()) && Route::getCurrentRoute()->uri() != '/' && Route::getCurrentRoute()->uri() != 'home')
                    <div class="wt-respsonsive-search"><a href="javascript:;" class="wt-searchbtn"><i class="fa fa-search"></i></a></div>
                @endif
            </div>
        </div>
    </div>
    </div>
</div>

@endif

</header>



    