<div class="wt-userrating wt-userratingvtwo">
    <div class="wt-ratingtitle">
       <h3>{{ !empty($symbol) ? $symbol['symbol'] : '$' }}{{{$service->price}}}</h3>
    </div>
    <div class="wt-rating-info">
       <ul class="wt-service-info">
          
          <li>
             <span><i class="fa fa-search iconcolor2"></i><strong>{{{$service->views}}}</strong>&nbsp;{{ trans('lang.views') }}</span>
          </li>
          
       </ul>
       
    </div>
 </div>