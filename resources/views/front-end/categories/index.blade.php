@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ? 
'extend.front-end.master':
 'front-end.master', ['body_class' => 'wt-innerbgcolor'] )
@section('content')
    @php $breadcrumbs = Breadcrumbs::generate('categoriesListing'); @endphp
    <div class="wt-haslayout wt-main-section" id="jobs">
        @if (Session::has('payment_message'))
            @php $response = Session::get('payment_message') @endphp
            <div class="flash_msg">
                <flash_messages :message_class="'{{{$response['code']}}}'" :time ='5' :message="'{{{ $response['message'] }}}'" v-cloak></flash_messages>
            </div>
        @endif
        
        <div class="wt-haslayout">
        <div class="wt-categories-header">
                                <div class="wt-sectiontitle wt-categorieswidgettitle">
                                    <h2>{{ trans('Browse All Job Categories:') }}</h2>
                                </div>
                            </div>
                            <section class="wt-haslayout wt-main-section category-section">
                    <div class="container">
                        <div class="row justify-content-md-center">
            <div class="container">
                <div class="row">
                    <div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
                        <!-- <div class="col-xs-12 col-sm-12 col-md-7 col-lg-9 col-xl-9 float-left"> -->
                            <!-- <div class="wt-companysinfoholder"> -->
                                <!-- <div class="row"> -->
                                <div class="wt-categoryexpl">
                                @php
                               // $categories = App\Category::get()->where('parent',0);  
                                @endphp    
                                    @foreach($categories as $category)
                                    <div class="col-12 col-sm-6 col-md-12 col-lg-12 col-xl-12 float-left categoryrow">
                                                <h3 class="wt-sectiontitle"><a href="{{{url('search-results?type='.$type.'&category%5B%5D='.$category->slug)}}}">{{{ $category->title }}}</a></h3>
                                            {{-- {{$category->getChildCategories($category->id)}} --}}
                                        @if(count($category->getChildCategories($category->id)) > 0)
                                        <div class="row">
                                            @foreach ($category->getChildCategories($category->id) as $item => $value)
                                            <div class="col-md-3 columcategory">
                                               <a href="{{{url('search-results?type='.$type.'&category%5B%5D='.$value->slug)}}}" >{{$value->title}}</a>({{count($value->jobstotal)}})
                                            </div>    
                                            @endforeach
                                        </div>
                                        @endif
                                            
                                    </div>
                                    @endforeach
                                    </div>
                                <!-- </div> -->
                            <!-- </div> -->
                            <!-- </div> -->
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .categoryrow {
            margin: 10px 0px;
        }
        .categoryrow a{
            padding: 5px 5px;
    margin: 0px 5px;
        }
       
        .categoryrow .wt-sectiontitle{
            margin-bottom: 20px;
        }
        .columcategory a{
            font-size: 17px;
        }
        </style>
@endsection