@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ?
'extend.front-end.master':
 'front-end.master')
@push('sliderStyle')
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
@endpush
@push('stylesheets')
    <link href="{{ asset('css/prettyPhoto-min.css') }}" rel="stylesheet">
@endpush
@section('title')
        @if ($home == false)
            {{ $page['title'] }}
        @else
            {{ config('app.name') }}
        @endif
    @stop
@section('description', "$meta_desc")
@if ($slider_order == 0)
    @if ($slider_style == 'style2' || $slider_style == 'style3')
        @section('homeSlider')
            <div id="slider">
                <div v-if="sliderSkeleton">
                    <slider-skeleton/>
                </div>
                <div v-else>
                    @if ($slider_style == 'style2')
                        <second-slider
                            :page_id="{{$page['id']}}">
                        </second-slider>
                        @elseif ($slider_style == 'style3')
                        <third-slider
                        :page_id="{{$page['id']}}">
                    </third-slider>
                    @endif
                </div>
            </div>
        @endsection
    @endif
@endif
@section('content')
    @if ($home == false)
        @php $breadcrumbs = Breadcrumbs::generate('showPage',$page, $slug); @endphp
        @if (file_exists(resource_path('views/extend/front-end/includes/inner-banner.blade.php')))
            @include('extend.front-end.includes.inner-banner',
                ['title' => $page['title'], 'inner_banner' => '', 'pageType' => 'showPage', 'show_banner' => $show_banner_image]
            )
        @else
            @include('front-end.includes.inner-banner',
                ['title' =>  $page['title'], 'inner_banner' => '', 'pageType' => 'showPage', 'show_banner' => $show_banner_image]
            )
        @endif
    @endif

    <div id="pages-list">
        @if (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time ='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
            @php session()->forget('error'); @endphp
        @endif
        @if ($home == false)
            @if ($show_banner_image == false && !empty($page['title']) && $show_title == true)
                <div class="wt-innerbannercontent wt-without-banner-title">
                    <div class="wt-title">
                        <h2>{{{ $page['title'] }}}</h2>
                    </div>
                </div>
            @endif
        @endif
        @if (!empty($page))
            @if (!empty($sections))
                <show-new-page
                :page_id="'{{$page['id']}}'"
                :access_type="'{{$type}}'"
                :symbol="'{{ !empty($symbol['symbol']) ? $symbol['symbol'] : '$' }}'"
                :auth_role="'{{Auth::user() ? Auth::user()->getRoleNames()[0] : 'false'}}'"
                :slider_style= "'{{$slider_style}}'"
                >
                </show-new-page>
            @endif
            @if (!empty($description && $description != 'null'))
                <div class="dc-contentwrappers">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left">
                                <div class="dc-howitwork-hold dc-haslayout">
                                    <div class="dc-haslayout dc-main-section">
                                        @php echo htmlspecialchars_decode(stripslashes($description)); @endphp
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @else
            @if (file_exists(resource_path('views/extend/errors/404.blade.php')))
                @include('extend.errors.404')
            @else
                @include('errors.404')
            @endif
        @endif
        @php
            $page_footer = Helper::getPageFooter($page['id']);
        @endphp
        @if($home == true)

        <section class="wt-haslayout wt-main-section category-section category-section1">
                    <div class="container container-cat">
                        <div class="row justify-content-md-center">
                            <div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3">
                                <div class="wt-sectionhead wt-textcenter">
                                    <div class="wt-sectiontitle">
                                        <h2>Browse Jobs By Categories </h2>
                                        <span>Empowering Clients and Freelancers to do their Best Work</span>
                                    </div>
                                </div>
                            </div>
                            <div class="wt-categoryexpl">
                            @php
                            $categories = App\Category::get()->take(7);
                            @endphp
                                @foreach ($categories as $category)
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 float-left">
                                        <div class="wt-categorycontent">
                                            <figure><img src="{{{ asset(Helper::getCategoryImage($category->image)) }}}" alt="{{{ $category->title }}}"></figure>
                                            <div class="wt-cattitle">
                                                <h3><a href="{{{url('search-results?type='.$type.'&category%5B%5D='.$category->slug)}}}">{{{ $category->title }}}</a></h3>
                                            </div>
                                            <div class="wt-categoryslidup">
                                                @if (!empty($category->abstract))
                                                    <p>{{{ str_limit($category->abstract, 82) }}}</p>
                                                @endif
                                                <a href="{{{url('search-results?type='.$type.'&category%5B%5D='.$category->slug)}}}">{{ trans('lang.explore') }} <i class="fa fa-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 float-left">

                                        <li class="wt-morecategory wt-categorycontent1">
                                            <div class="">
                                            <div class="wt-cattitlevtwo">
                                                <h4><a href="javascrip:void(0);">Explore All Categories</a></h4>
                                            </div>
                                            <div class="wt-btnarea">
                                                <a href="{{URL::to('/categories')}}" class="wt-btn wt-btn-2">Show All</a>
                                            </div>
                                            </div>
                                        </li>
                                </div>
                            </div>
                        </div>
                    </div>
        </section>

        <div class="fourServices section-padding" id="recent">
            <div class="container text-xs-center">
            <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3 cat-title">
            <div class="wt-sectionhead wt-textcenter">
                    <div class="wt-sectiontitle">
                         <h2>Get Your Job Done In The Most Convenient And Controlled Way</h2>
                     </div>
            </div>
            </div>
            </div>
            </div>
                    <div class="clearfix"></div>
                    <div class="container">
                    <div class="row">
                    <div class="col-lg-3 col-md-6">
                    <div class="fourBlock text-xs-center blue-color">
                        <img class="img1" src="../uploads/project.png" alt="post project to hire freelancer">
                        <h4 class="bold">Post a Project</h4>
                    <div class="divider">
                    <div class="hr-line"></div>
                    </div>
                        <p>Post a project and start receiving proposals.</p>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                    <div class="fourBlock text-xs-center blue-color">
                        <img class="img1" src="../uploads/hiring.png" alt="search freelancers and hire online">
                        <h4 class="bold">Find &amp; Hire</h4>
                    <div class="divider">
                    <div class="hr-line"></div>
                    </div>
                        <p>Hire the best freelancer for your project.</p>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                    <div class="fourBlock text-xs-center blue-color">
                        <img class="img1" src="../uploads/award.png" alt="pay freelancer">
                        <h4 class="bold">Award a Project</h4>
                    <div class="divider">
                    <div class="hr-line"></div>
                    </div>
                        <p>Grant a project to a freelancer.</p>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                    <div class="fourBlock text-xs-center blue-color">
                        <img class="img1" src="../uploads/payment-method.png" alt="work with freelancer">
                        <h4 class="bold">Approve &amp; Pay</h4>
                    <div class="divider">
                    <div class="hr-line"></div>
                    </div>
                        <p>Interact with your freelancer and release the payment when satisfied.</p>
                    </div>
                    </div>
                    </div>
                    </div>
            </div>


        <section class="signup_home home_signup_box">
          <div class="">
          <div class="container text-xs-center">
            <div class="row justify-content-md-center">
            <div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3 cat-title">
            <div class="wt-sectionhead wt-textcenter">
                                    <div class="wt-sectiontitle">
                                        <h2>Become A Part Of The Community</h2>
                                        <span>Embrace the Freelance Revolution</span>
                                        <!-- <span>{{{ Helper::getHomeSection('cat_sec_subtitle') }}}</span> -->
                                    </div>
            </div>
            </div>
            </div>
          </div>
            <div  class="la-signup-page">
                        <section class="signup_home home_signup_box">
                            <div class="">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="wt-companydetails" style="padding-bottom: 20px;">
                                    <div class="wt-companycontent wt-joinasfreelancer">
                                    <div style="padding-top:60px;">
                                        <img src="{{asset('/uploads/settings/general/Procurer.png')}}">
                                    </div>
                                    <div class="wt-companyinfotitle">
                                        <h2>Find A Job</h2>
                                    </div>
                                    <div class="wt-description">Take your First Step to a Propitious Future, Become a Part of the KP's IT Freelancing Community.</div>
                                    <div class="wt-btnarea"><a href="{{URL::to('/become-a-freelancer')}}" class="wt-btn btn1">SIGN UP</a>
                                    </div>
                                    </div>
                                    <div id="wrapper1"></div>
                                    <div class="wt-companycontent wt-joinasemployer">
                                    <div  style="padding-top:60px;">
                                        <img src="{{asset('/uploads/settings/general/Provider.png')}}" style="margin-top: 5px;">
                                    </div>
                                    <div class="wt-companyinfotitle">
                                        <h2>Post a Job</h2>
                                    </div>
                                    <div class="wt-description">Join our Network to Collaborate with KP's Most Talented Independent Professionals to get your Job Done.</div>
                                    <div class="wt-btnarea">
                                        <a href="{{URL::to('/become-an-employee')}}" class="wt-btn btn2">SIGN UP</a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    </div>
                        </section>
                    </div>
                    </div>

                </section>


                     <section class="wt-haslayout wt-main-section category-section ternding-bg">
                        <div class="container">
                        <div class="row justify-content-md-center">
                            <div class="col-xs-12 col-sm-12 col-md-8 push-md-2 col-lg-6 push-lg-3">
                                <div class="wt-sectionhead wt-textcenter">
                                    <div class="wt-sectiontitle cat-title">
                                        <h2>Explore Our Trending Insights</h2>
                                    </div>
                                </div>
                            </div>

                            <!-- ARTICLES SECTION -->
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left">
                            <div class="wt-articlesholder">
                            <div class="row">
                            @php
                            $first = true;
                            @endphp
                            @foreach ( $articles as $key => $article )
                                    @if ( $first )
                                    <!-- FIRST ARTICLE -->
                                    <div class="col-sm-12 col-md-5">
                                        <div class="wt-articles wt-articles1">
                                            <figure class="wt-articleimg wt-articleimg1"><img src="{{{asset(Helper::getImage('uploads/articles', $article->banner))}}}" alt="image description"></figure>
                                            <div class="wt-articlecontents wt-articlecontents1">
                                            <span class="time-articles"><i class="icon-articles lnr lnr-calendar-full"></i>{{ \Carbon\Carbon::parse($article->updated_at)->format('M d, Y')}}</span>
                                            <div class="wt-title">
                                                <h3><a href="{{{ url('article/'.$article->slug) }}}">{{{ str_limit($article->title, 58) }}}</a></h3>

                                            <div class="wt-description article-description">

                                              
                                                      
                                                     {{-- echo htmlspecialchars_decode( stripslashes (str_limit(preg_replace("/<img[^>]+\>/i", " ", $article->description), 400))); --}}
                                                    {!! strip_tags(htmlspecialchars_decode(str_limit($article->description, 800))) !!}
                                              
                                            </div>
                                            </div>
                                            <div class="wt-btnarea btn-read-more">
                                                <a href="{{{ url('article/'.$article->slug) }}}" class="wt-btn wt-btn-2">Read More</a>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    @php
                                    $first = false;
                                    @endphp
                                    <!-- REMAINING 4 ARTICLES $articles_description = htmlspecialchars_decode( stripslashes (str_limit($content, 400)));-->
                                    <div class="col-sm-12 col-md-7">
                                    <div class="wt-articlesholder">
                                    <div class="row">
                                @else
                                <!-- EACH ARTICLE -->
                                    <div class="col-sm-12 col-md-6 article-margin">
                                        <div class="wt-articles">
                                            <figure class="wt-articleimg wt-articlesimg1"><img src="{{{asset(Helper::getImage('uploads/articles', $article->banner))}}}" alt="image description"></figure>
                                            <div class="wt-articlecontents">
                                            <span class="time-articles"><i class="icon-articles lnr lnr-calendar-full"></i>{{ \Carbon\Carbon::parse($article->updated_at)->format('M d, Y')}}</span>
                                            <div class="wt-title">
                                                <h3><a href="{{{ url('article/'.$article->slug) }}}">{{{ str_limit($article->title, 40) }}}</a></h3>
                                            </div>

                                            </div>
                                        </div>
                                    </div>

                                @endif

                            @endforeach
                                    </div>
                                    </div>
                                    </div>
                                    <!-- REMAINING 4 ARTICLES END -->
                        </div>
                        </div>
                        </div>
                        <!-- ARTICLE SECTION END -->
                    </div>
                    </div>
                </section>


        <section class="wt-haslayaout wt-main-section wt-footeraboutus">
        <div class="col-xs-12 col-sm-12 col-md-12 push-md-12 col-lg-12 push-lg-12">
        <div class="wt-sectionhead wt-textcenter">
                                    <div class="wt-sectiontitle">
                                        <h2>Are You Still Looking?</h2>
                                        <span>Explore our Freelancers</span>
                                        <!-- <span>{{{ Helper::getHomeSection('cat_sec_subtitle') }}}</span> -->
                                    </div>
            </div>
                            </div>
                            <div class="container">
                        <div class="row">
                
                            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                                <div class="wt-widgetskills">
                                    <div class="wt-fwidgettitle">
                                        <h3 style="margin-bottom:30px;" class="footer_heading">By Budget</h3>
                                        @foreach (Helper::getHourlyRate() as $key => $hourly_rate)
                                            @php $checked = ( !empty($_GET['hourly_rate']) && in_array($key, $_GET['hourly_rate'])) ? 'checked' : '' @endphp
                                            <p class="top-skils-para"><a target="_blank" class="top-skill-anchor" href="{{URL::to('/search-results?type=freelancer&hourly_rate[]='.$key)}}">{{ $hourly_rate }}</a></p>
                                        @endforeach

                                    </div>

                                </div>
                            </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="wt-footercol wt-widgetcategories">
                                <div class="wt-fwidgettitle">
                                  <h3  style="margin-bottom:30px;" class="footer_heading">By Skills</h3>
                                  @foreach($skills as $skill)
                                  <p class="top-skils-para"><a target="_blank" class="top-skill-anchor" href="{{URL::to('/search-results?type=freelancer&skills[]='.$skill->slug)}}">{{$skill->title}}</a></p>
                                  @endforeach
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="wt-widgetbylocation">
                                <div class="wt-fwidgettitle">
                                  <h3 style="margin-bottom:30px;" class="footer_heading">By Type</h3>
                                  @foreach (Helper::getFreelancerLevelList() as $key => $freelancer_skills)
                                    @php $checked = ( !empty($_GET['freelancer_skills']) && in_array($key, $_GET['freelancer_skills'])) ? 'checked' : '' @endphp
                                        <p class="top-skils-para"><a target="_blank" class="top-skill-anchor" href="{{URL::to('/search-results?type=freelancer&freelancer_skills[]=' .$key)}}">{{ $freelancer_skills }}</a></p>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                        <!-- <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="wt-widgetbylocation">
                                <div class="wt-fwidgettitle">
                                  <h3 style="margin-bottom:30px;" class="footer_heading">By Locations</h3>
                                  @foreach($locations as $location)
                                  <p class="top-skils-para"><a target="_blank" class="top-skill-anchor" href="{{URL::to('/search-results?type=freelancer&locations[]='.$location->slug)}}">{{$location->title}}</a></p>
                                  @endforeach
                                </div>

                            </div>
                        </div> -->
                </div>

            </div>
        </section>
        @endif
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/prettyPhoto-min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    @if ($page_header == 'style5')
        @if (empty($slider_style))
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style5')
            </script>
        @elseif (!empty($slider_style) && $slider_order != 0)
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style5')
            </script>
        @endif
    @elseif ($page_header == 'style3')
        @if (empty($slider_style))
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style3')
            </script>
        @elseif ($slider_style != 'style3')
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style3')
            </script>
        @endif
    @endif
    @if ($slider_style == 'style2')
        {{-- <script>
            jQuery('#wt-header').addClass('wt-headervthhree')
            jQuery('#wt-header').removeClass('wt-headervtwo')
            jQuery('.wt-formtheme.wt-formbanner.wt-formbannervtwo').remove()
        </script> --}}
        
    @else
    @endif
    <script src="{{ asset('js/tilt.jquery.js') }}"></script>
@endpush
