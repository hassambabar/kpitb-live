@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ?
'extend.front-end.master':
 'front-end.master')
@push('sliderStyle')
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
@endpush
@push('stylesheets')
    <link href="{{ asset('css/prettyPhoto-min.css') }}" rel="stylesheet">
@endpush
@section('title')
      
          Contact us |  {{ config('app.name') }}
    @stop
@section('description', "Contact us and ask your questions, call us, or find us on Twitter, Facebook, YouTube, LinkedIn, Google+, or Flickr.")

@section('content')
        @php $breadcrumbs = Breadcrumbs::generate('showPage','', 'contact-us'); @endphp
            {{-- @include('extend.front-end.includes.inner-banner',
                ['title' => 'Contact us', 'inner_banner' => '', 'pageType' => 'showPage', 'show_banner' => $show_banner_image]
            ) --}}
        
    <div id="pages-list">
       
                <div class="wt-innerbannercontent wt-without-banner-title">
                    {{-- <div class="wt-title">
                        <h2>Please get in touch and our expert support team will answer all your questions.
                        </h2>
                    </div> --}}
                </div>
            
                <div class="dc-contentwrappers">
                    <div class="container">
                        <div class="wt-title titlecontact">
                            {{-- <h3>Please get in touch and our expert support team will answer all your questions.
                            </h3> --}}
                        </div>

                        <div class="row">
                            
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 float-left">
                                <div class="dc-howitwork-hold dc-haslayout">
                                    <div class="dc-haslayout dc-main-section">
                                      
                                          <div class="infographicGrid section"><div class="infographic-panel-grid">

                                            <div class="infographicRow section ">
                                                <div class="infographic-panel-pair small-right">
                                        <div class="row">
                                            <div class="infographic-panel ip-red ip-left col-md-8">
                                            <div class="content">
                                        
                                                <h3>Get in touch with us</h3>
                                                <p>
                                                    For any further assistance or quires about our platform feel free to drop us a  message  , our support team will be happy to help to you.

                                                </p>

                                                <p>
                                                    Have a query? Send us a message</p>
                                        
                                                <div class="links">
                                                        <a href="#now" class="arrow-cta arrow-cta-inverse">Send a message</a>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="infographic-panel ip-black ip-right col-md-4">
                                            <div class="contactbox content ">
                                        
                                                <h3 style="padding-top: 50px;">Old fashion phone call works too</h3>
                       
                                                <div class="links">
                                                   <span class="callus" style="color: #fff"> Call Us @  </span> <a href="tel:091-9219505" class="arrow-cta arrow-cta-inverse">091-9219505</a>
                                                    </div>
                                                </div>
                                        </div>

                                    </div>
                                    </div>
                                        
                                        </div>


                                        <div class="infographicRow section"><div class="infographic-panel-pair small-left">
                                            <div class="row">

                                            <div class="infographic-panel ip-grey-light ip-left col-md-4">
                                            <div class="content">
                                        
                                                <h3>Contact our regional office</h3>
                                        
                                                <p>Abbottabad IT Park<br> Address: KP IT Park, Silk Road, Mandian, Abbottabad<br></p>
                                                <p>Phone: 0992 383090</p>
                                        
                                                </div>
                                        </div>
                                        <div class="infographic-panel ip-grey ip-right col-md-8">
                                            <div class="content contactbox">
                                        
                                                <h3>Visit our Main Office</h3>
                                                <p> Address: Khyber Pakhtunkhwa IT Board 134, Industrial Estate, Hayatabad, Peshawar, Khyber Pakhtunkhwa, Pakistan</p>
                                                <p>Phone: 091-9219505</p>
                                               

                                             
                                                </div>
                                        </div></div>
                                    </div>
                                        </div> 
                                        </div>
                                        </div>

                                        <div class="row contactform" id="now">
                                            <div class="col-md-8">
                                                <div class="col-sm-12">

                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div><br />
                                                    @endif
                                                </div>
                                                <form action="{{route('contactstore')}}" method="post">
                                                    @csrf
                                                  <input required class="form-control" name="from" placeholder="Name..." /><br />
                                                  <input required class="form-control" name="subject" placeholder="Subject..." /><br />
                                                  <input required class="form-control" name="email" placeholder="E-mail..." /><br />
                                                  <textarea required class="form-control" name="body" placeholder="How can we help you?" style="height:150px;"></textarea><br />
                                                  <br />
                                                  @if(config('services.recaptcha.key'))
                                                  <div class="g-recaptcha"
                                                      data-sitekey="{{config('services.recaptcha.key')}}">
                                                  </div>
                                              @endif

                                                  <input class="wt-btn" type="submit" value="Send" /><br /><br />
                                               
                                                </form>
                                            </div>
                                            <div class="col-md-4 contactinfo">
                                              <span>Main Office:</span> <br />
                                              Address: Khyber Pakhtunkhwa IT Board 134, Industrial Estate, Hayatabad, Peshawar, Khyber Pakhtunkhwa, Pakistan<br />
                                              Phone: 091-9219505<br>
                                              091-9219508<br>
                                              091-5891613<br>
                                              091-5891516<br>
                                              Email: <a href="mailto:info@kpitb.gov.pk">info@kpitb.gov.pk</a><br>

                                              <br /><br />
                                              <span>Abbottabad IT Park</span><br />
                                              Address: KP IT Park, Silk Road, Mandian, Abbottabad
                                              <br />
                                              Phone: 0992 383090<br />
                                          
                                          
                                          
                                              {{-- <br /><br />
                                              <span>Hong kong:</span><br />
                                              Company HK Litd, <br />
                                              25/F.168 Queen<br />
                                              Wan Chai District, Hong Kong<br />
                                              Phone: +852 129 209 291<br />
                                              <a href="mailto:hk@mysite.com">hk@mysite.com</a><br /> --}}
                                          
                                          
                                            </div>
                                          </div>


                                        
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           <style>
               .callus{
                   color: #fff;
               }
               .contactbox h3{
                padding-top: 50px !important;
               }
               </style>
        @php
                       @include('front-end.includes.footers.footer2')

        @endphp
      <script src='https://www.google.com/recaptcha/api.js'></script>

    </div>
    <?php
     $page_header = 'style5';
     $slider_style = '0';
    ?>
@endsection
@push('scripts')
    <script src="{{ asset('js/prettyPhoto-min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    @if ($page_header == 'style5')
        @if (empty($slider_style))
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style5')
            </script>
        @elseif (!empty($slider_style) && $slider_order != 0)
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style5')
            </script>
        @endif
    @elseif ($page_header == 'style3')
        @if (empty($slider_style))
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style3')
            </script>
        @elseif ($slider_style != 'style3')
            <script>
                jQuery('.wt-contentwrapper').addClass('inner-header-style3')
            </script>
        @endif
    @endif
    @if ($slider_style == 'style2')
        {{-- <script>
            jQuery('#wt-header').addClass('wt-headervthhree')
            jQuery('#wt-header').removeClass('wt-headervtwo')
            jQuery('.wt-formtheme.wt-formbanner.wt-formbannervtwo').remove()
        </script> --}}
        @if (isset($_SERVER["SERVER_NAME"]) && $_SERVER["SERVER_NAME"] === 'cybervision.com.pk')
            <script>
                jQuery('.wt-logo a img').attr('src',(APP_URL+'/images/logo-white.png'));
            </script>
        @endif
    @else
    @endif
    <script src="{{ asset('js/tilt.jquery.js') }}"></script>
@endpush
