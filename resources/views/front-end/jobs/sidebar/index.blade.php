<aside id="wt-sidebar" class="wt-sidebar">

    <div class="wt-proposalsr wt-freelancers-sidebar">
        <div class="wt-proposalsrcontent">
            
            @php
            $project_type  = Helper::getProjectTypeList($job->project_type);
            @endphp
            @if($job->project_type == 'fixed')
            <span class="wt-proposalsicon"><i class="fa fa-angle-double-down"></i><i class="fa fa-money"></i></span>
            <div class="wt-title">
                <h3>{{ !empty($symbol['symbol']) ? $symbol['symbol'] : '$' }}</i> {{{ $english_format_number = number_format($job->price) }}}</h3>
                <span>{{ trans('lang.client_budget') }}</span>
            </div>
            @endif
            
        </div>
        @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-jobproposals-widget.blade.php')))
            @include('extend.front-end.jobs.sidebar.wt-jobproposals-widget')
        @else
            @include('front-end.jobs.sidebar.wt-jobproposals-widget')
        @endif
        <!-- @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-qrcode-widget.blade.php')))
            @include('extend.front-end.jobs.sidebar.wt-qrcode-widget')
        @else
            @include('front-end.jobs.sidebar.wt-qrcode-widget')
        @endif -->
        @if(Auth::check())
        @if($job->user_id !== Auth::user()->id)
        @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-addtofavourite-widget.blade.php')))
            @include('extend.front-end.jobs.sidebar.wt-addtofavourite-widget')
        @else
            @include('front-end.jobs.sidebar.wt-addtofavourite-widget')
        @endif
        @endif
        @endif

    </div>
    @if (!empty($job->employer))
        @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-employerinfo-widget.blade.php')))
            @include('extend.front-end.jobs.sidebar.wt-employerinfo-widget')
        @else
            @include('front-end.jobs.sidebar.wt-employerinfo-widget')
        @endif
    @endif
    @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-sharejob-widget.blade.php')))
        @include('extend.front-end.jobs.sidebar.wt-sharejob-widget')
    @else
        @include('front-end.jobs.sidebar.wt-sharejob-widget')
    @endif
    @if (file_exists(resource_path('views/extend/front-end/jobs/sidebar/wt-reportjob-widget.blade.php')))
        @include('extend.front-end.jobs.sidebar.wt-reportjob-widget')
    @else
        @include('front-end.jobs.sidebar.wt-reportjob-widget')
    @endif
</aside>
