@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ?
'extend.front-end.master':
 'front-end.master', ['body_class' => 'wt-innerbgcolor'] )
@section('title'){{ $job_list_meta_title }} @stop
@section('description', $job_list_meta_desc)
@section('content')
    @php $breadcrumbs = Breadcrumbs::generate('searchResults'); @endphp
    @if (file_exists(resource_path('views/extend/front-end/includes/inner-banner.blade.php')))
        @include('extend.front-end.includes.inner-banner',
            ['title' => trans('lang.jobs'), 'inner_banner' => $job_inner_banner, 'show_banner' => $show_job_banner]
        )
    @else
        @include('front-end.includes.inner-banner',
            ['title' =>  trans('lang.jobs'), 'inner_banner' => $job_inner_banner, 'show_banner' => $show_job_banner ]
        )
    @endif

    <div class="wt-haslayout wt-main-section" id="jobs">
        @if (Session::has('payment_message'))
            @php $response = Session::get('payment_message') @endphp
            <div class="flash_msg">
                <flash_messages :message_class="'{{{$response['code']}}}'" :time ='5' :message="'{{{ $response['message'] }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <div class="wt-haslayout">
            <div class="container">
                <div class="row">
                    <div id="wt-twocolumns" class="wt-twocolumns wt-haslayout">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-xl-3 float-left wt-freelancers-sidebar">
                            @if (file_exists(resource_path('views/extend/front-end/jobs/filters.blade.php')))
                                @include('extend.front-end.jobs.filters')
                            @else
                                @include('front-end.jobs.filters')
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xl-9 float-left">
                            <div class="wt-userlistingholder wt-userlisting wt-haslayout">
                            <div class="wt-userlistingtitletop">
                                @if (!empty($jobs))
                                        {{-- <span>{{ trans('lang.01') }} {{$jobs->count()}} of {{$Jobs_total_records}} results</span> --}}
                                        <!-- <span>{{ trans('lang.01') }} {{$jobs->count()}} </span> -->

                                        @endif
                            </div>
                            <div class="wt-userlistingtitletop" style="text-align: right;">
                               <select name="sortByFilter" id="sortByFilter">
                                <option value="">Sort By</option>

                                   <option value="recently_added" <?php if( !empty($_GET['sortby']) && $_GET['sortby'] == 'recently_added') { echo "selected"; }?>>Sort By Recently Added</option>
                                   <option value="expiry_date" <?php if( !empty($_GET['sortby']) && $_GET['sortby'] == 'expiry_date') { echo "selected"; }?> >Sort By Expiry Date</option>
                               </select>
                            </div>
                                @if (!empty($jobs) && $jobs->count() > 0)
                                    @foreach ($jobs as $job)
                                    @if($job->unpublished == '1')
                                        @php
                                            $job = \App\Job::find($job->id);
                                            //$description = strip_tags(stripslashes($job->description));
                                            $description = strip_tags(stripslashes($job->description));
                                            $featured_class = $job->is_featured == 'true' ? 'wt-featured' : '';
                                            $user = Auth::user() ? \App\User::find(Auth::user()->id) : '';
                                            $project_type  = Helper::getProjectTypeList($job->project_type);
                                        @endphp
                                        <div class="wt-userlistinghold wt-userlistingholdvtwo {{$featured_class}}">
                                            <!-- @if ($job->is_featured == 'true')
                                                <span class="wt-featuredtag"><img src="images/featured.png" alt="{{{ trans('ph.is_featured') }}}" data-tipso="Plus Member" class="template-content tipso_style"></span>
                                            @endif -->
                                            <div class="wt-userlistingcontent">
                                                <div class="wt-contenthead wt-contenthead12">
                                                    <div class="wt-title">
                                                        @if (!empty($job->employer->slug))
                                                            <a href="{{ url('profile/'.$job->employer->slug) }}"><i class="fa fa-check-circle"></i> {{{ Helper::getUserName($job->employer->id) }}}</a>
                                                        @endif
                                                        <h2><a href="{{ url('job/'.$job->slug) }}">{{{$job->title}}}</a></h2>
                                                    </div>
                                                    <div class="wt-description">
                                                        <p>{{ str_limit(html_entity_decode($description), 200) }}</p>
                                                    </div>
                                                     <?php
                                                    //  echo '<pre>';
                                                    //         print_r($job->skills);
                                                    //     echo '</pre>';
                                                     ?>
                                                    @if (!empty($job->skills[0]->slug))
                                                    <div class="wt-description wt-skills-heading">
                                                        <span>Skills:</span>
                                                    </div>
                                                        <div class="wt-tag wt-widgettag">
                                                            @foreach ($job->skills as $skill )
                                                                <a href="{{{url('search-results?type=job&skills%5B%5D='.$skill->slug)}}}">{{$skill->title}}</a>
                                                            @endforeach
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="wt-viewjobholder">
                                                    <ul>
                                                        @if (!empty($job->project_level))
                                                            <li><span><i class="fa fa-tag wt-viewjobtag"></i>{{{Helper::getProjectLevel($job->project_level)}}}</span></li>
                                                        @endif
                                                        @if (!empty($job->location->title))
                                                            <li><span><i class="far fa-map"></i>{{{ $job->location->title }}}</span></li>
                                                        @endif
                                                        <li><span><i class="far fa-folder wt-viewjobfolder"></i>{{{ trans('lang.type') }}} {{{$project_type}}}</span></li>
                                                        <li><span><i class="far fa-clock wt-viewjobclock"></i>{{{ Helper::getJobDurationList($job->duration)}}}</span></li>
                                                        <!-- <li><span><i class="fa fa-tag wt-viewjobtag"></i>{{{ trans('lang.job_id') }}} {{{$job->code}}}</span></li> -->
                                                        @php $proposals_count = !empty($job->proposals) ? $job->proposals->count() : 0; @endphp
                                                        <li><span><i class="wt-viewjobclock fa fa-newspaper"></i> {{{$proposals_count }}} proposals received</span></li>
                                                        <li><span><i class="far fa-calendar"></i>Expiry Date: {{{ \Carbon\Carbon::parse($job->expiry_date)->format('M d, Y') }}}</span></li>
                                                        <li class="wt-btnarea"><a href="{{url('job/'.$job->slug)}}" class="wt-btn">{{{ trans('lang.view_job') }}}</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @endforeach
                                    @if ( method_exists($jobs,'links') )
                                        {{ $jobs->links('pagination.custom') }}
                                    @endif
                                @else
                                    @if (file_exists(resource_path('views/extend/errors/no-record.blade.php')))
                                        @include('extend.errors.no-record')
                                    @else
                                        @include('errors.no-record')
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('pagespecificscripts')
    <script type="text/javascript">
    $(document).ready(function() {
    sortval = $("#sortByFilter").val();
   
    if(sortval !== ''){
        $("#sortby").val(sortval);
    }
});
    $('#sortByFilter').on('change', function() {
        value = $(this).val();
        //alert(value);
        $("#sortby").val(value);

      document.forms['filterJobs'].submit();
  });
</script>
@stop