@if(count((array)$govtDepts) > 0)
<option value="" selected="selected">Select Department</option>
@foreach($govtDepts as $govtDept)
<option value="{{$govtDept->id}}">{{$govtDept->department_name}}</option>
@endforeach
@else
<option value="" selected="selected"></option>
@endif
