@if(count((array)$dists) > 0)
<option value="" selected="selected">Select District</option>
@foreach($dists as $dist)
<option value="{{$dist->id}}">{{$dist->district_name}}</option>
@endforeach
@else
<option value="" selected="selected"></option>
@endif
