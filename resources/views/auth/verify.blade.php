@extends(file_exists(resource_path('views/extend/front-end/master.blade.php')) ? 'extend.front-end.master' : 'front-end.master')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
    @if (\Session::has('error'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('error') !!}</li>
        </ul>
    </div>
@endif
    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div> --}}

    <form method="POST" action="{{ route('user-verify') }}" class="verificationform">
        @csrf
        <h1 class="wt-sectiontitle">Verification</h1>
        <p class="text-muted">
            You have received an email which contains verification/link code.
            If you haven't received it, press <a href="{{ route('verification.resend') }}">here</a>.
        </p>
    
        <div class="form-group form-focus">
           
            <input name="two_factor_code" type="text" 
                class="form-control floating {{ $errors->has('two_factor_code') ? ' is-invalid' : '' }}" 
                required autofocus placeholder="Verification Code">
            @if($errors->has('two_factor_code'))
                <div class="invalid-feedback">
                    {{ $errors->first('two_factor_code') }}
                </div>
            @endif
        </div>
        <input type="hidden" readonly name="user_id" value="{{Auth::user()->id}}">

        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary btn-block account-btn">
                Verify Now
            </button>
        </div>
        
    </form>
</div></div>
    
</div>

<style>
    .verificationform{
        margin: 30px 10px;
    }
    .verificationform button
    {
        background: #FFC93C !important;
    color: #000 !important;
    border: none;
    border-radius: 0 !important;
    padding: 15px 15px;
    font-size: 20px;

    }
    .verificationform input[type="text"]
    {
        padding: 12px 15px;
    height: 60px !important;
    font-size: 20px !important;
    }
    .verificationform p{
        font-size: 17px;
    line-height: 25px;
    }
    </style>
@endsection
