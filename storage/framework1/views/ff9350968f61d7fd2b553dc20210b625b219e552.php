<footer id="wt-footer" class="wt-footer wt-haslayout">
    <?php if(!empty($footer)): ?>
        <div class="wt-footerholder wt-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="wt-footerlogohold">
                            <?php if(!empty($footer['footer_logo'])): ?>
                                <strong class="wt-logo"><a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset(\App\Helper::getFooterLogo($footer['footer_logo']))); ?>" alt="company logo here"></a></strong>
                            <?php endif; ?>
                            <?php if(!empty($footer['description'])): ?>
                                <div class="wt-description">
                                    <p><?php echo e(str_limit($footer['description'], 500)); ?></p>
                                </div>
                            <?php endif; ?>
                            <?php Helper::displaySocials(); ?>
                        </div>
                    </div>
                    <?php if(!empty($footer['menu_title_1']) || !empty($footer['menu_pages_1'])): ?>
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="wt-footercol wt-widgetcompany">
                                <?php if(!empty($footer['menu_title_1'])): ?>
                                    <div class="wt-fwidgettitle">
                                        <h3>Important Links</h3>
                                    </div>
                                <?php endif; ?>
                                <?php if(!empty($footer['menu_pages_1'])): ?>
                                    <ul class="wt-fwidgetcontent">
                                        <?php $__currentLoopData = $footer['menu_pages_1']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu_1_page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php  $page = \App\Page::where('id', $menu_1_page)->first(); ?>
                                            <?php if(!empty($page)): ?>
                                                <li><a href="<?php echo e(url('page/'.$page->slug)); ?>"><?php echo e($page->title); ?></a></li>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo url('contactus'); ?>">Contact Us</a></li>

                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(!empty($search_menu) || !empty($menu_title)): ?>
                        <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="wt-footercol wt-widgetcompany">
                                <?php if(!empty($menu_title)): ?>
                                    <div class="wt-fwidgettitle">
                                        <h3>Explore Freelancers by Region</h3>
                                    </div>
                                <?php endif; ?>
                                <?php
                                 $locations = App\Location::get()->take(4);
                                ?>
                                <ul class="wt-fwidgetcontent">
                                    <?php $__currentLoopData = $locations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(URL::to('/search-results?type=freelancer&locations[]='.$location->slug)); ?>">Freelancers in <?php echo e($location->title); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                   
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="wt-haslayout wt-footerbottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="wt-copyrights"><span><?php echo e(!empty($footer['copyright']) ? $footer['copyright'] : 'KPITB. All Rights Reserved.'); ?></p>
                    <?php if(!empty($footer['pages'])): ?>
                        <nav class="wt-addnav">
                            <ul>
                                <?php $__currentLoopData = $footer['pages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu_page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $page = \App\Page::where('id', $menu_page)->first(); ?>
                                    <?php if(!empty($page)): ?>
                                        <li><a href="<?php echo e(url('page/'.$page->slug)); ?>"><?php echo e($page->title); ?></a></li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </nav>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>