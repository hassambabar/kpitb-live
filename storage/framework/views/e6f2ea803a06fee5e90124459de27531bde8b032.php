<?php $__env->startPush('stylesheets'); ?>
<?php $__env->stopPush(); ?>
<?php if(Schema::hasTable('site_managements')): ?>
    <?php
    $app_description =  env('APP_DESCRIPTION');
    ?>
    <?php $__env->startSection('title'); ?><?php echo e(config('app.name')); ?> <?php $__env->stopSection(); ?>
    <?php $__env->startSection('description', "$app_description"); ?>
        <?php $__env->startSection('content'); ?>
        <div  class="la-signup-page">
            <section class="singup-page wt-haslayout  wt-main-section wt-paddingnull">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="wt-companydetails">
                                <div class="wt-companycontent signup-left-box"><div>
                                    <img src="<?php echo URL::to('/images/Provider.png' );?>" />
                                </div>
                                <div class="wt-companyinfotitle">
                                    <h2>As a Freelancer</h2>
                                </div>
                                <div class="wt-description">
                                    Consectetur adipisicing elit sed dotem eiusmod tempor incune utnaem labore etdolore maigna aliqua enim poskina ilukita ylokem lokateise ination voluptate velit esse cillum.
                                </div>
                                <div class="wt-btnarea">
                                    <a href="<?php echo e(route('become-a-freelancer')); ?>" class="wt-btn">JOIN NOW</a>
                                </div>
                            </div>
                            <div class="wt-companycontent signup-right-box"><div>
                                <img src="<?php echo URL::to('/images/Procurer.png' );?>" />
                            </div>
                            <div class="wt-companyinfotitle">
                                <h2>As an Employee</h2>
                            </div>
                            <div class="wt-description">
                                Consectetur adipisicing elit sed dotem eiusmod tempor incune utnaem labore etdolore maigna aliqua enim poskina ilukita ylokem lokateise ination voluptate velit esse cillum.
                            </div>
                            <div class="wt-btnarea">
                                <a href="javascript::void(0)" class="wt-btn" >JOIN NOW</a>
                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
    <?php $__env->stopSection(); ?>
<?php endif; ?>
<?php $__env->startPush('scripts'); ?>
<?php $__env->stopPush(); ?>
<?php echo $__env->make(file_exists(resource_path('views/extend/front-end/master.blade.php')) ? 'extend.front-end.master' : 'front-end.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>