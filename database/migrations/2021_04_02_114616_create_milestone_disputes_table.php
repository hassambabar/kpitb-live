<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestoneDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestone_disputes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('milestone_id')->nullable();
            $table->integer('milestone_invoice_id')->nullable();
            $table->integer('job_id')->nullable();

            $table->integer('status')->default(0);
            $table->integer('freelancer_id')->nullable();
            $table->string('freelancer_reason')->nullable();
            $table->text('freelancer_description')->nullable();
            $table->integer('pe_id')->nullable();
            $table->string('pe_reason')->nullable();
            $table->text('pe_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('milestone_disputes');
    }
}
