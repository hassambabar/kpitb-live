<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToJobBid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->string('service_type')->nullable();
            $table->integer('selection_procurement_system')->nullable();
            $table->integer('single_source')->nullable();
            $table->string('single_source_email')->nullable();
            $table->string('project_budget')->nullable();
            $table->integer('district')->nullable();
            $table->integer('teachniqal_score')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            //
        });
    }
}
