<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpraInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpra_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('NTN')->nullable();

            $table->string('Business_Name')->nullable();
            $table->string('Enrollment_Date')->nullable();
            $table->text('Principal_Activity')->nullable();
            $table->string('Activity_Code')->nullable();
            $table->string('Addresse')->nullable();
            $table->string('City_Name')->nullable();
            $table->string('District_Name')->nullable();
            $table->string('Activetaxpayer')->nullable();
            $table->string('Compliance_Level')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpra_infos');
    }
}
