<?php
/**
 * Class PageSeeder.
 *
 
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// Dummy Pages Data

/**
 * Class PageSeeder
 */
class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert(
            [
                [
                    // 'title' => 'Main',
                    // 'slug' => 'main',
                    // 'body' => '<div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    // </div>',
                    // 'relation_type' => 0,
                    // 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 'sections' => null,
                ],
            ]
        );
    }
}
