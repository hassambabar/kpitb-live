<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

// Dummy Article

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert(
            [
                [
                    // 'title' => 'Dummy Title',
                    // 'slug'  => 'dummy,
                    // 'banner'  => 'dummy-img',
                    // 'description'  => '<p>Dummy Description</p>
                    // 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 'user_id' => 1,
                ]
            ]
        );
    }
}
