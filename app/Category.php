<?php
/**
 * Class Category
 *
 
 */

namespace App;

use DB;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use File;
use Storage;

/**
 * Class Category
 *
 */
class Category extends Model
{
    /**
     * Fillables for the database
     *
     * @access protected
     *
     * @var array $fillable
     */
    protected $fillable = array(
        'title', 'slug', 'abstract'
    );

    /**
     * Protected Date
     *
     * @access protected
     * @var    array $dates
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get all of the users that are assigned this category.
     *
     * @return relation
     */
    public function freelancers()
    {
        return $this->morphedByMany('App\User', 'catable');
    }


    /**
     * Get all of the jobs that are assigned this category.
     *
     * @return relation
     */
    public function jobs()
    {
        return $this->morphedByMany('App\Job', 'catable');
    }
    public function jobstotal()
    {
       
        
        return $this->jobs()->where('status','=','posted')->WhereDate('expiry_date', '>', date('Y-m-d'));
    }
    /**
     * Get all of the services that are assigned this category.
     *
     * @return relation
     */
    public function services()
    {
        return $this->morphedByMany('App\Service', 'catable');
    }

    /**
     * Set slug before saving in DB
     *
     * @param string $value value
     *
     * @access public
     *
     * @return string
     */
    public function setSlugAttribute($value)
    {
        if (!empty($value)) {
            $temp = str_slug($value, '-');
            if (!Category::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!Category::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }
            $this->attributes['slug'] = $temp;
        }
    }

    /**
     * Saving categories
     *
     * @param string $request get req attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function saveCategories($request)
    {
        if (!empty($request)) {
            
            $parent_cat = filter_var($request['parent_category'], FILTER_VALIDATE_INT);
            $this->parent = $parent_cat;

            $this->title = filter_var($request['category_title'], FILTER_SANITIZE_STRING);
            $this->slug = filter_var($request['category_title'], FILTER_SANITIZE_STRING);

            // $this->abstract = filter_var($request['category_abstract'], FILTER_SANITIZE_STRING);
            $this->abstract = $request['category_abstract'];
            $old_path = Helper::PublicPath() . '/uploads/categories/temp';
            if (!empty($request['uploaded_image'])) {
                $filename = $request['uploaded_image'];
                if (file_exists($old_path . '/' . $request['uploaded_image'])) {
                    $new_path = Helper::PublicPath().'/uploads/categories/';
                    if (!file_exists($new_path)) {
                        File::makeDirectory($new_path, 0755, true, true);
                    }
                    $filename = time() . '-' . $request['uploaded_image'];
                    rename($old_path . '/' . $request['uploaded_image'], $new_path . '/' . $filename);
                    rename($old_path . '/small-' . $request['uploaded_image'], $new_path . '/small-' . $filename);
                    rename($old_path . '/medium-' . $request['uploaded_image'], $new_path . '/medium-' . $filename);
                }
                $this->image = filter_var($filename, FILTER_SANITIZE_STRING);
            } else {
                $this->image = null;
            }
            
            $error = "0";
            $check_slug = Str::slug($request['category_title']);
            if (self::where('slug', $check_slug)->exists()) {
                $error = "1";
                return $error;
                $json['type'] = 'error';
                $json['message'] = trans('Category Already Found');
             }
            else{
                $this->save(); 
                $json['type'] = 'success';
                $json['message'] = trans('lang.cat_created');
                return $json;
            }
           
        }
    }

    /**
     * Updating Categories
     *
     * @param string $request get request attributes
     * @param int    $id      get department id for updation
     *
     * @return \Illuminate\Http\Response
     */

    public function getChildCategories($id){

        $chilCategory = Category::where('parent',$id)->get();
        return $chilCategory;
    }


    public function updateCategories($request, $id)
    {
        if (!empty($request)) {
            $cats = self::find($id);
            if ($cats->title != $request['category_title']) {
                $cats->slug  =  filter_var($request['category_title'], FILTER_SANITIZE_STRING);
            }
            $parent_cat = filter_var($request['parent_category'], FILTER_VALIDATE_INT);
            $cats->parent = $parent_cat;

            $cats->title = filter_var($request['category_title'], FILTER_SANITIZE_STRING);
            // $cats->abstract = filter_var($request['category_abstract'], FILTER_SANITIZE_STRING);
            $cats->abstract = $request['category_abstract'];
            $old_path = Helper::PublicPath() . '/uploads/categories/temp';
            if (!empty($request['uploaded_image'])) {
                $filename = $request['uploaded_image'];
                if (file_exists($old_path . '/' . $request['uploaded_image'])) {
                    $new_path = Helper::PublicPath().'/uploads/categories/';
                    if (!file_exists($new_path)) {
                        File::makeDirectory($new_path, 0755, true, true);
                    }
                    $filename = time() . '-' . $request['uploaded_image'];
                    rename($old_path . '/' . $request['uploaded_image'], $new_path . '/' . $filename);
                    rename($old_path . '/small-' . $request['uploaded_image'], $new_path . '/small-' . $filename);
                    rename($old_path . '/medium-' . $request['uploaded_image'], $new_path . '/medium-' . $filename);
                }
                $cats->image = filter_var($filename, FILTER_SANITIZE_STRING);
            } else {
                $cats->image = null;
            }

            // $error = "0";
            // $check_slug = Str::slug($request['category_title']);
            // if (self::where('slug', $check_slug)->exists()) {
            //     $error = "1";
            //     return $error;
            //  }
            // else{
                $cats->save(); 
            // }
            
        }
    }
}
