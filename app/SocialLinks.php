<?php

/**
 * Class Profile.
 *
 
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use File;
use Storage;
use function Opis\Closure\serialize;
use function Opis\Closure\unserialize;
use DB;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;

/**
 * Class Profile
 *
 */
class SocialLinks extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'social_links',
       
    ];


    /**
     * Get the user that has the social links.
     *
     * @return relation
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Store social links in database
     *
     * @param mixed   $request Request Attributes
     * @param integer $user_id User ID
     *
     * @return json response
     */
    public function storeSocialLinks($request, $user_id)
    {
        // $user = User::find($user_id);
        $user = User::find(Auth::user()->id);
        $user_id = $user->id;
        dd($user_id); exit;
        
        if (!empty($social_links)) {
            foreach ($social_links as $key => $social_link) {
                $social_link[$key]['url'] = $social_link['url'];
            }
            $social_links->social_links = serialize($social_links);
        } else {
            $social_links->social_links = null;
        }
        return $social_links->save();
    }

    


    

    /**
     * Store Profile in database
     *
     * @param mixed   $request Request Attributes
     * @param integer $user_id User ID
     *
     * @return json response
     */
    public function updateAwardProjectSettings($request, $user_id)
    {
        $json = array();
        $user = User::find($user_id);
        $count = 0;
        $award_counter = 0;
        $request_project = array();
        $old_path = Helper::PublicPath() . '/uploads/users/temp';
        if (!empty($request['project'])) {
            foreach ($request['project'] as $key => $project) {
                if ($project['project_title'] == 'Project title here' || $project['project_url'] == 'Project url here') {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.empty_fields_not_allowed');
                    return $json;
                }
                $request_project[$count]['project_title'] = $project['project_title'];
                $request_project[$count]['project_url'] = $project['project_url'];
                if (!empty($project['project_hidden_image'])) {
                    $filename = $project['project_hidden_image'];
                    if (file_exists($old_path . '/' . $project['project_hidden_image'])) {
                        $new_path = Helper::PublicPath() . '/uploads/users/' . $user_id . '/projects';
                        if (!file_exists($new_path)) {
                            File::makeDirectory($new_path, 0755, true, true);
                        }
                        $filename = time() . $count . '-' . $project['project_hidden_image'];
                        rename($old_path . '/' . $project['project_hidden_image'], $new_path . '/' . $filename);
                        rename($old_path . '/small-' . $project['project_hidden_image'], $new_path . '/small-' . $filename);
                        rename($old_path . '/medium-' . $project['project_hidden_image'], $new_path . '/medium-' . $filename);
                    }
                    $request_project[$count]['project_hidden_image'] = $filename;
                } else {
                    $request_project[$count]['project_hidden_image'] = $project['project_hidden_image'];
                }
                $count++;
            }
        }
       
        $project = !empty($request['project']) ? serialize($request_project) : '';
        $user_profile = $this::select('id')->where('user_id', $user_id)
            ->get()->first();
        if (!empty($user_profile->id)) {
            $profile = Profile::find($user_profile->id);
        } else {
            $profile = $this;
        }
        $profile->user()->associate($user_id);
        $profile->projects = $project;
        $profile->save();
        $json['type'] = 'success';
        return $json;
    }

    public function updateAwardSettings($request, $user_id)
    {
        $json = array();
        $user = User::find($user_id);
        $count = 0;
        $award_counter = 0;
        $request_award = array();
        $old_path = Helper::PublicPath() . '/uploads/users/temp';
        
        if (!empty($request['award'])) {
            foreach ($request['award'] as $key => $award) {
                if ($award['award_title'] == 'Award title here' || $award['award_date'] == 'Select Award date') {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.empty_fields_not_allowed');
                    return $json;
                }
                $request_award[$award_counter]['award_title'] = $award['award_title'];
                $request_award[$award_counter]['award_date'] = $award['award_date'];
                if (!empty($award['award_hidden_image'])) {
                    $filename = $award['award_hidden_image'];
                    if (file_exists($old_path . '/' . $award['award_hidden_image'])) {
                        $new_path = Helper::PublicPath() . '/uploads/users/' . $user_id . '/awards';
                        if (!file_exists($new_path)) {
                            File::makeDirectory($new_path, 0755, true, true);
                        }
                        $filename = time() . $award_counter . '-' . $award['award_hidden_image'];
                        rename($old_path . '/' . $award['award_hidden_image'], $new_path . '/' . $filename);
                        rename($old_path . '/small-' . $award['award_hidden_image'], $new_path . '/small-' . $filename);
                        rename($old_path . '/medium-' . $award['award_hidden_image'], $new_path . '/medium-' . $filename);
                    }
                    $request_award[$award_counter]['award_hidden_image'] = $filename;
                } else {
                    $request_award[$award_counter]['award_hidden_image'] = $award['award_hidden_image'];
                }
                $award_counter++;
            }
        }
        $award = !empty($request['award']) ? serialize($request_award) : '';
        $user_profile = $this::select('id')->where('user_id', $user_id)
            ->get()->first();
        if (!empty($user_profile->id)) {
            $profile = Profile::find($user_profile->id);
        } else {
            $profile = $this;
        }
        $profile->user()->associate($user_id);
        $profile->awards = $award;
        $profile->save();
        $json['type'] = 'success';
        return $json;
    }

    /**
     * Add to whish list
     *
     * @param mixed   $column  Request Attributes
     * @param integer $id      ID
     * @param integer $user_id UserID
     *
     * @return json response
     */
    public function addWishlist($column, $id, $user_id)
    {
        $wishlist = array();
        $user_profile = $this::select('id')->where('user_id', $user_id)->get()->first();
        if (!empty($user_profile->id)) {
            $profile = $this::find($user_profile->id);
        } else {
            $profile = $this;
            $profile->user()->associate($user_id);
        }
        $wishlist = unserialize($profile[$column]);
        $wishlist = !empty($wishlist) && is_array($wishlist) ? $wishlist : array();
        $wishlist[] = $id;
        $wishlist = array_unique($wishlist);
        $profile->$column = serialize($wishlist);
        $profile->save();
        if (!empty($column) && ($column === 'saved_employers' || $column === 'saved_freelancer')) {
            DB::table('followers')->insert(
                [
                    'follower' => $user_id, 'following' => $id,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]
            );
        }

        return "success";
    }

    /**
     * Update experienceEducation
     *
     * @param mixed   $request Request Attributes
     * @param integer $user_id User ID
     *
     * @return json response
     */
    public function updateExperienceEducation($request, $user_id)
    {
        $json = array();
        $count = 0;
        $count2 = 0;
        $request_experiance = array();
        $request_education = array();
        if ($request['experience']) {
            foreach ($request['experience'] as $key => $experinence) {
                if ($experinence['job_title'] == 'Job title' || $experinence['start_date'] == 'Start Date'
                    || $experinence['end_date'] == 'End Date'
                ) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.empty_fields_not_allowed');
                    return $json;
                }
                $request_experiance[$count] = $experinence;
                $count++;
            }
        }
        if ($request['education']) {
            foreach ($request['education'] as $key => $education) {
                if ($education['degree_title'] == 'Degree title' || $education['start_date'] == 'Start Date'
                    || $education['end_date'] == 'End Date'
                ) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.empty_fields_not_allowed');
                    return $json;
                }
                $request_education[$count2] = $education;
                $count2++;
            }
        }
        $experience = !empty($request['experience']) ? serialize($request_experiance) : '';
        $education = !empty($request['education']) ? serialize($request_education) : '';
        $user_profile = $this::select('id')->where('user_id', $user_id)
            ->get()->first();
        if (!empty($user_profile->id)) {
            $profile = Profile::find($user_profile->id);
        } else {
            $profile = $this;
        }
        $profile->user()->associate($user_id);
        $profile->experience = $experience;
        $profile->education = $education;
        $profile->save();
        $json['type'] = 'success';
        return $json;
    }

    /**
     * Save Experiences.
     *
     * @param string $request req->attr
     * @param string $user_id user ID
     *
     * @return \Illuminate\Http\Response
     */
    public function savePayoutDetail($request, $user_id)
    {
        $json = array();
        $count = 0;
        $payouts = array();
        $user_profile = $this::select('id')->where('user_id', $user_id)
            ->get()->first();
        if (!empty($user_profile->id)) {
            $profile = Profile::find($user_profile->id);
        } else {
            $profile = $this;
        }
        $profile->user()->associate($user_id);
        if (!empty($request['payout_settings'])) {
            $payout_setting = $request['payout_settings'];
            $payouts['type'] = $payout_setting['type'];
            if ($payout_setting['type'] == 'paypal') {
                $payouts['paypal_email'] = $payout_setting['paypal_email'];
            } elseif ($payout_setting['type'] == 'bacs') {
                $payouts['bank_account_name'] = $payout_setting['bank_account_name'];
                $payouts['bank_account_number'] = $payout_setting['bank_account_number'];
                $payouts['bank_name'] = $payout_setting['bank_name'];
                $payouts['bank_routing_number'] = $payout_setting['bank_routing_number'];
                $payouts['bank_iban'] = $payout_setting['bank_iban'];
                $payouts['bank_bic_swift'] = $payout_setting['bank_bic_swift'];
            }
        }
        $profile->payout_settings  = serialize($payouts);
        $profile->save();
    }
}
