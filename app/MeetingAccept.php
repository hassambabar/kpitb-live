<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingAccept extends Model
{
    //
    protected $fillable = array(
        'meeting_minutes_id','user_id', 'status'
    );
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
