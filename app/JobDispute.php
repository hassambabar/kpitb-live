<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDispute extends Model
{
    protected $fillable = array(
        'job_id', 'invoice_id', 'freelancer_id','freelancer_reason','freelancer_description','pe_id', 'pe_reason','pe_description','status','pe_status','freelancer_status'
    );
}
