<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidScore extends Model
{
    //
    protected $fillable = array(
        'question_id', 'score', 'proposal_id', 'member_id'
    );
}
