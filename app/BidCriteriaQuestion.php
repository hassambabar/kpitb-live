<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidCriteriaQuestion extends Model
{
    //
    protected $fillable = array(
        'question', 'question_type'
    );

}
