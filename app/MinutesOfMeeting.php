<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MinutesOfMeeting extends Model
{
    //
    protected $fillable = array(
        'title', 'location', 'attachments','date','body','user_id', 'job_id','status'
    );

    public function comments()
    {
        
     return $this->hasMany(CommentMeeting::class,'post_id','id')->whereNull('parent_id');
    }

}
