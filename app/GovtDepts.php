<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class GovtDepts extends Model
{
    protected $table = 'govt_departments';
    public $timestamps = false;
    protected $fillable = ['id', 'org_type_id', 'department_name'];


    public static function saveGovtDepts($insArr)
    {
        DB::table('govt_departments')->insert($insArr);
    }

    // public static function load_team($id = 0)
    // {
    //     $dataAdapter = DB::table('meet_team')->select('*')->where('id',$id)->first();
    //     return (array) $dataAdapter;
    // }

    public static function updateGovtDepts($data, $id = 0)
    {
      $affected = DB::table('govt_departments')
            ->where('id', $id)
            ->update($data);
    }

    // public static function delete_team($id)
    // {
    //   MeetTeam::where('id',$id)->delete();

    // }

    public static function getAllDepts($id)
    {
          //$dataAdapter = DB::table('govt_departments')->select('*')->where('org_type_id',$id)->get()->toArray();
          $dataAdapter = DB::table('organization_type')
            ->join('govt_departments', 'organization_type.id', '=', 'govt_departments.org_type_id')
            ->select('organization_type.*', 'govt_departments.*')
            ->get()->toArray();
          return (array) $dataAdapter;
    }

    public static function getAllDeptsDropDown($id)
    {
          //$dataAdapter = DB::table('govt_departments')->select('*')->where('org_type_id',$id)->get()->toArray();
          $dataAdapter = DB::table('govt_departments')
                     ->select('*')
                     ->where('org_type_id', '=', $id)
                     ->get()->toArray();
          return (array) $dataAdapter;
    }
}