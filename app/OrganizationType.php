<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class OrganizationType extends Model
{
    protected $table = 'organization_type';
    public $timestamps = false;
    protected $fillable = ['id', 'organization_name'];


    public static function saveOrganizationtype($insArr)
    {
        DB::table('organization_type')->insert($insArr);
    }

    // public static function load_team($id = 0)
    // {
    //     $dataAdapter = DB::table('meet_team')->select('*')->where('id',$id)->first();
    //     return (array) $dataAdapter;
    // }

    public static function updateOrganizationType($data, $id = 0)
    {
      $affected = DB::table('organization_type')
            ->where('id', $id)
            ->update($data);
    }

    // public static function delete_team($id)
    // {
    //   MeetTeam::where('id',$id)->delete();

    // }

    public static function getAllOrgs()
    {
          return OrganizationType::all();
    }


}
