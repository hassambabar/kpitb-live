<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KpraInfo extends Model
{
    //
    protected $fillable = ['user_id', 'Business_Name', 'Enrollment_Date','Principal_Activity','Activity_Code','Addresse','City_Name','District_Name','Activetaxpayer','Compliance_Level','NTN'];

}
