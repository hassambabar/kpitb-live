<?php

/**
 * Class Page.
 *
 
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use File;

/**
 * Class Page
 *
 */
class Skilltypes extends Model
{
  protected $table = 'skills_by_type';
  public $timestamps = false;
  protected $fillable = ['id', 'skill_name', 'skill_type'];
    /**
     * Pages can have multiple meta
     *
     * @return relation
     */
    public function meta()
    {
        return $this->morphMany('App\Meta', 'metable');
    }

    /**
     * Posts can have multiple meta
     *
     * @return relation
     */
    public function metaValue($meta_key)
    {
        return $this->morphMany('App\Meta', 'metable')->where('meta_key', $meta_key)->select('id', 'meta_value')->first();
    }


    public static function getSkilltypes()
    {
        $skillsbytype = DB::table('skills_by_type')->paginate(10);
        return $skillsbytype;
    }

    public function saveSkills($insArr)
    {
        DB::table('skills_by_type')->insert($insArr);
    }

    public static function find($id = 0)
    {
        $dataAdapter = DB::table('skills_by_type')->select('*')->where('id',$id)->first();
        return (array) $dataAdapter;
    }

    public static function updateSkills($data, $id = 0)
    {
      $affected = DB::table('skills_by_type')
            ->where('id', $id)
            ->update($data);
    }

    public static function deleteSkill($id)
  	{
      Skilltypes::where('id',$id)->delete();

  	}

    public static function getAllSkillTypes()
    {
      return Skilltypes::all();
    }


}
