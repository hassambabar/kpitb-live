<?php
/**
 * Class Invoice
 *
 
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Invoice
 *
 */
class Invoice extends Model
{

    /**
     * Set scope of the variables
     *
     * @access public
     *
     * @return array
     */
    protected $fillable = ['title', 'price', 'paid','reciver_status','reciver_id','proposal_id','job_id'];
    protected $dates = ['created_at'];

    /**
     * Items
     *
     * @access public
     *
     * @return array
     */
    public function items()
    {
        return $this->hasMany(Item::class, 'invoice_id');
    }

    
    /**
     * Items
     *
     * @access public
     *
     * @return array
     */
    public function order()
    {
        return $this->hasOne(Order::class, 'invoice_id');
    }
}
