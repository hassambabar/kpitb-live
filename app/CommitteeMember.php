<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommitteeMember extends Model
{
    //
    protected $fillable = array(
        'member_id', 'parent_id', 'job_id'
    );

}
