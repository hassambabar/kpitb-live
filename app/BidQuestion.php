<?php

namespace App;
use App\BidScore;

use Illuminate\Database\Eloquent\Model;

class BidQuestion extends Model
{
    //

    protected $fillable = array(
        'title', 'score', 'type', 'user_id', 'job_id'
    );

    
}
