<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentMeeting extends Model
{
    //
    protected $fillable = ['user_id', 'post_id', 'parent_id', 'body'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->hasMany(CommentMeeting::class, 'parent_id');
    }
}
