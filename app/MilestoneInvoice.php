<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MilestoneInvoice extends Model
{
    //
    protected $fillable = array(
        'title', 'price', 'sender_id','reciver_id','job_id','order_id', 'detail','status','milestone_id','attachments','reciver_status'
    );

}
