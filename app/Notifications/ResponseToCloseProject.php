<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResponseToCloseProject extends Notification
{
    use Queueable;
    protected $ResponseToCloseProject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ResponseToCloseProject)
    {
        $this->ResponseToCloseProject=$ResponseToCloseProject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ResponseToCloseProject = $this->ResponseToCloseProject;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('The Procurer has rejected to close the project.')
            ->line('Procurer: '.$ResponseToCloseProject['employer_name'])
            ->line('Job: '.$ResponseToCloseProject['job_name'])
            ->line('Proposal Amount: '.$ResponseToCloseProject['amount']);
            // ->line('Detail : '.$ResponseToCloseProject['detail']);
            // ->action('View Job Details', url('proposal',$ResponseToCloseProject['slug'],'hired'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ResponseToCloseProject = $this->ResponseToCloseProject;
        return [
            'Slug' => $ResponseToCloseProject['slug'],
            'Employer' => $ResponseToCloseProject['employer_name'],
            'Job' => $ResponseToCloseProject['job_name'],
        ];
    }
}
