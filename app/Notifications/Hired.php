<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Hired extends Notification
{
    use Queueable;
    protected $Hired;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($Hired)
    {
        $this->Hired=$Hired;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail'];
        return ['database','mail'];
        // return ['notice']; in web notification will also be handled
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $Hired = $this->Hired;
        // dd($Hired['employer_name']); exit;
       
        

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You have been hired.')
            ->line('Procurer: '.$Hired['employer_name'])
            ->line('Job: '.$Hired['job_name'])
            ->line('Proposal Amount: '.$Hired['amount'])

            ->action('View Job Details', url('job',$Hired['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $Hired = $this->Hired;
        $slug = 'proposals';
        $url = url('freelancer',$slug);
        return [
            'Job' => $Hired['job_name'],
            'Url' => $url,
        ];
    }
}
