<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MinutesOfMeetingAccept extends Notification
{
    use Queueable;
    protected $MinutesOfMeetingAccept;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MinutesOfMeetingAccept)
    {
        $this->MinutesOfMeetingAccept=$MinutesOfMeetingAccept;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MinutesOfMeetingAccept =$this->MinutesOfMeetingAccept;
        return (new MailMessage)
                   ->line('Committee Member accepted your minutes of meeting for the job.')
                   ->line('Committee Member:' .$MinutesOfMeetingAccept['first_name'])
                   ->line('Minutes of meeting:'.$MinutesOfMeetingAccept['minutesofmeeting']) 
                   ->line('Job:' .$MinutesOfMeetingAccept['job'])
                   ->action('Meeting Detail', url('/add-minutes-of-meeting',$MinutesOfMeetingAccept['job_id']));
                    // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MinutesOfMeetingAccept =$this->MinutesOfMeetingAccept;
        $url = url('/add-minutes-of-meeting',$MinutesOfMeetingAccept['job_id']);
        return [
            'Meeting' => $MinutesOfMeetingAccept['minutesofmeeting'],
            'Job' => $MinutesOfMeetingAccept['job'],
            'Job_id' => $MinutesOfMeetingAccept['job_id'],
            'First_name' => $MinutesOfMeetingAccept['first_name'],
            'Last_name' => $MinutesOfMeetingAccept['last_name'],
            'Url' => $url,
        ];
    }
}
