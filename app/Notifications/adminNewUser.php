<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class adminNewUser extends Notification
{
    use Queueable;
    protected $adminNewUser;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($adminNewUser)
    {
        $this->adminNewUser=$adminNewUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $adminNewUser = $this->adminNewUser;

        return (new MailMessage)
            ->greeting('Hello! '.$adminNewUser['name'])
            ->line('Your account has been created by the KPITB Administration. Kindly, have a look at your account details.')
                       ->line('Email: '.$adminNewUser['email'])
            ->line('Password: '.$adminNewUser['password'])
            ->line('Verification Code: '.$adminNewUser['verification_code']);
            
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
