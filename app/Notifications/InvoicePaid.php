<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InvoicePaid extends Notification
{
    use Queueable;
    protected $InvoicePaid;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($InvoicePaid)
    {
        $this->InvoicePaid=$InvoicePaid;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $InvoicePaid = $this->InvoicePaid;
        // dd($InvoicePaid['employer_name']); exit;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('An Invoice has been paid.')
            ->line('Procurer: '.$InvoicePaid['employer_name'])
            ->line('Job: '.$InvoicePaid['job_name'])
            ->line('Proposal Amount: '.$InvoicePaid['amount'])
            ->line('Detail : '.$InvoicePaid['detail']);


            // ->action('View Job Details', url('job',$InvoicePaid['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $InvoicePaid = $this->InvoicePaid;
        $url = url('job',$InvoicePaid['slug']);
        return [
            'Slug' => $InvoicePaid['slug'],
            'Employer' => $InvoicePaid['employer_name'],
            'Job' => $InvoicePaid['job_name'],
            'Invoice' => $InvoicePaid['amount'],
            'Url' => $url,
        ];
    }
}
