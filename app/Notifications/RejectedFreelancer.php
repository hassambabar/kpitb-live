<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RejectedFreelancer extends Notification
{
    use Queueable;
    protected $RejectedFreelancer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($RejectedFreelancer)
    {
        $this->RejectedFreelancer=$RejectedFreelancer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail'];
        return ['database','mail'];
        // return ['notice']; in web notification will also be handled
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $RejectedFreelancer = $this->RejectedFreelancer;
        // dd($Hired['employer_name']); exit;
       
        

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You have been rejected for the job.')
            ->line('Procurer: '.$RejectedFreelancer['employer_name'])
            ->line('Job: '.$RejectedFreelancer['job_name'])
            ->line('Proposal Amount: '.$RejectedFreelancer['amount'])

            ->action('View Job Details', url('job',$RejectedFreelancer['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $RejectedFreelancer = $this->RejectedFreelancer;
        $slug = 'proposals';
        $url = url('freelancer',$slug);
        return [
            'Job' => $RejectedFreelancer['job_name'],
            'Url' => $url,
        ];
    }
}
