<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DisputeRaised extends Notification
{
    use Queueable;
    protected $DisputeRaised;
    // var $name;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($DisputeRaised)
    {
        $this->DisputeRaised=$DisputeRaised;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $DisputeRaised = $this->DisputeRaised;
        // dd($DisputeRaised['name']);
       
        return (new MailMessage)
                    // ->line('The introduction to the notification.')
                    // ->action('Notification Action', url('/'))
                    // ->line('Thank you for using our application!');
                    
                    ->greeting('Hello!')
                    ->line('A dispute has been raised')
                    ->line('Bidder: '.$DisputeRaised['name'])
                    ->line('Job: '.$DisputeRaised['project_link'])
                    ->line('Message: '.$DisputeRaised['message'])
                    ->line('Reason: '.$DisputeRaised['reason'])

                    ->action('Dispute Action', url('job',$DisputeRaised['project_title']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $DisputeRaised = $this->DisputeRaised;
        $url = url('job',$DisputeRaised['project_title']);
        return [
            'Job_id' => $DisputeRaised['job_id'],
            'Freelancer' => $DisputeRaised['name'],
            'Job' => $DisputeRaised['project_link'],
            'Url' => $url,
        ];
    }
}
