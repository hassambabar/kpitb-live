<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class JobPublished extends Notification
{
    use Queueable;
    protected $JobPublished;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($JobPublished)
    {
        $this->JobPublished=$JobPublished;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $JobPublished = $this->JobPublished;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Procurer: '.$JobPublished['first_name'])
            ->line('Administration has published your job:'.$JobPublished['job']);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $JobPublished = $this->JobPublished;
        return [
            'first_name' => $JobPublished['first_name'],
            'last_name' => $JobPublished['last_name'],
            'Job' => $JobPublished['job'],
        ];
    }
}
