<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProposalNegotiationsEmployer extends Notification
{
    use Queueable;
    protected $ProposalNegotiationsEmployer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ProposalNegotiationsEmployer)
    {
        $this->ProposalNegotiationsEmployer=$ProposalNegotiationsEmployer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ProposalNegotiationsEmployer =$this->ProposalNegotiationsEmployer;
        return [
            'Job_id' => $ProposalNegotiationsEmployer['job_id'],
            'Job' => $ProposalNegotiationsEmployer['job_name'],
            'Sender' => $ProposalNegotiationsEmployer['sender_name'],
            'Message' => $ProposalNegotiationsEmployer['message'],
        ];
    }
}
