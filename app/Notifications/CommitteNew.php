<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommitteNew extends Notification
{
    use Queueable;
    protected $CommitteNew;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    
    public function __construct($CommitteNew)
    {
         
        $this->CommitteNew=$CommitteNew;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $CommitteNew = $this->CommitteNew;

        return (new MailMessage)
            ->greeting('Hello! '.$CommitteNew['first_name'])
            ->line('Your account has been created. Kindly, have a look at your account details.')
            ->line('First Name: '.$CommitteNew['first_name'])
            ->line('Last Name: '.$CommitteNew['last_name'])          
            ->line('Email: '.$CommitteNew['email'])
            ->line('Verification Code: '.$CommitteNew['verification_code'])
            ->line('Password: '.$CommitteNew['password']);
            
            
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}