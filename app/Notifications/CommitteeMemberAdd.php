<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommitteeMemberAdd extends Notification
{
    use Queueable;
    protected $jobDetails;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($jobDetails)
    {
        $this->jobDetails=$jobDetails;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
         $jobDetails =$this->jobDetails;
        // dd($jobDetails);
        return (new MailMessage)
                    ->line("You Are Added as committee members for $jobDetails->title ")
                    ->action('Notification Action', url('/committee/dashboard'));
    }
    public function toDatabase($notifiable)
    {
        // dd($jobDetails);
        $jobDetails = $this->jobDetails;
        $url = url('/committee/dashboard');
        return [
            'jobDetails' => $jobDetails['title'],
            'Url' => $url,
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        // dd($jobDetails);
        $jobDetails = $this->jobDetails;
        $url = url('/committee/dashboard');
        return [
            'jobDetails' => $jobDetails['title'],
            'Url' => $url,
        ];
    }
}
