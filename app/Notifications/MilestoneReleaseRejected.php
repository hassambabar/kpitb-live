<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MilestoneReleaseRejected extends Notification
{
    use Queueable;
    protected $MilestoneReleaseRejected;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MilestoneReleaseRejected)
    {
        $this->MilestoneReleaseRejected=$MilestoneReleaseRejected;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MilestoneReleaseRejected = $this->MilestoneReleaseRejected;
       

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Procurer has rejected to release the milestone invoice.')
            ->line('Procurer: '.$MilestoneReleaseRejected['employer_name'])
            ->line('Milestone: '.$MilestoneReleaseRejected['milestone_name'])
            ->line('Job: '.$MilestoneReleaseRejected['job_name'])
            ->line('Percentage: '.$MilestoneReleaseRejected['amount'])

            ->action('View Job', url('job',$MilestoneReleaseRejected['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MilestoneReleaseRejected = $this->MilestoneReleaseRejected;
        $url = url('job',$MilestoneReleaseRejected['slug']);
        return [
            'Slug' => $MilestoneReleaseRejected['slug'],
            'Employer' => $MilestoneReleaseRejected['employer_name'],
            'Milestone' => $MilestoneReleaseRejected['milestone_name'],
            'Job' => $MilestoneReleaseRejected['job_name'],
            'Url' => $url,
        ];
    }
}
