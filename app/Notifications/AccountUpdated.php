<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccountUpdated extends Notification
{
    use Queueable;
    protected $AccountUpdated;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($AccountUpdated)
    {
        $this->AccountUpdated=$AccountUpdated;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $AccountUpdated = $this->AccountUpdated;

        return (new MailMessage)
            ->greeting('Hello! '.$AccountUpdated['First_Name'])
            ->line('your account has been updated by the KPITB Administration. Kindly, have a look at your account details.')
            ->line('Name: '.$AccountUpdated['First_Name'].''.$AccountUpdated['Last_Name'])
            ->line('Email: '.$AccountUpdated['Email'])
            ->line('Contact Number: '.$AccountUpdated['Contact_Number'])
            ->line('Government Department: '.$AccountUpdated['Govt_Dept'])
            ->line('Password: '.$AccountUpdated['Password']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $AccountUpdated = $this->AccountUpdated;
        return [
            'Title' => 'Admin updated your profile: ',
            'Name' => $AccountUpdated['First_Name'],
        ];
    }
}
