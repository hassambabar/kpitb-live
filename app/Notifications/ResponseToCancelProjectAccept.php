<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResponseToCancelProjectAccept extends Notification
{
    use Queueable;
    protected $ResponseToCancelProjectAccept;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ResponseToCancelProjectAccept)
    {
        $this->ResponseToCancelProjectAccept=$ResponseToCancelProjectAccept;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ResponseToCancelProjectAccept = $this->ResponseToCancelProjectAccept;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('The Procurer has accepted to cancel the project.')
            ->line('Procurer: '.$ResponseToCancelProjectAccept['employer_name'])
            ->line('Job: '.$ResponseToCancelProjectAccept['job_name'])
            ->line('Proposal Amount: '.$ResponseToCancelProjectAccept['amount']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ResponseToCancelProjectAccept = $this->ResponseToCancelProjectAccept;
        return [
            'Job_id' => $ResponseToCancelProjectAccept['job_id'],
            'Employer' => $ResponseToCancelProjectAccept['employer_name'],
            'Job' => $ResponseToCancelProjectAccept['job_name'],
        ];
    }
}
