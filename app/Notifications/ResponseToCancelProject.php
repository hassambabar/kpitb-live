<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResponseToCancelProject extends Notification
{
    use Queueable;
    protected $ResponseToCancelProject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ResponseToCancelProject)
    {
        $this->ResponseToCancelProject=$ResponseToCancelProject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ResponseToCancelProject = $this->ResponseToCancelProject;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('The Procurer has rejected to close the project.')
            ->line('Procurer: '.$ResponseToCancelProject['employer_name'])
            ->line('Job: '.$ResponseToCancelProject['job_name'])
            ->line('Proposal Amount: '.$ResponseToCancelProject['amount']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ResponseToCancelProject = $this->ResponseToCancelProject;

        return [
            'Job_id' => $ResponseToCancelProject['job_id'],
            'Employer' => $ResponseToCancelProject['employer_name'],
            'Job' => $ResponseToCancelProject['job_name'],
        ];
    }
}
