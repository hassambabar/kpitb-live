<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProposalsReceived extends Notification
{
    use Queueable;
    protected $ProposalsReceived;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ProposalsReceived)
    {
        $this->ProposalsReceived=$ProposalsReceived;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail'];
        return ['database', 'mail'];
        // return ['notice']; in web notification will also be handled
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ProposalsReceived = $this->ProposalsReceived;
        // dd($Hired['employer_name']); exit;
       
        

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You have received a proposal.')
            ->line('Freelancer: '.$ProposalsReceived['freelancer_first_name'].$ProposalsReceived['freelancer_second_name'])
            ->line('Job: '.$ProposalsReceived['job_name'])

            ->action('View Job Details', url('job',$ProposalsReceived['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ProposalsReceived = $this->ProposalsReceived;
        $slug = 'proposals';
        $url = url('freelancer',$slug);
        return [
            'first_name' => $ProposalsReceived['freelancer_first_name'],
            'second_name' => $ProposalsReceived['freelancer_second_name'],
            'Job' => $ProposalsReceived['job_name'],
            'slug' => $ProposalsReceived['slug'],
            'Url' => $url,
        ];
    }
}
