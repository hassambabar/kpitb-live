<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdministrationsMessage extends Notification
{
    use Queueable;
    protected $AdministrationsMessage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($AdministrationsMessage)
    {
        $this->AdministrationsMessage=$AdministrationsMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $AdministrationsMessage = $this->AdministrationsMessage;

        return (new MailMessage)
            ->greeting('Hello! '.$AdministrationsMessage['Name'])
            ->line('The KPITB Administration has sent you a message.')
            ->line('Message: '.$AdministrationsMessage['message']);
    }

    public function toDatabase($notifiable)
    {
        $AdministrationsMessage = $this->AdministrationsMessage;
        return [
            
            'Name' => $AdministrationsMessage['Name'],
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $AdministrationsMessage = $this->AdministrationsMessage;
        return [
            
            'Name' => $AdministrationsMessage['Name'],
        ];
    }
}
