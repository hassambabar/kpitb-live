<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MilestoneCompleted extends Notification
{
    use Queueable;
    protected $MilestoneCompleted;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MilestoneCompleted)
    {
        $this->MilestoneCompleted=$MilestoneCompleted;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MilestoneCompleted = $this->MilestoneCompleted;
        // dd($MilestoneCompleted['slug']); exit;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Your Milestone has been completed')
            ->line('Bidder: '.$MilestoneCompleted['freelancer_name'])
            ->line('Milestone: '.$MilestoneCompleted['milestone_name'])
            ->line('Job: '.$MilestoneCompleted['job_name'])
            ->line('Procurer: '.$MilestoneCompleted['employer_name'])
            ->line('Amount: '.$MilestoneCompleted['amount'])
            ->line('Description: '.$MilestoneCompleted['message'])

            ->action('View Job', url('job',$MilestoneCompleted['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MilestoneCompleted = $this->MilestoneCompleted;
        $url = url('job',$MilestoneCompleted['slug']);
        return [
            'Slug' => $MilestoneCompleted['slug'],
            'Job' => $MilestoneCompleted['job_name'],
            'Milestone' => $MilestoneCompleted['milestone_name'],
            'Url' => $url,
        ];
    }
}
