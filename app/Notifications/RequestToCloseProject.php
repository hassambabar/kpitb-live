<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestToCloseProject extends Notification
{
    use Queueable;
    protected $RequestToCloseProject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($RequestToCloseProject)
    {
        $this->RequestToCloseProject=$RequestToCloseProject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $RequestToCloseProject = $this->RequestToCloseProject;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('The Bidder has requested to close the project.')
            ->line('Bidder: '.$RequestToCloseProject['freelancer_name'])
            ->line('Job: '.$RequestToCloseProject['job_name'])
            ->line('Proposal Amount: '.$RequestToCloseProject['amount']);
            // ->line('Detail : '.$RequestToCloseProject['detail']);
            // ->action('View Job Details', url('proposal',$RequestToCloseProject['slug'],'hired'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $RequestToCloseProject = $this->RequestToCloseProject;
        return [
            'Slug' => $RequestToCloseProject['slug'],
            'Freelancer' => $RequestToCloseProject['freelancer_name'],
            'Job' => $RequestToCloseProject['job_name'],
        ];
    }
}
