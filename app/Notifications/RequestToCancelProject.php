<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestToCancelProject extends Notification
{
    use Queueable;
    protected $RequestToCancelProject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($RequestToCancelProject)
    {
        $this->RequestToCancelProject=$RequestToCancelProject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $RequestToCancelProject = $this->RequestToCancelProject;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('The Bidder has requested to cancel the project.')
            ->line('Bidder: '.$RequestToCancelProject['freelancer_name'])
            ->line('Job: '.$RequestToCancelProject['job_name'])
            ->line('Proposal Amount: '.$RequestToCancelProject['amount']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $RequestToCancelProject = $this->RequestToCancelProject;
        return [
            'Job_id' => $RequestToCancelProject['job_id'],
            'Freelancer' => $RequestToCancelProject['freelancer_name'],
            'Job' => $RequestToCancelProject['job_name'],
        ];
    }
}
