<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AssignNewRole extends Notification
{
    use Queueable;
    protected $AssignNewRole;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($AssignNewRole)
    {
        $this->AssignNewRole=$AssignNewRole;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $AssignNewRole = $this->AssignNewRole;

        return (new MailMessage)
            ->greeting('Hello! '.$AssignNewRole['first_name'].''.$AssignNewRole['last_name'])
            ->line('Your role has been updated by the KPITB Administration.')
            ->line('Your Previous Role was: '.$AssignNewRole['previous_role'])
            ->line('Your New Role is: '.$AssignNewRole['new_role']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $AssignNewRole = $this->AssignNewRole;
        return [
            'Name' => $AssignNewRole['first_name'],
            'Previous_Role' => $AssignNewRole['previous_role'],
            'New_Role' => $AssignNewRole['new_role'],
        ];
    }
}
