<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InvoiceAccepted extends Notification
{
    use Queueable;
    protected $InvoiceAccepted;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($InvoiceAccepted)
    {
        $this->InvoiceAccepted=$InvoiceAccepted;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $InvoiceAccepted = $this->InvoiceAccepted;
        //dd($InvoiceAccepted['freelancer_name']); exit;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Your Invoice has been accepted.')
            ->line('Bidder: '.$InvoiceAccepted['freelancer_name'])
            ->line('Job: '.$InvoiceAccepted['job_name'])
            ->line('Amount: '.$InvoiceAccepted['amount'])

            ->action('View Job Details', url('job',$InvoiceAccepted['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $InvoiceAccepted = $this->InvoiceAccepted;
        $url = url('job',$InvoiceAccepted['slug']);
        return [
            'Slug' => $InvoiceAccepted['slug'],
            'Freelancer' => $InvoiceAccepted['freelancer_name'],
            'Job' => $InvoiceAccepted['job_name'],
            'Amount' => $InvoiceAccepted['amount'],
            'Url' => $url,
        ];
    }
}
