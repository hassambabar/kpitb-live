<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;
use Auth;
use DB;
use App\EmailTemplate;
use App\Helper;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationMailable;

class VerifyEmail extends VerifyEmailBase
{
//    use Queueable;

    // change as you want
    public function toMail($notifiable)
    {
        $user = User::find(Auth::user()->id);

         $verification_code = Auth::user()->verification_code; 
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }
        // $template = DB::table('email_types')->select('id')->where('email_type', 'verification_code')->get()->first();
        // $template_data = EmailTemplate::getEmailTemplateByID($template->id);
        // $email_params = array();
        // $email_params['name'] = Helper::getUserName(Auth::user()->id);
        // $email_params['email'] = $user->email;
                           

    //    return Mail::to($user->email)->send(
    //                                     new EmailVerificationMailable(
    //                                         $verification_code,
    //                                         $template_data,
    //                                         $email_params
    //                                     )
    //                                 );


//echo "<pre>"; print_r($template_data); exit;
        return (new MailMessage)
            ->subject(Lang::getFromJson('Verify Email Address'))
            ->line(Lang::getFromJson('Please click the button below to verify your email address.'))
            ->action(
                Lang::getFromJson('Verify Email Address'),
                $this->verificationUrl($notifiable)
            )
            ->line('Your Verification code is '.$verification_code)

            ->line(Lang::getFromJson('If you did not create an account, no further action is required.'));
    }
    
}