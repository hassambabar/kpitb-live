<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class JobUnpublished extends Notification
{
    use Queueable;
    protected $JobUnpublished;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($JobUnpublished)
    {
        $this->JobUnpublished=$JobUnpublished;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $JobUnpublished = $this->JobUnpublished;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Procurer: '.$JobUnpublished['first_name'])
            ->line('Administration has unpublished your job:'.$JobUnpublished['job']);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $JobUnpublished = $this->JobUnpublished;
        return [
            'first_name' => $JobUnpublished['first_name'],
            'last_name' => $JobUnpublished['last_name'],
            'Job' => $JobUnpublished['job'],
        ];
    }
}
