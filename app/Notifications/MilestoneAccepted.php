<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MilestoneAccepted extends Notification
{
    use Queueable;
    protected $MilestoneAccepted;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MilestoneAccepted)
    {
        $this->MilestoneAccepted=$MilestoneAccepted;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MilestoneAccepted = $this->MilestoneAccepted;
        // dd($MilestoneCompleted['slug']); exit;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Your Milestone Invoice has been accepted')
            ->line('Bidder: '.$MilestoneAccepted['name'])
            ->line('Milestone: '.$MilestoneAccepted['Milestone_title'])
            ->line('Job: '.$MilestoneAccepted['project_title'])
            ->line('Percentage: '.$MilestoneAccepted['Milestone_price'])

            ->action('View Job', url('job',$MilestoneAccepted['project_link']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MilestoneAccepted = $this->MilestoneAccepted;
        $url = url('job',$MilestoneAccepted['project_link']);
        return [
            'Slug' => $MilestoneAccepted['slug'],
            'Freelancer' => $MilestoneAccepted['name'],
            'Milestone' => $MilestoneAccepted['Milestone_title'],
            'Job' => $MilestoneAccepted['project_title'],
            'Url' => $url,
        ];
    }
}
