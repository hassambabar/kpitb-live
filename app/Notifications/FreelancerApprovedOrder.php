<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FreelancerApprovedOrder extends Notification
{
    use Queueable;
    protected $FreelancerApprovedOrder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($FreelancerApprovedOrder)
    {
        $this->FreelancerApprovedOrder=$FreelancerApprovedOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $FreelancerApprovedOrder = $this->FreelancerApprovedOrder;

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You order has been approved by the bidder.')
            ->line('Bidder: '.$FreelancerApprovedOrder['freelancer_name'])
            ->line('Job: '.$FreelancerApprovedOrder['job_name'])
            ->line('Proposal Amount: '.$FreelancerApprovedOrder['amount'])

            ->action('View Job Details', url('job',$FreelancerApprovedOrder['slug']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $FreelancerApprovedOrder = $this->FreelancerApprovedOrder;
        $url = url('job',$FreelancerApprovedOrder['slug']);
        return [
            'Job_id' => $FreelancerApprovedOrder['job_id'],
            'Freelancer' => $FreelancerApprovedOrder['freelancer_name'],
            'Job' => $FreelancerApprovedOrder['job_name'],
            'Amount' => $FreelancerApprovedOrder['amount'],
            'Url' => $url,
        ];
    }
}
