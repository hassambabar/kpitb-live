<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProposalNegotiationsFreelancer extends Notification
{
    use Queueable;
    protected $ProposalNegotiationsFreelancer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ProposalNegotiationsFreelancer)
    {
        $this->ProposalNegotiationsFreelancer=$ProposalNegotiationsFreelancer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ProposalNegotiationsFreelancer =$this->ProposalNegotiationsFreelancer;
        return [
            'Job_id' => $ProposalNegotiationsFreelancer['job_id'],
            'Job' => $ProposalNegotiationsFreelancer['job_name'],
            'Sender' => $ProposalNegotiationsFreelancer['sender_name'],
            'Message' => $ProposalNegotiationsFreelancer['message'],
        ];
    }
}
