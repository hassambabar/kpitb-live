<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactUsQuery extends Notification
{
    use Queueable;
    protected $ContactUsQuery;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ContactUsQuery)
    {
        $this->ContactUsQuery=$ContactUsQuery;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $ContactUsQuery =$this->ContactUsQuery;
        
        return (new MailMessage)
                    ->line("You have got a new message from Contact Us page.")
                    ->line('From: '.$ContactUsQuery['user'])
                    ->line('Email: '.$ContactUsQuery['email'])
                    ->line('Subject: '.$ContactUsQuery['subject'])
                    ->line('Body: '.$ContactUsQuery['body']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
       
            $ContactUsQuery = $this->ContactUsQuery;
            return [
                'From' => $ContactUsQuery['user'],
                'Email' => $ContactUsQuery['email'],
                'Subject' => $ContactUsQuery['subject'],
                'Body' => $ContactUsQuery['body'],
            ];
       
    }
}
