<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MilestoneReleaseRequest extends Notification
{
    use Queueable;
    protected $MilestoneReleaseRequest;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MilestoneReleaseRequest)
    {
        $this->MilestoneReleaseRequest=$MilestoneReleaseRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
        $MilestoneReleaseRequest = $this->MilestoneReleaseRequest;
       

        return (new MailMessage)
            ->greeting('Hello!')
            ->line('Bidder has requested to release the milestone invoice.')
            ->line('Bidder: '.$MilestoneReleaseRequest['name'])
            ->line('Milestone: '.$MilestoneReleaseRequest['Milestone_title'])
            ->line('Job: '.$MilestoneReleaseRequest['project_title'])
            ->line('Percentage: '.$MilestoneReleaseRequest['Milestone_price'])

            ->action('View Job', url('job',$MilestoneReleaseRequest['project_link']));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MilestoneReleaseRequest = $this->MilestoneReleaseRequest;
        $url = url('job',$MilestoneReleaseRequest['project_link']);
        return [
            'Job_id' => $MilestoneReleaseRequest['job_id'],
            'Freelancer' => $MilestoneReleaseRequest['name'],
            'Milestone' => $MilestoneReleaseRequest['Milestone_title'],
            'Job' => $MilestoneReleaseRequest['project_title'],
            'Url' => $url,
        ];
    }
}
