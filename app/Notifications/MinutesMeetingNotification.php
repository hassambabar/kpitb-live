<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class MinutesMeetingNotification extends Notification
{
    use Queueable;
    protected $MinutesOfMeeting;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MinutesOfMeeting)
    {
        $this->MinutesOfMeeting=$MinutesOfMeeting;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MinutesOfMeeting =$this->MinutesOfMeeting;
        return (new MailMessage)
                   ->greeting('Minutes Of Meeting For :'.$MinutesOfMeeting->title)
                   ->subject('Minutes Of Meeting For :'.$MinutesOfMeeting->title)
                   ->line(new HtmlString('<strong>Title: </strong> '.$MinutesOfMeeting->MinutesOfMeeting->title))

                   ->line(new HtmlString('<strong>Date: </strong> '.$MinutesOfMeeting->MinutesOfMeeting->date))
                   ->line(new HtmlString('<strong>Location: </strong> '.$MinutesOfMeeting->MinutesOfMeeting->location))
                    ->line(''.$MinutesOfMeeting->MinutesOfMeeting->title.'.')
                    ->line(new HtmlString($MinutesOfMeeting->MinutesOfMeeting->body))
                    ->action('Meeting Detail', url('/minutes-of-meeting',$MinutesOfMeeting->MinutesOfMeeting->id));
                    // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MinutesOfMeeting =$this->MinutesOfMeeting;
        $url = url('/minutes-of-meeting',$MinutesOfMeeting->MinutesOfMeeting->id);
        return [
            'Meeting' => $MinutesOfMeeting->MinutesOfMeeting->title,
            'Url' => $url,
        ];
    }
}
