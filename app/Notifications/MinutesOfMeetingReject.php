<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MinutesOfMeetingReject extends Notification
{
    use Queueable;
    protected $MinutesOfMeetingReject;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($MinutesOfMeetingReject)
    {
        $this->MinutesOfMeetingReject=$MinutesOfMeetingReject;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $MinutesOfMeetingReject =$this->MinutesOfMeetingReject;
        return (new MailMessage)
                   ->line('Committee Member rejected your minutes of meeting for the job.')
                   ->line('Committee Member:' .$MinutesOfMeetingReject['first_name'])
                   ->line('Minutes of meeting:'.$MinutesOfMeetingReject['minutesofmeeting']) 
                   ->line('Job:' .$MinutesOfMeetingReject['job'])
                   ->action('Meeting Detail', url('/add-minutes-of-meeting',$MinutesOfMeetingReject['job_id']));
                    // ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $MinutesOfMeetingReject =$this->MinutesOfMeetingReject;
        $url = url('/add-minutes-of-meeting',$MinutesOfMeetingReject['job_id']);
        return [
            'Meeting' => $MinutesOfMeetingReject['minutesofmeeting'],
            'Job' => $MinutesOfMeetingReject['job'],
            'Job_id' => $MinutesOfMeetingReject['job_id'],
            'First_name' => $MinutesOfMeetingReject['first_name'],
            'Last_name' => $MinutesOfMeetingReject['last_name'],
            'Url' => $url,
        ];
    }
}
