<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommitteUpdate extends Notification
{
    use Queueable;
    protected $CommitteUpdate;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    
    public function __construct($CommitteUpdate)
    {
         
        $this->CommitteUpdate=$CommitteUpdate;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $CommitteUpdate = $this->CommitteUpdate;

        return (new MailMessage)
            ->greeting('Hello! '.$CommitteUpdate['first_name'])
            ->line('Your account has been updated. Kindly, have a look at your account details.')
            ->line('First Name: '.$CommitteUpdate['first_name'])
            ->line('Last Name: '.$CommitteUpdate['last_name'])          
            ->line('Email: '.$CommitteUpdate['email'])
            ->line('Password: '.$CommitteUpdate['password']);
            
            
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
