<?php

/**
 * Class Job.
 *
 
 */

namespace App;

use DB;
use Auth;
use File;
use Storage;
use App\Comments;
use App\Language;
use App\Location;
use App\Proposal;
use App\Milestone;

use Carbon\Carbon;
use App\BidQuestion;
use App\SiteManagement;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 *
 */
class Job extends Model
{
     /**
     * Protected Date
     *
     * @access protected
     * @var    array $dates
     */
    protected $dates = [
        'expiry_date',
        'created_at',
        'updated_at',
    ];
    /**
     * Get all of the categories for the job.
     *
     * @return relation
     */
    public function categories()
    {
        return $this->morphToMany('App\Category', 'catable');
    }

    /**
     * Get all of the languages for the user.
     *
     * @return relation
     */
    public function languages()
    {
        return $this->morphToMany('App\Language', 'langable');
    }

    /**
     * The skills that belong to the job.
     *
     * @return relation
     */
    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }

    /**
     * Get the location that owns the job.
     *
     * @return relation
     */
    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function review()
    {
        
        return $this->belongsTo('App\Review','id','job_id');
    }

    /**
     * Get the employer that owns the job.
     *
     * @return relation
     */
    public function employer()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the proposals associated with job.
     *
     * @return relation
     */
    public function proposals()
    {
        return $this->hasMany('App\Proposal');
    }

    /**
     * Get all of the job's reports.
     *
     * @return relation
     */
    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

    /**
     * Uppload Attcahments.
     *
     * @param mixed $uploadedFile uploaded file
     *
     * @return relation
     */
    public function uploadTempattachments($uploadedFile, $path)
    {
        $filename = $uploadedFile->getClientOriginalName();
        if (!file_exists($path)) {
            File::makeDirectory($path, 0755, true, true);
        }
        Storage::disk('local')->putFileAs(
            $path,
            $uploadedFile,
            $filename
        );
        return 'success';
    }

    /**
     * Set slug before saving in DB
     *
     * @param string $value value
     *
     * @access public
     *
     * @return string
     */
    public function setSlugAttribute($value)
    {
        if (!empty($value)) {
            $temp = str_slug($value, '-');
            if (!Job::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!Job::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }
            $this->attributes['slug'] = $temp;
        }
    }

    /**
     * Store Jobs.
     *
     * @param mixed $request request->attr
     *
     * @return relation
     */
    public function storeJobs($request)
    {
        $json = array();
        if (!empty($request)) {

            $random_number = Helper::generateRandomCode(8);
            $code = strtoupper($random_number);
            $user_id = Auth::user()->id;
            $location = $request['locations'];


            $this->location()->associate($location);
            $this->employer()->associate($user_id);
            $this->title = filter_var($request['title'], FILTER_SANITIZE_STRING);
            $this->slug = filter_var($request['title'], FILTER_SANITIZE_STRING);
            $this->price = filter_var($request['project_cost'], FILTER_SANITIZE_STRING);
            $this->project_level = filter_var($request['project_levels'], FILTER_SANITIZE_STRING);
            $this->description = $request['description'];
            $this->english_level = filter_var($request['english_level'], FILTER_SANITIZE_STRING);
            $this->duration = filter_var($request['job_duration'], FILTER_SANITIZE_STRING);
            $this->freelancer_type = filter_var($request['freelancer_type'], FILTER_SANITIZE_STRING);
            $this->is_featured = filter_var($request['is_featured'], FILTER_SANITIZE_STRING);
            $this->show_attachments = filter_var($request['show_attachments'], FILTER_SANITIZE_STRING);
            $this->address = filter_var($request['address'], FILTER_SANITIZE_STRING);
            $this->longitude = filter_var($request['longitude'], FILTER_SANITIZE_STRING);
            $this->latitude = filter_var($request['latitude'], FILTER_SANITIZE_STRING);

            $this->payment_system = filter_var($request['payment_system'], FILTER_SANITIZE_STRING);

            $this->service_type = filter_var($request['service_type'], FILTER_SANITIZE_STRING);
            $this->selection_procurement_system = filter_var($request['selection_procurement_system'], FILTER_SANITIZE_STRING);
            $this->single_source = filter_var($request['single_source'], FILTER_SANITIZE_STRING);
            $this->single_source_email = filter_var($request['single_source_email'], FILTER_SANITIZE_STRING);
            $this->project_budget = filter_var($request['project_budget'], FILTER_SANITIZE_STRING);
            $this->district = filter_var($request['district'], FILTER_SANITIZE_STRING);
            $this->teachniqal_score = filter_var($request['teachniqal_score'], FILTER_SANITIZE_STRING);
            $this->financial_score = filter_var($request['financial_score'], FILTER_SANITIZE_STRING);
            if (Schema::hasColumn('jobs', 'expiry_date')) {
                $this->expiry_date = $request['expiry_date'];
            }
            $old_path = 'uploads\jobs\temp';
            $job_attachments = array();
            if (!empty($request['attachments'])) {
                $attachments = $request['attachments'];
                foreach ($attachments as $key => $attachment) {
                    if (Storage::disk('local')->exists($old_path . '/' . $attachment)) {
                        $new_path = 'uploads/jobs/' . $user_id;
                        if (!file_exists($new_path)) {
                            File::makeDirectory($new_path, 0755, true, true);
                        }
                        $filename = time() . '-' . $attachment;
                        Storage::move($old_path . '/' . $attachment, $new_path . '/' . $filename);
                        $job_attachments[] = $filename;
                    }
                }
                $this->attachments = serialize($job_attachments);
            }
            $this->code = $code;
            $this->save();
            $job_id = $this->id;

            if(isset($_POST['question_title'])){
                $questions = count($_POST['question_title']);
                if($questions > 0){
                   foreach ($_POST['question_title'] as $key => $value) {
                     $question_title = $value;
                     $question_score = $_POST['question_score'][$key];
                     $question_type = $_POST['question_type'][$key]; 
    
                     $userId = Auth::id();
             
                     $BidQuestion = new BidQuestion([
                         'title' => $question_title,
                         'score' => $question_score,
                         'type' => $question_type,
                         'job_id' => $job_id,
                         'user_id' => $userId,
                     ]);
                    $BidQuestion->save();
                    
                   } 
                }

            }
           

            if(isset($_POST['payment_system']) && $_POST['payment_system'] == 'milestone' ){
                $milestones = count($_POST['milestones_title']);
                if($milestones > 0){
                   foreach ($_POST['milestones_title'] as $key => $value) {
                     $title = $value;
                     $score = $_POST['milestones_percentage'][$key];
    
                     $userId = Auth::id();
             
                     $Milestone = new Milestone([
                         'title' => $title,
                         'percentage' => $score,
                         'job_id' => $job_id,
                         'user_id' => $userId,
                     ]);
                    $Milestone->save();
                    
                   } 
                }

            }


            
            $skills = $request['skills'];
            $this->skills()->detach();
            if (!empty($skills)) {
                foreach ($skills as $skill) {
                    $this->skills()->attach($skill['id']);
                }
            }
            $job = Job::find($job_id);
            $languages = $request['languages'];
            $job->languages()->sync($languages);
            $categories = $request['categories'];
            $job->categories()->sync($categories);
            $json['type'] = 'success';

            return $json;
        } else {
            $json['type'] = 'error';
            return $json;
        }
    }

    /**
     * Update Jobs.
     *
     * @param mixed   $request request
     * @param integer $id      ID
     *
     * @return $request, ID
     */
    public function updateJobs($request, $id)
    {
        $json = array();
        if (!empty($request)) {
            $job = self::find($id);
            $random_number = Helper::generateRandomCode(8);
            $user_id = $job->user_id;
            $location = $request['locations'];
            $job->location()->associate($location);
            $job->employer()->associate($user_id);
            if ($job->title != $request['title']) {
                $job->slug = filter_var($request['title'], FILTER_SANITIZE_STRING);
            }
            $job->title = filter_var($request['title'], FILTER_SANITIZE_STRING);
            $job->price = filter_var($request['project_cost'], FILTER_SANITIZE_STRING);
            $job->functional_score = filter_var($request['functional_score'], FILTER_SANITIZE_STRING);
            $job->financial_score = filter_var($request['financial_score'], FILTER_SANITIZE_STRING);
            $job->project_level = filter_var($request['project_levels'], FILTER_SANITIZE_STRING);
            $job->description = $request['description'];
            $job->english_level = filter_var($request['english_level'], FILTER_SANITIZE_STRING);
            $job->duration = filter_var($request['job_duration'], FILTER_SANITIZE_STRING);
            $job->freelancer_type = filter_var($request['freelancer_type'], FILTER_SANITIZE_STRING);
            $job->is_featured = filter_var($request['is_featured'], FILTER_SANITIZE_STRING);
            $job->show_attachments = filter_var($request['show_attachments'], FILTER_SANITIZE_STRING);
            $job->address = filter_var($request['address'], FILTER_SANITIZE_STRING);
            $job->longitude = filter_var($request['longitude'], FILTER_SANITIZE_STRING);
            $job->latitude = filter_var($request['latitude'], FILTER_SANITIZE_STRING);

            if(!empty($request['selection_procurement_system'])){

                $job->selection_procurement_system = filter_var($request['selection_procurement_system'], FILTER_SANITIZE_STRING);

            }

            if (Schema::hasColumn('jobs', 'expiry_date')) {
                $job->expiry_date = $request['expiry_date'];
            }
            $old_path = 'uploads\jobs\temp';
            $job_attachments = array();
            if (!empty($request['attachments'])) {
                $attachments = $request['attachments'];
                foreach ($attachments as $key => $attachment) {
                    $filename = $attachment;
                    if (Storage::disk('local')->exists($old_path . '/' . $attachment)) {
                        $new_path = 'uploads/jobs/' . $user_id;
                        if (!file_exists($new_path)) {
                            File::makeDirectory($new_path, 0755, true, true);
                        }
                        $filename = time() . '-' . $attachment;
                        Storage::move($old_path . '/' . $attachment, $new_path . '/' . $filename);
                    }
                    $job_attachments[] = $filename;
                }
                $job->attachments = serialize($job_attachments);
            } else {
                $job->attachments = null;
            }
            if (empty($job->code)) {
                $code = strtoupper($random_number);
                $job->code = $code;
            }
            $job->save();
            $job_id = $job->id;
            $skills = $request['skills'];
            $job->skills()->detach();
            if (!empty($skills)) {
                foreach ($skills as $skill) {
                    $job->skills()->attach($skill['id']);
                }
            }
            $job = Job::find($job_id);
            $languages = $request['languages'];
            $job->languages()->sync($languages);
            $categories = $request['categories'];
            $job->categories()->sync($categories);
            $json['type'] = 'success';
            return $json;
        } else {
            $json['type'] = 'error';
            return $json;
        }
    }

    /**
     * Get all of the categories for the job.
     *
     * @param string $keyword                keyword
     * @param string $search_categories      search_categories
     * @param string $search_locations       search_locations
     * @param string $search_skills          search_skills
     * @param string $search_project_lengths search_project_lengths
     * @param string $search_languages       search_languages
     *
     * @return relation
     */
    public static function getSearchResult(
        $keyword,
        $search_categories,
        $search_locations,
        $search_skills,
        $search_project_lengths,
        $search_languages,
        $display_completed_projects,
        $sortby,
        $search_project_levels,
        $min_price,
        $max_price
    ) {

//dd($search_project_lengths); 
        $project_settings = !empty(SiteManagement::getMetaValue('project_settings')) ? SiteManagement::getMetaValue('project_settings') : array();

        $completed_project_setting = !empty($project_settings) && !empty($project_settings['enable_completed_projects']) ? $project_settings['enable_completed_projects'] : 'true';
        $json = array();
        $jobs = Job::select('*');
        $job_id = array();
        $filters = array();
        $filters['type'] = 'job';
        if (!empty($keyword)) {
            $filters['s'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        };
        if (!empty($search_categories)) {
            $filters['category'] = $search_categories;
            foreach ($search_categories as $key => $search_category) {
                $categor_obj = Category::where('slug', $search_category)->first();
                $category = !empty($categor_obj) && !empty($categor_obj->id) ? Category::find($categor_obj->id) : '';
                if (!empty($category) && !empty($category->jobs)) {
                    $category_jobs = $category->jobs->pluck('id')->toArray();
                    foreach ($category_jobs as $id) {
                        $job_id[] = $id;
                    }
                }
            }
            $jobs->whereIn('id', $job_id);
        }
        if (!empty($search_locations)) {

            $locations = array();
            $filters['locations'] = $search_locations;
            if (is_array($search_locations)) {
                $locations = Location::select('id')->whereIn('slug', $search_locations)->get()->pluck('id')->toArray();
            } else {
                $locations = Location::select('id')->where('slug', $search_locations)->get()->pluck('id')->toArray();
            }
            $jobs->whereIn('location_id', $locations);
        }
        if (!empty($search_skills)) {
            $filters['skills'] = $search_skills;
            foreach ($search_skills as $key => $search_skill) {
                $skill_obj = Skill::where('slug', $search_skill)->first();
                $skill = Skill::find($skill_obj->id);
                if (!empty($skill->jobs)) {
                    $skill_jobs = $skill->jobs->pluck('id')->toArray();
                    foreach ($skill_jobs as $id) {
                        $job_id[] = $id;
                    }
                }
            }
            $jobs->whereIn('id', $job_id);
        }
        if (!empty($search_project_lengths)) {
            $filters['project_lengths'] = $search_project_lengths;
            $jobs->whereIn('duration', $search_project_lengths);
        }
        if (!empty($search_project_levels)) {
            $filters['project_levels'] = $search_project_levels;
            $jobs->whereIn('project_level', $search_project_levels);
        }
        


        if (!empty($search_languages)) {
            $filters['languages'] = $search_languages;
            $languages = Language::whereIn('slug', $search_languages)->get();
            foreach ($languages as $key => $language) {
                if (!empty($language->jobs[$key]->id)) {
                    $job_id[] = $language->jobs[$key]->id;
                }
            }
            $jobs->whereIn('id', $job_id);
        }

    


        if (!empty($max_price)) {
            $jobs->whereBetween('price', [$min_price, $max_price]);
        }
       // echo "<pre>"; print_r($completed_project_setting); 
        if ($completed_project_setting == 'false') {
            $jobs = $jobs->where('status', '!=', 'completed');
        }
        $jobs = $jobs->where('status', '=', 'posted');

        if (Schema::hasColumn('jobs', 'expiry_date')) {
            //$jobs = $jobs->WhereNull('expiry_date')->orWhereDate('expiry_date', '>', date('Y-m-d'));
            $jobs = $jobs->WhereDate('expiry_date', '>', date('Y-m-d'));

        }
        $query = str_replace(array('?'), array('\'%s\''), $jobs->toSql());
        // $query = vsprintf($query, $jobs->getBindings());
        // dump($query);
        if (!empty($sortby)) {
            $filters['sortby'] = $sortby;

            if($sortby == 'recently_added'){
                $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');

            }elseif($sortby == 'expiry_date'){
                $jobs = $jobs->orderByRaw("expiry_date asc")->paginate(10)->setPath('');

            }else{
                $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');

            }
        }else{
            $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');

        }

      //  dd($filters);
        foreach ($filters as $key => $filter) {
            $pagination = $jobs->appends(
                array(
                    $key => $filter
                )
            );
        }
        $json['jobs'] = $jobs;
        return $json;
    }

    /**
     * Delete recoed from storage
     *
     * @param int $id id
     *
     * @return relation
     */
    public static function deleteRecord($id)
    {
        $job = self::find($id);
        if (!empty($job->proposals)) {
            foreach ($job->proposals as $key => $proposal) {
                $proposal = Proposal::find($proposal->id);
                $proposal->delete();
            }
        }
        $job->skills()->detach();
        $job->languages()->detach();
        $job->categories()->detach();
        return $job->delete();
    }

    /**
     * Get order
     *
     * @param int   $project_id project_id
     *
     * @return response
     */
    public static function getProjectOrder($project_id)
    {
        return DB::table('orders')
            ->join('proposals', 'orders.product_id', '=', 'proposals.id')
            ->join('jobs', 'proposals.job_id', '=', 'jobs.id')
            ->select('orders.product_id')
            ->where('orders.type', 'job')
            ->where('jobs.id', $project_id)
            ->first();
    }

    public function comments()
    {
        return $this->hasMany(Comments::class)->whereNull('parent_id');
    }

    public static function UnpublishJob($id)
    {
        return $job_unpublished = DB::table('jobs')->where('id', $id)->update(['unpublished' => '0']);
    }

    public static function PublishJob($id)
    {
        return $job_published = DB::table('jobs')->where('id', $id)->update(['unpublished' => '1']);
    }

}
