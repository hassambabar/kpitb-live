<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    //
    protected $fillable = array(
        'title', 'percentage', 'start_date','end_date','body','user_id', 'job_id','status','status_request','amount','invoice_id','reciver_status'
    );

    public function milestonInvoice()
    {
        return $this->hasOne(MilestoneInvoice::class, 'id', 'invoice_id');
    }

    public function milestonDispute()
    {
        return $this->hasOne(milestoneDispute::class, 'milestone_id');
    }

}
