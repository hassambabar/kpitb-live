<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class milestoneDispute extends Model
{
    //
    protected $fillable = array(
        'milestone_id', 'milestone_invoice_id', 'freelancer_id','freelancer_reason','freelancer_description','pe_id', 'pe_reason','pe_description','status','job_id','pe_status','freelancer_status'
    );
}
