<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'district';
    public $timestamps = false;
    protected $fillable = ['id', 'loc_id', 'district_name'];


    public static function savedistrict($insArr)
    {
        DB::table('district')->insert($insArr);
    }

    // public static function load_team($id = 0)
    // {
    //     $dataAdapter = DB::table('meet_team')->select('*')->where('id',$id)->first();
    //     return (array) $dataAdapter;
    // }

    public static function updatedistrict($data, $id = 0)
    {
      $affected = DB::table('district')
            ->where('id', $id)
            ->update($data);
    }

    // public static function delete_team($id)
    // {
    //   MeetTeam::where('id',$id)->delete();

    // }

    public static function getalldistrict($id)
    {
          //$dataAdapter = DB::table('govt_departments')->select('*')->where('org_type_id',$id)->get()->toArray();
          $dataAdapter = DB::table('locations')
            ->join('district', 'locations.id', '=', 'district.org_type_id')
            ->select('locations.*', 'district.*')
            ->get()->toArray();
          return (array) $dataAdapter;
    }
    
    public static function getAllDistDropDown($id)
    {
          //$dataAdapter = DB::table('govt_departments')->select('*')->where('org_type_id',$id)->get()->toArray();
          $dataAdapter = DB::table('district')
                     ->select('*')
                     ->where('loc_id', '=', $id)
                     ->get()->toArray();
          return (array) $dataAdapter;
    }


}
