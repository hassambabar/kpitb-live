<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MileStoneValue implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $message = 'MileStone Value Will Provide a Valid KPRA No';
    public function __construct($message = '')
    {
        if (strlen($message)) {
            $this->message = $message;
        }
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //


        $this->message = 'Milestone/s percentage should equal 100%';

        return false;


        // else{
        //     

        // }
        //curl_close ($ch);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
