<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ExpireRangeCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $message = 'Please Choose Corect Expiry Date According to Job Duration';
    public function __construct($message = '')
    {
        if (strlen($message)) {
            $this->message = $message;
        }
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
            
                $this->message = 'Please Choose Corect Expiry Date According to Job Duration';
               
                return false;          
        
        // else{
        //     

        // }
        //curl_close ($ch);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}

