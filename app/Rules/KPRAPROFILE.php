<?php

namespace App\Rules;
use App\KpraInfo;

use Illuminate\Contracts\Validation\Rule;

class KPRAPROFILE implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $message = 'Please Provide a Valid KPRA No';
    public function __construct($message = '')
    {
        if (strlen($message)) {
            $this->message = $message;
        }
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://175.107.62.188:4448/kpradss/index.php/Api/showStatus/$value");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $jsonArrayResponse = json_decode($server_output);
       // echo "<pre>"; print_r($jsonArrayResponse); exit;
        if (isset($jsonArrayResponse->status)) {
            return false;
        } else {
            $KpraInfo = KpraInfo::where('NTN',$value)->first();
            
            if($KpraInfo){
                $this->message = 'This KPRA Number Already Registered';
                return true;
            }
            return true;
        }
        // else{
        //     

        // }
        //curl_close ($ch);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
