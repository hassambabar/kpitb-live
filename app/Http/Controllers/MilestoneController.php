<?php

namespace App\Http\Controllers;

use Auth;
use App\Job;
use App\User;
use App\Helper;
use App\Profile;
use App\BidScore;
use App\Proposal;
use App\Milestone;
use App\BidQuestion;


use App\MinutesOfMeeting;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

class MilestoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    }

    public function getMilestone($id)
    {
        $Milestones = Milestone::where('job_id', $id)->get();
        $jobID = $id;
        return view('back-end.employer.Milestone.index', compact('Milestones', 'jobID'));
    }
    public function show($id)
    {
        $MinutesOfMeeting = Milestone::where('job_id', $id);
        $attachments = !empty($MinutesOfMeeting->attachments) ? unserialize($MinutesOfMeeting->attachments) : '';

        return view('back-end.employer.jobs.MinutesOfMeetingShow', compact('MinutesOfMeeting', 'attachments'));
    }

    public function edit($id)
    {
        $userId = Auth::id();
        $Milestone = Milestone::where('id', $id)->where('user_id', $userId)->first();
        return view('back-end.employer.Milestone.edit', compact('Milestone'));
    }
    public function createScore($id, $jobID)
    {
        $userId = Auth::id();
        $job = Job::find($jobID);
        $proposal = Proposal::find($id);
        $accepted_proposal = array();
        $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)->first();

        $BidQuestions = BidQuestion::where('job_id', $jobID)->get();
        $TechnicalQuestions = BidQuestion::where('job_id', $jobID)->where('type', 0)->get();
        $FinancialQuestions = BidQuestion::where('job_id', $jobID)->where('type', 1)->get();

        foreach ($TechnicalQuestions as $key => $value) {
            # code...
            $BidScores = BidScore::where('proposal_id', $proposal->id)->where('question_id', $value->id)->get();
            $user_score = 0;
            foreach ($BidScores as $bid_score) {
                if ($bid_score->member_id == $userId) {
                    $user_score = $bid_score->score;
                }
            }
            $BidScoreSingle = $user_score;
            $TechnicalQuestions[$key]->BidScore = $BidScoreSingle;
        }

        foreach ($FinancialQuestions as $key => $value) {
            # code...
            $BidScores= BidScore::where('proposal_id', $proposal->id)->where('question_id', $value->id)->get();
            $user_score = 0;
            foreach ($BidScores as $bid_score) {
                if ($bid_score->member_id == $userId) {
                    $user_score = $bid_score->score;
                }
            }
            $BidScoreSingle = $user_score;
            $FinancialQuestions[$key]->BidScore = $BidScoreSingle;
        }

        $BidScore = BidScore::where('proposal_id', $proposal->id)->count();


        return view('back-end.employer.jobs.AddQuestionScore', compact('BidQuestions', 'proposal', 'job', 'accepted_proposal', 'TechnicalQuestions', 'FinancialQuestions', 'BidScore'));
    }


    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'title' => 'required',
                // 'start_date'    => 'required',
                // 'end_date'    => 'required',
                'amount'    => 'required',
                'percentage'    => 'required',
                'body'    => 'required',
                'job_id'    => 'required',
                'start_date' => 'required|before:end_date',
                'end_date' => 'required',

            ]
        );


        $userId = Auth::id();

        $job = Job::findOrFail($_POST['job_id']);

        if ($job) {
            if (strtotime($_POST['start_date']) > strtotime($job->expiry_date) || strtotime($_POST['end_date']) > strtotime($job->expiry_date)) {
                return redirect()->back()->withErrors(['Milestone dates cannot exceed Job expiry date! Job Expiry Date "' . date('m/d/Y', strtotime($job->expiry_date)) . '".']);
            }

            if ($_POST['amount'] > $job->price) {
                return redirect()->back()->withErrors(['Milestone amount cannot exceed Job price! Job Price "' . $job->price . '".']);
            }

            $All_Job_Milestones = Milestone::where('job_id', $job->id)->get();
            $total_milestones_cost = 0;
            $total_milestones_percentage = 0;
            foreach ($All_Job_Milestones as $job_milestone) {
                $total_milestones_cost = $total_milestones_cost + $job_milestone->amount;
                $total_milestones_percentage = $total_milestones_percentage + $job_milestone->percentage;
            }

            if (($total_milestones_cost + $_POST['amount']) > $job->price) {
                return redirect()->back()->withErrors(['Total Milestone/s amount cannot exceed Job price! Job Price "' . $job->price . '".']);
            }

            if (($total_milestones_percentage + $_POST['percentage']) > 100) {
                return redirect()->back()->withErrors(['Total Milestone/s percentage cannot exceed 100%']);
            }
        }

        # code...
        $Milestone = new Milestone([
            'title' => $_POST['title'],
            'amount' => $_POST['amount'],
            'percentage' => $_POST['percentage'],
            'body' => $_POST['body'],
            'start_date' => $_POST['start_date'],
            'end_date' => $_POST['end_date'],
            'job_id' => $_POST['job_id'],
            'user_id' => $userId,

        ]);
        $Milestone->save();

        return redirect()->back()->withSuccess('Saved!');
    }


    public function update(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'title' => 'required',
                'start_date' => 'required|before:end_date',

                'end_date'    => 'required',
                'amount'    => 'required',
                'body'    => 'required',
            ]
        );


        # code...
        $Milestone = Milestone::where('id', $id)->first();

        $job = Job::findOrFail($Milestone->job_id);
        if ($job) {
            if (strtotime($_POST['start_date']) > strtotime($job->expiry_date) || strtotime($_POST['end_date']) > strtotime($job->expiry_date)) {
                return redirect()->back()->withErrors(['Milestone dates cannot exceed Job expiry date! Job Expiry Date "' . date('m/d/Y', strtotime($job->expiry_date)) . '".']);
            }

            if ($_POST['amount'] > $job->price) {
                return redirect()->back()->withErrors(['Milestone amount cannot exceed Job price! Job Price "' . $job->price . '".']);
            }

            $All_Job_Milestones = Milestone::where('job_id', $job->id)->get();
            $total_milestones_cost = 0;
            $total_milestones_percentage = 0;
            foreach ($All_Job_Milestones as $job_milestone) {
                $total_milestones_cost = $total_milestones_cost + $job_milestone->amount;
                $total_milestones_percentage = $total_milestones_percentage + $job_milestone->percentage;
            }
            // dd($total_milestones_cost);
            if ((($total_milestones_cost - $Milestone->amount) + $_POST['amount']) > $job->price) {
                return redirect()->back()->withErrors(['Total Milestone/s amount cannot exceed Job price! Job Price "' . $job->price . '".']);
            }

            // dd($total_milestones_percentage);
            if ((($total_milestones_percentage - $Milestone->percentage) + $_POST['percentage']) > 100) {
                return redirect()->back()->withErrors(['Total Milestone/s percentage cannot exceed 100%']);
            }
        }

        $Milestone->title =  $_POST['title'];
        $Milestone->start_date =  $_POST['start_date'];
        $Milestone->end_date =  $_POST['end_date'];
        $Milestone->amount =  $_POST['amount'];
        $Milestone->body =  $_POST['body'];
        $Milestone->percentage =  $_POST['percentage'];

        $Milestone->save();

        return redirect("employer/milestone/$Milestone->job_id")->withSuccess('Updated!');
    }



    public function destroy($id)
    {

        $Milestone = Milestone::find($id);
        $Milestone->delete();

        return \Redirect::back()->withSuccess(['Deleted']);
    }
}
