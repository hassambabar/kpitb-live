<?php

/**
 * Class UserController
 *

 */

namespace App\Http\Controllers;
// namespace Illuminate\Notifications;
use DB;
use PDF;
use Auth;
use File;
use Hash;
use View;
use Cache;
use App\Job;
use Session;
use Storage;
use App\Item;
use App\Page;
use App\User;
use App\Offer;
use App\Order;
use App\Skill;
use App\Helper;
use App\Payout;
use App\Report;
use App\Review;
use App\Invoice;
use App\Message;
use App\Package;
use App\Profile;
use App\Service;
use App\BidScore;
use App\KpraInfo;
use App\Language;
use App\Location;
use App\Proposal;
use App\Milestone;
use Carbon\Carbon;
use App\JobDispute;
use App\BidQuestion;
use App\EmailTemplate;
use App\SiteManagement;
use App\CommitteeMember;
use App\milestoneDispute;
use App\MilestoneInvoice;
use App\MinutesOfMeeting;
use App\MeetingAccept;
use App\Rules\KPRAPROFILE;
use Illuminate\Support\Arr;
use App\Notifications\Hired;
use Illuminate\Http\Request;
use App\Mail\AdminEmailMailable;
use App\Mail\GeneralEmailMailable;
use App\Notifications\CommitteNew;
use App\Notifications\InvoicePaid;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Exception;
use App\Mail\EmployerEmailMailable;
use App\Notifications\adminNewUser;
use App\Notifications\JobPublished;
use App\Notifications\SubmitReport;
use App\Notifications\AssignNewRole;
use App\Notifications\DisputeRaised;
use Illuminate\Support\Facades\Mail;
use App\Mail\FreelancerEmailMailable;
use App\Notifications\AccountUpdated;
use App\Notifications\CommitteUpdate;
use App\Notifications\ContactUsQuery;
use App\Notifications\JobUnpublished;
use Illuminate\Support\Facades\Input;
use App\Notifications\InvoiceAccepted;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\MilestoneAccepted;
use App\Notifications\ProposalsReceived;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\CommitteeMemberAdd;
use App\Notifications\MilestoneCompleted;
use App\Notifications\RejectedFreelancer;
use Illuminate\Support\Facades\Validator;
use App\Notifications\CommitteeMemberDelete;
use App\Notifications\RequestToCloseProject;
use Illuminate\Support\Facades\Notification;
use App\Notifications\AdministrationsMessage;
use App\Notifications\MinutesOfMeetingAccept;
use App\Notifications\MinutesOfMeetingReject;
use App\Notifications\RequestToCancelProject;
use App\Notifications\ResponseToCloseProject;
use App\Notifications\FreelancerApprovedOrder;
use App\Notifications\MilestoneReleaseRequest;
use App\Notifications\ResponseToCancelProject;
use App\Notifications\MilestoneReleaseRejected;
use App\Notifications\MinutesMeetingNotification;
use App\Notifications\ProposalNegotiationsEmployer;
use App\Notifications\ResponseToCancelProjectAccept;

/**
 * Class UserController
 *
 */
class UserController extends Controller
{
    /**
     * Defining public scope of varriable
     *
     * @access public
     *
     * @var array $user
     */
    use HasRoles;
    protected $user;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @param instance $user    make instance
     * @param instance $profile make profile instance
     *
     * @return void
     */
    public function __construct(User $user, Profile $profile)
    {
        $this->user = $user;
        $this->profile = $profile;
    }


    public function userOnlineStatus()
    {
        $users = User::all();
        foreach ($users as $user) {
            if (Cache::has('user-is-online-' . $user->id))
                echo $user->first_name . " is online. Last seen: " . Carbon::parse($user->last_seen)->diffForHumans() . " <br>";
            else
                echo $user->first_name . " is offline. Last seen: " . Carbon::parse($user->last_seen)->diffForHumans() . " <br>";
        }
    }

    /**
     * Profile Manage Account/ Profile Settings
     *
     * @access public
     *
     * @return View
     */
    public function accountSettings()
    {
        $languages = Language::pluck('title', 'id');
        $user_id = Auth::user()->id;
        $profile = new Profile();
        $saved_options = $profile::select('profile_searchable', 'profile_blocked', 'english_level')
            ->where('user_id', $user_id)->get()->first();
        $english_levels = Helper::getEnglishLevelList();
        $user_level = !empty($saved_options->english_level) ? $saved_options->english_level : trans('lang.basic');
        $user = $this->user::find($user_id);
        $user_languages = array();
        if (!empty($user->languages)) {
            foreach ($user->languages as $user_language) {
                $user_languages[] = $user_language->id;
            }
        }
        if (file_exists(resource_path('views/extend/back-end/settings/security-settings.blade.php'))) {
            return view(
                'extend.back-end.settings.security-settings',
                compact('languages', 'saved_options', 'user_languages', 'english_levels', 'user_level')
            );
        } else {
            return view(
                'back-end.settings.security-settings',
                compact('languages', 'saved_options', 'user_languages', 'english_levels', 'user_level')
            );
        }
    }

    /**
     * Save user account settings.
     *
     * @param mixed $request request attribute
     *
     * @access public
     *
     * @return View
     */
    public function saveAccountSettings(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $profile = new Profile();
        $user_id = Auth::user()->id;
        $profile->storeAccountSettings($request, $user_id);
        Session::flash('message', trans('lang.account_settings_saved'));
        return Redirect::back();
    }

    /**
     * Reset password form.
     *
     * @access public
     *
     * @return View
     */
    public function resetPassword()
    {
        if (file_exists(resource_path('views/extend/back-end/settings/reset-password.blade.php'))) {
            return view('extend.back-end.settings.reset-password');
        } else {
            return view('back-end.settings.reset-password');
        }
    }

    /**
     * Update reset password.
     *
     * @param mixed $request request attributes
     *
     * @access public
     *
     * @return View
     */
    public function requestPassword(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        if (!empty($request)) {
            Validator::extend(
                'old_password',
                function ($attribute, $value, $parameters) {
                    return Hash::check($value, Auth::user()->password);
                }
            );
            $this->validate(
                $request,
                [
                    'old_password'         => 'required',
                    'confirm_password'     => 'required',
                    'confirm_new_password' => 'required',
                ]
            );
            $user_id = $request['user_id'];
            $user = User::find($user_id);
            if (Hash::check($request->old_password, $user->password)) {
                if ($request->confirm_password === $request->confirm_new_password) {
                    $user->password = Hash::make($request->confirm_password);
                    // Send email
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $email_params = array();
                        $template = DB::table('email_types')->select('id')->where('email_type', 'reset_password_email')->get()->first();
                        if (!empty($template->id)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                            $email_params['name'] = Helper::getUserName($user_id);
                            $email_params['email'] = $user->email;
                            $email_params['password'] = $request->confirm_password;
                            try {
                                Mail::to($user->email)
                                    ->send(
                                        new GeneralEmailMailable(
                                            'reset_password_email',
                                            $template_data,
                                            $email_params
                                        )
                                    );
                            } catch (\Exception $e) {
                                Session::flash('error', trans('lang.ph_email_warning'));
                                return Redirect::back();
                            }
                        }
                    }
                    $user->save();
                    Session::flash('message', trans('passwords.reset'));
                    Auth::logout();
                    return Redirect::to('/');
                } else {
                    Session::flash('error', trans('lang.confirmation'));
                    return Redirect::back();
                }
            } else {
                Session::flash('error', trans('lang.pass_not_match'));
                return Redirect::back();
            }
        } else {
            Session::flash('error', trans('lang.something_wrong'));
            return Redirect::back();
        }
    }

    /**
     * Email Notification Settings Form.
     *
     * @access public
     *
     * @return View
     */
    public function emailNotificationSettings()
    {
        $user_email = !empty(Auth::user()) ? Auth::user()->email : '';
        if (file_exists(resource_path('views/extend/back-end/settings/email-notifications.blade.php'))) {
            return view('extend.back-end.settings.email-notifications', compact('user_email'));
        } else {
            return view('back-end.settings.email-notifications', compact('user_email'));
        }
    }

    /**
     * Email Verification Settings Form.
     *
     * @access public
     *
     * @return View
     */
    public function emailVerificationSettings()
    {
        if (file_exists(resource_path('views/extend/back-end/settings/email-verification.blade.php'))) {
            return view('extend.back-end.settings.email-verification');
        } else {
            return view('back-end.settings.email-verification');
        }
    }

    /**
     * Get Verfication code
     * 
     * @return JSON Response
     */
    public function resendCode()
    {
        $json = array();
        $random_number = Helper::generateRandomCode(4);
        $verification_code = strtoupper($random_number);
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            if (empty($user->verification_code)) {
                $user->verification_code = !empty($verification_code) ? $verification_code : null;
                $user->save();
            }
            if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                $email_params = array();
                $template = DB::table('email_types')->select('id')
                    ->where('email_type', 'verification_code')->get()->first();
                if (!empty($template->id)) {
                    $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                    $email_params['verification_code'] = $user->verification_code;
                    $email_params['name']  = Helper::getUserName($user->id);
                    $email_params['email'] = $user->email;
                    Mail::to($user->email)
                        ->send(
                            new GeneralEmailMailable(
                                'verification_code',
                                $template_data,
                                $email_params
                            )
                        );
                }
            }
            $json['type'] = 'success';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.email_not_config');
            return $json;
        }
    }

    /**
     * Set slug before saving in DB
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @access public
     *
     * @return \Illuminate\Http\Response
     */
    public function reVerifyUserCode(Request $request)
    {
        $json = array();
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            if (!empty($request['code'])) {
                if ($request['code'] === $user->verification_code) {
                    $user->user_verified = 1;
                    $user->verification_code = null;
                    $user->save();
                    $json['type'] = 'success';
                    $json['message'] = trans('lang.email_verified');
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.invalid_verification_code');
                    return $json;
                }
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.verify_code');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.session_expire');
            return $json;
        }
    }

    /**
     * Save Email Notification Settings.
     *
     * @param mixed $request request attribute
     *
     * @access public
     *
     * @return View
     */
    public function saveEmailNotificationSettings(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $profile = new Profile();
        $user_id = Auth::user()->id;
        $profile->storeEmailNotification($request, $user_id);
        Session::flash('message', trans('lang.email_settings_saved'));
        return Redirect::back();
    }

    /**
     * Delete Account From.
     *
     * @access public
     *
     * @return View
     */
    public function deleteAccount()
    {
        if (file_exists(resource_path('views/extend/back-end/settings/delete-account.blade.php'))) {
            return view('extend.back-end.settings.delete-account');
        } else {
            return view('back-end.settings.delete-account');
        }
    }

    /**
     * User delete account.
     *
     * @param mixed $request request attributes
     *
     * @access public
     *
     * @return View
     */
    public function destroy(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $this->validate(
            $request,
            [
                'old_password' => 'required',
                'retype_password'    => 'required',
            ]
        );
        $json = array();
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        if (Hash::check($request->old_password, $user->password)) {
            if ($request->old_password === $request->retype_password) {
                if (!empty($user_id)) {
                    $user->profile()->delete();
                    $user->skills()->detach();
                    $user->languages()->detach();
                    $user->categories()->detach();
                    $user->roles()->detach();
                    $user->delete();
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $delete_reason = Helper::getDeleteAccReason($request['delete_reason']);
                        $email_params = array();
                        $template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_delete_account')->get()->first();
                        if (!empty($template->id)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                            $email_params['reason'] = $delete_reason;
                            Mail::to(config('mail.username'))
                                ->send(
                                    new AdminEmailMailable(
                                        'admin_email_delete_account',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }
                    }
                    Auth::logout();
                    $json['acc_del'] = trans('lang.acc_deleted');
                    return $json;
                } else {
                    $json['type'] = 'warning';
                    $json['msg'] = trans('lang.something_wrong');
                    return $json;
                }
            } else {
                $json['type'] = 'warning';
                $json['msg'] = trans('lang.pass_mismatched');
                return $json;
            }
        } else {
            $json['type'] = 'warning';
            $json['msg'] = trans('Wrong Password');
            return $json;
        }
    }

    /**
     * Delete user by admin.
     *
     * @param mixed $request request attributes
     *
     * @access public
     *
     * @return View
     */
    public function deleteUser(Request $request)
    {


        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        if (!empty($request['user_id'])) {
            $user = User::find($request['user_id']);
            if (!empty($user)) {
                $role = $user->getRoleNames()->first();
                if ($role == 'employer') {
                    if (!empty($user->jobs)) {
                        foreach ($user->jobs as $key => $job) {
                            Job::deleteRecord($job->id);
                        }
                    }
                } else if ($role == 'freelancer') {
                    if (!empty($user->proposals)) {
                        foreach ($user->proposals as $key => $proposal) {
                            Proposal::deleteRecord($proposal->id);
                        }
                    }
                }
                $user->profile()->delete();
                $user->skills()->detach();
                $user->services()->detach();
                $user->categories()->detach();
                $user->roles()->detach();
                $user->languages()->detach();
                DB::table('reviews')->where('user_id', $request['user_id'])
                    ->orWhere('receiver_id', $request['user_id'])->delete();
                DB::table('payouts')->where('user_id', $request['user_id'])->delete();
                DB::table('offers')->where('user_id', $request['user_id'])
                    ->orWhere('freelancer_id', $request['user_id'])->delete();
                DB::table('messages')->where('user_id', $request['user_id'])
                    ->orWhere('receiver_id', $request['user_id'])->delete();
                DB::table('items')->where('subscriber', $request['user_id'])
                    ->delete();
                DB::table('followers')->where('follower', $request['user_id'])
                    ->orWhere('following', $request['user_id'])->delete();
                DB::table('disputes')->where('user_id', $request['user_id'])->delete();
                $user->delete();
                $json['type'] = 'success';
                $json['message'] = trans('lang.ph_user_delete_message');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Get Manage Account Data
     *
     * @access public
     *
     * @return View
     */
    public function getManageAccountData()
    {
        if (Auth::user()) {
            $json = array();
            $user_id = Auth::user()->id;
            $profile = User::find($user_id)->profile->first();
            if (!empty($profile)) {
                $json['type'] = 'success';
                if ($profile->profile_searchable == 'true') {
                    $json['profile_searchable'] = 'true';
                }
                if ($profile->profile_blocked == 'true') {
                    $json['profile_blocked'] = 'true';
                }
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        }
    }

    /**
     * Get User Notification Settings
     *
     * @access public
     *
     * @return View
     */
    public function getUserEmailNotificationSettings()
    {
        $json = array();
        $profile = new Profile();
        $notifications = $profile::select('weekly_alerts', 'message_alerts')
            ->where('user_id', Auth::user()->id)->get()->first();
        if (!empty($notifications)) {
            $json['type'] = 'success';
            if ($notifications->weekly_alerts == 'true') {
                $json['weekly_alerts'] = 'true';
            }
            if ($notifications->message_alerts == 'true') {
                $json['message_alerts'] = 'true';
            }
        } else {
            $json['type'] = 'error';
        }
        return $json;
    }

    /**
     * Get User Searchable Settings
     *
     * @access public
     *
     * @return View
     */
    public function getUserSearchableSettings()
    {
        $json = array();
        $profile = new Profile();
        // $user_data = $profile::select('profile_searchable', 'profile_blocked')
        //     ->where('user_id', Auth::user()->id)->get()->first();
        $user_data = User::find(Auth::user()->id);
        if (!empty($user_data)) {
            $json['type'] = 'success';
            if ($user_data->is_disabled == 'true') {
                $json['profile_blocked'] = 'true';
            }
        } else {
            $json['type'] = 'error';
        }
        return $json;
    }

    /**
     * Get user saved item list
     *
     * @param mixed $request request attributes
     * @param int   $role    role
     *
     * @access public
     *
     * @return View
     */
    public function getSavedItems(Request $request, $role = '')
    {
        if (Auth::user()) {
            $user = $this->user::find(Auth::user()->id);
            $profile = $user->profile;
            $saved_jobs        = !empty($profile->saved_jobs) ? unserialize($profile->saved_jobs) : array();
            $saved_freelancers = !empty($profile->saved_freelancer) ? unserialize($profile->saved_freelancer) : array();
            // echo dd($saved_freelancers); exit;
            $saved_employers   = !empty($profile->saved_employers) ? unserialize($profile->saved_employers) : array();
            $currency          = SiteManagement::getMetaValue('commision');
            $symbol            = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            if ($request->path() === 'employer/saved-items') {
                if (file_exists(resource_path('views/extend/back-end/employer/saved-items.blade.php'))) {
                    return view(
                        'extend.back-end.employer.saved-items',
                        compact(
                            'profile',
                            'saved_jobs',
                            'saved_freelancers',
                            'saved_employers',
                            'symbol'
                        )
                    );
                } else {
                    return view(
                        'back-end.employer.saved-items',
                        compact(
                            'profile',
                            'saved_jobs',
                            'saved_freelancers',
                            'saved_employers',
                            'symbol'
                        )
                    );
                }
            } elseif ($request->path() === 'freelancer/saved-items') {
                if (file_exists(resource_path('views/extend/back-end/freelancer/saved-items.blade.php'))) {
                    return view(
                        'extend.back-end.freelancer.saved-items',
                        compact(
                            'profile',
                            'saved_jobs',
                            'saved_freelancers',
                            'saved_employers',
                            'symbol'
                        )
                    );
                } else {
                    return view(
                        'back-end.freelancer.saved-items',
                        compact(
                            'profile',
                            'saved_jobs',
                            'saved_freelancers',
                            'saved_employers',
                            'symbol'
                        )
                    );
                }
            }
        } else {
            abort(404);
        }
    }

    /**
     * Get User Saved Item
     *
     * @param mixed $request request attributes
     *
     * @access public
     *
     * @return View
     */
    public function getUserWishlist(Request $request)
    {
        if (Auth::user()) {
            $user = $this->user::find(Auth::user()->id);
            $profile = $user->profile;
            if (!empty($request['slug'])) {
                $json = array();
                $selected_user = DB::table('users')->select('id')
                    ->where('slug', $request['slug'])->get()->first();
                $role = $this->user::getUserRoleType($selected_user->id);
                if ($role->role_type == 'freelancer') {
                    $json['user_type'] = 'freelancer';
                    if (in_array($selected_user->id, unserialize($profile->saved_freelancer))) {
                        $json['current_freelancer'] = 'true';
                    }
                    return $json;
                } else if ($role->role_type == 'employer') {
                    $json['user_type'] = 'employer';
                    $employer_jobs = $this->user::find($selected_user->id)
                        ->jobs->pluck('id')->toArray();
                    if (!empty($employer_jobs) && !empty(unserialize($profile->saved_jobs))) {
                        if (in_array($employer_jobs, unserialize($profile->saved_jobs))) {
                            $json['employer_jobs'] = 'true';
                        }
                    }
                    if (in_array($selected_user->id, unserialize($profile->saved_employers))) {
                        $json['current_employer'] = 'true';
                    }
                    return $json;
                }
            }
        }
    }

    /**
     * Add job to whishlist.
     *
     * @param mixed $request request->attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function addWishlist(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        if (Auth::user()) {
            $json['authentication'] = true;
            if (!empty($request['id'])) {
                $user_id = Auth::user()->id;
                $id = $request['id'];
                if (!empty($request['column']) && ($request['column'] === 'saved_employers' || $request['column'] === 'saved_freelancer' || $request['column'] === 'saved_services')) {
                    if (!empty($request['seller_id'])) {
                        if ($user_id == $request['seller_id']) {
                            $json['type'] = 'error';
                            $json['message'] = trans('lang.login_from_different_user');
                            return $json;
                        }
                    } else {
                        if ($user_id == $id) {
                            $json['type'] = 'error';
                            $json['message'] = trans('lang.login_from_different_user');
                            return $json;
                        }
                    }
                }
                $profile = new Profile();
                $add_wishlist = $profile->addWishlist($request['column'], $id, $user_id);
                if ($add_wishlist == "success") {
                    $json['type'] = 'success';
                    $json['message'] = trans('lang.added_to_wishlist');
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.something_wrong');
                    return $json;
                }
            }
        } else {
            $json['authentication'] = false;
            $json['message'] = trans('lang.need_to_reg');
            return $json;
        }
    }

    /**
     * Submit Reviews.
     *
     * @param \Illuminate\Http\Request $request request->attr
     *
     * @return \Illuminate\Http\Response
     */

    public function submitReviewjob(Request $request)
    {

        // $this->validate(
        //     $request,
        //     [
        //         'feedback' => 'required',

        //     ]
        // );
        $user_id = Auth::user()->id;
        $project_type = 'job';

        $submit_review = Review::submitReviewJob($request, $user_id, $project_type);
        if ($submit_review['type'] == "success") {
            $json['type'] = 'success';
            $json['message'] = trans('lang.feedback_submit');
            //send email

            //return $json;

            return redirect("employer/dashboard")->with('message', 'Thank You For Proving Feedback');
        } elseif ($submit_review['type'] == "rating_error") {


            return \Redirect::back()->withErrors([trans('lang.rating_required')]);
        } else {

            return \Redirect::back()->withErrors([trans('lang.something_wrong')]);

            return $json;
        }
        return redirect("employer/dashboard")->with('message', 'Thank You For Proving Feedback');
    }
    public function submitReview(Request $request)
    {

        $json = array();
        if (Auth::user()) {
            if ($request['type']) {
                $project_type = $request['type'];
            } else {
                $project_type = 'job';
            }
            $user_id = Auth::user()->id;
            $submit_review = Review::submitReview($request, $user_id, $project_type);
            if ($submit_review['type'] == "success") {
                $json['type'] = 'success';
                $json['message'] = trans('lang.feedback_submit');
                //send email
                if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                    $freelancer = User::find($request['receiver_id']);
                    $email_params = array();
                    $email_params['name'] = Helper::getUserName($freelancer->id);
                    $email_params['link'] = url('profile/' . $freelancer->slug);
                    $email_params['employer'] = Helper::getUserName($user_id);
                    $email_params['employer_profile'] = url('profile/' . Auth::user()->slug);
                    $email_params['ratings'] = $submit_review['rating'];
                    $email_params['review'] = $request['feedback'];
                    if ($project_type == 'job') {
                        $job = Job::find($request['job_id']);
                        $email_params['project_title'] = $job->title;
                        $email_params['completed_project_link'] = url('/job/' . $job->slug);
                        //$freelancer = Proposal::select('freelancer_id')->where('status', 'completed')->first();
                        $job_completed_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_job_completed')->get()->first();
                        if (!empty($job_completed_template->id)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($job_completed_template->id);
                            Mail::to(config('mail.username'))
                                ->send(
                                    new AdminEmailMailable(
                                        'admin_email_job_completed',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }
                        $freelancer_job_completed_template = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_job_completed')->get()->first();
                        if (!empty($freelancer_job_completed_template->id)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($freelancer_job_completed_template->id);
                            Mail::to($freelancer->email)
                                ->send(
                                    new FreelancerEmailMailable(
                                        'freelancer_email_job_completed',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }
                    } else if ($project_type == 'service') {
                        $service = Service::find($request['service_id']);
                        $email_params['project_title'] = $service->title;
                        $email_params['completed_project_link'] = url('service/' . $service->slug);
                        $template_data = Helper::getFreelancerCompletedServiceEmailContent();
                        Mail::to($freelancer->email)
                            ->send(
                                new FreelancerEmailMailable(
                                    'freelancer_email_job_completed',
                                    $template_data,
                                    $email_params
                                )
                            );
                    }
                }
                return $json;
            } elseif ($submit_review['type'] == "rating_error") {
                $json['type'] = 'error';
                $json['message'] = trans('lang.rating_required');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.not_authorize');
            return $json;
        }
    }

    /**
     * Download Attachements.
     *
     * @param \Illuminate\Http\Request $request request->attr
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadAttachments(Request $request)
    {
        if (!empty($request['attachments'])) {
            $freelancer_id = $request['freelancer_id'];
            $path = storage_path() . '/app/uploads/proposals/' . $freelancer_id;
            if (!file_exists($path)) {
                File::makeDirectory($path, 0755, true, true);
            }
            $zip = new \Chumper\Zipper\Zipper();
            foreach ($request['attachments'] as $attachment) {
                $zip->make($path . '/attachments.zip')->add($path . '/' . $attachment);
            }
            $zip->close();
            return response()->download(storage_path('app/uploads/proposals/' . $freelancer_id . '/attachments.zip'));
        } else {
            Session::flash('error', trans('lang.files_not_found'));
            return Redirect::back();
        }
    }

    /**
     * Submit Report
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @access public
     *
     * @return \Illuminate\Http\Response
     */
    public function storeReport(Request $request)
    {

        $json = array();
        if (Auth::user()) {
            $this->validate(
                $request,
                [
                    'description' => 'required',
                    'reason' => 'required',
                ]
            );

            if ($request['model'] == "App\Job" && $request['report_type'] == 'job-report') {
                $job = Job::find($request['id']);
                $job_title = $job->title;
                if ($job->employer->id == Auth::user()->id) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_authorize');
                    return $json;
                }
            }

            if ($request['model'] == "App\Service" && $request['report_type'] == 'service_cancel') {
                $service = Service::find($request['id']);
                $freelancer = $service->seller->first();
                if ($freelancer->id == Auth::user()->id) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_authorize');
                    return $json;
                }
            }
            if ($request['model'] == "App\User" && $request['report_type'] == 'freelancer-report') {
                // dd($request);
                $freelancer = User::find($request['id']);
                $freelancer_name = $freelancer->first_name;
                if ($freelancer->id == Auth::user()->id) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_authorize');
                    return $json;
                }
            }
            // dd($request);
            $request['user_id'] = Auth::user()->id;
            $report = Report::submitReport($request);
            if ($report == 'success') {
                $json['type'] = 'success';
                $user = $this->user::find(Auth::user()->id);
                // notification
                $description = $request->input('description');

                // $reason = $request->input('reason');

                $admin_mail = User::role('admin')->select('*')->first();
                // $job_admin_mail = User::role('job-administrator')->select('*')->first();
// dd($admin_mail);

                $data = array();

                $data['user'] = $user->first_name;
                $data['description'] = $description;
                // $data['reason'] = $reason;
                if (!empty($job)) {
                    $data['subject'] = $job_title;
                    $data['cat'] = 'job';
                } elseif (!empty($freelancer)) {
                    $data['subject'] = $freelancer_name;
                    $data['cat'] = 'freelancer';
                } elseif (!empty($service)) {
                    $data['subject'] = $service;
                    $data['cat'] = 'service';
                }

                if ($request['model'] == "App\Job" && $request['report_type'] == 'job-report') {
                    $roles = array();
                    $roles['admin'] = 'admin';
                    $roles['job-administrator'] = 'job-administrator';
                    foreach ($roles as $role) {
                        $users = User::role($role)->select('*')->get();
                    }

                    // dd($data);
                    // Send the notifications
                    Notification::send($users, new SubmitReport($data));
                } else {
                    // dd($data);
                    Notification::send($admin_mail, new SubmitReport($data));
                }


                // Notification::send($admin_mail, new SubmitReport($data));

                //send email
                if (
                    $request['report_type'] == 'job-report'
                    || $request['report_type'] == 'employer-report'
                    || $request['report_type'] == 'freelancer-report'
                ) {
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $email_params = array();
                        if ($request['report_type'] == 'job-report') {
                            $report_project_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_report_project')->get()->first();
                            if (!empty($report_project_template->id)) {
                                $job = Job::where('id', $request['id'])->first();
                                $template_data = EmailTemplate::getEmailTemplateByID($report_project_template->id);
                                $email_params['reported_project'] = $job->title;
                                $email_params['link'] = url('job/' . $job->slug);
                                $email_params['report_by_link'] = url('profile/' . $user->slug);
                                $email_params['reported_by'] = Helper::getUserName(Auth::user()->id);
                                $email_params['message'] = $request['description'];
                                Mail::to(config('mail.username'))
                                    ->send(
                                        new AdminEmailMailable(
                                            'admin_email_report_project',
                                            $template_data,
                                            $email_params
                                        )
                                    );
                            }
                        } else if ($request['report_type'] == 'employer-report') {
                            $report_employer_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_report_employer')->get()->first();
                            if (!empty($report_employer_template->id)) {
                                $template_data = EmailTemplate::getEmailTemplateByID($report_employer_template->id);
                                $employer = User::find($request['id']);
                                $email_params['reported_employer'] = Helper::getUserName($request['id']);
                                $email_params['link'] = url('profile/' . $employer->slug);;
                                $email_params['report_by_link'] = url('profile/' . $user->slug);
                                $email_params['reported_by'] = Helper::getUserName(Auth::user()->id);
                                $email_params['message'] = $request['description'];
                                Mail::to(config('mail.username'))
                                    ->send(
                                        new AdminEmailMailable(
                                            'admin_email_report_employer',
                                            $template_data,
                                            $email_params
                                        )
                                    );
                            }
                        } else if ($request['report_type'] == 'freelancer-report') {
                            $report_freelancer_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_report_freelancer')->get()->first();
                            if (!empty($report_freelancer_template->id)) {
                                $freelancer = User::find($request['id']);
                                $template_data = EmailTemplate::getEmailTemplateByID($report_freelancer_template->id);
                                $email_params['reported_freelancer'] = Helper::getUserName($request['id']);
                                $email_params['link'] = url('profile/' . $freelancer->slug);
                                $email_params['report_by_link'] = url('profile/' . $user->slug);
                                $email_params['reported_by'] = Helper::getUserName(Auth::user()->id);
                                $email_params['message'] = $request['description'];
                                Mail::to(config('mail.username'))
                                    ->send(
                                        new AdminEmailMailable(
                                            'admin_email_report_freelancer',
                                            $template_data,
                                            $email_params
                                        )
                                    );
                            }
                        }
                    }
                } else if ($request['report_type'] == 'proposal_cancel') {
                    $freelancer_job_cancelled = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_cancel_job')->get()->first();
                    $json['message'] = trans('lang.job_cancelled');
                    if (!empty($freelancer_job_cancelled->id)) {
                        if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                            $template_data = EmailTemplate::getEmailTemplateByID($freelancer_job_cancelled->id);
                            $job = Job::find($request['id']);
                            $proposal = Proposal::where('id', $request['proposal_id'])->first();
                            $freelancer = User::find($proposal->freelancer_id);
                            $email_params['project_title'] = $job->title;
                            $email_params['cancelled_project_link'] = url('job/' . $job->slug);
                            $email_params['name'] = Helper::getUserName($proposal->freelancer_id);
                            $email_params['link'] = url('profile/' . $freelancer->slug);
                            $email_params['employer_profile'] = url('profile/' . Auth::user()->slug);
                            $email_params['emp_name'] = Helper::getUserName(Auth::user()->id);
                            $email_params['msg'] = $request['description'];
                            Mail::to($freelancer->email)
                                ->send(
                                    new FreelancerEmailMailable(
                                        'freelancer_email_cancel_job',
                                        $template_data,
                                        $email_params
                                    )
                                );
                            $job_cancelle_admin_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_cancel_job')->get()->first();
                            if (!empty($job_cancelle_admin_template)) {
                                $template_data = EmailTemplate::getEmailTemplateByID($job_cancelle_admin_template->id);
                            } else {
                                $template_data = '';
                            }
                            Mail::to(config('mail.username'))
                                ->send(
                                    new AdminEmailMailable(
                                        'admin_email_cancel_job',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }
                    }
                } else if ($request['report_type'] == 'service_cancel') {
                    $json['message'] = trans('lang.service_cancelled');
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $freelancer_job_cancelled = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_cancel_job')->get()->first();
                        if (!empty($freelancer_job_cancelled->id)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($freelancer_job_cancelled->id);
                            $service = Service::find($request['id']);
                            $freelancer = $service->seller->first();
                            $email_params['project_title'] = $service->title;
                            $email_params['cancelled_project_link'] = url('service/' . $service->slug);
                            $email_params['name'] = Helper::getUserName($freelancer->id);
                            $email_params['link'] = url('profile/' . $freelancer->slug);
                            $email_params['employer_profile'] = url('profile/' . Auth::user()->slug);
                            $email_params['emp_name'] = Helper::getUserName(Auth::user()->id);
                            $email_params['msg'] = $request['description'];
                            Mail::to($freelancer->email)
                                ->send(
                                    new FreelancerEmailMailable(
                                        'freelancer_email_cancel_job',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }

                        $job_cancelle_admin_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_cancel_job')->get()->first();
                        if (!empty($job_cancelle_admin_template)) {
                            $template_data = EmailTemplate::getEmailTemplateByID($job_cancelle_admin_template->id);
                        } else {
                            $template_data = '';
                        }
                        Mail::to(config('mail.username'))
                            ->send(
                                new AdminEmailMailable(
                                    'admin_email_cancel_job',
                                    $template_data,
                                    $email_params
                                )
                            );
                    }
                }
                if ($request['report_type'] == 'service_cancel') {
                    $json['progress'] = trans('lang.report_submitting');
                }
                $json['message'] = trans('lang.report_submitted');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.not_authorize');
            return $json;
        }
    }

    /**
     * Store resource in DB.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function sendPrivateMessage(Request $request)
    {
        if (Auth::user()) {
            $server = Helper::kpitbIsDemoSiteAjax();
            if (!empty($server)) {
                $response['type'] = 'error';
                $response['message'] = $server->getData()->message;
                return $response;
            }

            $user_id = Auth::user()->id;
            $json = array();

            if ($request['project_type'] == 'job') {
                $purchased_proposal = DB::table('proposals')->select('status')->where('id', $request['proposal_id'])->get()->first();
                $status = $purchased_proposal->status;
                if ($status == "hired") {
                    $proposal = new Proposal();
                    $send_message = $proposal::sendMessage($request, $user_id);
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_allowed_msg');
                    return $json;
                }
            } else {
                $purchase_service = Helper::getPivotService($request['proposal_id']);
                $status = $purchase_service->status;
                if ($status == "hired") {
                    $service = new Service();
                    $send_message = $service::sendMessage($request, $user_id);
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_allowed_msg');
                    return $json;
                }
            }
            if ($send_message = 'success') {
                $json['type'] = 'success';
                $json['progress_message'] = trans('lang.sending_msg');
                $json['message'] = trans('lang.msg_sent');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.not_authorize');
            return $json;
        }
    }

    /**
     * Get Private Messages.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function getPrivateMessage(Request $request)
    {
        $json = array();
        $messages = array();
        if (Auth::user()) {
            $user_id = Auth::user()->id;
            if (!empty($request['id'])) {
                $freelancer_id = $request['recipent_id'];
                $proposal_id = $request['id'];
                $project_type = !empty($request['project_type']) ? $request['project_type'] : 'job';
                $proposal = new Proposal();
                if (Auth::user()->getRoleNames()[0] == 'admin') {
                    if ($project_type == 'service') {
                        $project = DB::table('service_user')->select('user_id')->where('id', $proposal_id)->first();
                    } else {
                        $job = DB::table('proposals')->select('job_id')->where('id', $proposal_id)->first();
                        $project = DB::table('jobs')->where('id', $job->job_id)->select('user_id')->first();
                    }
                    $message_data = $proposal::getProjectHistory($project->user_id, $freelancer_id, $proposal_id, $project_type);
                } else {
                    $freelancer_id = '';
                    $message_data = $proposal::getMessages($user_id, $freelancer_id, $proposal_id, $project_type);
                }
                // $message_data = $proposal::getMessages($user_id, $freelancer_id, $proposal_id, $project_type);
                if (!empty($message_data)) {
                    foreach ($message_data as $key => $data) {
                        $content = strip_tags(stripslashes($data->content));
                        $excerpt = str_limit($content, 100);
                        $default_avatar = url('images/user-login.png');
                        $profile_image = !empty($data->avater)
                            ? '/uploads/users/' . $data->author_id . '/' . $data->avater
                            : $default_avatar;
                        $messages[$key]['id'] = $data->id;
                        $messages[$key]['author_id'] = $data->author_id;
                        $messages[$key]['proposal_id'] = $data->proposal_id;
                        $messages[$key]['content'] = $content;
                        $messages[$key]['excerpt'] = $excerpt;
                        $messages[$key]['user_image'] = asset($profile_image);
                        $messages[$key]['created_at'] = Carbon::parse($data->created_at)->format('d-m-Y');
                        $messages[$key]['notify'] = $data->notify;
                        $messages[$key]['attachments'] = !empty($data->attachments) ? 1 : 0;
                    }
                    $json['type'] = 'success';
                    $json['messages'] = $messages;
                    return $json;
                } else {
                    $json['messages'] = trans('lang.something_wrong');
                    return $json;
                }
            } else {
                $json['messages'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.not_authorize');
            return $json;
        }
    }

    /**
     * Download Attachments.
     *
     * @param \Illuminate\Http\Request $id ID
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadMessageAttachments($id)
    {
        if (!empty($id)) {
            $messages = DB::table('private_messages')->select('attachments', 'author_id', 'project_type')->where('id', $id)->get()->toArray();
            $attachments = unserialize($messages[0]->attachments);
            if ($messages[0]->project_type == 'service') {
                $project_type = 'services';
            } elseif ($messages[0]->project_type == 'job') {
                $project_type = 'proposals';
            }
            $path = storage_path() . '/app/uploads/' . $project_type . '/' . $messages[0]->author_id;
            if (!file_exists($path)) {
                File::makeDirectory($path, 0755, true, true);
            }
            $zip = new \Chumper\Zipper\Zipper();
            foreach ($attachments as $attachment) {
                if (Storage::disk('local')->exists('uploads/' . $project_type . '/' . $messages[0]->author_id . '/' . $attachment)) {
                    $zip->make($path . '/' . $id . '-attachments.zip')->add($path . '/' . $attachment);
                }
            }
            $zip->close();
            if (Storage::disk('local')->exists('uploads/' . $project_type . '/' . $messages[0]->author_id . '/' . $id . '-attachments.zip')) {
                return response()->download(storage_path('app/uploads/' . $project_type . '/' . $messages[0]->author_id . '/' . $id . '-attachments.zip'));
            } else {
                Session::flash('error', trans('lang.file_not_found'));
                return Redirect::back();
            }
        }
    }

    /**
     * Checkout Page.
     *
     * @param \Illuminate\Http\Request $id ID
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout($id)
    {
        if (!empty($id)) {
            $package_options = Helper::getPackageOptions(Auth::user()->getRoleNames()[0]);
            $package = Package::find($id);
            $stripe_settings = SiteManagement::getMetaValue('stripe_settings');
            $stripe_img = !empty($stripe_settings) ? $stripe_settings[0]['stripe_img'] : '';
            $payout_settings = SiteManagement::getMetaValue('commision');
            $payment_gateway = !empty($payout_settings) && !empty($payout_settings[0]['payment_method']) ? $payout_settings[0]['payment_method'] : array();
            $symbol = !empty($payout_settings) && !empty($payout_settings[0]['currency']) ? Helper::currencyList($payout_settings[0]['currency']) : array();
            $mode = !empty($payout_settings) && !empty($payout_settings[0]['payment_mode']) ? $payout_settings[0]['payment_mode'] : 'true';
            if (file_exists(resource_path('views/extend/back-end/package/checkout.blade.php'))) {
                return view::make('extend.back-end.package.checkout', compact('stripe_img', 'package', 'package_options', 'payment_gateway', 'symbol', 'mode'));
            } else {
                return view::make('back-end.package.checkout', compact('stripe_img', 'package', 'package_options', 'payment_gateway', 'symbol', 'mode'));
            }
        }
    }

    /**
     * Store profile settings.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function generateOrder($id, $type)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        if (!empty($id)) {
            $order = new Order();
            $new_order = $order->saveOrder(Auth::user()->id, $id, $type);
            if ($type == 'service') {
                $json['service_order'] = $new_order['service_order'];
            }
            $json['type'] = 'success';
            $json['order_type'] = $type;
            $json['order_id'] = $new_order['id'];
            $json['process'] = trans('lang.saving_profile');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['process'] = trans('lang.something_wrong');
            return $json;
        }
    }
    public function generateOrderJob($id, $type)
    {

        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        if (!empty($id)) {
            $order = new Order();
            $new_order = $order->saveOrderJob(Auth::user()->id, $id, $type);
            if ($type == 'service') {
                $json['service_order'] = $new_order['service_order'];
            }

            //send email

            $proposal = Proposal::where('id', $id)->get()->first();

            $job_id = $proposal->job_id;
            $job = Job::where('id', $job_id)->first();
            $rejected_proposals = Proposal::where('job_id', $job_id)->where('id', '!=', $id)->get();
            $title = $job->title;

            $employer_name = Helper::getUserName(Auth::user()->id);

            $amount = $proposal->amount;

            //  $job_slug = Job::where('slug', $slug)->first();

            $userF = User::where('id', $proposal->freelancer_id)->first();
            // dd($proposal);
            //  $freelancer = $proposal->freelancer_id;
            //  dd($freelancer); exit;
            $data = array();

            $data['employer_name'] = $employer_name;
            $data['job'] = $job;
            $data['job_name'] = $title;
            $data['amount'] = $amount;
            $data['slug'] = $job->slug;
            Notification::send($userF, new Hired($data));

            foreach($rejected_proposals as $rejected_proposal){
                $amount = $rejected_proposal->amount;
                $userF = User::where('id', $rejected_proposal->freelancer_id)->first();

                $data = array();

                $data['employer_name'] = $employer_name;
                $data['job'] = $job;
                $data['job_name'] = $title;
                $data['amount'] = $amount;
                $data['slug'] = $job->slug;
                // dd($data);
                Notification::send($userF, new RejectedFreelancer($data));
            }

            $json['type'] = 'success';
            $json['order_type'] = $type;
            $json['order_id'] = $new_order['id'];
            $json['process'] = trans('lang.saving_profile');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['process'] = trans('lang.something_wrong');
            return $json;
        }
    }
    /**
     * Checkout Page.
     *
     * @param \Illuminate\Http\Request $id ID
     *
     * @return \Illuminate\Http\Response
     */
    public function bankCheckout($id, $order, $type, $project_type = '')
    {
        if (!empty($id) && Auth::user()) {
            $subtitle = '';
            $options = '';
            $seller = '';
            if ($type == 'project') {
                if ($project_type == 'service') {
                    $service_order = DB::table('service_user')->select('service_id')->where('id', $id)->first();
                    $service = Service::find($service_order->service_id);
                    $title = $service->title;
                    $cost = $service->price;
                    $product_id = $id;
                } else {
                    $proposal = Proposal::where('id', $id)->get()->first();
                    if (!empty($proposal)) {
                        $job = $proposal->job;
                        $product_id = $proposal->id;
                        $title = $job->title;
                        $cost = $proposal->amount;
                    } else {
                        abort(404);
                    }
                }
            } else {
                $package = Package::find($id);
                if (!empty($package)) {
                    $options = unserialize($package->options);
                    $product_id = $package->id;
                    $title = $package->title;
                    $cost = $package->cost;
                    $subtitle = $package->subtitle;
                } else {
                    abort(404);
                }
            }
            $payout_settings = SiteManagement::getMetaValue('commision');
            $symbol = !empty($payout_settings) && !empty($payout_settings[0]['currency']) ? Helper::currencyList($payout_settings[0]['currency']) : array();
            $mode = !empty($payout_settings) && !empty($payout_settings[0]['payment_mode']) ? $payout_settings[0]['payment_mode'] : 'true';
            $bank_detail = SiteManagement::getMetaValue('bank_detail');
            if (file_exists(resource_path('views/extend/back-end/package/bank-checkout.blade.php'))) {
                return view::make(
                    'extend.back-end.package.bank-checkout',
                    compact('product_id', 'title', 'symbol', 'mode', 'bank_detail', 'order', 'cost', 'subtitle', 'options', 'type')
                );
            } else {
                return view::make(
                    'back-end.package.bank-checkout',
                    compact('product_id', 'title', 'symbol', 'mode', 'bank_detail', 'order', 'cost', 'subtitle', 'options', 'type')
                );
            }
        } else {
            abort(404);
        }
    }

    public function bankCheckoutJob($id, $order, $type, $project_type = '')
    {


        if (!empty($id) && Auth::user()) {
            $subtitle = '';
            $options = '';
            $seller = '';
            if ($type == 'project') {
                if ($project_type == 'service') {
                    $service_order = DB::table('service_user')->select('service_id')->where('id', $id)->first();
                    $service = Service::find($service_order->service_id);
                    $title = $service->title;
                    $cost = $service->price;
                    $product_id = $id;
                } else {
                    $proposal = Proposal::where('id', $id)->get()->first();
                    if (!empty($proposal)) {
                        $job = $proposal->job;
                        $product_id = $proposal->id;
                        $title = $job->title;
                        $cost = $proposal->amount;
                    } else {
                        abort(404);
                    }
                }
            } else {
                $package = Package::find($id);
                if (!empty($package)) {
                    $options = unserialize($package->options);
                    $product_id = $package->id;
                    $title = $package->title;
                    $cost = $package->cost;
                    $subtitle = $package->subtitle;
                } else {
                    abort(404);
                }
            }
            $payout_settings = SiteManagement::getMetaValue('commision');
            $symbol = !empty($payout_settings) && !empty($payout_settings[0]['currency']) ? Helper::currencyList($payout_settings[0]['currency']) : array();
            $mode = !empty($payout_settings) && !empty($payout_settings[0]['payment_mode']) ? $payout_settings[0]['payment_mode'] : 'true';
            $bank_detail = SiteManagement::getMetaValue('bank_detail');
            if (file_exists(resource_path('views/extend/back-end/package/bank-checkout-job.blade.php'))) {
                return view::make(
                    'extend.back-end.package.bank-checkout-job',
                    compact('product_id', 'title', 'symbol', 'mode', 'bank_detail', 'order', 'cost', 'subtitle', 'options', 'type')
                );
            } else {
                return view::make(
                    'back-end.package.bank-checkout-job',
                    compact('product_id', 'title', 'symbol', 'mode', 'bank_detail', 'order', 'cost', 'subtitle', 'options', 'type')
                );
            }
        } else {
            abort(404);
        }
    }
    /**
     * Store profile settings.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */

    public function submitTransectionProcessNew(Request $request)
    {


        $json = array();
        if (!empty($request)) {
            $type = $_POST['type'];
            $product_id = $_POST['product_id'];
            $product_title = $_POST['product_title'];


            $product_price = $_POST['product_price'];

            $order = $_POST['order_id'];

            $payment_settings = SiteManagement::getMetaValue('commision');

            $currency_symbol  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? Helper::currencyList($payment_settings[0]['currency']) : array();
            $currency = !empty($currency_symbol['code']) ? $currency_symbol['code'] : 'USD';

            $proposal = Proposal::where('id', $product_id)->get()->first();
            // $job = Job::find($proposal->job->id);
            $freelancer_email = User::where('id', $proposal->freelancer_id)->first();
            //  dd($proposl);
            if (!empty($type) && !empty($product_id)) {
                $invoice = new Invoice();
                $invoice->title = trans('lang.bank_transfer');
                $invoice->price = $product_price;
                $invoice->payer_name = Helper::getUserName(Auth::user()->id);
                $invoice->payer_email = Auth::user()->email;
                $invoice->seller_email = $freelancer_email->email;
                $invoice->reciver_id = $proposal->freelancer_id;
                $invoice->job_id = $proposal->job_id;
                $invoice->proposal_id = $proposal->id;
                $invoice->currency_code = !empty($currency) ? $currency : 'PKR';
                $invoice->payer_status = 'unverified';

                $invoice->shipping_amount = 0;
                $invoice->handling_amount = 0;
                $invoice->insurance_amount = 0;
                $invoice->sales_tax = $_POST['price_tax'] ? $_POST['price_tax'] : 0;
                $invoice->payment_mode = 'bacs';
                $invoice->paid = 0;
                $invoice->type = $type;
                $invoice->detail = !empty($request['trans_detail']) ? $request['trans_detail'] : '';
                $old_path = 'uploads\users\temp';
                $trans_attachments = array();
                if (!empty($request['attachments'])) {
                    $attachments = $request['attachments'];
                    foreach ($attachments as $key => $attachment) {
                        if (Storage::disk('local')->exists($old_path . '/' . $attachment)) {
                            $new_path = 'uploads/users/' . Auth::user()->id;
                            if (!file_exists($new_path)) {
                                File::makeDirectory($new_path, 0755, true, true);
                            }
                            $filename = time() . '-' . $attachment;
                            Storage::move($old_path . '/' . $attachment, $new_path . '/' . $filename);
                            $trans_attachments[] = $filename;
                        }
                    }
                    $invoice->transection_doc = serialize($trans_attachments);
                }
                $invoice->save();
                $invoice_id = DB::getPdo()->lastInsertId();
                DB::table('orders')
                    ->where('id', $order)
                    ->update(['invoice_id' => $invoice_id]);

                if (!empty(config('mail.username')) && !empty(config('mail.password'))) {


                    //send email  
                    $product_slug_id = $proposal->job_id;
                    $product_slug = Job::where('id',$product_slug_id)->first();

                    $userF = User::where('id', $proposal->freelancer_id)->first();

                    $data = array();

                    $data['employer_name'] = $invoice->payer_name;
                    $data['job'] = $invoice->job_id;
                    $data['job_name'] = $product_title;
                    $data['amount'] = $invoice->price;
                    $data['slug'] = $product_slug->slug;
                    $data['detail'] = $invoice->detail;
                    Notification::send($userF, new InvoicePaid($data));


                    $order = DB::table('orders')->where('id', $order)->first();

                    $email_params = array();
                    $template_data = array();
                    $order_settings = SiteManagement::getMetaValue('order_settings');
                    $template_data['subject'] = !empty($order_settings) && !empty($order_settings['admin_order']['subject']) ? $order_settings['admin_order']['subject'] : '';
                    $template_data['content'] = !empty($order_settings) && !empty($order_settings['admin_order']['email_content']) ? $order_settings['admin_order']['email_content'] : '';
                    $email_params['name'] = Helper::getUserName(Auth::user()->id);
                    $email_params['order_id'] = $order->id;
                    Mail::to(Auth::user()->email)
                        ->send(
                            new AdminEmailMailable(
                                'admin_new_order_received',
                                $template_data,
                                $email_params
                            )
                        );
                }






                $title = $proposal->job->title;
                $amount = $proposal->amount;

                // $job = Job::find($proposal->job->id);

                // // send message to freelancer
                // $message = new Message();
                // $user = User::find(intval($order->user_id));
                // $message->user()->associate($user);
                // $message->receiver_id = intval($proposal->freelancer_id);
                // $message->body = trans('lang.hire_for') . ' ' . $job->title . ' ' . trans('lang.project');
                // $message->status = 0;
                // $message->save();


                session()->forget('product_id');
                session()->forget('product_title');
                session()->forget('product_price');
                session()->forget('order');
                session()->put(['message' => trans('lang.transection_uploaded')]);
                $json['type'] = 'success';
                $json['return_url'] = url(Auth::user()->getRoleNames()[0] . '/dashboard');
                return \Redirect::back()->withErrors(['success', 'Update']);
            } else {
                $json['type'] = 'error';
                $json['process'] = trans('lang.something_wrong');
                return \Redirect::back()->withErrors(['success', 'Update']);
            }
        }
    }

    public function submitTransectionProcess(Request $request)
    {

        //   echo dd($request); exit;
        //  $server = Helper::kpitbIsDemoSiteAjax();
        //  if (!empty($server)) {
        //      $response['type'] = 'error';
        //      $response['message'] = $server->getData()->message;
        //      return $response;
        //  }
        $json = array();
        if (!empty($request)) {
            $type = $_POST['type'];
            $product_id = $_POST['product_id'];
            $product_title = $_POST['product_title'];

            if ($_POST['decided_amount'] != '') {
                $product_price = $_POST['decided_amount'];
            } else {
                $product_price = $_POST['product_price'];
            }

            $order = $_POST['order_id'];

            $payment_settings = SiteManagement::getMetaValue('commision');

            $currency_symbol  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? Helper::currencyList($payment_settings[0]['currency']) : array();
            $currency = !empty($currency_symbol['code']) ? $currency_symbol['code'] : 'USD';
            if (!empty($type) && !empty($product_id)) {
                $invoice = new Invoice();
                $invoice->title = trans('lang.bank_transfer');
                $invoice->price = $product_price;
                $invoice->payer_name = Helper::getUserName(Auth::user()->id);
                $invoice->payer_email = Auth::user()->email;
                $invoice->seller_email = '';
                $invoice->currency_code = !empty($currency) ? $currency : 'PKR';
                $invoice->shipping_amount = 0;
                $invoice->handling_amount = 0;
                $invoice->insurance_amount = 0;
                $invoice->sales_tax = 0;
                $invoice->payment_mode = 'bacs';
                $invoice->paid = 0;
                $invoice->type = $type;
                $invoice->detail = !empty($request['trans_detail']) ? $request['trans_detail'] : '';
                $old_path = 'uploads\users\temp';
                $trans_attachments = array();
                if (!empty($request['attachments'])) {
                    $attachments = $request['attachments'];
                    foreach ($attachments as $key => $attachment) {
                        if (Storage::disk('local')->exists($old_path . '/' . $attachment)) {
                            $new_path = 'uploads/users/' . Auth::user()->id;
                            if (!file_exists($new_path)) {
                                File::makeDirectory($new_path, 0755, true, true);
                            }
                            $filename = time() . '-' . $attachment;
                            Storage::move($old_path . '/' . $attachment, $new_path . '/' . $filename);
                            $trans_attachments[] = $filename;
                        }
                    }
                    $invoice->transection_doc = serialize($trans_attachments);
                }
                $invoice->save();
                $invoice_id = DB::getPdo()->lastInsertId();
                DB::table('orders')
                    ->where('id', $order)
                    ->update(['invoice_id' => $invoice_id]);
                if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                    $order = DB::table('orders')->where('id', $order)->first();
                    $email_params = array();
                    $template_data = array();
                    $order_settings = SiteManagement::getMetaValue('order_settings');
                    $template_data['subject'] = !empty($order_settings) && !empty($order_settings['admin_order']['subject']) ? $order_settings['admin_order']['subject'] : '';
                    $template_data['content'] = !empty($order_settings) && !empty($order_settings['admin_order']['email_content']) ? $order_settings['admin_order']['email_content'] : '';
                    $email_params['name'] = Helper::getUserName(Auth::user()->id);
                    $email_params['order_id'] = $order->id;
                    Mail::to(Auth::user()->email)
                        ->send(
                            new AdminEmailMailable(
                                'admin_new_order_received',
                                $template_data,
                                $email_params
                            )
                        );
                }





                $proposal = Proposal::find($order->product_id);

                $title = $proposal->job->title;
                $amount = $proposal->amount;
                $proposal->status_hired = 1;
                $proposal->paid = 'pending';
                $proposal->save();

                $job = Job::find($proposal->job->id);
                $job->status = 'hired';
                $job->save();
                // send message to freelancer
                $message = new Message();
                $user = User::find(intval($order->user_id));
                $message->user()->associate($user);
                $message->receiver_id = intval($proposal->freelancer_id);
                $message->body = trans('lang.hire_for') . ' ' . $job->title . ' ' . trans('lang.project');
                $message->status = 0;
                $message->save();


                session()->forget('product_id');
                session()->forget('product_title');
                session()->forget('product_price');
                session()->forget('order');
                session()->put(['message' => trans('lang.transection_uploaded')]);
                $json['type'] = 'success';
                $json['return_url'] = url(Auth::user()->getRoleNames()[0] . '/dashboard');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['process'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['process'] = trans('lang.something_wrong');
            return $json;
        }
    }
    public function submitTransection(Request $request)
    {

        //  echo dd($request); exit;
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        if (!empty($request)) {
            $type = !empty(session()->get('type')) ? session()->get('type') : '';
            $product_id = !empty(session()->get('product_id')) ? session()->get('product_id') : '';
            $product_title = !empty(session()->get('product_title')) ? session()->get('product_title') : '';
            $product_price = !empty(session()->get('product_price')) ? session()->get('product_price') : '';
            $order = !empty(session()->get('order')) ? session()->get('order') : '';
            $payment_settings = SiteManagement::getMetaValue('commision');
            $currency_symbol  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? Helper::currencyList($payment_settings[0]['currency']) : array();
            $currency = !empty($currency_symbol['code']) ? $currency_symbol['code'] : 'USD';
            if (!empty($type) && !empty($product_id)) {
                $invoice = new Invoice();
                $invoice->title = trans('lang.bank_transfer');
                $invoice->price = $product_price;
                $invoice->payer_name = Helper::getUserName(Auth::user()->id);
                $invoice->payer_email = Auth::user()->email;
                $invoice->seller_email = '';
                $invoice->currency_code = !empty($currency) ? $currency : 'PKR';
                $invoice->shipping_amount = 0;
                $invoice->handling_amount = 0;
                $invoice->insurance_amount = 0;
                $invoice->sales_tax = 0;
                $invoice->payment_mode = 'bacs';
                $invoice->paid = 0;
                $invoice->type = $type;
                $invoice->detail = !empty($request['trans_detail']) ? $request['trans_detail'] : '';
                $old_path = 'uploads\users\temp';
                $trans_attachments = array();
                if (!empty($request['attachments'])) {
                    $attachments = $request['attachments'];
                    foreach ($attachments as $key => $attachment) {
                        if (Storage::disk('local')->exists($old_path . '/' . $attachment)) {
                            $new_path = 'uploads/users/' . Auth::user()->id;
                            if (!file_exists($new_path)) {
                                File::makeDirectory($new_path, 0755, true, true);
                            }
                            $filename = time() . '-' . $attachment;
                            Storage::move($old_path . '/' . $attachment, $new_path . '/' . $filename);
                            $trans_attachments[] = $filename;
                        }
                    }
                    $invoice->transection_doc = serialize($trans_attachments);
                }
                $invoice->save();
                $invoice_id = DB::getPdo()->lastInsertId();
                DB::table('orders')
                    ->where('id', $order)
                    ->update(['invoice_id' => $invoice_id]);
                if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                    $order = DB::table('orders')->where('id', $order)->first();
                    $email_params = array();
                    $template_data = array();
                    $order_settings = SiteManagement::getMetaValue('order_settings');
                    $template_data['subject'] = !empty($order_settings) && !empty($order_settings['admin_order']['subject']) ? $order_settings['admin_order']['subject'] : '';
                    $template_data['content'] = !empty($order_settings) && !empty($order_settings['admin_order']['email_content']) ? $order_settings['admin_order']['email_content'] : '';
                    $email_params['name'] = Helper::getUserName(Auth::user()->id);
                    $email_params['order_id'] = $order->id;
                    Mail::to(Auth::user()->email)
                        ->send(
                            new AdminEmailMailable(
                                'admin_new_order_received',
                                $template_data,
                                $email_params
                            )
                        );
                }
                session()->forget('product_id');
                session()->forget('product_title');
                session()->forget('product_price');
                session()->forget('order');
                session()->put(['message' => trans('lang.transection_uploaded')]);
                $json['type'] = 'success';
                $json['return_url'] = url(Auth::user()->getRoleNames()[0] . '/dashboard');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['process'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['process'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Store profile settings.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function changeOrderStatus(Request $request)
    {
        // dd($request);
        // $server = Helper::kpitbIsDemoSiteAjax();
        // if (!empty($server)) {
        //     $response['type'] = 'error';
        //     $response['message'] = $server->getData()->message;
        //     return $response;
        // }
        $json = array();
        if (!empty($request)) {
            if (!empty($request['id']) && !empty($request['status'])) {
                $item_type = '';
                $order = Order::find($request['id']);
                $order->status = $request['status'];
                $order->save();
                $invoice = Invoice::find($order->invoice->id);
                $invoice->paid = 1;
                $invoice->save();
                $title = '';
                $amount = '';
                if ($order->type == 'job') {
                    $item_type = 'project';
                    $proposal = Proposal::find($order->product_id);
                    $title = $proposal->job->title;
                    $amount = $proposal->amount;
                    $proposal->hired = 1;
                    $proposal->status = 'hired';
                    $proposal->paid = 'pending';
                    $proposal->save();
                    $job = Job::find($proposal->job->id);
                    $job->status = 'hired';
                    $job->save();
                    // send message to freelancer
                    $message = new Message();
                    $user = User::find(intval($order->user_id));
                    $message->user()->associate($user);
                    $message->receiver_id = intval($proposal->freelancer_id);
                    $message->body = trans('lang.hire_for') . ' ' . $job->title . ' ' . trans('lang.project');
                    $message->status = 0;
                    $message->save();
                    // send mail
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $freelancer = User::find($proposal->freelancer_id);
                        $employer = User::find($job->user_id);
                        if (!empty($freelancer->email)) {
                            $email_params = array();
                            $template = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_hire_freelancer')->get()->first();
                            if (!empty($template->id)) {
                                $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                                $email_params['project_title'] = $job->title;
                                $email_params['hired_project_link'] = url('job/' . $job->slug);
                                $email_params['name'] = Helper::getUserName($freelancer->id);
                                $email_params['link'] = url('profile/' . $freelancer->slug);
                                $email_params['employer_profile'] = url('profile/' . $employer->slug);
                                $email_params['emp_name'] = Helper::getUserName($employer->id);
                                Mail::to($freelancer->email)
                                    ->send(
                                        new FreelancerEmailMailable(
                                            'freelancer_email_hire_freelancer',
                                            $template_data,
                                            $email_params
                                        )
                                    );
                            }
                        }
                    }
                } elseif ($order->type == 'service') {
                    $item_type = 'project';
                    DB::table('service_user')
                        ->where('id', $order->product_id)
                        ->update(['status' => 'hired']);
                    $order_service = DB::table('service_user')->select('service_id')->where('id', $order->product_id)->first();
                    $service = Service::find($order_service->service_id);
                    $title = $service->title;
                    $amount = $service->price;
                    // $service->users()->attach($order->user_id, ['type' => 'employer', 'status' => 'hired', 'seller_id' => $service->seller->id, 'paid' => 'pending']);
                    // $service->save();
                    // send message to freelancer
                    $message = new Message();
                    $user = User::find(intval($order->user_id));
                    $message->user()->associate($user);
                    $message->receiver_id = intval($service->seller[0]->id);
                    $message->body = Helper::getUserName($order->user_id) . ' ' . trans('lang.service_purchase') . ' ' . $service->title;
                    $message->status = 0;
                    $message->save();
                    // send mail
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $email_params = array();
                        $template_data = Helper::getFreelancerNewOrderEmailContent();
                        $email_params['title'] = $service->title;
                        $email_params['service_link'] = url('service/' . $service->slug);
                        $email_params['amount'] = $service->price;
                        $email_params['freelancer_name'] = Helper::getUserName($service->seller[0]->id);
                        $email_params['employer_profile'] = url('profile/' . $user->slug);
                        $email_params['employer_name'] = Helper::getUserName($user->id);
                        $freelancer_data = User::find(intval($service->seller[0]->id));
                        Mail::to($freelancer_data->email)
                            ->send(
                                new FreelancerEmailMailable(
                                    'freelancer_email_new_order',
                                    $template_data,
                                    $email_params
                                )
                            );
                    }
                } elseif ($order->type == 'package') {
                    $item_type = 'package';
                    $package = Package::find($order->product_id);
                    $title = !empty($package->title) ? $package->title : '';
                    $amount = !empty($package->cost) ? $package->cost : '';
                }

                if ($order->type == 'package') {
                    if (Schema::hasColumn('items', 'type')) {
                        $item = DB::table('items')->select('id')->where('type', 'package')->where('subscriber', $order->user_id)->first();
                        if (empty($item)) {
                            $item = DB::table('items')->select('id')->where('subscriber', $order->user_id)->first();
                        }
                    } else {
                        $item = DB::table('items')->select('id')->where('subscriber', $order->user_id)->first();
                    }
                    if (!empty($item)) {
                        $item = Item::find($item->id);
                    } else {
                        $item = new Item();
                    }
                } else {
                    $item = DB::table('items')->select('id')->where('invoice_id', $order->invoice->id)->first();
                    if (!empty($item)) {
                        $item = Item::find($item->id);
                    } else {
                        $item = new Item();
                    }
                }
                $item->invoice_id = filter_var($order->invoice->id, FILTER_SANITIZE_NUMBER_INT);
                $item->product_id = filter_var($order->product_id, FILTER_SANITIZE_NUMBER_INT);
                $item->subscriber = $order->user_id;
                $item->item_name = filter_var($title, FILTER_SANITIZE_STRING);
                $item->item_price = $amount;
                $item->type = $item_type;
                $item->item_qty = 1;
                $item->save();
                // send package mail
                if ($order->type == 'package') {
                    $option = !empty($package->options) ? unserialize($package->options) : '';
                    $expiry = !empty($option) ? $item->created_at->addDays($option['duration']) : '';
                    $expiry_date = !empty($expiry) ? Carbon::parse($expiry)->toDateTimeString() : '';
                    $user = User::find($order->user_id);
                    if (!empty($package->badge_id) && $package->badge_id != 0) {
                        $user->badge_id = $package->badge_id;
                    }
                    $user->expiry_date = $expiry_date;
                    $user->save();
                    // send mail
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $role = $user->getRoleNames()->first();
                        $package_options = !empty($package->options) ? unserialize($package->options) : '';
                        $expiry_date = '';
                        if (!empty($invoice) && !empty($package_options)) {
                            if ($package_options['duration'] === 'Quarter') {
                                $expiry_date = $invoice->created_at->addDays(4);
                            } elseif ($package_options['duration'] === 'Month') {
                                $expiry_date = $invoice->created_at->addMonths(1);
                            } elseif ($package_options['duration'] === 'Year') {
                                $expiry_date = $invoice->created_at->addYears(1);
                            }
                        }
                        if ($role === 'employer') {
                            if (!empty($user->email)) {
                                $email_params = array();
                                $template = DB::table('email_types')->select('id')->where('email_type', 'employer_email_package_subscribed')->get()->first();
                                if (!empty($template->id)) {
                                    $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                                    $email_params['employer'] = Helper::getUserName($user->id);
                                    $email_params['employer_profile'] = url('profile/' . $user->slug);
                                    $email_params['name'] = !empty($package) ? $package->title : '';
                                    $email_params['price'] = !empty($package) ? $package->cost : '';
                                    $email_params['expiry_date'] = !empty($expiry_date) ? Carbon::parse($expiry_date)->format('M d, Y') : '';
                                    Mail::to($user->email)
                                        ->send(
                                            new EmployerEmailMailable(
                                                'employer_email_package_subscribed',
                                                $template_data,
                                                $email_params
                                            )
                                        );
                                }
                            }
                        } elseif ($role === 'freelancer') {
                            if (!empty($user->email)) {
                                $email_params = array();
                                $template = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_package_subscribed')->get()->first();
                                if (!empty($template->id)) {
                                    $template_data = EmailTemplate::getEmailTemplateByID($template->id);
                                    $email_params['freelancer'] = Helper::getUserName($user->id);
                                    $email_params['freelancer_profile'] = url('profile/' . $user->slug);
                                    $email_params['name'] = !empty($package) ? $package->title : '';
                                    $email_params['price'] = !empty($package) ? $package->cost : '';
                                    $email_params['expiry_date'] = !empty($expiry_date) ? Carbon::parse($expiry_date)->format('M d, Y') : '';
                                    Mail::to($user->email)
                                        ->send(
                                            new FreelancerEmailMailable(
                                                'freelancer_email_package_subscribed',
                                                $template_data,
                                                $email_params
                                            )
                                        );
                                }
                            }
                        }
                    }
                }
                $json['type'] = 'success';
                $json['message'] = trans('lang.status_updated');
                return $json;
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }




    /**
     * Print Thankyou.
     *
     * @return \Illuminate\Http\Response
     */
    public function thankyou()
    {
        if (Auth::user()) {
            echo trans('lang.thankyou');
        } else {
            abort(404);
        }
    }

    /**
     * Get Invoices.
     *
     * @param \Illuminate\Http\Request $type type
     *
     * @return \Illuminate\Http\Response
     */

    public function checkoutMilestone($id, $slug)
    {
        $job = Job::where('slug', $slug)->first();
        // MilestoneProject
        $Milestone = Milestone::where('id', $id)->where('job_id', $job->id)->first();
        $payment_settings = SiteManagement::getMetaValue('commision');
        $job_tax = !empty($payment_settings) && !empty($payment_settings[0]['tax']) ? $payment_settings[0]['tax'] : 0;
        $job_tax_amount = ($Milestone->amount / 100) * $job_tax;
        $newAmount = $Milestone->amount - (($Milestone->amount / 100) * $job_tax);

        return view('back-end.employer.invoices.MilestoneProject', compact('Milestone', 'job_tax', 'job_tax_amount', 'newAmount', 'job'));
    }
    public function checkoutProject($id)

    {
        $job = Job::where('id', $id)->first();
        $user_id = Auth::user()->id;
        $proposal = Proposal::where('status_hired', 1)->where('hired', 1)->where('job_id', $job->id)->first();
        $order = Order::where('product_id', $proposal->id)->where('user_id', $user_id)->first();
        $job = $proposal->job;
        $product_id = $proposal->id;
        $title = $job->title;
        $cost = $proposal->amount;
        $type = 'project';
        $order = $order->id;
        $Invoice = Invoice::where('job_id', $job->id)->first();
        $JobDispute = JobDispute::where('job_id', $job->id)->first();

        $payout_settings = SiteManagement::getMetaValue('commision');
        $job_tax = !empty($payout_settings) && !empty($payout_settings[0]['tax']) ? $payout_settings[0]['tax'] : 0;
        $job_tax_amount = ($cost / 100) * $job_tax;
        $newTotalCost = $cost - (($cost / 100) * $job_tax);
        $symbol = !empty($payout_settings) && !empty($payout_settings[0]['currency']) ? Helper::currencyList($payout_settings[0]['currency']) : array();
        $mode = !empty($payout_settings) && !empty($payout_settings[0]['payment_mode']) ? $payout_settings[0]['payment_mode'] : 'true';
        $bank_detail = SiteManagement::getMetaValue('bank_detail');

        return view('back-end.employer.invoices.projectComplete', compact('proposal', 'job', 'product_id', 'title', 'cost', 'job_tax', 'job_tax_amount', 'newTotalCost', 'type', 'order', 'bank_detail', 'mode', 'Invoice', 'JobDispute'));
    }
    public function freelancejobCheck($id, $slug)
    {
        $job = Job::where('slug', $slug)->first();

        $Invoice = Invoice::where('id', $id)->where('job_id', $job->id)->first();
        $JobDispute = JobDispute::where('job_id', $job->id)->first();

        return view('back-end.freelancer.invoices.JobInvoiceProject', compact('Invoice', 'job', 'JobDispute'));
    }

    public function freelanceMilestoneCheck($id, $slug)
    {
        $job = Job::where('slug', $slug)->first();

        $Milestone = Milestone::where('id', $id)->where('job_id', $job->id)->first();
        $payment_settings = SiteManagement::getMetaValue('commision');
        $job_tax = !empty($payment_settings) && !empty($payment_settings[0]['tax']) ? $payment_settings[0]['tax'] : 0;
        $job_tax_amount = ($Milestone->amount / 100) * $job_tax;

        return view('back-end.freelancer.invoices.MilestoneProject', compact('Milestone', 'job_tax', 'job_tax_amount', 'job'));
    }

    public function Milestonereleaserequest($id, $slug)
    {
        $status = 1;
        $job = Job::where('slug', $slug)->first();
        $Milestone = Milestone::where('id', $id)->where('job_id', $job->id)->first();
        // dd($job->id);
        // dd($Milestone->id);
        $milestone_release = DB::table('milestones')->where('id', $id)->where('job_id', $job->id)->update(['milestone_release' => $status]);

        //send email
        if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
            $email_params = array();
            $dispute_raised_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_dispute_raised')->get()->first();


            $userE = User::where('id', $job->user_id)->first();
            $data = array();
            // dd($userE);
            $data['name'] = Helper::getUserName(Auth::user()->id);
            $data['job_id'] = $job->id;
            $data['project_title'] = $job->title;
            $data['project_link'] = url('job/' . $job->slug);
            $data['Milestone_title'] = 'Milestone - ' . $Milestone->title . ' - ' . $job->title;
            $data['Milestone_price'] = $Milestone->percentage;
            // dd($data);
            Notification::send($userE, new MilestoneReleaseRequest($data));
        }
        return \Redirect::back()->withErrors([trans('Request has been sent')]);
    }

    public function rejectmilestonerelease($id, $slug)
    {
        $status = 0;
        $job = Job::where('slug', $slug)->first();
        $Proposal = Proposal::where('job_id', $job->id)->where('status', 'hired')->where('hired', 1)->first();
        $Milestone = Milestone::where('id', $id)->where('job_id', $job->id)->first();
        // dd($job->id);
        // dd($Milestone->id);
        $milestone_release = DB::table('milestones')->where('id', $id)->where('job_id', $job->id)->update(['milestone_release' => $status]);

        //send email 
        $userF = User::where('id', $Proposal->freelancer_id)->first();
        // dd($userF); exit;
        $data = array();

        $data['milestone_name'] = $Milestone->title;
        $data['job_name'] = $job->title;
        $data['employer_name'] = Helper::getUserName(Auth::user()->id);
        $data['amount'] = $Milestone->price;
        $data['slug'] = $job->slug;
        Notification::send($userF, new MilestoneReleaseRejected($data));

        return \Redirect::back()->withErrors([trans('Request has been rejected')]);
    }


    public function freelanceMilestonestatus($id, $status, $slug)
    {
        $job = Job::where('slug', $slug)->first();
        $Milestone = Milestone::where('id', $id)->first();
        if ($status == 'accept') {


            if ($Milestone->milestonInvoice) {

                $MilestoneInvoice = MilestoneInvoice::where('id', $Milestone->milestonInvoice->id)->first();
                $MilestoneInvoice->status = 1;
                $MilestoneInvoice->reciver_status = 1;
                $MilestoneInvoice->save();
            }

            if ($Milestone->milestonDispute) {

                $milestoneDispute = milestoneDispute::where('id', $Milestone->milestonDispute->id)->first();

                $milestoneDispute->status = 1;
                $milestoneDispute->freelancer_status = 1;
                $milestoneDispute->save();
            }

            // dd($Milestone->milestonDispute);
            $Milestone->status = 1;
            $Milestone->save();
            // $MilestoneInvoice = new MilestoneInvoice([  
            //     'reciver_status' =>1,
            // ]);
            // $MilestoneInvoice->save();

            //send email
            if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                $email_params = array();
                $dispute_raised_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_dispute_raised')->get()->first();
                if ($Milestone->status === 1) {



                    $userE = User::where('id', $job->user_id)->first();
                    $data = array();
                    //dd($userPe);
                    $data['name'] = Helper::getUserName(Auth::user()->id);
                    $data['project_title'] = $job->title;
                    $data['slug'] = $job->slug;
                    $data['project_link'] = url('job/' . $job->slug);
                    $data['Milestone_title'] = 'Milestone - ' . $Milestone->title . ' - ' . $job->title;
                    $data['Milestone_price'] = $Milestone->percentage;
                    Notification::send($userE, new MilestoneAccepted($data));
                }
            }
        }

        return redirect("/freelancer/job/$job->slug")->with('success', 'Milestone Status Completed !');
    }

    public function freelanceJobtatus($id, $status, $job_id)
    {
        $job = Job::where('id', $job_id)->first();
        $Invoice = Invoice::where('id', $id)->first();
        $Order = Order::where('invoice_id', $id)->first();
        $proposal = Proposal::where('job_id', $job_id)->first();
        if ($status == 'accept') {

            $Invoice->paid = 1;
            $Invoice->reciver_status = 1;
            $Invoice->save();

            $job->status = 'completed';
            $job->save();

            $Order->status = 'completed';
            $Order->save();

            $proposal->paid = 'completed';
            $proposal->paid_progress = 'completed';
            $proposal->status = 'completed';
            $proposal->save();

            $completed_proposals = Proposal::where('freelancer_id', Auth::user()->id)->where('status', 'completed')->count();
            if ($completed_proposals >= 5) {
                if ($completed_proposals % 5 == 0) {
                    $level = ($completed_proposals / 5) + 1;
                    User::where('id', Auth::user()->id)->update(['level' => $level]);
                }
            }

            // if ($Milestone->milestonDispute) {

            //     $milestoneDispute = milestoneDispute::where('id', $Milestone->milestonDispute->id)->first();

            //     $milestoneDispute->status = 1;
            //     $milestoneDispute->freelancer_status = 1;
            //     $milestoneDispute->save();
            // }

            // $Milestone->status = 1;
            // $Milestone->save();


            //send email

            $employer_name = $Invoice->reciver_id;

            //   $proposal = Proposal::where('id', $id)->get()->first();

            $job_id = $proposal->job_id;
            $job = Job::where('id', $job_id)->first();

            $title = $job->title;

            $freelancer_name = Helper::getUserName(Auth::user()->id);

            $amount = $Invoice->price;

            //  $job_slug = Job::where('slug', $slug)->first();

            $userE = User::where('id', $job->user_id)->first();

            $data = array();

            $data['freelancer_name'] = $freelancer_name;
            $data['job'] = $job;
            $data['job_name'] = $title;
            $data['amount'] = $amount;
            $data['slug'] = $job->slug;
            Notification::send($userE, new InvoiceAccepted($data));
        }
        Session::flash('message', trans("Invoice Accepted. Job Complete"));
        return redirect("freelancer/jobs/completed")->with('success', 'Milestone Status Updated !');
    }
    public function pedisputeupdate(Request $request)
    {
        $request->validate([
            'disputeOption' => 'required',
            'reason' => 'required',
        ]);

        $milestoneDispute = milestoneDispute::where('id', $_POST['dispute_id'])->first();
        $milestoneDispute->pe_description = $request->get('reason');
        $milestoneDispute->pe_status = $request->get('disputeOption');
        $milestoneDispute->save();

        return Redirect::back()->with('message', 'Dispute Status Updated');
    }

    public function freelanceMilestonedisputesreject($id)
    {

        $milestoneDispute = milestoneDispute::where('id', $id)->first();
        // dd($milestoneDispute);
        $milestoneDispute->freelancer_status = 0;
        $milestoneDispute->save();
        return Redirect::back()->with('message', 'Dispute Updated.');
    }
    public function freelanceMilestonedisputes(Request $request)
    {

        $job = Job::where('id', $_POST['job_id'])->first();
        $authUser = Auth::user()->id;
        // dd($request);

        $request->validate([
            'reason' => 'required',
            'freelancer_description' => 'required',
        ]);


        $milestoneDispute = new milestoneDispute([
            'milestone_id' => $request->get('milestone_id'),
            'milestone_invoice_id' => $request->get('milestone_invoice_id'),
            'freelancer_id' => $authUser,
            'freelancer_reason' => $request->get('reason'),
            'freelancer_description' => $request->get('freelancer_description'),
            'job_id' => $job->id,
            'pe_id' => $job->user_id,


        ]);
        $milestoneDispute->save();

        return Redirect::back()->with('message', 'Dispute created. please wait for reply');
    }

    public function pedisputeJobupdate(Request $request)
    {
        $request->validate([
            'disputeOption' => 'required',
            'reason' => 'required',
        ]);

        $JobDispute = JobDispute::where('id', $_POST['dispute_id'])->first();
        $JobDispute->pe_description = $request->get('reason');
        $JobDispute->pe_status = $request->get('disputeOption');
        $JobDispute->save();
        Session::flash('message', 'Dispute Status Updated');
        return Redirect::back()->with('message', 'Dispute Status Updated');
    }

    public function freelanceJobdisputes(Request $request)
    {

        // invoice_id
        $job = Job::where('id', $_POST['job_id'])->first();
        $authUser = Auth::user()->id;
        // dd($request);

        $request->validate([
            'reason' => 'required',
            'freelancer_description' => 'required',
        ]);


        $JobDispute = new JobDispute([
            'job_id' => $request->get('job_id'),
            'invoice_id' => $request->get('invoice_id'),
            'freelancer_id' => $authUser,
            'freelancer_reason' => $request->get('reason'),
            'freelancer_description' => $request->get('freelancer_description'),
            'pe_id' => $job->user_id,
        ]);
        $JobDispute->save();
        $json['type'] = 'message';
        $json['message'] = 'Dispute created. please wait for reply';
        Session::flash('message', 'Dispute created. please wait for reply');
        return Redirect::back()->with('message', 'Dispute created. please wait for reply');
    }
    public function checkoutMilestoneProcess(Request $request)
    {

        //dd($request);



        $status = 3;
        $job = Job::where('id', $_POST['job_id'])->first();
        $Proposal = Proposal::where('job_id', $_POST['job_id'])->where('status', 'hired')->where('hired', 1)->first();
        $Milestone = Milestone::where('id', $_POST['milestone_id'])->where('job_id', $_POST['job_id'])->first();
        $milestone_release = DB::table('milestones')->where('id', $_POST['milestone_id'])->where('job_id', $_POST['job_id'])->update(['milestone_release' => $status]);
        //  $MilestoneInvoicedate = MilestoneInvoice::where('job_id',$job->id)->where('milestone_id',$Milestone->id)->first();
        // dd($MilestoneInvoicedate);
        $attach_file = $request['attachments'];

        $invoice_attachments = array();
        $attachments = '';
        if (!empty($request['attachments'])) {
            foreach ($attach_file as $key => $value) {

                $destinationPath = 'uploads/jobs';
                $filename = time() . '-' . $value->getClientOriginalName();
                $value->move($destinationPath, $filename);

                $invoice_attachments[] = $filename;
            }
            $attachments = serialize($invoice_attachments);
        }


        $MilestoneInvoice = new MilestoneInvoice([
            'title' => 'Milestone - ' . $Milestone->title . ' - ' . $job->title,
            'price' => $request->get('amount'),
            'sender_id' => $job->user_id,
            'milestone_id' => $Milestone->id,
            'reciver_id' => $Proposal->freelancer_id,
            'job_id' => $job->id,
            'order_id' => $Proposal->id,
            'attachments' => $attachments,
            'detail' => $request->get('body'),
            'status' => 1,

        ]);

        $details = strip_tags($MilestoneInvoice->detail);


        $MilestoneInvoice->save();
        // $Milestone->status = 1;
        $Milestone->invoice_id = $MilestoneInvoice->id;
        $Milestone->save();

        //dd($MilestoneInvoice->title); exit;

        //send email


        $userF = User::where('id', $MilestoneInvoice->reciver_id)->first();
        // dd($userF); exit;
        $data = array();

        $data['freelancer_name'] = Helper::getUserName($MilestoneInvoice->reciver_id);
        $data['milestone_name'] = $MilestoneInvoice->title;
        $data['job_name'] = $job->title;
        $data['employer_name'] = Helper::getUserName(Auth::user()->id);
        $data['amount'] = $MilestoneInvoice->price;
        $data['message'] = $details;
        $data['slug'] = $job->slug;
        Notification::send($userF, new MilestoneCompleted($data));


        return redirect("/proposal/$job->slug/hired")->with('success', 'Milestone Status Updated !');
    }
    public function getEmployerInvoices($type = '')
    {
        if (Auth::user()->getRoleNames()[0] != 'admin' && Auth::user()->getRoleNames()[0] === 'employer') {
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            $invoices = array();
            $expiry_date = '';
            if ($type === 'project') {
                $invoices = DB::table('invoices')
                    ->select('*')
                    ->where('payer_email', Auth::user()->email)
                    ->where('type', $type)
                    ->get();
                // $milestone_invoices = DB::table('milestone_invoices')
                //     ->select('*')
                //     ->where('sender_id', Auth::user()->id)
                //     ->get();
                // echo"<pre>";dd($invoices); exit;
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/project.blade.php'))) {
                    return view('extend.back-end.employer.invoices.project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.employer.invoices.project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            } elseif ($type === 'package') {
                $invoices = DB::table('invoices')
                    ->join('items', 'items.invoice_id', '=', 'invoices.id')
                    ->join('packages', 'packages.id', '=', 'items.product_id')
                    ->select('invoices.*', 'packages.options')
                    ->where('items.subscriber', Auth::user()->id)
                    ->where('invoices.type', $type)
                    ->get();
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/package.blade.php'))) {
                    return view('extend.back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            }
        }
    }

    public function getEmployerMilestoneInvoices($type = '')
    {
        if (Auth::user()->getRoleNames()[0] != 'admin' && Auth::user()->getRoleNames()[0] === 'employer') {
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            $invoices = array();
            $expiry_date = '';
            if ($type === 'project') {
                $invoices = DB::table('invoices')
                    ->select('*')
                    ->where('payer_email', Auth::user()->email)
                    ->where('type', $type)
                    ->get();
                $milestone_invoices = DB::table('milestone_invoices')
                    ->select('*')
                    ->where('sender_id', Auth::user()->id)
                    ->get();
                // echo"<pre>";dd($invoices); exit;
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/project.blade.php'))) {
                    return view('extend.back-end.employer.invoices.milestone_invoice_project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.employer.invoices.milestone_invoice_project', compact('invoices', 'type', 'expiry_date', 'symbol', 'milestone_invoices'));
                }
            } elseif ($type === 'package') {
                $invoices = DB::table('invoices')
                    ->join('items', 'items.invoice_id', '=', 'invoices.id')
                    ->join('packages', 'packages.id', '=', 'items.product_id')
                    ->select('invoices.*', 'packages.options')
                    ->where('items.subscriber', Auth::user()->id)
                    ->where('invoices.type', $type)
                    ->get();
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/package.blade.php'))) {
                    return view('extend.back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            }
        }
    }

    /**
     * Get Freelancer Invoices.
     *
     * @param \Illuminate\Http\Request $type type
     *
     * @return \Illuminate\Http\Response
     */
    public function getFreelancerInvoices($type = '')
    {
        if (Auth::user()->getRoleNames()[0] != 'admin' && Auth::user()->getRoleNames()[0] === 'freelancer') {
            $invoices = array();
            if ($type === 'project') {
                $invoices = DB::table('invoices')
                    ->select('*')
                    ->where('seller_email', Auth::user()->email)
                    ->where('type', $type)
                    ->get();
            } elseif ($type === 'package') {
                $invoices = DB::table('invoices')
                    ->join('items', 'items.invoice_id', '=', 'invoices.id')
                    ->join('packages', 'packages.id', '=', 'items.product_id')
                    ->select('invoices.*', 'packages.options')
                    ->where('items.subscriber', Auth::user()->id)
                    ->where('invoices.type', $type)
                    ->get();
            }
            $expiry_date = '';
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            if ($type === 'project') {
                if (file_exists(resource_path('views/extend/back-end/freelancer/invoices/project.blade.php'))) {
                    return view('extend.back-end.freelancer.invoices.project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.freelancer.invoices.project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            } elseif ($type === 'package') {
                if (file_exists(resource_path('views/extend/back-end/freelancer/invoices/package.blade.php'))) {
                    return view('extend.back-end.freelancer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.freelancer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            }
        } else {
            abort(404);
        }
    }

    public function getFreelancerMilestoneInvoices($type = '')
    {
        if (Auth::user()->getRoleNames()[0] != 'admin' && Auth::user()->getRoleNames()[0] === 'freelancer') {
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            $invoices = array();
            $expiry_date = '';
            if ($type === 'project') {
                $invoices = DB::table('invoices')
                    ->select('*')
                    ->where('seller_email', Auth::user()->email)
                    ->where('type', $type)
                    ->get();
                $milestone_invoices = DB::table('milestone_invoices')
                    ->select('*')
                    ->where('reciver_id', Auth::user()->id)
                    ->get();
                // echo"<pre>";dd($invoices); exit;
                if (file_exists(resource_path('views/extend/back-end/freelancer/invoices/project.blade.php'))) {
                    return view('extend.back-end.freelancer.invoices.milestone_invoice_project', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.freelancer.invoices.milestone_invoice_project', compact('invoices', 'type', 'expiry_date', 'symbol', 'milestone_invoices'));
                }
            } elseif ($type === 'package') {
                $invoices = DB::table('invoices')
                    ->join('items', 'items.invoice_id', '=', 'invoices.id')
                    ->join('packages', 'packages.id', '=', 'items.product_id')
                    ->select('invoices.*', 'packages.options')
                    ->where('items.subscriber', Auth::user()->id)
                    ->where('invoices.type', $type)
                    ->get();
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/package.blade.php'))) {
                    return view('extend.back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                } else {
                    return view('back-end.employer.invoices.package', compact('invoices', 'type', 'expiry_date', 'symbol'));
                }
            }
        }
    }

    /**
     * Get Invoices.
     *
     * @param integer $id roletype
     *
     * @return \Illuminate\Http\Response
     */
    public function showInvoice($id)
    {
        if (!empty($id)) {
            $symbol = '';
            $code = '';
            $invoice_info = DB::table('invoices')
                ->select('*')
                ->where('id', $id)
                ->get()->first();

            $item_info = DB::table('items')
                ->select('*')
                ->where('invoice_id', $id)
                ->get()->first();
            if (!empty($invoice_info->currency_code)) {
                $currency_code = !empty($invoice_info->currency_code) ? strtoupper($invoice_info->currency_code) : 'PKR';
                $code = Helper::currencyList($currency_code);
                $symbol = !empty($code) && !empty($code['symbol']) ? $code['symbol'] : 'PKR';
            } else {
                $payment_settings = SiteManagement::getMetaValue('commision');
                $currency_symbol  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? Helper::currencyList($payment_settings[0]['currency']) : array();
                $currency_code = !empty($currency_symbol['code']) ? $currency_symbol['code'] : 'PKR';
                $code = Helper::currencyList($currency_code);
                $symbol = !empty($code) && !empty($code['symbol']) ? $code['symbol'] : 'PKR';
            }
            if (Auth::user()->getRoleNames()->first() === 'freelancer') {
                if (file_exists(resource_path('views/extend/back-end/freelancer/invoices/show.blade.php'))) {
                    return view::make('extend.back-end.freelancer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                } else {
                    return view::make('back-end.freelancer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                }
            } elseif (Auth::user()->getRoleNames()->first() === 'employer') {
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/show.blade.php'))) {
                    return view::make('extend.back-end.employer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                } else {
                    return view::make('back-end.employer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                }
            }
        }
    }

    public function showmilestoneInvoice($id)
    {
        if (!empty($id)) {
            $symbol = '';
            $code = '';
            $invoice_info = DB::table('milestone_invoices')
                ->select('*')
                ->where('id', $id)
                ->get()->first();

            // $item_info = DB::table('items')
            //     ->select('*')
            //     ->where('invoice_id', $id)
            //     ->get()->first();
            if (!empty($invoice_info->currency_code)) {
                $currency_code = !empty($invoice_info->currency_code) ? strtoupper($invoice_info->currency_code) : 'USD';
                $code = Helper::currencyList($currency_code);
                $symbol = !empty($code) && !empty($code['symbol']) ? $code['symbol'] : '$';
            } else {
                $payment_settings = SiteManagement::getMetaValue('commision');
                $currency_symbol  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? Helper::currencyList($payment_settings[0]['currency']) : array();
                $currency_code = !empty($currency_symbol['code']) ? $currency_symbol['code'] : 'USD';
                $code = Helper::currencyList($currency_code);
                $symbol = !empty($code) && !empty($code['symbol']) ? $code['symbol'] : '$';
            }
            if (Auth::user()->getRoleNames()->first() === 'freelancer') {
                if (file_exists(resource_path('views/extend/back-end/freelancer/invoices/show.blade.php'))) {
                    return view::make('extend.back-end.freelancer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                } else {
                    return view::make('back-end.freelancer.invoices.milestone_show', compact('invoice_info', 'symbol', 'currency_code'));
                }
            } elseif (Auth::user()->getRoleNames()->first() === 'employer') {
                if (file_exists(resource_path('views/extend/back-end/employer/invoices/show.blade.php'))) {
                    return view::make('extend.back-end.employer.invoices.show', compact('invoice_info', 'symbol', 'currency_code'));
                } else {
                    return view::make('back-end.employer.invoices.milestone_show', compact('invoice_info', 'symbol', 'currency_code'));
                }
            }
        }
    }

    /**
     * Get Orders.
     *
     * @param integer $id roletype
     *
     * @return \Illuminate\Http\Response
     */
    public function showOrders()
    {
        $orders = Order::all();
        $currency   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
        $status_list = Helper::getOrderStatus();
        if (file_exists(resource_path('views/extend/back-end/admin/orders/index.blade.php'))) {
            return view::make('extend.back-end.admin.orders.index', compact('orders', 'symbol', 'status_list'));
        } else {
            return view::make('back-end.admin.orders.index', compact('orders', 'symbol', 'status_list'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminProfileSettings()
    {
        $profile = Profile::where('user_id', Auth::user()->id)
            ->get()->first();
        $banner = !empty($profile->banner) ? $profile->banner : '';
        $avater = !empty($profile->avater) ? $profile->avater : '';
        $tagline = !empty($profile->tagline) ? $profile->tagline : '';
        $description = !empty($profile->description) ? $profile->description : '';
        if (file_exists(resource_path('views/extend/back-end/admin/profile-settings/personal-detail/index.blade.php'))) {
            return view(
                'extend.back-end.admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        } else {
            return view(
                'back-end.admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        }
    }
    public function jobadminProfileSettings()
    {
        $profile = Profile::where('user_id', Auth::user()->id)
            ->get()->first();
        $banner = !empty($profile->banner) ? $profile->banner : '';
        $avater = !empty($profile->avater) ? $profile->avater : '';
        $tagline = !empty($profile->tagline) ? $profile->tagline : '';
        $description = !empty($profile->description) ? $profile->description : '';
        if (file_exists(resource_path('views/extend/back-end/job-admin/profile-settings/personal-detail/index.blade.php'))) {
            return view(
                'extend.back-end.job-admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        } else {
            return view(
                'back-end.job-admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        }
    }

    public function pageadminProfileSettings()
    {
        $profile = Profile::where('user_id', Auth::user()->id)
            ->get()->first();
        $banner = !empty($profile->banner) ? $profile->banner : '';
        $avater = !empty($profile->avater) ? $profile->avater : '';
        $tagline = !empty($profile->tagline) ? $profile->tagline : '';
        $description = !empty($profile->description) ? $profile->description : '';
        if (file_exists(resource_path('views/extend/back-end/page-admin/profile-settings/personal-detail/index.blade.php'))) {
            return view(
                'extend.back-end.page-admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        } else {
            return view(
                'back-end.page-admin.profile-settings.personal-detail.index',
                compact(
                    'banner',
                    'avater',
                    'tagline',
                    'description'
                )
            );
        }
    }


    /**
     * Store profile settings.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function storeProfileSettings(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $this->validate(
            $request,
            [
                'first_name'    => 'required',
                'last_name'    => 'required',
                'email' => 'required|email',
            ]
        );
        $json = array();
        if (!empty($request)) {
            $user_id = Auth::user()->id;
            $this->profile->storeProfile($request, $user_id);
            $json['type'] = 'success';
            $json['process'] = trans('lang.saving_profile');
            return $json;
        }
    }

    /**
     * Upload Image to temporary folder.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadTempImage(Request $request, $type = '')
    {
        $path = Helper::PublicPath() . '/uploads/users/temp/';
        if (!empty($request['hidden_avater_image'])) {
            $profile_image = $request['hidden_avater_image'];
            $image_size = array(
                'small' => array(
                    'width' => 36,
                    'height' => 36,
                ),
                'medium-small' => array(
                    'width' => 60,
                    'height' => 60,
                ),
                'medium' => array(
                    'width' => 100,
                    'height' => 100,
                ),
            );
            // return Helper::uploadTempImage($path, $profile_image);
            return Helper::uploadTempImageWithSize($path, $profile_image, '', $image_size);
        } elseif (!empty($request['hidden_banner_image'])) {
            $profile_image = $request['hidden_banner_image'];
            return Helper::uploadTempImage($path, $profile_image);
        } elseif (!empty($type) && $type == 'file') {
            $path = 'uploads/users/temp/';
            return Helper::uploadTempattachments($path, $request->file);
        } else {
            return Helper::uploadTempImage($path, $request->file);
        }
    }

    /**
     * Store project Offer
     *
     * @param mixed $request get req attributes
     *
     * @access public
     *
     * @return View
     */
    public function storeProjectOffers(Request $request)
    {
        $this->validate(
            $request,
            [
                'projects' => 'required',
                'desc'    => 'required',
            ]
        );

        $json = array();
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        if (!empty($request)) {
            $offer = new Offer();
            if (Auth::user()->getRoleNames()->first() === 'employer') {
                $storeProjectOffers = $offer->saveProjectOffer($request, $request['freelancer_id']);
                if ($storeProjectOffers == "success") {
                    $json['type'] = 'success';
                    $json['progressing'] = trans('lang.send_offer');
                    $json['message'] = trans('lang.offer_sent');
                    $user = $this->user::find(Auth::user()->id);
                    //send email
                    if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        $email_params = array();
                        $send_freelancer_offer = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_send_offer')->get()->first();
                        $message = new Message();
                        if (!empty($send_freelancer_offer->id)) {
                            $job = Job::where('id', $request['projects'])->first();
                            $freelancer = User::find($request['freelancer_id']);
                            $f_link = url('profile/' . $freelancer->slug);
                            $f_name = Helper::getUserName($freelancer->id);
                            $e_name = Helper::getUserName(Auth::user()->id);
                            $e_link = url('profile/' . $user->slug);
                            $p_link = url('job/' . $job->slug);
                            $p_title = $job->title;
                            $msg = $request['desc'];
                            $template_data = EmailTemplate::getEmailTemplateByID($send_freelancer_offer->id);
                            $message->user_id = intval(Auth::user()->id);
                            $message->receiver_id = intval($request['freelancer_id']);
                            $message->body = Helper::getProjectOfferContent($e_name, $e_link, $p_link, $p_title, $msg);
                            $message->status = 0;
                            $message->save();
                            $email_params['project_title'] = $p_title;
                            $email_params['project_link'] = $p_link;
                            $email_params['employer_profile'] = $e_link;
                            $email_params['emp_name'] = $e_name;
                            $email_params['link'] = $f_link;
                            $email_params['name'] = $f_name;
                            $email_params['msg'] = $msg;
                            Mail::to($freelancer->email)
                                ->send(
                                    new FreelancerEmailMailable(
                                        'freelancer_email_send_offer',
                                        $template_data,
                                        $email_params
                                    )
                                );
                        }
                    }
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.not_send_offer');
                    return $json;
                }
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.not_authorize');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Raise Dispute
     *
     * @param mixed $slug get job slug
     *
     * @access public
     *
     * @return View
     */
    public function raiseDispute($slug)
    {
        $breadcrumbs_settings = SiteManagement::getMetaValue('show_breadcrumb');
        $show_breadcrumbs = !empty($breadcrumbs_settings) ? $breadcrumbs_settings : 'true';
        $job = Job::where('slug', $slug)->first();
        $reasons = Arr::pluck(Helper::getReportReasons(), 'title', 'title');
        if (file_exists(resource_path('views/extend/back-end/freelancer/jobs/dispute.blade.php'))) {
            return View(
                'extend.back-end.freelancer.jobs.dispute',
                compact(
                    'job',
                    'reasons',
                    'show_breadcrumbs'
                )
            );
        } else {
            return View(
                'back-end.freelancer.jobs.dispute',
                compact(
                    'job',
                    'reasons',
                    'show_breadcrumbs'
                )
            );
        }
    }
    public function DisputeDetails($slug)
    {
        $job = Job::where('slug', $slug)->first();

        $breadcrumbs_settings = SiteManagement::getMetaValue('show_breadcrumb');
        $show_breadcrumbs = !empty($breadcrumbs_settings) ? $breadcrumbs_settings : 'true';
        $job = Job::where('slug', $slug)->first();
        $reasons = Arr::pluck(Helper::getReportReasons(), 'title', 'title');
        $Proposal = Proposal::where('status', 'cancelled')->where('hired', 1)->where('job_id', $job->id)->first();
        $Report = Report::where('reportable_id', $job->id)->where('reportable_type', 'App\Job')->first();
        $Disputes = DB::table('disputes')->where('proposal_id', $job->id)->take(2)->get();
        // DB::table('disputes')->where('user_id', $request['user_id'])->delete();


        if (file_exists(resource_path('views/extend/back-end/employer/jobs/JobDispute.blade.php'))) {
            return View(
                'extend.back-end.employer.jobs.JobDispute',
                compact(
                    'job',
                    'reasons',
                    'show_breadcrumbs',
                    'Proposal',
                    'Report',
                    'Disputes'
                )
            );
        } else {
            return View(
                'back-end.employer.jobs.JobDispute',
                compact(
                    'job',
                    'reasons',
                    'show_breadcrumbs',
                    'Proposal',
                    'Report',
                    'Disputes'
                )
            );
        }
    }
    /**
     * Raise dispute
     *
     * @param mixed $request $req->attr
     *
     * @access public
     *
     * @return View
     */
    public function storeDispute(Request $request)
    {
        $json = array();
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }

        $storeDispute = $this->user->saveDispute($request);
        if ($storeDispute == "success") {
            $json['type'] = 'success';
            $json['message'] = trans('lang.dispute_raised');
            $user = $this->user::find(Auth::user()->id);



            //send email
            if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                $email_params = array();
                $dispute_raised_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_dispute_raised')->get()->first();
                if (!empty($dispute_raised_template->id)) {


                    $job = Job::where('id', $request['proposal_id'])->first();
                    $userPe = User::where('id', $job->user_id)->first();
                    $data = array();
                    //dd($userPe);
                    $data['name'] = Helper::getUserName(Auth::user()->id);
                    $data['job_id'] = $job->id;
                    $data['project_title'] = $job->slug;
                    $data['project_link'] = url('job/' . $job->slug);
                    $data['message'] = $request['description'];
                    $data['reason'] = $request['reason'];
                    Notification::send($userPe, new DisputeRaised($data));


                    $template_data = EmailTemplate::getEmailTemplateByID($dispute_raised_template->id);
                    $email_params['project_title'] = $job->title;
                    $email_params['project_link'] = url('job/' . $job->slug);
                    $email_params['sender_link'] = url('profile/' . $user->slug);
                    $email_params['name'] = Helper::getUserName(Auth::user()->id);
                    $email_params['msg'] = $request['description'];
                    $email_params['reason'] = $request['reason'];
                    Mail::to(config('mail.username'))
                        ->send(
                            new AdminEmailMailable(
                                'admin_email_dispute_raised',
                                $template_data,
                                $email_params
                            )
                        );
                }
            }
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Raise dispute
     *
     * @access public
     *
     * @return View
     */
    public function userListing()
    {
        if (Auth::user() && Auth::user()->getRoleNames()->first() === 'admin') {
            if (!empty($_GET['keyword'])) {
                $keyword = $_GET['keyword'];
                $users = $this->user::where('first_name', 'like', '%' . $keyword . '%')->orWhere('last_name', 'like', '%' . $keyword . '%')->paginate(7)->setPath('');
                $pagination = $users->appends(
                    array(
                        'keyword' => Input::get('keyword')
                    )
                );
            } else {
                $users = User::select('*')->latest()->paginate(10);
            }
            if (file_exists(resource_path('views/extend/back-end/admin/users/index.blade.php'))) {
                return view('extend.back-end.admin.users.index', compact('users'));
            } else {
                return view('back-end.admin.users.index', compact('users'));
            }
        } else {
            abort(404);
        }
    }
    public function procurerListing()
    {

        //  $userslist =$this->user::with('userrole')->get();
        $type = 'employer';

        // $users =  User::role($type)->select('id')->paginate(7)->setPath('');

        //$users = $this->user::with('comments_with_deleted')->where('role_id',1)->get();

        // $comments = $this->user::with(['userrole' => function ($query) {
        //     $query->where('role_id ', '=', 1);
        // }])->get();



        // dd($users);
        if (Auth::user() && Auth::user()->getRoleNames()->first() === 'admin') {
            if (!empty($_GET['keyword'])) {

                $keyword = $_GET['keyword'];
                $users = $this->user::where('first_name', 'like', '%' . $keyword . '%')->role($type)->paginate(7)->setPath('');
                $pagination = $users->appends(
                    array(
                        'keyword' => Input::get('keyword')
                    )
                );
            } else {
                $users = User::select('*')->role($type)->latest()->paginate(10);
            }
            if (file_exists(resource_path('views/extend/back-end/admin/users/procurer.blade.php'))) {
                return view('extend.back-end.admin.users.procurer', compact('users'));
            } else {
                return view('back-end.admin.users.procurer', compact('users'));
            }
        } else {
            abort(404);
        }
    }
    /**
     * Get Freelancer Payouts.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPayouts()
    {
        if (!empty($_GET['year']) && !empty($_GET['month'])) {
            $year = $_GET['year'];
            $month = $_GET['month'];
            $payouts =  DB::table('payouts')
                ->select('*')
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month)
                ->paginate(7)->setPath('');
            $pagination = $payouts->appends(
                array(
                    'year' => Input::get('year'),
                    'month' => Input::get('month')
                )
            );
        } else {
            $payouts =  Payout::paginate(7);
        }
        $selected_year = !empty($_GET['year']) ? $_GET['year'] : '';
        $selected_month = !empty($_GET['month']) ? $_GET['month'] : '';
        $months = Helper::getMonthList();
        $years = array_combine(range(date("Y"), 1970), range(date("Y"), 1970));
        if (file_exists(resource_path('views/extend/back-end/admin/payouts.blade.php'))) {
            return view(
                'extend.back-end.admin.payouts',
                compact('payouts', 'years', 'selected_year', 'months', 'selected_month')
            );
        } else {
            return view(
                'back-end.admin.payouts',
                compact('payouts', 'years', 'selected_year', 'months', 'selected_month')
            );
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($year, $month, $id = array())
    {
        $slected_ids = array();
        if (!empty($id)) {
            $slected_ids = explode(',', $id);
        }
        $payouts =  DB::table('payouts')
            ->select('*')
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->whereIn('id', $slected_ids)
            ->get();
        $pdf = PDF::loadView('back-end.admin.payouts-pdf', compact('payouts', 'year', 'month'));
        return $pdf->download('payout-' . $month . '-' . $year . '.pdf');
    }

    // /**
    //  * Verify Code
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function verifyUserEmailCode(Request $request)
    // {
    //     $role = Auth::user()->getRoleNames()->first();
    //     if (!empty($request['code'])) {
    //         if ($request['code'] === $user->verification_code) {
    //             $user->user_verified = 1;
    //             $user->verification_code = null;
    //             $user->save();
    //             return Redirect::to($role . '/dashboard');
    //         } else {
    //             Session::flash('error', trans('lang.ph_email_warning'));
    //             return Redirect::back();
    //         }
    //     } else {
    //         $json['type'] = 'error';
    //         $json['message'] = trans('lang.verify_code');
    //         return $json;
    //     }
    // }

    /**
     * Verify Code
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePayoutStatus(Request $request)
    {
        $role = Auth::user()->getRoleNames()->first();
        if (!empty($request['id'])) {
            $payout = Payout::find($request['id']);
            $payout->status = $request['status'];
            $payout->save();
            if (!empty($request['projects_ids'])) {
                $projects_ids = Unserialize($request['projects_ids']);
                foreach ($projects_ids as $key => $id) {
                    if ($payout->type == 'job') {
                        $proposal = Proposal::find($id);
                        $proposal->paid_progress = 'completed';
                        $proposal->save();
                    } elseif ($payout->type == 'service') {
                        DB::table('service_user')
                            ->where('id', $id)
                            ->update(['paid_progress' => 'completed']);
                    }
                }
            }
            $json['type'] = 'success';
            $json['message'] = trans('lang.status_updated');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Submit user refound to payouts
     *
     * @return \Illuminate\Http\Response
     */
    public function submitUserRefund(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $json = array();
        if (!empty($request)) {
            $this->validate(
                $request,
                [
                    'refundable_user_id' => 'required',
                ]
            );
            $role = $this->user::getUserRoleType($request['refundable_user_id']);
            if ($role->role_type == 'freelancer') {
                $update_status = '';
                if ($request['type'] == 'job') {
                    $update_status = $this->user->updateCancelProject($request);
                } elseif ($request['type'] == 'service') {
                    $update_status = $this->user->updateCancelService($request);
                }
                if ($update_status = 'success') {
                    $json['type'] = 'success';
                    $json['message'] = trans('lang.status_updated');
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.something_wrong');
                    return $json;
                }
            } elseif ($role->role_type == 'employer') {
                $refound = $this->user->transferRefund($request);
                if ($refound == 'payout_not_available') {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.user_payout_not_set');
                    return $json;
                } else if ($refound == 'success') {
                    $json['type'] = 'success';
                    $json['message'] = trans('lang.refund_transfer');
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.all_required');
                    return $json;
                }
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Verify Code
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePayoutDetail(Request $request)
    {
        $user_id = $request['id'];
        if (!empty($user_id)) {

            $payout_setting = $this->profile->savePayoutDetail($request, $user_id);
            $json['type'] = 'success';
            $json['message'] = 'payout update successfully';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.verify_code');
            return $json;
        }
    }

    /**
     * Get payout detail
     *
     */
    public function getPayoutDetail()
    {
        $json = array();
        if (Auth::user()) {
            $user = User::find(Auth::user()->id);
            $payout_detail = !empty($user->profile) ? Helper::getUnserializeData($user->profile->payout_settings) : array();
            $json['type'] = 'success';
            $json['payouts'] = $payout_detail;
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.verify_code');
            return $json;
        }
    }

    /**
     * Update user verification status
     *
     * @param mixed $request request attributes
     * 
     * @return \Illuminate\Http\Response
     */


    public function getSingleSourceVerify(Request $request)
    {
        if (!empty($request['single_source_email'])) {

            $user = User::where('email', $request['single_source_email'])->first();

            if ($user) {
                $json['type'] = 'sucess';
                $json['message'] = $user;
            } else {
                $json['type'] = 'error';
                $json['message'] = 'No Associated accout found';
            }

            return $json;
        }
    }
    // admin veriying the user 
    public function updateUserVerification(Request $request)
    {
        if (!empty($request['user_id'])) {
            if ($request['type'] == 'not_verify') {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['user_verified' => 0]);
                $json['status_text'] = trans('lang.not_verified');
            } else {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['user_verified' => 1]);
                $json['status_text'] = trans('lang.verified');
            }
            $json['type'] = 'success';
            $json['message'] = trans('lang.verification_status_updated');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_went_wrong');
            return $json;
        }
    }

    public function updateUserDepartment(Request $request)
    {
        if (!empty($request['user_id'])) {
            if ($request['type'] == 'not_govt') {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['government_department' => 0]);
                $json['status_text'] = trans('Non-Government Department');
            } else {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['government_department' => 1]);
                $json['status_text'] = trans('Government Department');
            }
            $json['type'] = 'success';
            $json['message'] = trans('Department Updated');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_went_wrong');
            return $json;
        }
    }
    public function updateUserdisable(Request $request)
    {
        if (!empty($request['user_id'])) {
            if ($request['type'] == 'false') {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['is_disabled' => 'false']);
                $json['status_text'] = trans('User Enable');
            } else {
                DB::table('users')
                    ->where('id', $request['user_id'])
                    ->update(['is_disabled' => 'true']);
                $json['status_text'] = trans('User Disable');
            }
            $json['type'] = 'success';
            $json['message'] = trans('User Updated');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_went_wrong');
            return $json;
        }
    }
    public function updateUserVerificationSelf(Request $request)
    {

        $request->validate([
            'user_id' => 'required',
            'two_factor_code' => 'required',


        ]);
        $current_time = \Carbon\Carbon::now();
        if (!empty($request['user_id']) && !empty($request['two_factor_code'])) {

            $user = DB::table('users')->where('id', $request['user_id'])->where('verification_code', $request['two_factor_code'])->first();
            if ($user) {
                $updeUser = User::where('id', $user->id)->first();
                $updeUser->user_verified = 1;
                $updeUser->email_verified_at = $current_time;
                $updeUser->save();

                return redirect('/')->with('message', 'Activated Successfully!');
            } else {
                return Redirect::back()->with('error', 'Activation Code Error!');
            }
        }
    }

    public function getCommitteeMembers()
    {
        $userId = Auth::id();
        
        // $users = User::where('parent_member_id', 'LIKE', '%' . $userId . '%')->get();
           
            $users = array();
            $users_info = User::checkparent($userId);
            foreach($users_info as $user_info){
                $users[] = DB::table('users')->where('id', $user_info->committee_member_id)->first();
            }
            // dd($users);
            
      
        // $users = User::where('parent_member_id', $userId)->get();
        

        return view(
            'back-end.employer.CommitteeMembers',
            compact('users')
        );
    }

    public function CommitteeMembersAdd()
    {
        return view(
            'back-end.employer.CommitteeMembersAdd'
        );
    }
    public function CommitteeMembersEdit($id)
    {
        $user = User::find($id);
        return view('back-end.employer.CommitteeMembersEdit', compact('user'));
    }

    public function AlreadyExistingCommitteeStore($id){
        $user_info = User::where('id', $id)->first();
        return view('back-end.employer.AlreadyExistingCommitteeMember', compact('user_info'))->with('errormessage', 'Committee Members Already Exists!');
    }

    public function AddExistingCommitteeMember($id){
        // dd($id);
        $employer = Auth::user();
        $user_info = User::where('id', $id)->first();
        $user_committee_member = User::AsignCommitteeMemberParent($employer->id, $user_info);
        return redirect('committee-members')->with('message', 'Committee Members Add Successfully!');
    }

    public function CommitteeStore(Request $request)
    {
        $userId = Auth::id();
        if(User::where('email', $request['email'])->where('user_type', '2')->exists()){
            $user_info = User::where('email', $request['email'])->first();
            return redirect()->route('AlreadyexistingCommitteeMember', $user_info->id)->with('errormessage', 'Committee Members Already Exists!');
        }
        else{
            $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

            $name = $request['first_name'] . $request['last_name'];
            // $temp = str_slug($name, '-');

            $temp = str_slug($name, '-');
            if (!User::all()->where('slug', $temp)->isEmpty()) {
                $i = 1;
                $new_slug = $temp . '-' . $i;
                while (!User::all()->where('slug', $new_slug)->isEmpty()) {
                    $i++;
                    $new_slug = $temp . '-' . $i;
                }
                $temp = $new_slug;
            }

            $random_number = Helper::generateRandomCode(4);

            // $wishlist = !empty($wishlist) && is_array($wishlist) ? $wishlist : array();
            // $wishlist[] = $userId;
            // $wishlist = array_unique($wishlist);

            $user =  User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'user_type' => 2,
            'slug' =>  $temp,
            'user_verified' =>  0,
            // 'parent_member_id' =>  serialize($wishlist),
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'verification_code' => strtoupper($random_number),

        ]);
            $user->assignRole('committee-member');
            
            $parent_employer = User::AsignCommitteeMemberParent($userId,$user);

            $data = array();

            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['email'] = $user->email;
            $data['verification_code'] = $user->verification_code;
            $data['password'] = $request->get('password');
           
            
    
            \Notification::send($user, new CommitteNew($data));
            $profile = Profile::create([
            'user_id' => $user->id,
        ]);


            // $email_params = array();
            // $template = DB::table('email_types')->select('id')
            // ->where('email_type', 'verification_code')->get()->first();
            // if (!empty($template->id)) {
            //     $template_data = EmailTemplate::getEmailTemplateByID($template->id);
            //     $email_params['verification_code'] = $user->verification_code;
            //     $email_params['name']  = Helper::getUserName($user->id);
            //     $email_params['email'] = $user->email;
            //     $email_params['password'] = $request['password'];

            //     Mail::to($user->email)
            //     ->send(
            //         new GeneralEmailMailable(
            //             'verification_code',
            //             $template_data,
            //             $email_params
            //         )
            //     );
            // }
            
            return redirect('committee-members')->with('message', 'Committee Members Added Successfully!');
        }

        
    }
    public function CommitteeUpdate(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);


        $user = User::find($id);

        $user->first_name =  $request->get('first_name');
        $user->last_name =  $request->get('last_name');

        if ($request->get('password')) {
            $user->password = Hash::make($request['password']);
        }

        $user->save();
        $data = array();



        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['email'] = $user->email;
        $data['password'] = $request->get('password');

        \Notification::send($user, new CommitteUpdate($data));

        return redirect('committee-members')->with('message', 'Committee Members Updated Successfully!');
    }

    public function CommitteeDelete(Request $request, $id)
    {
        // dd($id);
        $userId = Auth::id();

        $committeeuser = User::deleteparent($userId, $id);
        
        return redirect('committee-members')->with('message', 'Committee Member Deleted Successfully!');
    }

    public function getCommitteeMembersForProject($id)
    {
        $userId = Auth::id();
        // $users = User::where('parent_member_id', $userId)->get();
        $users = array();
            $users_info = User::checkparent($userId);
            foreach($users_info as $user_info){
                $users[] = DB::table('users')->where('id', $user_info->committee_member_id)->first();
            }
        $CommitteeMembers = CommitteeMember::where('job_id', $id)->pluck('member_id');
        $CommitteeMembersinfo = CommitteeMember::where('job_id', $id)->get();

        foreach ($users as $key => $value) {
            $CommitteeMembers = CommitteeMember::where('job_id', $id)->where('member_id', $value->id)->first();
            $users[$key]->memberValue = $CommitteeMembers;
        }
        $project_id = $id;
        return view('back-end.employer.CommitteeMembersProject', compact('users', 'project_id', 'CommitteeMembers', 'CommitteeMembersinfo'));
    }
    public function getCommitteeMembersForProjectAdd(Request $request)
    {       
        // dd('here');

        $userId = Auth::id();
        $jobDetails = Job::where('id', $_POST['job_id'])->where('user_id', $userId)->first();
        if ($jobDetails) {

            // $CommitteeMembers = CommitteeMember::where('job_id', $_POST['job_id'])->delete();

            if (!empty($request->get('ids'))) {

                foreach ($request->get('ids') as $key => $value) {
                    $CommitteeMember = new CommitteeMember([
                        'member_id' => $value,
                        'parent_id' => $userId,
                        'job_id' => $request->get('job_id'),

                    ]);
// dd('here');
                    $CommitteeMember->save();
                    //  auth()->user()->notify(new CommitteeMemberAdd($jobDetails));
                    $user = User::find($value);
                    Notification::send($user, new CommitteeMemberAdd($jobDetails));
                }
            }
        }
        return \Redirect::route('project-committee-members', $request->get('job_id'))->with('message', 'Committee Members saved Successfully!');
    }

    public function CommitteeMembersForProjectDelete($project_id, $CommitteeMemberid){
       
        $userId = Auth::id();
        $jobDetails = Job::where('id', $project_id)->where('user_id', $userId)->first();
        $CommitteeMembers = CommitteeMember::where('job_id', $project_id)->where('member_id', $CommitteeMemberid)->delete();
        $user = User::find($CommitteeMemberid);
        Notification::send($user, new CommitteeMemberDelete($jobDetails));
        return \Redirect::route('project-committee-members', $project_id)->with('message', 'Committee Members removed Successfully!');
    }


    public function ajaxCommitteeMembersForProjectAdd(Request $request)
    {
        //  dd( $request);
        $userId = Auth::id();
        $jobDetails = Job::where('id', $request['job_id'])->where('user_id', $userId)->first();
        if ($jobDetails) {
           
            if ($request['ids']) { 
                
                foreach ($request['ids'] as $id) {
                    // if ($request['CheckStatus'] == 'checked') {dd('here');
                        $CommitteeMember = new CommitteeMember([
                        'member_id' => $id,
                        'parent_id' => $userId,
                        'job_id' => $request['job_id'],

                    ]);
                        
                        $CommitteeMember->save();
                        $user = User::find($id);
                        Notification::send($user, new CommitteeMemberAdd($jobDetails));
                    // } else {
                        // $CommitteeMembers = CommitteeMember::where('job_id', $request['job_id'])->where('member_id', $id)->delete();
                    // }
                }
            }
        }
        return \Redirect::route('project-committee-members', $request->get('job_id'))->with('message', 'Committee Members saved Successfully!');

    }

    public function getKpraData()
    {
        $user_id = Auth::user()->id;
        $profile = new Profile();
        $kpraInfo = Helper::getKpraInfo($user_id);
        $kntn = Auth::user()->kntn;
        if (file_exists(resource_path('views/extend/back-end/settings/kpra-info'))) {
            return view(
                'extend.back-end.settings.kpra-info',
                compact('kpraInfo', 'kntn')
            );
        } else {
            return view(
                'back-end.settings.kpra-info',
                compact('kpraInfo', 'kntn')
            );
        }
    }
    public function saveKPRASettings(Request $request)
    {

        $request->validate([
            'kntn' => new KPRAPROFILE,
        ]);

        $user_id = Auth::user()->id;

        $user = User::find($user_id);
        $user->kntn = $_POST['kntn'];
        $user->save();
        $ch = curl_init();
        $kpraNUmber = $_POST['kntn'];
        curl_setopt($ch, CURLOPT_URL, "http://175.107.62.188:4448/kpradss/index.php/Api/showStatus/$kpraNUmber");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $jsonArrayResponse = json_decode($server_output);
        $KPRADATA = $jsonArrayResponse[0];
        if (!empty($request['kntn'])) {
            $KpraInfo = new KpraInfo([
                'NTN' => $KPRADATA->NTN,
                'Business_Name' => $KPRADATA->Business_Name,
                'Enrollment_Date' => $KPRADATA->Enrollment_Date,
                'Principal_Activity' => $KPRADATA->Principal_Activity,
                'Activity_Code' => $KPRADATA->Activity_Code,
                'Addresse' => $KPRADATA->Addresse,
                'City_Name' => $KPRADATA->City_Name,
                'District_Name' => $KPRADATA->District_Name,
                'Activetaxpayer' => $KPRADATA->Activetaxpayer,
                'Compliance_Level' => $KPRADATA->Compliance_Level,
                'user_id' => $user_id,

            ]);
            $KpraInfo->save();

            return Redirect::back()->with('message', 'KPRA Information Added Successfully!');
        } else {
            return Redirect::back()->with('error', 'KPRA Information Cannot Be Added!');
            //    Session::flash('error', trans('KPRA Information Cannot Be Added!'));
            //     return Redirect::back();

        }
    }
    public function addCommentToProject($id)
    {
        $userId = Auth::id();
        $CommitteeMembers = CommitteeMember::where('job_id', $id)->where('member_id', $userId)->first();
        $MinutesOfMeetings = MinutesOfMeeting::where('job_id', $id)->get();
        $BidQuestion = BidQuestion::where('job_id', $id)->get();

        $jobDetails = Job::where('id', $id)->first();

        $accepted_proposal = array();
        $job = Job::where('slug', $jobDetails->slug)->first();
        $proposals = Job::find($job->id)->proposals;


        $TechnicalQuestions = BidQuestion::where('job_id', $id)->where('type', 0)->pluck('id');
        $FinancialQuestions = BidQuestion::where('job_id', $id)->where('type', 1)->pluck('id');

        $TechnicalQuestionsTotal = BidQuestion::where('job_id', $id)->where('type', 0)->sum('score');
        $FinancialQuestionsTotal = BidQuestion::where('job_id', $id)->where('type', 1)->sum('score');


        $scoreArray = array();
        if (!empty($job->selection_procurement_system)) {

            // 1= Quality Based Selection;
            // 2= Quality & Cost Based Selection
            // 3= Least Cost
            // 4= Fixed Budget
            //dd($job->selection_procurement_system);
            if (!empty($proposals)) {
                foreach ($proposals as $key => $value) {
                    # code...
                    $bid_scoresT_count = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->count();
                    $bid_scoresT_total = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
                    if($bid_scoresT_count > 0) {
                        $scoreArray[$value->id]['TechnicalQuestionsScore'] = $bid_scoresT_total / $bid_scoresT_count;
                    } else {
                        $scoreArray[$value->id]['TechnicalQuestionsScore'] = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
                    }

                    $bid_scoresF_count = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->count();
                    $bid_scoresF_total = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->sum('score');
                    if($bid_scoresF_count > 0) {
                        $scoreArray[$value->id]['FinancialQuestionsScore'] = $bid_scoresF_total / $bid_scoresF_count;
                    } else {
                        $scoreArray[$value->id]['FinancialQuestionsScore'] = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->sum('score');
                    }

                    $bid_scoresTF_users = BidScore::where('proposal_id', $value->id)->pluck('member_id');
                    $bid_scores_users = array();

                    if ($bid_scoresTF_users) {
                        $users_rm_duplicates = array_unique($bid_scoresTF_users->toArray());
                        foreach ($users_rm_duplicates as $user_id) {
                            $get_user = User::where('id', $user_id)->first();
                            if ($get_user) {
                                $user_techical_scores = BidScore::where('proposal_id', $value->id)->where('member_id', $user_id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
                                $user_financial_scores = BidScore::where('proposal_id', $value->id)->where('member_id', $user_id)->whereIn('question_id', $FinancialQuestions)->sum('score');
                                $usArray = [
                                    'name' => $get_user->first_name . ' ' . $get_user->last_name,
                                    'total_technical_score' => $user_techical_scores,
                                    'total_financial_score' => $user_financial_scores,
                                ];
                                array_push($bid_scores_users, $usArray);
                            }
                        }
                    }

                    $scoreArray[$value->id]['UserScores'] = $bid_scores_users;
                    $scoreArray[$value->id]['TotalScore'] = $scoreArray[$value->id]['TechnicalQuestionsScore'] + $scoreArray[$value->id]['FinancialQuestionsScore'];
                    $scoreArray[$value->id]['proposals_id'] = $value->id;
                }
            }
        }
        if ($job->selection_procurement_system == 1) {
            $TechnicalQuestionsScores = array();
            foreach ($scoreArray as $key => $row) {
                $TechnicalQuestionsScores[$key] = $row['TechnicalQuestionsScore'];
            }
            array_multisort($TechnicalQuestionsScores, SORT_DESC, $scoreArray);
        }
        if ($job->selection_procurement_system == 2) {
            $TotalScore = array();
            foreach ($scoreArray as $key => $row) {
                $TotalScore[$key] = $row['TotalScore'];
            }
            array_multisort($TotalScore, SORT_DESC, $scoreArray);
        }
        if ($job->selection_procurement_system == 3) {
            $FinancialQuestionsScore = array();
            foreach ($scoreArray as $key => $row) {
                $FinancialQuestionsScore[$key] = $row['FinancialQuestionsScore'];
            }
            array_multisort($FinancialQuestionsScore, SORT_DESC, $scoreArray);
        }
        // dd($scoreArray);

        foreach ($proposals as $key => $value) {

            $bid_scoresT_count = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->count();
            $bid_scoresT_total = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
            
            if ($bid_scoresT_count > 0) {
                $TechnicalQuestionsScore = $bid_scoresT_total / $bid_scoresT_count;
            } else {
                $TechnicalQuestionsScore = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
            }
            $proposals[$key]->TechnicalQuestionsScore = $TechnicalQuestionsScore;

            $bid_scoresF_count = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->count();
            $bid_scoresF_total = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->sum('score');
            
            if ($bid_scoresF_count > 0) {
                $FinancialQuestionsScore = $bid_scoresF_total / $bid_scoresF_count;
            } else {
                $FinancialQuestionsScore = BidScore::where('proposal_id', $value->id)->whereIn('question_id', $FinancialQuestions)->sum('score');
            }

            $proposals[$key]->FinancialQuestionsScore = $FinancialQuestionsScore;

            $bid_scoresTF_users = BidScore::where('proposal_id', $value->id)->pluck('member_id');
            $BidScoresUsers = array();

            if ($bid_scoresTF_users) {
                $users_rm_duplicates = array_unique($bid_scoresTF_users->toArray());
                foreach ($users_rm_duplicates as $user_id) {
                    $get_user = User::where('id', $user_id)->first();
                    if ($get_user) {
                        $user_techical_scores = BidScore::where('proposal_id', $value->id)->where('member_id', $user_id)->whereIn('question_id', $TechnicalQuestions)->sum('score');
                        $user_financial_scores = BidScore::where('proposal_id', $value->id)->where('member_id', $user_id)->whereIn('question_id', $FinancialQuestions)->sum('score');
                        $usArray = [
                            'name' => $get_user->first_name . ' ' . $get_user->last_name,
                            'total_technical_score' => $user_techical_scores,
                            'total_financial_score' => $user_financial_scores,
                        ];
                        array_push($BidScoresUsers, $usArray);
                    }
                }
            }
            
            $proposals[$key]->UserScores = $BidScoresUsers;

            if ($job->selection_procurement_system == 1 && $TechnicalQuestionsScore > $FinancialQuestionsScore) {
                $proposals[$key]->recommendation = "recommended";
            } elseif ($job->selection_procurement_system == 2 && $TechnicalQuestionsScore == $FinancialQuestionsScore) {
                $proposals[$key]->recommendation = "recommended";
            } elseif ($job->selection_procurement_system == 3 && $TechnicalQuestionsScore < $FinancialQuestionsScore) {
                $proposals[$key]->recommendation = "recommended";
            } elseif ($job->selection_procurement_system == 4 && $TechnicalQuestionsScore < $FinancialQuestionsScore) {
                $proposals[$key]->recommendation = "recommended";
            }
        }
        // dd($proposals);


        $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)->first();
        $order = Job::getProjectOrder($job->id);
        $duration = !empty($job->duration) ? Helper::getJobDurationList($job->duration) : '';
        $currency   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
        $payment_settings = SiteManagement::getMetaValue('commision');
        $enable_package = !empty($payment_settings) && !empty($payment_settings[0]['enable_packages']) ? $payment_settings[0]['enable_packages'] : 'true';
        $mode = !empty($payment_settings) && !empty($payment_settings[0]['payment_mode']) ? $payment_settings[0]['payment_mode'] : 'true';

        
        return view('back-end.freelancer.CommitteeMembersProjectDetail', compact(
            'jobDetails',
            'proposals',
            'job',
            'duration',
            'accepted_proposal',
            'symbol',
            'enable_package',
            'mode',
            'order',
            'MinutesOfMeetings',
            'BidQuestion',
            'TechnicalQuestionsTotal',
            'FinancialQuestionsTotal',
            'scoreArray'
        ));
    }

    public function createUser()
    {

        // dd($user_role);

        $roles = DB::table('roles')->where('id', '!=', '1')->get()->toArray();
        // dd($roles);

        return view('back-end.admin.users.createuser', compact('roles'));
    }
    public function add_usermanage_process(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role_id' => 'required',
        ]);

        $name = $request['first_name'] . $request['last_name'];
        // $temp = str_slug($name, '-');

        $temp = str_slug($name, '-');
        if (!User::all()->where('slug', $temp)->isEmpty()) {
            $i = 1;
            $new_slug = $temp . '-' . $i;
            while (!User::all()->where('slug', $new_slug)->isEmpty()) {
                $i++;
                $new_slug = $temp . '-' . $i;
            }
            $temp = $new_slug;
        }

        $random_number = Helper::generateRandomCode(4);
        if (!empty($request['role_emp'])) {
            if ($request['role_emp'] == 'govt_emp') {
                $govt_dept_id = 1;
                $government_department = 1;
            } else {
                $govt_dept_id = 0;
                $government_department = 0;
            }
        } else {
            $govt_dept_id = 0;
            $government_department = 0;
        }
        $user =  User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            // 'user_type' => 2,
            'slug' =>  $temp,
            'user_verified' =>  0,
            'parent_member_id' =>  $userId,
            'email' => $request['email'],
            'role_id' => $request['role_id'],
            'password' => Hash::make($request['password']),
            'verification_code' => strtoupper($random_number),
            'govt_dept_id' => $govt_dept_id,
            'government_department' => $government_department

        ]);

        if ($user->role_id == 1) {
            $user->assignRole('admin');
        } elseif ($user->role_id == 2) {
            $user->assignRole('employer');
        } elseif ($user->role_id == 3) {
            $user->assignRole('freelancer');
        } elseif ($user->role_id == 4) {
            $user->assignRole('job-administrator');
        } elseif ($user->role_id == 5) {
            $user->assignRole('page-management');
        }
        //   dd($user); 
        // $user->save();

        $data = array();



        $data['verification_code'] = $user->verification_code;
        $data['name']  = Helper::getUserName($user->id);
        $data['email'] = $user->email;
       
        $data['password'] = $request->get('password');

        \Notification::send($user, new adminNewUser($data));


        // $email_params = array();
        // $template = DB::table('email_types')->select('id')
        //     ->where('email_type', 'new_user')->get()->first();
        // if (!empty($template->id)) {
        //     $template_data = EmailTemplate::getEmailTemplateByID($template->id);
        //     $email_params['verification_code'] = $user->verification_code;
        //     $email_params['name']  = Helper::getUserName($user->id);
        //     $email_params['email'] = $user->email;
        //     $email_params['password'] = $request['password'];

        //     Mail::to($user->email)
        //         ->send(
        //             new GeneralEmailMailable(
        //                 'verification_code',
        //                 $template_data,
        //                 $email_params
        //             )
        //         );
        // }


        $profile = Profile::create([
            'user_id' => $user->id,
        ]);


        return redirect('users')->with('message', 'New User Added Successfully!');
    }

    public function updateUseradmin($slug)
    {
        $user = User::where('slug', $slug)->first();
        $user_role = $user::getUserRoleType($user->id);
        // dd($user_role);
        $roles = DB::table('roles')->where('id', '!=', 1)->get();
        // dd($roles);

        return view('back-end.admin.users.edit', compact(
            'user',
            'roles',
            'user_role'
        ));
    }
    public function updateUseradminprocess($id, Request $request)
    {

        $user = User::where('id', $id)->first();
        $user_role = $user::getUserRoleType($user->id);
        

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'role' => 'required',
        ]);
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->contact_number = $request->get('contact_number');
        if (!empty($request->get('government_department'))) {
            $user->government_department = $request->get('government_department');
        }
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->contact_number = $request->get('contact_number');
        $user->government_department = $request->get('government_department');

        if ($user_role->id != $request->get('role')) {
            
            $role_name = DB::table('roles')->where('id', $request->get('role'))->first();
            $data = array();
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['previous_role'] = $user_role->name;
            $data['new_role'] = $role_name->name;

            \Notification::send($user, new AssignNewRole($data));
        }

        $user_roles = DB::table('model_has_roles')->where('model_id', $user->id)->update(['role_id' => $request->get('role')]);
// dd($user_roles);
        if (!empty($request->get('password'))) {
            $user->password = Hash::make($request->password);
        } else {
            $user->password = $user->password;
        }
        $user->save();

        // Account has been updated mail

        $data = array();



        $data['First_Name'] = $user->first_name;
        $data['Last_Name'] = $user->last_name;
        $data['Email'] = $user->email;
        if (!empty($user->contact_number)) {
            $data['Contact_Number'] = $user->contact_number;
        } else {
            $data['Contact_Number'] = null;
        }
        if ($user->government_department == '0') {
            $data['Govt_Dept'] = "No";
        } else {
            $data['Govt_Dept'] = "Yes";
        }
        $data['Password'] = $request->get('password');

        \Notification::send($user, new AccountUpdated($data));


        return Redirect::to('/users')->with('message', 'Account Updated');
    }

    public function adminDashBoard()
    {
        // dd(Auth::user());
        $chart_user_freelancer_location = '';
        $chart_user_freelancer_skills = '';
        $total_jobs = Job::get();
        $ongoing_jobs = Job::where('status', 'hired')->get();
        $completed_jobs = Job::where('status', 'completed')->get();
        $cancelled_jobs = Job::where('status', 'cancelled')->get();
        $total_users = User::get();
        $Skill = Skill::get();
        $Review = Review::get();


        $ongoing_jobs_5 = Job::where('status', 'posted')->limit(10)->get();
        $count_total_jobs = count($total_jobs);
        $count_ongoing_jobs = count($ongoing_jobs);
        $count_completed_jobs = count($completed_jobs);
        $count_cancelled_jobs = count($cancelled_jobs);
        $count_total_users = count($total_users);
        $count_Skill = count($Skill);
        $count_Review = count($Review);

        $typefree = 'freelancer';
        $user_by_role_freelancer =  User::role($typefree)->where('is_disabled', 'false')->whereNotNull('email_verified_at')->limit(5)->orderBy('created_at', 'DESC')->get();

        $usertypefree = 'employer';
        $user_by_role_employer =  User::role($usertypefree)->where('is_disabled', 'false')->whereNotNull('email_verified_at')->limit(5)->orderBy('created_at', 'DESC')->get();
        $locations  = Location::orderBy('title', 'asc')->limit(10)->get();
        foreach ($locations as $key => $location) {
            $Users = User::where('location_id', $location->id)->where('is_disabled', 'false')->whereNotNull('email_verified_at')->get();
            // dd( count($Users) );
            $count_total = count($Users);
            if ($count_total > 0) {
                // $chart_user_freelancer_location .= "{
                //     'location': ' $location->title',
                //     'users': $count_total
                //     },";
                $chart_user_freelancer_location .= "['$location->title', $count_total],";
            }
        }


        $Skillchart  = Skill::limit(10)->get();
        foreach ($Skillchart as $key => $Skillsingle) {

            $skillscount = count($Skillsingle->freelancers);

            if ($skillscount > 0) {

                $chart_user_freelancer_skills .= "['$Skillsingle->title', $skillscount],";
            }
        }

        // dd($chart_user_freelancer_location);
        $user_emp = DB::table('model_has_roles')->where('role_id', '2')->get();
        $cout_user_emp = count($user_emp);

        $user_free = DB::table('model_has_roles')->where('role_id', '3')->get();
        $cout_user_free = count($user_free);
        return view('back-end.admin.dashboard', compact('chart_user_freelancer_skills', 'chart_user_freelancer_location', 'count_total_jobs', 'count_ongoing_jobs', 'count_completed_jobs', 'count_cancelled_jobs', 'count_total_users', 'cout_user_emp', 'cout_user_free', 'count_Skill', 'ongoing_jobs_5', 'user_by_role_freelancer', 'user_by_role_employer', 'count_Review'));
    }


    public function notifications()
    {
        // dd('here');
        if (Auth::user()) {
            $notifications = Auth::user()->notifications;
            // dd('here');
            // foreach($notifications as $notification) {
            //     $notification->markAsRead();
            // }
            return view('back-end.includes.notifications', compact('notifications'));
        }
    }
    public function markNotification(Request $request)
    {
        Auth::user()
            ->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
                return $query->where('id', $request->input('id'));
            })
            ->markAsRead();

        return redirect('/dashboard/notifications');
    }
    public function manageRoles()
    {
        $roles = DB::table('roles')->get();
        return view('back-end.admin.roles.manage-roles', compact('roles'));
    }

    public function jobadminDashBoard()
    {

        // dd(Auth::user());
        $total_jobs = Job::get();
        $ongoing_jobs = Job::where('status', 'posted')->get();
        $completed_jobs = Job::where('status', 'completed')->get();
        $cancelled_jobs = Job::where('status', 'cancelled')->get();

        $ongoing_jobs_5 = Job::where('status', 'posted')->limit(10)->get();
        $count_total_jobs = count($total_jobs);
        $count_ongoing_jobs = count($ongoing_jobs);
        $count_completed_jobs = count($completed_jobs);
        $count_cancelled_jobs = count($cancelled_jobs);

        return view('back-end.job-admin.dashboard', compact('count_total_jobs', 'count_ongoing_jobs', 'count_completed_jobs', 'count_cancelled_jobs', 'ongoing_jobs_5'));
    }

    public function pageadminDashBoard()
    {
        // dd(Auth::user());
        $pages = Page::getPages();
        return view('back-end.page-admin.dashboard', compact('pages'));
    }
}
