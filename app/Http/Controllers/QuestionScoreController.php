<?php

namespace App\Http\Controllers;

use Auth;
use App\Job;
use App\User;
use App\Helper;
use App\Profile;
use App\BidScore;
use App\Proposal;
use App\BidQuestion;

use App\MinutesOfMeeting;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;

class QuestionScoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    }
    public function show($id)
    {
        $MinutesOfMeeting = MinutesOfMeeting::find($id);
        $attachments = !empty($MinutesOfMeeting->attachments) ? unserialize($MinutesOfMeeting->attachments) : '';

        return view('back-end.employer.jobs.MinutesOfMeetingShow', compact('MinutesOfMeeting', 'attachments'));
    }

    public function list($id)
    {
        $userId = Auth::id();
        $MinutesOfMeetings = MinutesOfMeeting::where('job_id', $id)->get();
        $job_id = $id;
        return view('back-end.employer.jobs.ListMinutesOfMeeting', compact('MinutesOfMeetings', 'job_id'));
    }
    public function createScore($id, $jobID)
    {
        $userId = Auth::id();
        $job = Job::find($jobID);
        $TechnicalQuestion = '';
        $FinancialQuestion = '';
        $proposal = Proposal::find($id);
        $accepted_proposal = array();
        $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)->first();

        $BidQuestions = BidQuestion::where('job_id', $jobID)->get();
        $TechnicalQuestions = BidQuestion::where('job_id', $jobID)->where('type', 0)->get();
        $FinancialQuestions = BidQuestion::where('job_id', $jobID)->where('type', 1)->get();

        foreach ($TechnicalQuestions as $key => $value) {
            # code...
            $BidScores = BidScore::where('proposal_id', $proposal->id)->where('question_id', $value->id)->get();
            $user_score = 0;
            foreach ($BidScores as $bid_score) {
                if ($bid_score->member_id == $userId) {
                    $user_score = $bid_score->score;
                }
            }
            $BidScoreSingle = $user_score;
            $TechnicalQuestions[$key]->BidScore = $BidScoreSingle;
        }

        foreach ($FinancialQuestions as $key => $value) {
            # code...
            $BidScores = BidScore::where('proposal_id', $proposal->id)->where('question_id', $value->id)->get();
            $user_score = 0;
            foreach ($BidScores as $bid_score) {
                if ($bid_score->member_id == $userId) {
                    $user_score = $bid_score->score;
                }
            }
            $BidScoreSingle = $user_score;
            $FinancialQuestions[$key]->BidScore = $BidScoreSingle;
        }

        $BidScore = BidScore::where('proposal_id', $proposal->id)->count();

        return view('back-end.employer.jobs.AddQuestionScore', compact('BidQuestions', 'proposal', 'job', 'accepted_proposal', 'TechnicalQuestions', 'FinancialQuestions', 'BidScore', 'FinancialQuestion', 'TechnicalQuestion'));
    }

    public function create($id)
    {
    }


    public function store(Request $request)
    {
        $userId = Auth::id();
        foreach ($_POST['answer'] as $key => $value) {
            # code...
            $BidScore = new BidScore([
                'question_id' => $key,
                'score' => $value,
                'member_id' => $userId,
                'proposal_id' => $request->get('proposal_id'),
            ]);
            $BidScore->save();
        }

        return redirect()->back()->withSuccess('Saved!');
    }


    public function update(Request $request, $id)
    {
        $userId = Auth::id();
        foreach ($_POST['answer'] as $key => $value) {
            # code...
            // $BidScore = BidScore::where('proposal_id', $request->get('proposal_id'))->where('question_id', $key)->first();
            $BidScore = BidScore::updateOrCreate(
                ['proposal_id' => $request->get('proposal_id'), 'question_id' => $key, 'member_id' => $userId],
                ['score' => $value]
            );
            // if ($BidScore) {
            //     $BidScore->score = $value;
            //     $BidScore->save();
            // }
        }

        return redirect()->back()->withSuccess('Updated!');
    }

  

    public function destroy($id)
    {

        $MinutesOfMeeting = MinutesOfMeeting::find($id);
        $MinutesOfMeeting->delete();

        return \Redirect::back()->withErrors(['success', 'Deleted']);
    }
}
