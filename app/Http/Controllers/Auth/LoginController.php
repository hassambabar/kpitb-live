<?php
/**
 * Class LoginController.
 *
 * 
 *
 * 
 * 
 * 
 * 
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;
use App\User;
use Schema;
use Session;
use View;
use Socialite;
use Exception;
use DB;
use App\Location;
use App\Profile;
use App\Package;
use App\Invoice;
use App\Helper;
use App\Job;
use Carbon\Carbon;
/**
 * Class LoginController
 *
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @param string $request request attributes
     *
     * @return authenticated users
     */
    protected function authenticated(Request $request, $user)
    {
        if (Schema::hasTable('users')) {
            // if (!empty($user->verification_code)) {
            //     Session::flash('error', trans('lang.verification_code_not_verified'));
            //     Auth::logout();
            //     return Redirect::to('/');
            // } else {
                $user_id = Auth::user()->id;
                $user_role_type = User::getUserRoleType($user_id);
                if (empty($user_role_type)) {
                    Session::flash('error', trans('Unfortunately you have been logged out due to in-sufficient role privileges for you account, For Further details contact to administrator'));
                    Auth::logout();
                    return Redirect::to('/');
                }
                $user_role = $user_role_type->role_type;
                 $sessionUrl = session('link');
                 if(!empty($sessionUrl)){
                    return redirect(session('link'));
                 }
                if ($user_role === 'freelancer') {
                    return Redirect::to('freelancer/dashboard');
                } 
                elseif ($user_role === 'employer') {
                    return Redirect::to('employer/dashboard');
                } 
                elseif ($user_role === 'admin') {
                    return Redirect::to('admin/dashboard');
                }
                elseif ($user_role === 'job-administrator') {
                    return Redirect::to('job-administrator/dashboard');
                }
                elseif ($user_role === 'page-management') {
                    return Redirect::to('page-management/dashboard');
                }else {
                    return Redirect::to(url()->previous());
                }
            // }
        }
    }

    public function showLoginForm()
    {
        session(['link' => url()->previous()]);
        return view('auth.login');
    }

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (Schema::hasTable('users')) {
            $this->middleware('guest')->except('logout');
        }
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        $user = Socialite::driver('google')->user();
        
        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            
            // create a new user
            $newUser                  = new User;
            //echo "<pre>";print_r($user);exit();
            $fullname = explode(" ", $user->name);
            $newUser->first_name            = $fullname['0'];
            $newUser->last_name            = $fullname['1'];
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->save();

            $user_data = $newUser;
            $user_data = json_decode($user_data);
            $user_id = $user_data->id;
            
            $profile = new Profile();
            $profile->user()->associate($user_id);
            if (!empty($request['employees'])) {
                $profile->no_of_employees = intval($request['employees']);
            }
            if (!empty($request['department_name'])) {
                $department = Department::find($request['department_name']);
                $profile->department()->associate($department);
            }
            $profile->save();
            DB::table('model_has_roles')->insert(
                [
                    [
                        'role_id' => '3',
                        'model_id' => $user_id,
                        'model_type' => 'App\User',
                    ],
                ]
                );
            $role_id = Helper::getRoleByUserID($user_id);
            $package = Package::select('id', 'title', 'cost')->where('role_id', $role_id)->where('trial', 1)->get()->first();
            $trial_invoice = Invoice::select('id')->where('type', 'trial')->get()->first();
            
            if (!empty($package) && !empty($trial_invoice)) {
                DB::table('items')->insert(
                    [
                        'invoice_id' => $trial_invoice->id, 'product_id' => $package->id, 'subscriber' => $user_id,
                        'item_name' => $package->title, 'item_price' => $package->cost, 'item_qty' => 1,
                        "created_at" => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()
                    ]
                );
            }
                
            auth()->login($newUser, true);
        }
        return redirect()->to('/');
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

}
