<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\CommentMeeting;
   
class CommentMeetingController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$request->validate([
            'body'=>'required',
        ]);
   
        $input = $request->all();
        $input['user_id'] = auth()->user()->id;
    
        CommentMeeting::create($input);
   
        return back();
    }
}