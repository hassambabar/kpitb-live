<?php
/**
 
 */

namespace App\Http\Controllers;

use App\Location;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use View;
use App\Helper;

/**
 * Class LanguageController
 *
 */
class DistrictController extends Controller
{
    /**
     * Defining scope of variable
     *
     * @access public
     * @var    array $language
     */
    protected $District;

    /**
     * Create a new controller instance.
     *
     * @param instance $language instance
     *
     * @return void
     */
    public function __construct(District $District)
    {
        $this->District = $District;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $locations = Location::All();
            $district = $this->District::where('district_name', 'like', '%' . $keyword . '%')->paginate(7)->setPath('');
            $pagination = $district->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $locations = Location::All();
            $district = $this->District->paginate(10);
        }
        
        if (file_exists(resource_path('views/extend/back-end/admin/District/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.District.index',
                compact('district','locations')
            );
        } else {
            return View::make(
                'back-end.admin.District.index',
                compact('district','locations')
            );
        }
    }

    /**
     * Store a newly created languages
     *
     * @param string $request string
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'district_name' => 'required',
            ]
        );

        $loc_id = $request->input('loc_id');
        $district_name = $request->input('district_name');
        $insArr = array();
        $insArr['loc_id'] = $loc_id;
        $insArr['district_name'] = $district_name;
        $this->District->savedistrict($insArr);
        Session::flash('message', 'District is added successfully');
        return Redirect::back();
    }

    /**
     * Edit Languages.
     *
     * @param int $id integer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (!empty($id)) {

            $district = $this->District::find($id);
            $locations = Location::All();
            
            if (!empty($district)) {
                
                if (file_exists(resource_path('views/extend/back-end/admin/District/edit.blade.php'))) {
                    return View::make(
                        'extend.back-end.admin.District.edit', compact('id', 'district','locations')
                    );
                } else {
                    return View::make(
                        'back-end.admin.District.edit', compact('id', 'district','locations')
                    );
                }
                Session::flash('message', trans('lang.lang_updated'));
                return Redirect::to('admin/district');
            }
        }
    }

    /**
     * Update language.
     *
     * @param string $request string
     * @param int    $id      integer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'district_name' => 'required',
            ]
        );
        $loc_id = $request->input('loc_id');
        $district_name = $request->input('district_name');
        $insArr = array();
        $insArr['loc_id'] = $loc_id;
        $insArr['district_name'] = $district_name;
        $this->District->updatedistrict($insArr, $id);
        Session::flash('message', 'District is updated successfully');
        return Redirect::to('admin/district');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $id = $request['id'];
        if (!empty($id)) {
            $this->District::where('id', $id)->delete();
            $json['type'] = 'success';
            $json['message'] = 'District is deleted successfully';
            return $json;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $checked = $request['ids'];
        foreach ($checked as $id) {
            $this->District::where("id", $id)->delete();
        }
        if (!empty($checked)) {
            $json['type'] = 'success';
            $json['message'] = 'District is deleted successfully';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }
}
