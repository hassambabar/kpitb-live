<?php
/**
 * Class ReviewController
 *
 
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\ReviewOptions;
use View;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Helper;
use App\Category;
use App\User;

/**
 * Class ReviewController
 *
 */
class ReviewController extends Controller
{
    /**
     * Defining scope of the variable
     *
     * @access public
     * @var    array $review
     */
    protected $review;

    /**
     * Create a new controller instance.
     *
     * @param instance $review         review instance
     * @param instance $review_options review options instance
     *
     * @return void
     */
    public function __construct(Review $review, ReviewOptions $review_options)
    {
        $this->review = $review;
        $this->review_options = $review_options;
    }

    /**
     * Display a listing of the resource.
     *
     * @param mixed $request Request Attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $review_options = $this->review_options::paginate(10);
        
        $categories = Category::where('parent','!=',0)->get();
        $roles = DB::table('roles')->whereBetween('id', [2, 3])->get();
        if (file_exists(resource_path('views/extend/back-end/admin/review-options/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.review-options.index',
                compact('review_options','categories', 'roles')
            );
        } else {
            return View::make(
                'back-end.admin.review-options.index',
                compact('review_options','categories', 'roles')
            );
        }
    }
    public function userreview()
    {

        
        $Review = Review::paginate(10);
        if (file_exists(resource_path('views/extend/back-end/admin/review/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.review.index',
                compact('Review')
            );
        } else {
            return View::make(
                'back-end.admin.review.index',
                compact('Review')
            );
        }
    }
    public function userreviewsearch(Request $request)
        {
            $Review = Review::where('project_type', 'job');
            if(!empty($_POST['from'])){
                $user_from = User::where('email',$_POST['from'])->first();
                if(!empty( $user_from)){
                    $Review = $Review->where('user_id', $user_from->id);
                }
            }
            if(!empty($_POST['to'])){
                $user_to = User::where('email',$_POST['to'])->first();
                if(!empty( $user_to)){
                    $Review = $Review->where('receiver_id', $user_to->id);
                }
            }
            $Review = $Review->paginate(10);
            //dd($Review);
        if (file_exists(resource_path('views/extend/back-end/admin/review/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.review.index',
                compact('Review')
            );
        } else {
            return View::make(
                'back-end.admin.review.index',
                compact('Review')
            );
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param string $request string
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        if (!empty($request)) {
            $this->validate(
                $request, [
                    'review_option_title' => 'required',
                    'role_id' => 'required'
                ]
            );
            $this->review_options->saveReviewOptions($request);
        }
        Session::flash('message', trans('lang.save_review_option'));
        return Redirect::back();
    }

    /**
     * Edit review options.
     *
     * @param int $id integer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!empty($id)) {
            $review_options = $this->review_options::find($id);
            $categories = Category::all();
            $roles = DB::table('roles')->whereBetween('id', [2, 3])->get();
            if (!empty($review_options)) {
                if (file_exists(resource_path('views/extend/back-end/admin/review-options/edit.blade.php'))) {
                    return View::make(
                        'extend.back-end.admin.review-options.edit', compact('id', 'review_options','categories', 'roles')
                    );
                } else {
                    return View::make(
                        'back-end.admin.review-options.edit', compact('id', 'review_options','categories', 'roles')
                    );
                }
                Session::flash('message', trans('lang.review_options_updated'));
                return Redirect::to('admin/review_options');
            }
        }
    }

    /**
     * Update review options.
     *
     * @param string $request string
     * @param int    $id      integer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
            'review_option_title' => 'required',
            'role_id' => 'required'
            ]
        );
        $this->review_options->updateReviewOptions($request, $id);
        Session::flash('message', trans('lang.review_option_updated'));
        return Redirect::to('admin/review-options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $id = $request['id'];
        if (!empty($id)) {
            $this->review_options::where('id', $id)->delete();
            $json['type'] = 'success';
            return $json;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $checked = $request['ids'];
        foreach ($checked as $id) {
            $this->review_options::where("id", $id)->delete();
        }
        if (!empty($checked)) {
            $json['type'] = 'success';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }
}
