<?php

/**
 * Class PageController
 *
 
 */

namespace App\Http\Controllers;

use App\Skilltypes;
use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use App\User;
use App\Helper;
use App\SiteManagement;
use Illuminate\Support\Facades\Schema;

/**
 * Class PageController
 *
 */
class SkillbytypeController extends Controller
{
    /**
     * Defining scope of the variable
     *
     * @access public
     * @var    array $page
     */
    protected $Skilltypes;

    /**
     * Create a new controller instance.
     *
     * @param instance $page instance
     *
     * @return void
     */
    public function __construct(Skilltypes $Skilltypes)
    {
        $this->Skilltypes = $Skilltypes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skilltypes = $this->Skilltypes::getSkilltypes();

        if (file_exists(resource_path('views/extend/back-end/admin/pages/skilltype.blade.php'))) {
            return View::make('extend.back-end.admin.pages.skilltype', compact('skillstypes'));
        } else {
            return View::make(
                'back-end.admin.pages.skillstype',
                compact('skilltypes')
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $parent_page = $this->page->getParentPages();
        $parent_page = DB::table('pages')->select('id', 'title')->where('relation_type', '=', 0)->get()->toArray();
        $page_created = trans('lang.page_created');
        $sections = Helper::getPageSections();
        $app_style_list = Helper::getAppStyleList();
        $slider_style_list = Helper::getSliderStyleList();
        $access_type = Helper::getAccessType() == 'services' ? 'service' : Helper::getAccessType();
        //return view('back-end.admin.pages.createskilltype');
        if (file_exists(resource_path('views/extend/back-end/admin/pages/createskilltype.blade.php'))) {
            return View::make(
                'extend.back-end.admin.pages.createskilltype',
                compact(
                    'parent_page',
                    'page_created',
                    'sections',
                    'app_style_list',
                    'slider_style_list',
                    'access_type'
                )
            );
        } else {
            return View::make(
                'back-end.admin.pages.createskilltype',
                compact(
                    'parent_page',
                    'page_created',
                    'sections',
                    'app_style_list',
                    'slider_style_list',
                    'access_type'
                )
            );
        }
    }

    public function edit($id)
    {
            $skills = $this->Skilltypes::find($id);

            return View::make('back-end.admin.pages.editskillstype',compact('id', 'skills'));


    }

     public function store(Request $request)
     {
       	$skill_name = $request->input('skill_title');
        $skill_type = $request->input('skill_type');


           $insArr = array();
           $insArr['skill_name'] = $skill_name;
           $insArr['skill_type'] = $skill_type;


           $this->Skilltypes->saveSkills($insArr);


         return redirect('admin/skillsbytype');


     }

     public function update(Request $request)
     {
       $id = $request->input('id');
        $skill_name = $request->input('skill_title');
        $skill_type = $request->input('skill_type');

           $insArr = array();
           $insArr['skill_name'] = $skill_name;
           $insArr['skill_type'] = $skill_type;


           $this->Skilltypes->updateSkills($insArr,$id);


         return redirect('admin/skillsbytype');
     }

     public function destroy(Request $request)
     {
         $server = Helper::kpitbIsDemoSiteAjax();
         if (!empty($server)) {
             $json['type'] = 'error';
             $json['message'] = $server->getData()->message;
             return $json;
         }
         $json = array();
         $id = $request['id'];
         if (!empty($id)) {
             $this->Skilltypes::where('id', $id)->delete();
             $json['type'] = 'success';
             $json['message'] = trans('lang.skill_deleted');
             return $json;
         } else {
             $json['type'] = 'error';
             $json['message'] = trans('lang.something_wrong');
             return $json;
         }
     }


}
