<?php
/**
 
 */

namespace App\Http\Controllers;

use View;
use Session;
use App\Helper;
use App\District;
use App\Location;
use App\BidCriteriaQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/**
 * Class LanguageController
 *
 */
class BidQuestionController extends Controller
{
    /**
     * Defining scope of variable
     *
     * @access public
     * @var    array $language
     */

    /**
     * Create a new controller instance.
     *
     * @param instance $language instance
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $BidCriteriaQuestion = BidCriteriaQuestion::where('question', 'like', '%' . $keyword . '%')->paginate(10)->setPath('');
            $pagination = $BidCriteriaQuestion->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $BidCriteriaQuestion = BidCriteriaQuestion::orderBy('question')->get();
        }
        // $BidCriteriaQuestion = BidCriteriaQuestion::orderBy('question')->get();
      //  return View::make('back-end.admin.District.index', compact('district','locations'));
        return view('back-end.admin.BidQuestions.index', compact('BidCriteriaQuestion'));

    }

    /**
     * Store a newly created languages
     *
     * @param string $request string
     *
     * @return \Illuminate\Http\Response
     */

     public function get_by_id($id){
       
        $BidCriteriaQuestion = BidCriteriaQuestion::find($id);
        return $BidCriteriaQuestion;
     }
     public function get_by_list_id(){
       
       
        $BidCriteriaQuestion = BidCriteriaQuestion::whereIn('id', $_POST['valmultiplebidquestions'])->get();
    //    echo "<pre>"; print_r($BidCriteriaQuestion); exit;
       $return = '';
       $next = 0;
        if($BidCriteriaQuestion->count() > 0){
            foreach ($BidCriteriaQuestion as $key => $value) {
                $return .= '<div id="fieldcode'.$next.'" name="field'.$next.'"><div class="form-group col-md-6  wt-formwithlabel"><input id="action_id" name="question_title[]" type="text" value="'.str_replace('"', '', $value->question).'" class="form-control"> </div><div class="form-group col-md-6 wt-formwithlabel row"> <span class="col-md-4"> <input id="action_name" name="question_score[]" type="number" placeholder="weight" class="form-control qs"></span> <span class="col-md-4">';
                if($value->question_type == 0){
                    $return .= "<select name='question_type[]'><option selected='selected' value='0'>Technical</option></select>";

                }
                elseif($value->question_type == 2){
                    $return .= "<select name='question_type[]'><option value='0'>Technical</option><option value='1'>Financial</option></select>";

                }
                else{
                    $return .= "<select name='question_type[]'><option value='1' selected='selected'>Financial</option></select>";

                }
                $return .= '</span><span class="col-md-4"><button   data-id='.$next.' id="remove-code'.$next.'" class="btn btn-danger remove-me-code" >Remove</button></span</div>';
                $return .= '</div><br>';

                $next++;
            }
         
        }
        return $return;
     }
     
    public function store(Request $request)
    {
       // echo "<pre>"; print_r($_POST); exit;
        $request->validate([
            'question' => 'required',
            'question_type' => 'required',
        ]);

        $BidCriteriaQuestion = new BidCriteriaQuestion([
            'question' => $request->get('question'),
            'question_type' => $request->get('question_type')
        ]);
       $BidCriteriaQuestion->save();
       Session::flash('message', 'Bid Criteria Question is added successfully');
        return Redirect::back();
    }

    /**
     * Edit Languages.
     *
     * @param int $id integer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (!empty($id)) {

            $BidCriteriaQuestion = BidCriteriaQuestion::find($id);            
            if (!empty($BidCriteriaQuestion)) {
                return view('back-end.admin.BidQuestions.edit', compact('BidCriteriaQuestion'));
               
            }
        }
    }

    /**
     * Update language.
     *
     * @param string $request string
     * @param int    $id      integer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => 'required',
            'question_type' => 'required',
        ]);

        $BidCriteriaQuestion = BidCriteriaQuestion::find($id);
        $BidCriteriaQuestion->question =  $request->get('question');
        $BidCriteriaQuestion->question_type =  $request->get('question_type');
        $BidCriteriaQuestion->save();
        Session::flash('message', 'Bid Criteria Question Updated');
        return redirect('/admin/bid-questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $BidCriteriaQuestion = BidCriteriaQuestion::find($id);

        if (!empty($BidCriteriaQuestion)) {
          $BidCriteriaQuestion->delete();
          Session::flash('message', 'Bid Criteria Question Deleted');
           return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $checked = $request['ids'];
        foreach ($checked as $id) {
            BidCriteriaQuestion::where("id", $id)->delete();
        }
        if (!empty($checked)) {
            $json['type'] = 'success';
            $json['message'] = 'Bid Criteria is deleted successfully';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }
}
