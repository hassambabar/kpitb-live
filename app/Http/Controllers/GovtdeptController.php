<?php
/**
 
 */

namespace App\Http\Controllers;

use App\GovtDepts;
use App\OrganizationType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use View;
use App\Helper;
use DB;

/**
 * Class LanguageController
 *
 */
class GovtdeptController extends Controller
{
    /**
     * Defining scope of variable
     *
     * @access public
     * @var    array $language
     */
    protected $GovtDepts;

    /**
     * Create a new controller instance.
     *
     * @param instance $language instance
     *
     * @return void
     */
    public function __construct(GovtDepts $GovtDepts)
    {
        $this->GovtDepts = $GovtDepts;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // govt controller
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $orgs = OrganizationType::getAllOrgs();
            $GovtDepts = $this->GovtDepts::where('department_name', 'like', '%' . $keyword . '%')->paginate(7)->setPath('');
            $pagination = $GovtDepts->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $orgs = OrganizationType::getAllOrgs();
            $GovtDepts = $this->GovtDepts->paginate(10);
        }
        
        if (file_exists(resource_path('views/extend/back-end/admin/GovtDepts/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.GovtDepts.index',
                compact('GovtDepts','orgs')
            );
        } else {
            return View::make(
                'back-end.admin.GovtDepts.index',
                compact('GovtDepts','orgs')
            );
        }
    }

    /**
     * Store a newly created languages
     *
     * @param string $request string
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'department_name' => 'required',
            ]
        );

        $org_type_id = $request->input('org_type_id');
        $department_name = $request->input('department_name');
        $insArr = array();
        $insArr['org_type_id'] = $org_type_id;
        $insArr['department_name'] = $department_name;

        if (DB::table('govt_departments')->where('department_name', $insArr['department_name'])->exists()) {
            
            Session::flash('error', 'Department already found');
            
        }
        else{
            $this->GovtDepts->saveGovtDepts($insArr);
            Session::flash('message', 'Department is added successfully');
        }
        
        return Redirect::back();
    }
    // ...

    /**
     * Edit Department.
     *
     * @param int $id integer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!empty($id)) {
            $GovtDept = $this->GovtDepts::find($id);
            $orgs = OrganizationType::getAllOrgs();
            if (!empty($GovtDept)) {
                if (file_exists(resource_path('views/extend/back-end/admin/GovtDepts/edit.blade.php'))) {
                    return View::make(
                        'extend.back-end.admin.GovtDepts.edit', compact('id', 'GovtDept','orgs')
                    );
                } else {
                    return View::make(
                        'back-end.admin.GovtDepts.edit', compact('id', 'GovtDept','orgs')
                    );
                }
                Session::flash('message', trans('lang.lang_updated'));
                return Redirect::to('admin/govtdept');
            }
        }
    }

    /**
     * Update language.
     *
     * @param string $request string
     * @param int    $id      integer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'department_name' => 'required',
            ]
        );
        $org_type_id = $request->input('org_type_id');
        $department_name = $request->input('department_name');
        $insArr = array();
        $insArr['org_type_id'] = $org_type_id;
        $insArr['department_name'] = $department_name;
        $this->GovtDepts->updateGovtDepts($insArr, $id);
        Session::flash('message', 'Department is updated successfully');
        return Redirect::to('admin/govtdept');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $id = $request['id'];
        if (!empty($id)) {
            $this->GovtDepts::where('id', $id)->delete();
            $json['type'] = 'success';
            $json['message'] = 'Department is deleted successfully';
            return $json;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $checked = $request['ids'];
        foreach ($checked as $id) {
            $this->GovtDepts::where("id", $id)->delete();
        }
        if (!empty($checked)) {
            $json['type'] = 'success';
            $json['message'] = 'Department is deleted successfully';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }
}
