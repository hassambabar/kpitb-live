<?php
/**
 
 */

namespace App\Http\Controllers;

use App\OrganizationType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;
use View;
use App\Helper;
use DB;

/**
 * Class LanguageController
 *
 */
class OrganizationtypeController extends Controller
{
    /**
     * Defining scope of variable
     *
     * @access public
     * @var    array $language
     */
    protected $OrganizationType;

    /**
     * Create a new controller instance.
     *
     * @param instance $language instance
     *
     * @return void
     */
    public function __construct(OrganizationType $OrganizationType)
    {
        $this->OrganizationType = $OrganizationType;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $orgs = $this->OrganizationType::where('organization_name', 'like', '%' . $keyword . '%')->paginate(7)->setPath('');
            $pagination = $orgs->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $orgs = $this->OrganizationType->paginate(10);
        }
        if (file_exists(resource_path('views/extend/back-end/admin/Organizationtype/index.blade.php'))) {
            return View::make(
                'extend.back-end.admin.Organizationtype.index',
                compact('orgs')
            );
        } else {
            return View::make(
                'back-end.admin.Organizationtype.index',
                compact('orgs')
            );
        }
    }

    /**
     * Store a newly created languages
     *
     * @param string $request string
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'organization_name' => 'required',
            ]
        );

        $organization_name = $request->input('organization_name');
        $insArr = array();
        $insArr['organization_name'] = $organization_name;
        
        if (DB::table('organization_type')->where('organization_name', $insArr['organization_name'])->exists()) {
            
            Session::flash('error', 'Organization Type already found');
            
        }
        else{
            $this->OrganizationType->saveOrganizationtype($insArr);
            Session::flash('message', 'Organization Type is added successfully');
        }
        
        return Redirect::back();
    }

    /**
     * Edit Organization Type.
     *
     * @param int $id integer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!empty($id)) {
            $orgs = $this->OrganizationType::find($id);
            if (!empty($orgs)) {
                if (file_exists(resource_path('views/extend/back-end/admin/organizationtype/edit.blade.php'))) {
                    return View::make(
                        'extend.back-end.admin.organizationtype.edit', compact('id', 'orgs')
                    );
                } else {
                    return View::make(
                        'back-end.admin.organizationtype.edit', compact('id', 'orgs')
                    );
                }
                Session::flash('message', trans('lang.lang_updated'));
                return Redirect::to('admin/organizationtype');
            }
        }
    }

    /**
     * Update language.
     *
     * @param string $request string
     * @param int    $id      integer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server_verification = Helper::kpitbIsDemoSite();
        if (!empty($server_verification)) {
            Session::flash('error', $server_verification);
            return Redirect::back();
        }
        $this->validate(
            $request, [
                'organization_name' => 'required',
            ]
        );
        $organization_name = $request->input('organization_name');
        $insArr = array();
        $insArr['organization_name'] = $organization_name;
        $this->OrganizationType->updateOrganizationType($insArr, $id);
        Session::flash('message', 'Organization Type is updated successfully');
        return Redirect::to('admin/organizationtype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $id = $request['id'];
        if (!empty($id)) {
            $this->OrganizationType::where('id', $id)->delete();
            $json['type'] = 'success';
            $json['message'] = 'Organization Type is deleted successfully';
            return $json;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSelected(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $checked = $request['ids'];
        foreach ($checked as $id) {
            $this->OrganizationType::where("id", $id)->delete();
        }
        if (!empty($checked)) {
            $json['type'] = 'success';
            $json['message'] = 'Organization Type is deleted successfully';
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }
}
