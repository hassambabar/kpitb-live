<?php


namespace App\Http\Controllers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationMailable;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Auth;
use DB;
use Session;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;
use App\GovtDepts;
use App\District;

/**
 * Class PublicController
 *
 */
class AjaxController extends Controller
{

    public function getAjaxGovtDepts(Request $request)
    {
        $orgId = $request->input('orgId');
        $govtDepts = GovtDepts::getAllDeptsDropDown($orgId);
        //echo "<pre>";print_r($govtDepts);exit();
        echo view('partials/govtDepts')->with('govtDepts',$govtDepts);
    }

    public function getAjaxDistrics(Request $request)
    {
        $locId = $request->input('locId');
        $dists = District::getAllDistDropDown($locId);
        //echo "<pre>";print_r($govtDepts);exit();
        echo view('partials/dists')->with('dists',$dists);
    }

}
