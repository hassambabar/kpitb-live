<?php

/**
 * Class JobController.
 *
 
 */

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Job;
use Session;
use App\Item;
use App\User;
use App\Skill;
use App\Helper;
use App\Package;
use App\Profile;
use App\Category;
use App\District;
use App\Language;
use App\Location;
use App\Proposal;
use Carbon\Carbon;
use App\BidQuestion;
use ValidateRequests;
use App\EmailTemplate;
use App\SiteManagement;
use Illuminate\Support\Arr;
use App\BidCriteriaQuestion;
use Illuminate\Http\Request;
use App\Rules\MileStoneValue;
use App\Rules\ExpireRangeCheck;
use App\Mail\AdminEmailMailable;
use Spatie\Permission\Models\Role;
use App\Mail\EmployerEmailMailable;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Notifications\JobUnpublished;
use App\Notifications\JobPublished;

/**
 * Class JobController
 *
 */
class JobController extends Controller
{
    /**
     * Defining scope of the variable
     *
     * @access protected
     * @var    array $job
     */
    protected $job;

    /**
     * Defining scope of the variable
     *
     * @access protected
     * @var    array $job
     */
    // public $email_settings;

    /**
     * Create a new controller instance.
     *
     * @param instance $job instance
     *
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * Post Job Form.
     *
     * @return post jobs page
     */
    public function postJob()
    {
        $weekdays = [
            trans('lang.weekdays.mon'),
            trans('lang.weekdays.tue'),
            trans('lang.weekdays.wed'),
            trans('lang.weekdays.thu'),
            trans('lang.weekdays.fri'),
            trans('lang.weekdays.sat'),
            trans('lang.weekdays.sun'),
        ];

        $months = [
            trans('lang.months.january'),
            trans('lang.months.february'),
            trans('lang.months.march'),
            trans('lang.months.april'),
            trans('lang.months.may'),
            trans('lang.months.june'),
            trans('lang.months.july'),
            trans('lang.months.august'),
            trans('lang.months.september'),
            trans('lang.months.october'),
            trans('lang.months.november'),
            trans('lang.months.december'),
        ];
        $languages = Language::pluck('title', 'id');
        $locations = Location::pluck('title', 'id');
        $english_levels = Helper::getEnglishLevelList();
        $project_levels = Helper::getProjectLevel();
        $project_budget = Helper::getProjectbudget();

        $BidCriteriaQuestion = BidCriteriaQuestion::pluck('question', 'id');
        $service_type = Helper::getProjectTypeService();
        $selection_procurement_system = Helper::getSelectionProcurementSystem();

        $getSelectionProcurementSystemSingleSource = Helper::getSelectionProcurementSystemSingleSource();


        $job_duration = Helper::getJobDurationList();
        $freelancer_level = Helper::getFreelancerLevelList();
        $skills = Skill::pluck('title', 'id');
        $categories = Category::where('parent', '=', 0)->pluck('title', 'id');
        $categories_child = Category::where('parent', '!=', 0)->pluck('title', 'id');

        $role_id =  Helper::getRoleByUserID(Auth::user()->id);
        $package_options = Package::select('options')->where('role_id', $role_id)->first();
        $options = !empty($package_options) ? unserialize($package_options['options']) : array();
        if (file_exists(resource_path('views/extend/back-end/employer/jobs/create.blade.php'))) {
            return view(
                'extend.back-end.employer.jobs.create',
                compact(
                    'english_levels',
                    'languages',
                    'project_levels',
                    'job_duration',
                    'freelancer_level',
                    'skills',
                    'categories',
                    'categories_child',
                    'locations',
                    'options',
                    'weekdays',
                    'months',
                    'project_budget',
                    'service_type',
                    'selection_procurement_system',
                    'getSelectionProcurementSystemSingleSource',
                    'BidCriteriaQuestion'
                )
            );
        } else {
            return view(
                'back-end.employer.jobs.create',
                compact(
                    'english_levels',
                    'languages',
                    'project_levels',
                    'job_duration',
                    'freelancer_level',
                    'skills',
                    'categories',
                    'categories_child',
                    'locations',
                    'options',
                    'weekdays',
                    'months',
                    'project_budget',
                    'service_type',
                    'selection_procurement_system',
                    'getSelectionProcurementSystemSingleSource',
                    'BidCriteriaQuestion'
                )
            );
        }
    }

    /**
     * Manage Jobs.
     *
     * @return manage jobs page
     */
    public function index()
    {
        $job_details = $this->job->latest()->where('user_id', Auth::user()->id)->paginate(5);
        $currency   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
        if (file_exists(resource_path('views/extend/back-end/employer/jobs/index.blade.php'))) {
            return view('extend.back-end.employer.jobs.index', compact('job_details', 'symbol'));
        } else {
            return view('back-end.employer.jobs.index', compact('job_details', 'symbol'));
        }
    }

    /**
     * Job Edit Form.
     *
     * @param integer $job_slug Job Slug
     *
     * @return show job edit page
     */
    public function edit($job_slug)
    {
        if (!empty($job_slug)) {
            $job = Job::where('slug', $job_slug)->first();

            $jobCat = $job->categories; 
            $jobCat_parent = Category::where('id', $jobCat)->first();
            $categories_parent = Category::where('id', $jobCat_parent->parent)->first();
            // echo "<pre>"; print_r( $jobCat[0]->parent); exit;

            // $categories_parent = Category::where('id', $jobCat[0]->parent)->first();
            // echo "<pre>"; print_r( $categories_parent); exit;

            $District = District::where('loc_id', $job->location_id)->pluck('district_name', 'id');



            $json = array();
            $languages = Language::pluck('title', 'id');
            $locations = Location::pluck('title', 'id');

            $project_levels = Helper::getProjectLevel();
            $project_budget = Helper::getProjectbudget();

            $service_type = Helper::getProjectTypeService();
            $selection_procurement_system = Helper::getSelectionProcurementSystem();
            $getSelectionProcurementSystemSingleSource = Helper::getSelectionProcurementSystemSingleSource();
            $job_duration = Helper::getJobDurationList();
            $categories_child = Category::where('parent', '!=', 0)->pluck('title', 'id');


            $BidQuestion = BidQuestion::where('job_id', $job->id)->get();

            $skills = Skill::pluck('title', 'id');
            $categories = Category::where('parent', '=', 0)->pluck('title', 'id');

            $project_levels = Helper::getProjectLevel();
            $english_levels = Helper::getEnglishLevelList();
            $job_duration = Helper::getJobDurationList();
            $freelancer_level_list = Helper::getFreelancerLevelList();
            $attachments = !empty($job->attachments) ? unserialize($job->attachments) : '';
            $weekdays = [
                trans('lang.weekdays.mon'),
                trans('lang.weekdays.tue'),
                trans('lang.weekdays.wed'),
                trans('lang.weekdays.thu'),
                trans('lang.weekdays.fri'),
                trans('lang.weekdays.sat'),
                trans('lang.weekdays.sun'),
            ];
            $months = [
                trans('lang.months.january'),
                trans('lang.months.february'),
                trans('lang.months.march'),
                trans('lang.months.april'),
                trans('lang.months.may'),
                trans('lang.months.june'),
                trans('lang.months.july'),
                trans('lang.months.august'),
                trans('lang.months.september'),
                trans('lang.months.october'),
                trans('lang.months.november'),
                trans('lang.months.december'),
            ];
            if (!empty($job)) {
                if (file_exists(resource_path('views/extend/back-end/employer/jobs/edit.blade.php'))) {
                    return View(
                        'extend.back-end.employer.jobs.edit',
                        compact(
                            'job',
                            'project_levels',
                            'english_levels',
                            'job_duration',
                            'freelancer_level_list',
                            'languages',
                            'categories',
                            'skills',
                            'locations',
                            'attachments',
                            'weekdays',
                            'months',
                            'project_budget',
                            'service_type',
                            'selection_procurement_system',
                            'getSelectionProcurementSystemSingleSource',
                            'BidQuestion',
                            'categories_child',
                            'categories_parent',
                            'District'
                        )
                    );
                } else {
                    return View(
                        'back-end.employer.jobs.edit',
                        compact(
                            'job',
                            'project_levels',
                            'english_levels',
                            'job_duration',
                            'freelancer_level_list',
                            'languages',
                            'categories',
                            'skills',
                            'locations',
                            'attachments',
                            'weekdays',
                            'months',
                            'project_budget',
                            'service_type',
                            'selection_procurement_system',
                            'getSelectionProcurementSystemSingleSource',
                            'BidQuestion',
                            'categories_child',
                            'categories_parent',
                            'District'
                        )
                    );
                }
            }
        }
    }

    /**
     * Get job attachment settings.
     *
     * @param integer $request $request->attributes
     *
     * @return show job single page
     */
    public function getAttachmentSettings(Request $request)
    {
        $json = array();
        if ($request['slug']) {
            $settings = Job::where('slug', $request['slug'])
                ->select('is_featured', 'show_attachments')->first();
            if (!empty($settings)) {
                $json['type'] = 'success';
                if ($settings->is_featured == 'true') {
                    $json['is_featured'] = 'true';
                }
                if ($settings->show_attachments == 'true') {
                    $json['show_attachments'] = 'true';
                }
            } else {
                $json['type'] = 'error';
            }
            return $json;
        }
    }

    /**
     * Upload image to temporary folder.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadTempImage(Request $request)
    {
        if (!empty($request['file'])) {
            $attachments = $request['file'];
            $path = 'uploads/jobs/temp/';
            return $this->job->uploadTempattachments($attachments, $path);
        }
    }

    // Get Child Category By Parent ID
    public function get_by_parent(Request $request)
    {

        $categories = Category::where('parent', '=', $request['parent_id'])->get();

        if (!$request->parent_id) {
            $html = '<option value="">Please Select Category</option>';
        } else {
            $html = '<option value="">Please Select Sub Category</option>';
            foreach ($categories as $category) {
                $html .= '<option value="' . $category->id . '">' . $category->title . '</option>';
            }
        }
        return response()->json(['html' => $html]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $json = array();
        $todayDate = date('Y-m-d');
        $cMonth = date('m');
        $cWeek = date('Y-m-d');
        $cYear = date('Y-m-d');
        //dd( $request);
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['message'] = $server->getData()->message;
            return $response;
        }
        if (Helper::getAccessType() == 'services') {
            $json['type'] = 'job_warning';
            return $json;
        }


        // dd($month);

        if (($_REQUEST['payment_system'] == 'milestone')) {
            $milesonetotal = 0;
            foreach ($_REQUEST['milestones_percentage'] as $key => $valueMilestone) {
                # code...
                $milesonetotal = $milesonetotal + $valueMilestone;
            }
            if ($milesonetotal > 100 || $milesonetotal < 100) {
                $request->validate([
                    'milestones_percentage' => new MileStoneValue,
                ]);
            }
        }

        if (($_REQUEST['job_duration'] == 'monthly')) {
            $start  = new Carbon($todayDate);
            $end    = new Carbon($_REQUEST['expiry_date']);
            $interval = date_diff($start, $end);
            if ($interval->m > 0) {
                $request->validate([
                    'exp_range_error' => new ExpireRangeCheck,
                ]);
            }
        }

        if (($_REQUEST['job_duration'] == 'weekly')) {
            $start  = new Carbon($todayDate);
            $end    = new Carbon($_REQUEST['expiry_date']);
            $interval = date_diff($start, $end);
            if ($interval->d > 7) {
                $request->validate([
                    'exp_range_error' => new ExpireRangeCheck,
                ]);
            }
        }

        if (($_REQUEST['job_duration'] == 'three_month')) {
            $start  = new Carbon($todayDate);
            $end    = new Carbon($_REQUEST['expiry_date']);
            $interval = date_diff($start, $end);
            if ($interval->m > 3) {
                $request->validate([
                    'exp_range_error' => new ExpireRangeCheck,
                ]);
            }
        }

        if (($_REQUEST['job_duration'] == 'six_month')) {
            $start  = new Carbon($todayDate);
            $end    = new Carbon($_REQUEST['expiry_date']);
            $interval = date_diff($start, $end);
            if ($interval->m > 6) {
                $request->validate([
                    'exp_range_error' => new ExpireRangeCheck,
                ]);
            }
        }

        // echo $milesonetotal;
        if (Auth::user()->user_verified == 1) {
            $this->validate(
                $request,
                [
                    'title' => 'required',
                    'project_levels'    => 'required',
                    'job_duration'    =>  'required',
                    // 'district'    => 'required',
                    'locations'    => 'required',
                    'project_cost'    => 'required',
                    'payment_system'    => 'required',
                    'description'    => 'required',
                    'categories'    => 'required',
                    'categories_parent'    => 'required',
                    // 'address' => 'required',
                    'service_type' => 'required',
                    'skills' => 'required',
                    'expiry_date' => 'required|date_format:Y-m-d|after:' . $todayDate,

                ]
            );

            if (Auth::user()->government_department == 1) {
                $this->validate(
                    $request,
                    [
                        'teachniqal_score' => 'required',
                        'financial_score'    => 'required',
                        'selection_procurement_system'    => 'required',
                        'project_budget'    => 'required',
                        'project_budget' => 'required',

                    ]
                );
            }

            $package_item = Item::where('subscriber', Auth::user()->id)->first();
            $qcount = 0;
            $total_technical = 0;
            $total_financial = 0;

            
            if (Auth::user()->government_department == 1) {
                foreach ($request->question_type as $qt) {
                    if ($qt == 0) {
                        $total_technical = $total_technical + $request->question_score[$qcount];
                    }
                    if ($qt == 1) {
                        $total_financial = $total_financial + $request->question_score[$qcount];
                    }
                    $qcount++;
                }

                if ($request->teachniqal_score) {
                    if ($total_technical != $request->teachniqal_score) {
                        $json['type'] = 'error';
                        $json['message'] = 'Bid Evaluation Criteria scores do not match the Bid Score Weightage total technical and/or financial scores.';
                        return $json;
                    }
                }

                if ($request->financial_score) {
                    if ($total_financial != $request->financial_score) {
                        $json['type'] = 'error';
                        $json['message'] = 'Bid Evaluation Criteria scores do not match the Bid Score Weightage total technical and/or financial scores.';
                        return $json;
                    }
                }
            }

            $job_post = $this->job->storeJobs($request);
            if ($job_post = 'success') {
                $json['type'] = 'success';
                $json['message'] = trans('lang.job_post_success');
                // Send Email
                $user = User::find(Auth::user()->id);
                //send email to admin
                if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                    $job = $this->job::where('user_id', Auth::user()->id)->latest()->first();
                    $email_params = array();
                    $new_posted_job_template = DB::table('email_types')->select('id')->where('email_type', 'admin_email_new_job_posted')->get()->first();
                    $new_posted_job_template_employer = DB::table('email_types')->select('id')->where('email_type', 'employer_email_new_job_posted')->get()->first();
                    if (!empty($new_posted_job_template->id) || !empty($new_posted_job_template_employer)) {
                        $template_data = EmailTemplate::getEmailTemplateByID($new_posted_job_template->id);
                        $template_data_employer = EmailTemplate::getEmailTemplateByID($new_posted_job_template_employer->id);
                        $email_params['job_title'] = $job->title;
                        $email_params['posted_job_link'] = url('/job/' . $job->slug);
                        $email_params['name'] = Helper::getUserName(Auth::user()->id);
                        $email_params['link'] = url('profile/' . $user->slug);
                        Mail::to(config('mail.username'))
                            ->send(
                                new AdminEmailMailable(
                                    'admin_email_new_job_posted',
                                    $template_data,
                                    $email_params
                                )
                            );
                        if (!empty($user->email)) {
                            Mail::to($user->email)
                                ->send(
                                    new EmployerEmailMailable(
                                        'employer_email_new_job_posted',
                                        $template_data_employer,
                                        $email_params
                                    )
                                );
                        }
                    }
                }

                return $json;
            }
            // }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.verify_accnt_post_job');
            return $json;
        }
    }

    /**
     * Updated resource in DB.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['type'] = 'error';
            $response['message'] = $server->getData()->message;
            return $response;
        }
        $json = array();
        $this->validate(
            $request,
            [
                'title' => 'required',
                'project_levels'    => 'required',
                // 'english_level'    => 'required',
                'project_cost'    => 'required',
            ]
        );
        if (Schema::hasColumn('jobs', 'expiry_date')) {
            if ($request['expiry_date'] == trans('lang.project_expiry')) {
                $json['type'] = 'error';
                $json['message'] = trans('lang.job_expiry_req');
                return $json;
            }
            $expiry = Carbon::parse($request['expiry_date']);
            if ($expiry->lessThan(Carbon::now())) {
                $json['type'] = 'error';
                $json['message'] = trans('lang.past_expiry_date');
                return $json;
            }
            $this->validate($request, ['expiry_date' => 'required']);
        }
        if (!empty($request['latitude']) || !empty($request['longitude'])) {
            $this->validate(
                $request,
                [
                    'latitude' => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                    'longitude' => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                ]
            );
        }
        $id = $request['id'];
        $job_update = $this->job->updateJobs($request, $id);
        if ($job_update['type'] = 'success') {
            $json['type'] = 'success';
            $json['role'] = Auth::user()->getRoleNames()->first();
            $json['message'] = trans('lang.job_update_success');
            return $json;
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.something_wrong');
            return $json;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $slug Job Slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $job = $this->job::all()->where('slug', $slug)->first();
        if (!empty($job)) {
            $submitted_proposals = $job->proposals->where('status', '!=', 'cancelled')->pluck('freelancer_id')->toArray();
            $employer_id = !empty($job->employer) ? $job->employer->id : '';
            $profile = !empty($employer_id) ? User::find($employer_id)->profile : '';
            $user_image = !empty($profile) ? $profile->avater : '';
            $profile_image = !empty($user_image) ? '/uploads/users/' . $job->employer->id . '/' . $user_image : 'images/user-login.png';
            $reasons = Helper::getReportReasons();
            $auth_profile = Auth::user() ? auth()->user()->profile : '';
            $save_job = !empty($auth_profile->saved_jobs) ? unserialize($auth_profile->saved_jobs) : array();
            $save_jobs = '';
            foreach ($save_job as $saved_job_name) {
                if ($saved_job_name == $job->id) {
                    $save_jobs = $save_job;
                } else {
                    $save_jobs = '';
                }
            }
            // dd($save_jobs);
            $save_employers = !empty($auth_profile->saved_employers) ? unserialize($auth_profile->saved_employers) : array();
            $attachments  = unserialize($job->attachments);
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            $project_type  = Helper::getProjectTypeList($job->project_type);
            $breadcrumbs_settings = SiteManagement::getMetaValue('show_breadcrumb');
            $show_breadcrumbs = !empty($breadcrumbs_settings) ? $breadcrumbs_settings : 'true';
            $BidQuestion = BidQuestion::where('job_id', $job->id)->get();
            if (file_exists(resource_path('views/extend/front-end/jobs/show.blade.php'))) {
                return view(
                    'extend.front-end.jobs.show',
                    compact(
                        'job',
                        'reasons',
                        'profile_image',
                        'submitted_proposals',
                        'save_jobs',
                        'save_employers',
                        'attachments',
                        'symbol',
                        'project_type',
                        'show_breadcrumbs',
                        'BidQuestion'
                    )
                );
            } else {
                return view(
                    'front-end.jobs.show',
                    compact(
                        'job',
                        'reasons',
                        'profile_image',
                        'submitted_proposals',
                        'save_jobs',
                        'save_employers',
                        'attachments',
                        'symbol',
                        'project_type',
                        'show_breadcrumbs',
                        'BidQuestion'
                    )
                );
            }
        } else {
            abort(404);
        }
    }

    /**
     * Get job Skills.
     *
     * @param mixed $request $req->attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function getJobSkills(Request $request)
    {
        $json = array();
        if (!empty($request['slug'])) {
            $job = $this->job::where('slug', $request['slug'])->select('id')->first();
            if (!empty($job)) {
                $jobs = $this->job::find($job['id']);
                $skills = $jobs->skills->toArray();
                if (!empty($skills)) {
                    $json['type'] = 'success';
                    $json['skills'] = $skills;
                    return $json;
                } else {
                    $json['error'] = 'error';
                    return $json;
                }
            } else {
                $json['error'] = 'error';
                return $json;
            }
        }
    }

    /**
     * Display admin jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobsAdmin()
    {
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $jobs = $this->job::where('title', 'like', '%' . $keyword . '%')->paginate(6)->setPath('');
            $pagination = $jobs->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $jobs = $this->job->latest()->paginate(6);
        }


        // dd($jobs);
        $payment   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($payment) && !empty($payment[0]['currency']) ? Helper::currencyList($payment[0]['currency']) : array();
        $payment_methods = Arr::pluck(Helper::getPaymentMethodList(), 'title', 'value');
        if (file_exists(resource_path('views/extend/back-end/admin/jobs/index.blade.php'))) {
            return view(
                'extend.back-end.admin.jobs.index',
                compact('jobs', 'symbol', 'payment', 'payment_methods')
            );
        } else {
            return view(
                'back-end.admin.jobs.index',
                compact('jobs', 'symbol', 'payment', 'payment_methods')
            );
        }
    }

    public function jobsAdminUnpublish($id){
        // dd('heere');
        $job = Job::where('id', $id)->first();
        $user = User::where('id', $job->user_id)->first();
        $job_unpublished = Job::UnpublishJob($id);
        // dd($job_unpublished);
        if($job_unpublished == '1'){

            $data = array();
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['job'] = $job->title;

            \Notification::send($user, new JobUnpublished($data));
           
            Session::flash('message', trans('Job Unpublished Successfully!'));
            return Redirect::back();
        }
        else {
            Session::flash('error', trans('Some Error Occured!'));
            return Redirect::back();
        }
    }

    public function jobsAdminPublish($id){
        // dd('heere');
        $job = Job::where('id', $id)->first();
        $user = User::where('id', $job->user_id)->first();
        $job_published = Job::PublishJob($id);
        // dd($job_unpublished);
        if($job_published == '1'){

            $data = array();
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;
            $data['job'] = $job->title;

            \Notification::send($user, new JobPublished($data));
           
            Session::flash('message', trans('Job Published Successfully!'));
            return Redirect::back();
        }
        else {
            Session::flash('error', trans('Some Error Occured!'));
            return Redirect::back();
        }
    }

    public function jobsAdministrator()
    {
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $jobs = $this->job::where('title', 'like', '%' . $keyword . '%')->paginate(6)->setPath('');
            $pagination = $jobs->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
            $jobs = $this->job->latest()->paginate(6);
        }


        // dd($jobs);
        $payment   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($payment) && !empty($payment[0]['currency']) ? Helper::currencyList($payment[0]['currency']) : array();
        $payment_methods = Arr::pluck(Helper::getPaymentMethodList(), 'title', 'value');
        if (file_exists(resource_path('views/extend/back-end/admin/jobs/index.blade.php'))) {
            return view(
                'extend.back-end.job-admin.jobs.index',
                compact('jobs', 'symbol', 'payment', 'payment_methods')
            );
        } else {
            return view(
                'back-end.job-admin.jobs.index',
                compact('jobs', 'symbol', 'payment', 'payment_methods')
            );
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listjobs()
    {
        $jobs = array();
        $categories = array();
        $locations = array();
        $languages = array();
        $jobs = $this->job->latest()->paginate(6);
        $categories = Category::all();
        $locations = Location::all();
        $languages = Language::all();
        $freelancer_skills = Helper::getFreelancerLevelList();
        $project_length = Helper::getJobDurationList();
        $skills = Skill::all();
        $keyword = '';
        $Jobs_total_records = '';
        $type = 'job';
        $currency = SiteManagement::getMetaValue('commision');
        $symbol   = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
        $job_list_meta_title = !empty($inner_page) && !empty($inner_page[0]['job_list_meta_title']) ? $inner_page[0]['job_list_meta_title'] : trans('lang.job_listing');
        $job_list_meta_desc = !empty($inner_page) && !empty($inner_page[0]['job_list_meta_desc']) ? $inner_page[0]['job_list_meta_desc'] : trans('lang.job_meta_desc');
        $show_job_banner = !empty($inner_page) && !empty($inner_page[0]['show_job_banner']) ? $inner_page[0]['show_job_banner'] : 'true';
        $job_inner_banner = !empty($inner_page) && !empty($inner_page[0]['job_inner_banner']) ? $inner_page[0]['job_inner_banner'] : null;
        if (file_exists(resource_path('views/extend/front-end/jobs/index.blade.php'))) {
            return view(
                'extend.front-end.jobs.index',
                compact(
                    'jobs',
                    'categories',
                    'locations',
                    'languages',
                    'freelancer_skills',
                    'project_length',
                    'keyword',
                    'Jobs_total_records',
                    'type',
                    'skills',
                    'symbol',
                    'job_list_meta_title',
                    'job_list_meta_desc',
                    'show_job_banner',
                    'job_inner_banner'
                )
            );
        } else {
            return view(
                'front-end.jobs.index',
                compact(
                    'jobs',
                    'categories',
                    'locations',
                    'languages',
                    'freelancer_skills',
                    'project_length',
                    'keyword',
                    'Jobs_total_records',
                    'type',
                    'skills',
                    'symbol',
                    'job_list_meta_title',
                    'job_list_meta_desc',
                    'show_job_banner',
                    'job_inner_banner'
                )
            );
        }
    }

    /**
     * Add job to whishlist.
     *
     * @param mixed $request request->attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function addWishlist(Request $request)
    {
        $json = array();
        if (Auth::user()) {
            if (!empty($request['id'])) {
                $user_id = Auth::user()->id;
                $id = $request['id'];
                $profile = new Profile();
                $add_wishlist = $profile->addWishlist($request['column'], $id, $user_id);
                if ($add_wishlist == "success") {
                    $json['type'] = 'success';
                    $json['message'] = trans('lang.added_to_wishlist');
                    return $json;
                } else {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.something_wrong');
                    return $json;
                }
            }
        } else {
            $json['type'] = 'authentication';
            $json['message'] = trans('lang.need_to_reg');
            return $json;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param mixed $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        dd($request);
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $json['type'] = 'error';
            $json['message'] = $server->getData()->message;
            return $json;
        }
        $json = array();
        $id = $request['job_id'];
        if (!empty($id)) {
            $this->job->deleteRecord($id);
            $json['type'] = 'success';
            return $json;
        }
    }

    /**
     * Get Latest Jobs
     *
     * @return \Illuminate\Http\Response
     */
    public function getLatestJobs(Request $request)
    {
        $page = $request['page'];
        $per_page = $request['per_page'];
        $json = array();
        Paginator::currentPageResolver(
            function () use ($page) {
                return $page;
            }
        );
        $jobs = $this->job->latest()->paginate($per_page);
        $latest_jobs = array();
        if (!empty($jobs)) {
            foreach ($jobs as $key => $job) {
                if (Schema::hasColumn('jobs', 'expiry_date') && !empty($job->expiry_date)) {
                    $expiry = Carbon::parse($job->expiry_date);
                    if (Carbon::now()->lessThan($expiry)) {
                        // dd($job);
                        $user = User::find($job->user_id);
                        $latest_jobs[$key]['id'] = $job->id;
                        $latest_jobs[$key]['slug'] = $job->slug;
                        $latest_jobs[$key]['user_slug'] = $user->slug;
                        $latest_jobs[$key]['user_name'] = Helper::getUserName($job->user_id);
                        $latest_jobs[$key]['title'] = !empty($job->title) ? $job->title : '';
                        $latest_jobs[$key]['user_image'] = asset(Helper::getProfileImage($job->user_id));
                        $latest_jobs[$key]['location'] = !empty($job->location->title) ? $job->location->title : '';
                        $latest_jobs[$key]['price'] = !empty($job->price) ? $job->price : '';
                        $latest_jobs[$key]['duration'] = !empty($job->duration) ?  Helper::getJobDurationList($job->duration) : '';
                        $currency   = SiteManagement::getMetaValue('commision');
                        $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
                        $latest_jobs[$key]['symbol'] = !empty($symbol['symbol']) ? $symbol['symbol'] : '$';
                        $latest_jobs[$key]['skills'] = !empty($job->skills) ? $job->skills->toArray() : array();
                        $latest_jobs[$key]['saved_jobs'] = !empty(auth()->user()->profile->saved_jobs) ? unserialize(auth()->user()->profile->saved_jobs) : array();
                    }
                } else {
                    $user = User::find($job->user_id);
                    $latest_jobs[$key]['id'] = $job->id;
                    $latest_jobs[$key]['slug'] = $job->slug;
                    $latest_jobs[$key]['user_slug'] = $user->slug;
                    $latest_jobs[$key]['user_name'] = Helper::getUserName($job->user_id);
                    $latest_jobs[$key]['title'] = !empty($job->title) ? $job->title : '';
                    $latest_jobs[$key]['user_image'] = asset(Helper::getProfileImage($job->user_id));
                    $latest_jobs[$key]['location'] = !empty($job->location->title) ? $job->location->title : '';
                    $latest_jobs[$key]['price'] = !empty($job->price) ? $job->price : '';
                    $latest_jobs[$key]['duration'] = !empty($job->duration) ?  Helper::getJobDurationList($job->duration) : '';
                    $currency   = SiteManagement::getMetaValue('commision');
                    $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
                    $latest_jobs[$key]['symbol'] = !empty($symbol['symbol']) ? $symbol['symbol'] : '$';
                    $latest_jobs[$key]['skills'] = !empty($job->skills) ? $job->skills->toArray() : array();
                    $latest_jobs[$key]['saved_jobs'] = !empty(auth()->user()->profile->saved_jobs) ? unserialize(auth()->user()->profile->saved_jobs) : array();
                }
            }
        }
        if (!empty($latest_jobs)) {
            $json['type'] = 'success';
            $json['jobs'] = $latest_jobs;
            $json['current_page'] = $jobs->currentPage();
            $json['last_page'] = $jobs->lastPage();
            return $json;
        } else {
            $json['type'] = 'error';
            return $json;
        }
    }

    public function jobBySkills()
    {


        // $min_price = !empty($_GET['minprice']) ? $_GET['minprice'] : 0;
        // $max_price = !empty($_GET['maxprice']) ? $_GET['maxprice'] : 0;


        //   echo "<pre>"; print_r($_GET); exit;
        $skills = Skill::get();
        $categories = Category::orderBy('title', 'asc')->where('parent', '!=', 0)->get();
        //  $categories = Category::pluck('title', 'id');
        $job_id = array();
        $filters = array();
        $jobs = Job::select('*');
        if (!empty($_GET['skill_id'])) {
            $skill_id = $_GET['skill_id'];

            // $job_skills = where('')
            $search_skills = $_GET['skill_id'];
            $filters['skill_id'] = $search_skills;

            $skill_obj = Skill::where('id', $search_skills)->first();

            $skill = Skill::find($skill_obj->id);
            if (!empty($skill->jobs) && !empty($skill)) {


                $skill_jobs = $skill->jobs->pluck('id')->toArray();
                foreach ($skill_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }

        if (!empty($_GET['category_id'])) {
            // dd($_GET['category_id']);
            $category_id = $_GET['category_id'];
            $search_category = $_GET['category_id'];
            $filters['category_id'] = $search_category;

            $categor_obj = Category::where('id', $search_category)->first();


            $category = !empty($categor_obj) && !empty($categor_obj->id) ? Category::find($categor_obj->id) : '';

            // dd($category);
            if (!empty($category)) {

                $categor_jobs = Job::where('categories', $category->id)->pluck('id')->toArray();

                foreach ($categor_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        }


        // if (!empty($_GET['price_from'])) {
        //     $price_from = $_GET['price_from'];
        //     $filters['price_from'] = $price_from;

        //     $jobs->where('title', 'like', '%' . $keyword . '%');
        // }
        if (!empty($_GET['minprice'])) {
            $minprice = $_GET['minprice'];
            $filters['minprice'] = $minprice;
        }
        if (!empty($_GET['maxprice'])) {
            $maxprice = $_GET['maxprice'];
            $filters['maxprice'] =  $maxprice;
        }

        if (!empty($_GET['maxprice']) && !empty($_GET['minprice'])) {
            $jobs->whereBetween('price', [intval($_GET['minprice']), intval($_GET['maxprice'])]);
        }
        if (empty($_GET['maxprice']) && !empty($_GET['minprice'])) {
            $jobs->where('price', '>', intval($_GET['minprice']));
        }
        if (!empty($_GET['maxprice']) && empty($_GET['minprice'])) {
            $jobs->whereBetween('price', [0, intval($_GET['maxprice'])]);
        }

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        }


        if (!empty($_GET['status'])) {
            $status = $_GET['status'];
            $filters['status'] = $status;

            $jobs->where('status', $status);
        }
        if (!empty($_GET['btn_submit']) && $_GET['btn_submit'] == 'Download') {
            $jobs = $jobs->orderByRaw("updated_at DESC")->get();

            $mytime = Carbon::now();
            $fileName = 'jobs-' . $mytime . '.csv';
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Title', 'Url', 'employer', 'Start Date', 'Skills');

            $callback = function () use ($jobs, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($jobs as $job) {
                    $skills = '';
                    foreach ($job->skills as $item) {
                        $skills .= $item->title . ' - ';
                    }
                    $row['Title']  = $job->title;
                    $row['Url']    = url('job', $job->slug);
                    $row['employer']    = Helper::getUserName($job->employer->id);
                    $row['Start Date']  = $job->created_at;
                    $row['Skills']  = $skills;

                    fputcsv($file, array($row['Title'], $row['Url'], $row['employer'], $row['Start Date'], $row['Skills']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        } else {


            $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');
            foreach ($filters as $key => $filter) {
                $pagination = $jobs->appends(
                    array(
                        $key => $filter
                    )
                );
            }
        }



        return view('back-end.admin.jobs.jobBySkills', compact('skills', 'categories', 'jobs'));
    }

    public function jobBySkillsadmin()
    {


        // $min_price = !empty($_GET['minprice']) ? $_GET['minprice'] : 0;
        // $max_price = !empty($_GET['maxprice']) ? $_GET['maxprice'] : 0;


        //   echo "<pre>"; print_r($_GET); exit;
        $skills = Skill::get();
        $categories = Category::orderBy('title', 'asc')->where('parent', '!=', 0)->get();
        //  $categories = Category::pluck('title', 'id');
        $job_id = array();
        $filters = array();
        $jobs = Job::select('*');
        if (!empty($_GET['skill_id'])) {
            $skill_id = $_GET['skill_id'];

            // $job_skills = where('')
            $search_skills = $_GET['skill_id'];
            $filters['skill_id'] = $search_skills;

            $skill_obj = Skill::where('id', $search_skills)->first();

            $skill = Skill::find($skill_obj->id);
            if (!empty($skill->jobs) && !empty($skill)) {


                $skill_jobs = $skill->jobs->pluck('id')->toArray();
                foreach ($skill_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }

        if (!empty($_GET['category_id'])) {
            // dd($_GET['category_id']);
            $category_id = $_GET['category_id'];
            $search_category = $_GET['category_id'];
            $filters['category_id'] = $search_category;

            $categor_obj = Category::where('id', $search_category)->first();


            $category = !empty($categor_obj) && !empty($categor_obj->id) ? Category::find($categor_obj->id) : '';

            // dd($category);
            if (!empty($category)) {

                $categor_jobs = Job::where('categories', $category->id)->pluck('id')->toArray();

                foreach ($categor_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        }


        // if (!empty($_GET['price_from'])) {
        //     $price_from = $_GET['price_from'];
        //     $filters['price_from'] = $price_from;

        //     $jobs->where('title', 'like', '%' . $keyword . '%');
        // }
        if (!empty($_GET['minprice'])) {
            $minprice = $_GET['minprice'];
            $filters['minprice'] = $minprice;
        }
        if (!empty($_GET['maxprice'])) {
            $maxprice = $_GET['maxprice'];
            $filters['maxprice'] =  $maxprice;
        }

        if (!empty($_GET['maxprice']) && !empty($_GET['minprice'])) {
            $jobs->whereBetween('price', [intval($_GET['minprice']), intval($_GET['maxprice'])]);
        }
        if (empty($_GET['maxprice']) && !empty($_GET['minprice'])) {
            $jobs->where('price', '>', intval($_GET['minprice']));
        }
        if (!empty($_GET['maxprice']) && empty($_GET['minprice'])) {
            $jobs->whereBetween('price', [0, intval($_GET['maxprice'])]);
        }

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        }


        if (!empty($_GET['status'])) {
            $status = $_GET['status'];
            $filters['status'] = $status;

            $jobs->where('status', $status);
        }
        if (!empty($_GET['btn_submit']) && $_GET['btn_submit'] == 'Download') {
            $jobs = $jobs->orderByRaw("updated_at DESC")->get();

            $mytime = Carbon::now();
            $fileName = 'jobs-' . $mytime . '.csv';
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Title', 'Url', 'employer', 'Start Date', 'Skills');

            $callback = function () use ($jobs, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($jobs as $job) {
                    $skills = '';
                    foreach ($job->skills as $item) {
                        $skills .= $item->title . ' - ';
                    }
                    $row['Title']  = $job->title;
                    $row['Url']    = url('job', $job->slug);
                    $row['employer']    = Helper::getUserName($job->employer->id);
                    $row['Start Date']  = $job->created_at;
                    $row['Skills']  = $skills;

                    fputcsv($file, array($row['Title'], $row['Url'], $row['employer'], $row['Start Date'], $row['Skills']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        } else {


            $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');
            foreach ($filters as $key => $filter) {
                $pagination = $jobs->appends(
                    array(
                        $key => $filter
                    )
                );
            }
        }



        return view('back-end.job-admin.jobs.jobBySkills', compact('skills', 'categories', 'jobs'));
    }

    public function jobBySkillsDownload()
    {

        $mytime = Carbon::now();
        $fileName = 'jobs-' . $mytime . '.csv';

        $skills = Skill::get();
        $filters = array();
        $jobs = Job::select('*');
        if (!empty($_GET['skill_id'])) {
            $skill_id = $_GET['skill_id'];

            // $job_skills = where('')
            $search_skills = $_GET['skill_id'];
            $filters['skill_id'] = $search_skills;

            $skill_obj = Skill::where('id', $search_skills)->first();

            $skill = Skill::find($skill_obj->id);

            if (!empty($skill->jobs)) {


                $skill_jobs = $skill->jobs->pluck('id')->toArray();
                foreach ($skill_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        };



        $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');
        foreach ($filters as $key => $filter) {
            $pagination = $jobs->appends(
                array(
                    $key => $filter
                )
            );
        }

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Title', 'Url', 'employer', 'Start Date', 'Skills');

        $callback = function () use ($jobs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($jobs as $job) {
                $skills = '';
                foreach ($job->skills as $item) {
                    $skills .= $item->title . ' - ';
                }
                $row['Title']  = $job->title;
                $row['Url']    = url('job', $job->slug);
                $row['employer']    = Helper::getUserName($job->employer->id);
                $row['Start Date']  = $job->created_at;
                $row['Skills']  = $skills;

                fputcsv($file, array($row['Title'], $row['Url'], $row['employer'], $row['Start Date'], $row['Skills']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
        //return view('back-end.admin.jobs.jobBySkills',compact('skills','jobs'));

    }

    public function jobBySkillsDownloadadmin()
    {

        $mytime = Carbon::now();
        $fileName = 'jobs-' . $mytime . '.csv';

        $skills = Skill::get();
        $filters = array();
        $jobs = Job::select('*');
        if (!empty($_GET['skill_id'])) {
            $skill_id = $_GET['skill_id'];

            // $job_skills = where('')
            $search_skills = $_GET['skill_id'];
            $filters['skill_id'] = $search_skills;

            $skill_obj = Skill::where('id', $search_skills)->first();

            $skill = Skill::find($skill_obj->id);

            if (!empty($skill->jobs)) {


                $skill_jobs = $skill->jobs->pluck('id')->toArray();
                foreach ($skill_jobs as $id) {
                    $job_id[] = $id;
                }
            }

            $jobs->whereIn('id', $job_id);
        }
        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
            $filters['keyword'] = $keyword;
            $jobs->where('title', 'like', '%' . $keyword . '%');
        };



        $jobs = $jobs->orderByRaw("updated_at DESC")->paginate(10)->setPath('');
        foreach ($filters as $key => $filter) {
            $pagination = $jobs->appends(
                array(
                    $key => $filter
                )
            );
        }

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Title', 'Url', 'employer', 'Start Date', 'Skills');

        $callback = function () use ($jobs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($jobs as $job) {
                $skills = '';
                foreach ($job->skills as $item) {
                    $skills .= $item->title . ' - ';
                }
                $row['Title']  = $job->title;
                $row['Url']    = url('job', $job->slug);
                $row['employer']    = Helper::getUserName($job->employer->id);
                $row['Start Date']  = $job->created_at;
                $row['Skills']  = $skills;

                fputcsv($file, array($row['Title'], $row['Url'], $row['employer'], $row['Start Date'], $row['Skills']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
        //return view('back-end.admin.jobs.jobBySkills',compact('skills','jobs'));

    }

    public function userBySkills()
    {

        //   echo "<pre>"; print_r($_GET); exit;
        $skills = Skill::get();
        $categories = Category::orderBy('title', 'asc')->get();
        $freelancer_skills = Helper::getFreelancerLevelList();
        $locations  = Location::orderBy('title', 'asc')->get();
        $job_id = array();
        $filters = array();
        if (!empty($_GET['user_type'])) {
            $type = $_GET['user_type'];
        } else {
            $type = 'freelancer';
        }
        $user_by_role =  User::role($type)->select('id')->get()->pluck('id')->toArray();
        // $Users = !empty($user_by_role) ? User::whereIn('id', $user_by_role)->where('is_disabled', 'false')->whereNotNull('email_verified_at') : array();
        $Users = !empty($user_by_role) ? User::whereIn('id', $user_by_role)->whereNotNull('email_verified_at') : array();

        $Users = $Users->orderBy('created_at', 'DESC');

        if (!empty($_GET['user_type'])) {
            $search_user_types = $_GET['user_type'];
            $filters['user_type'] = $search_user_types;
            $userT = Profile::select('user_id')->whereIn('user_id', $user_by_role)->get()->pluck('user_id')->toArray();

            $Users->whereIn('id', $userT);
        }

        if (!empty($_GET['skill_id'])) {

            $search_skills = $_GET['skill_id'];
            $filters['skill_id'] = $search_skills;
            $skill_obj = Skill::where('id', $search_skills)->first();
            $skill = Skill::find($skill_obj->id);
            $user_id = array();

            if (!empty($skill->freelancers)) {

                $skill_jobs = $skill->freelancers->pluck('id')->toArray();

                foreach ($skill_jobs as $id) {
                    $user_id[] = $id;
                }
            }
            $Users->whereIn('id', $user_id);
        }

        if (!empty($_GET['gender'])) {

            $search_gender = $_GET['gender'];
            $filters['gender'] = $search_gender;
            $user_id = Profile::select('user_id')->whereIn('user_id', $user_by_role)
                ->where('gender', $search_gender)->get()->pluck('user_id')->toArray();

            $Users->whereIn('id', $user_id);
        }

        if (!empty($_GET['location'])) {
            $locationsfilter = array();
            $search_locations = $_GET['location'];
            $filters['location'] = $search_locations;

            $locationsfilter = Location::select('id')->where('id', $search_locations)->get()->pluck('id')->toArray();

            if (!empty($locationsfilter)) {
                $Users->whereIn('location_id', $locationsfilter);
            }
        }
        if (!empty($_GET['is_disabled'])) {
            $search_is_disabled = $_GET['is_disabled'];
            $filters['is_disabled'] = $search_is_disabled;


            $Users->where('is_disabled', 'true');
        }
        if (!empty($_GET['freelaner_type'])) {

            $search_freelaner_types = $_GET['freelaner_type'];

            $filters['freelaner_type'] = $search_freelaner_types;
            $freelancers = Profile::select('user_id')->whereIn('user_id', $user_by_role)
                ->where('freelancer_type', $search_freelaner_types)->get()->pluck('user_id')->toArray();



            $Users->whereIn('id', $freelancers)->get();
        }

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];

            $filters['keyword'] = $keyword;
            $Users->where('first_name', 'like', '%' . $keyword . '%');
            $Users->orWhere('last_name', 'like', '%' . $keyword . '%');
            $Users->orWhere('slug', 'like', '%' . $keyword . '%');
            $Users->whereIn('id', $user_by_role);
            // $Users->where('is_disabled', 'false');
        }
        //dd($Users);
        if (!empty($_GET['btn_submit']) && $_GET['btn_submit'] == 'Download') {
            $Users = $Users->orderByRaw("updated_at DESC")->get();

            $mytime = Carbon::now();
            $fileName = 'Users-' . $mytime . '.csv';
            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Name', 'Email', 'Total Earnings', 'Skills', 'Created Date');

            $callback = function () use ($Users, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($Users as $user) {
                    $skills = '';
                    foreach ($user->skills as $item) {
                        $skills .= $item->title . ' - ';
                    }
                    $row['Name']  = Helper::getUserName($user->id);
                    $row['Email']    = $user->email;
                    $row['Total Earnings']    = Helper::getProposalsBalance($user->id, 'completed');
                    $row['Skills']  = $skills;
                    $row['Created Date']  = $user->created_at;


                    fputcsv($file, array($row['Name'], $row['Email'], $row['Total Earnings'], $row['Skills'], $row['Created Date']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        } else {


            $Users = $Users->orderByRaw("updated_at DESC")->paginate(10)->setPath('');


            foreach ($filters as $key => $filter) {
                $pagination = $Users->appends(
                    array(
                        $key => $filter
                    )
                );
            }
        }
        //  dd($freelancer_skills);


        return view('back-end.admin.users.userBySkills', compact('skills', 'categories', 'Users', 'freelancer_skills', 'locations'));
    }





   
}
