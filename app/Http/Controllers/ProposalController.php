<?php
/**
 * Class ProposalController
 *
 
 */
namespace App\Http\Controllers;

use DB;
use Auth;
use App\Job;
use Storage;
use App\User;
use Response;
use App\Helper;
use App\Review;
use ZipArchive;
use App\Package;
use App\Profile;
use App\Proposal;
use App\Milestone;
use Carbon\Carbon;
use App\EmailTemplate;
use App\SiteManagement;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Mail\EmployerEmailMailable;
use Illuminate\Support\Facades\Mail;
use App\Mail\FreelancerEmailMailable;
use App\Mail\EmailVerificationMailable;
use App\Notifications\ProposalsReceived;
use App\Notifications\RequestToCloseProject;
use App\Notifications\RequestToCancelProject;
use App\Notifications\ResponseToCloseProject;
use App\Notifications\ResponseToCancelProject;
use App\Notifications\ProposalNegotiationsEmployer;
use App\Notifications\ResponseToCancelProjectAccept;
use App\Notifications\ProposalNegotiationsFreelancer;

/**
 * Class ProposalController
 *
 */
class ProposalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @param instance $proposal instance
     *
     * @return void
     */
    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }

    /**
     * View job proposal.
     *
     * @param int $job_slug jobslug
     *
     * @return \Illuminate\Http\Response
     */
    public function createProposal($job_slug)
    {
        if (!empty($job_slug)) {
            $job = Job::all()->where('slug', $job_slug)->first();
            if (!empty($job)) {
                $job_skills = $job->skills->pluck('id')->toArray();
                $check_skill_req = $this->proposal->getJobSkillRequirement($job_skills);
                $check_matched_skill_req = $this->proposal->getJobSkillMatchedRequirement($job_skills);
                $proposal_status = Job::find($job->id)->proposals()->where('status', 'hired')->first();
                $role_id =  Helper::getRoleByUserID(Auth::user()->id);
                $package_options = Package::select('options')->where('role_id', $role_id)->first();
                $options = !empty($package_options) ? unserialize($package_options['options']) : array();
                $settings = SiteManagement::getMetaValue('settings');
                $required_connects = !empty($settings) && !empty($settings[0]['connects_per_job']) ? $settings[0]['connects_per_job'] : 2;
                $remaining_proposals = !empty($options) && !empty($options['no_of_connects']) ? $options['no_of_connects'] / $required_connects : 0;
                $submitted_proposals = $this->proposal::where('freelancer_id', Auth::user()->id)->count();
                // dd($job->duration);
                $duration =  Helper::getJobDurationList($job->duration);
                $job_completion_time_list = Helper::getJobDurationList();
                $job_completion_time = Proposal::getJobDurationListCheck($job->duration);
                $commision_amount = SiteManagement::getMetaValue('commision');
                $tax = !empty($commision_amount) && !empty($commision_amount[0]["tax"]) ? $commision_amount[0]["tax"] : 0;
                $commision = !empty($commision_amount) && !empty($commision_amount[0]["commision"]) ? $commision_amount[0]["commision"] : 0;
                $currency = SiteManagement::getMetaValue('commision');
                $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
                $breadcrumbs_settings = SiteManagement::getMetaValue('show_breadcrumb');
                $show_breadcrumbs = !empty($breadcrumbs_settings) ? $breadcrumbs_settings : 'true';
                if (Auth::user() && !empty(Auth::user()->id)) {
                    $submitted_proposals_count = DB::table('proposals')
                        ->where('job_id', $job->id)
                        ->where('freelancer_id', Auth::user()->id)->count();
                }
                if (file_exists(resource_path('views/extend/front-end/jobs/proposal.blade.php'))) {
                    return View(
                        'extend.front-end.jobs.proposal',
                        compact(
                            'job',
                            'proposal_status',
                            'duration',
                            'job_completion_time',
                            'tax',
                            'commision',
                            'check_skill_req',
                            'remaining_proposals',
                            'submitted_proposals',
                            'symbol',
                            'submitted_proposals_count',
                            'show_breadcrumbs',
                            'check_matched_skill_req'
                        )
                    );
                } else {
                    return View(
                        'front-end.jobs.proposal',
                        compact(
                            'job',
                            'proposal_status',
                            'duration',
                            'job_completion_time',
                            'tax',
                            'commision',
                            'check_skill_req',
                            'remaining_proposals',
                            'submitted_proposals',
                            'symbol',
                            'submitted_proposals_count',
                            'show_breadcrumbs',
                            'check_matched_skill_req'
                        )
                    );
                }
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    /**
     * Upload Image to temporary folder.
     *
     * @param \Illuminate\Http\Request $request request attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadTempImage(Request $request)
    {
        if (!empty($request['file'])) {
            $attachments = $request['file'];
            $path = 'uploads/proposals/temp/';
            return Helper::uploadTempMultipleAttachments($attachments, $path);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request req attributes
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()) {
            $server = Helper::kpitbIsDemoSiteAjax();
            if (!empty($server)) {
                $response['message'] = $server->getData()->message;
                return $response;
            }
            if (!empty($request)) {
                $json = array();
                $this->validate(
                    $request,
                    [
                        'amount' => 'required',
                        'completion_time'    => 'required',
                        'description'    => 'required',
                    ]
                );
                $job = Job::find($request['id']);
                if ($job->status != 'posted') {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.job_not_avail');
                    return $json;
                }
                if (intval($request['amount']) > $job->price) {
                    $json['type'] = 'error';
                    $json['message'] = trans('lang.proposal_exceed');
                    return $json;
                }
                $package = DB::table('items')->where('subscriber', Auth::user()->id)->select('product_id')->first();
                $proposals = $this->proposal::where('freelancer_id', Auth::user()->id)->count();
                $settings = SiteManagement::getMetaValue('settings');
                $payment_settings = SiteManagement::getMetaValue('commision');
                $required_connects = !empty($settings) && !empty($settings[0]['connects_per_job']) ? $settings[0]['connects_per_job'] : 2;
                $package_status = '';
                if (Auth::user() && $request['user_id']) {
                    $proposals = DB::table('proposals')
                        ->where('job_id', $request['id'])
                        ->where('freelancer_id', $request['user_id'])->count();
                    if ($proposals > 0) {
                        $json['type'] = 'error';
                        $json['message'] = trans('lang.proposal_already_submitted');
                        return $json;
                    }
                }
                if (!empty($payment_settings) && empty($payment_settings[0]['enable_packages'])) {
                    $package_status = 'true';
                } else {
                    $package_status = $payment_settings[0]['enable_packages'];
                }
                if (!empty($payment_settings) && $package_status === 'true') {
                    if (empty($package->product_id)) {
                        $json['type'] = 'error';
                        $json['message'] = trans('lang.need_to_purchase_pkg');
                        return $json;
                    }
                    $package_options = Package::select('options')->where('id', $package->product_id)->get()->first();
                    $option = unserialize($package_options->options);
                    $allowed_proposals = $option['no_of_connects'] / $required_connects;
                    if ($proposals > $allowed_proposals) {
                        $json['type'] = 'error';
                        $json['message'] = trans('lang.not_enough_connects');
                        return $json;
                    } else {
                        $submit_propsal = $this->proposal->storeProposal($request, $request['id']);
                        if ($submit_propsal = 'success') {
                            $json['type'] = 'success';
                            $json['message'] = trans('lang.proposal_submitted');
                            $user = User::find(Auth::user()->id);

                            $userE = User::where('id', $job->user_id)->first();

                            //send email
                            $data = array();

                            $data['freelancer_first_name'] = $user->first_name;
                            $data['freelancer_second_name'] = $user->last_name;
                            $data['job'] = $job;
                            $data['job_name'] = $job->title;
                            $data['slug'] = $job->slug;
                            \Notification::send($userE, new ProposalsReceived($data));

                            // if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                            //     if (!empty($job->employer->email)) {
                            //         $email_params = array();
                            //         $proposal_received_template = DB::table('email_types')->select('id')->where('email_type', 'employer_email_proposal_received')->get()->first();
                            //         $proposal_submitted_template = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_new_proposal_submitted')->get()->first();
                            //         if (!empty($proposal_received_template->id)
                            //             || !empty($proposal_submitted_template->id)
                            //         ) {
                            //             $template_data = EmailTemplate::getEmailTemplateByID($proposal_received_template->id);
                            //             $template_submit_proposal = EmailTemplate::getEmailTemplateByID($proposal_submitted_template->id);
                            //             $email_params['employer'] = Helper::getUserName($job->employer->id);
                            //             $email_params['employer_profile'] = url('profile/' . $job->employer->slug);
                            //             $email_params['freelancer'] = Helper::getUserName(Auth::user()->id);
                            //             $email_params['freelancer_profile'] = url('profile/' . $user->slug);
                            //             $email_params['title'] = $job->title;
                            //             $email_params['link'] = url('job/' . $job->slug);
                            //             $email_params['amount'] = $request['amount'];
                            //             $email_params['duration'] = Helper::getJobDurationList($request['completion_time']);
                            //             $email_params['message'] = $request['description'];
                            //             Mail::to($job->employer->email)
                            //                 ->send(
                            //                     new EmployerEmailMailable(
                            //                         'employer_email_proposal_received',
                            //                         $template_data,
                            //                         $email_params
                            //                     )
                            //                 );
                            //             Mail::to($user->email)
                            //                 ->send(
                            //                     new FreelancerEmailMailable(
                            //                         'freelancer_email_new_proposal_submitted',
                            //                         $template_submit_proposal,
                            //                         $email_params
                            //                     )
                            //                 );
                            //         } else {
                            //             $json['type'] = 'error';
                            //             $json['message'] = trans('lang.something_wrong');
                            //             return $json;
                            //         }
                            //     }
                            // }
                            return $json;
                        } else {
                            $json['type'] = 'error';
                            $json['message'] = trans('lang.something_wrong');
                            return $json;
                        }
                    }
                } else {
                    $submit_propsal = $this->proposal->storeProposal($request, $request['id']);
                    if ($submit_propsal = 'success') {
                        $json['type'] = 'success';
                        $json['message'] = trans('lang.proposal_submitted');
                        $user = User::find(Auth::user()->id);

                        $userE = User::where('id', $job->user_id)->first();

                        //send email
                        $data = array();

                        $data['freelancer_first_name'] = $user->first_name;
                        $data['freelancer_second_name'] = $user->last_name;
                        $data['job'] = $job;
                        $data['job_name'] = $job->title;
                        $data['slug'] = $job->slug;
                        \Notification::send($userE, new ProposalsReceived($data));
                        
                        // if (!empty(config('mail.username')) && !empty(config('mail.password'))) {
                        //     if (!empty($job->employer->email)) {
                        //         $email_params = array();
                        //         $proposal_received_template = DB::table('email_types')->select('id')->where('email_type', 'employer_email_proposal_received')->get()->first();
                        //         $proposal_submitted_template = DB::table('email_types')->select('id')->where('email_type', 'freelancer_email_new_proposal_submitted')->get()->first();
                        //         if (
                        //             !empty($proposal_received_template->id)
                        //             || !empty($proposal_submitted_template->id)
                        //         ) {
                        //             $template_data = EmailTemplate::getEmailTemplateByID($proposal_received_template->id);
                        //             $template_submit_proposal = EmailTemplate::getEmailTemplateByID($proposal_submitted_template->id);
                        //             $email_params['employer'] = Helper::getUserName($job->employer->id);
                        //             $email_params['employer_profile'] = url('profile/' . $job->employer->slug);
                        //             $email_params['freelancer'] = Helper::getUserName(Auth::user()->id);
                        //             $email_params['freelancer_profile'] = url('profile/' . $user->slug);
                        //             $email_params['title'] = $job->title;
                        //             $email_params['link'] = url('job/' . $job->slug);
                        //             $email_params['amount'] = $request['amount'];
                        //             $email_params['duration'] = Helper::getJobDurationList($request['completion_time']);
                        //             $email_params['message'] = $request['description'];
                        //             Mail::to($job->employer->email)
                        //                 ->send(
                        //                     new EmployerEmailMailable(
                        //                         'employer_email_proposal_received',
                        //                         $template_data,
                        //                         $email_params
                        //                     )
                        //                 );
                        //             Mail::to($user->email)
                        //                 ->send(
                        //                     new FreelancerEmailMailable(
                        //                         'freelancer_email_new_proposal_submitted',
                        //                         $template_submit_proposal,
                        //                         $email_params
                        //                     )
                        //                 );
                        //         } else {
                        //             $json['type'] = 'error';
                        //             $json['message'] = trans('lang.something_wrong');
                        //             return $json;
                        //         }
                        //     }
                        // }
                        return $json;
                    } else {
                        $json['type'] = 'error';
                        $json['message'] = trans('lang.something_wrong');
                        return $json;
                    }
                }
            } else {
                $json['type'] = 'error';
                $json['message'] = trans('lang.something_wrong');
                return $json;
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = trans('lang.not_authorize');
            return $json;
        }
    }

    /**
     * Get job proposal listing.
     *
     * @param string $slug jobSlug
     *
     * @return \Illuminate\Http\Response
     */
    public function getJobProposals(Request $request, $slug)
    {
        if (!empty($slug)) {
            $accepted_proposal = array();
            $job = Job::where('slug', $slug)->first();
            if ($request->sortBy == 'latest') {
                $proposals = Proposal::where('job_id', $job->id)->latest()->get();
            } elseif ($request->sortBy == 'lowest') {
                $proposals = Proposal::where('job_id', $job->id)->orderBy('amount', 'asc')->get();
            } elseif ($request->sortBy == 'suitable') {
                $proposals = Proposal::where('job_id', $job->id)->get();
                $job = Job::where('id', $job->id)->with(['skills'])->first();
                $required_skills = array();
                $freelancer_skills = array();
                $acceptable_proposals = array();

                foreach ($job->skills as $skill) {
                    array_push($required_skills, $skill->id);
                }

                if (count($proposals) > 0) {
                    foreach ($proposals as $proposal) {
                        $freelancer = User::where('id', $proposal->freelancer_id)->with(['skills'])->first();
                        $skill_matches = 0;
                        foreach ($freelancer->skills as $skill) {
                            foreach ($required_skills as $reqSkill) {
                                if ($reqSkill == $skill->id) {
                                    $skill_matches++;
                                }
                            }
                        }
                        if ($skill_matches > 0) {
                            $proposal['skill_matches'] = $skill_matches;
                            array_push($acceptable_proposals, $proposal);
                        }
                    }
                    $collect_proposals = collect($acceptable_proposals);
                    $sorted = $collect_proposals->sortByDesc('skill_matches');
                    if (count($collect_proposals) > 0) {
                        $proposals = $sorted->values()->all();
                    }
                }
                // $proposals = Proposal::where('job_id', $job->id)->skills();
            } else {
                $proposals = Job::find($job->id)->proposals;
            }
            $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)->first();
            $order = Job::getProjectOrder($job->id);
            $duration = !empty($job->duration) ? Helper::getJobDurationList($job->duration) : '';
            $currency   = SiteManagement::getMetaValue('commision');
            $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
            $payment_settings = SiteManagement::getMetaValue('commision');
            $enable_package = !empty($payment_settings) && !empty($payment_settings[0]['enable_packages']) ? $payment_settings[0]['enable_packages'] : 'true';
            $mode = !empty($payment_settings) && !empty($payment_settings[0]['payment_mode']) ? $payment_settings[0]['payment_mode'] : 'true';
            if (file_exists(resource_path('views/extend/back-end/employer/proposals/index.blade.php'))) {
                return View(
                    'extend.back-end.employer.proposals.index',
                    compact(
                        'proposals',
                        'job',
                        'duration',
                        'accepted_proposal',
                        'symbol',
                        'enable_package',
                        'mode',
                        'order'
                    )
                );
            } else {
                return View(
                    'back-end.employer.proposals.index',
                    compact(
                        'proposals',
                        'job',
                        'duration',
                        'accepted_proposal',
                        'symbol',
                        'enable_package',
                        'mode',
                        'order'
                    )
                );
            }
        } else {
            abort(404);
        }
    }

    /**
     * Hire freelancer.
     *
     * @param \Illuminate\Http\Request $request req->attr
     *
     * @return \Illuminate\Http\Response
     */
    public function hiredFreelencer(Request $request)
    {
        $json = array();
        $server = Helper::kpitbIsDemoSiteAjax();
        if (!empty($server)) {
            $response['message'] = $server->getData()->message;
            return $response;
        }
        if (!empty($request['id'])) {
            $this->proposal->assignJob($request['id']);
            $json['type'] = 'success';
            $json['message'] = trans('lang.freelancer_hire');
            return $json;
        }
    }



    /**
     * Proposal Details.
     *
     * @param string $slug slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $profile = array();
        $accepted_proposal = array();
        $freelancer_name = '';
        $profile_image = '';
        $user_slug = '';
        $attachments = array();
        $job = Job::where('slug', $slug)->first();

        
        if($job->payment_system == 'milestone'){
            $Milestone = Milestone:: where('job_id',$job->id)->get();
            if(count($Milestone) > 0){
                $Milestones = $Milestone;
            }else{
                $Milestones = '';
            }
        }else{
            $Milestones = '';
        }
        $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)
            ->first();
        $freelancer_name = Helper::getUserName($accepted_proposal->freelancer_id);
        $completion_time = !empty($accepted_proposal->completion_time) ?
            Helper::getJobDurationList($accepted_proposal->completion_time) : '';
        $profile = User::find($accepted_proposal->freelancer_id)->profile;
        $attachments = !empty($accepted_proposal->attachments) ? unserialize($accepted_proposal->attachments) : '';
        $user_image = !empty($profile) ? $profile->avater : '';
        $profile_image = !empty($user_image) ? '/uploads/users/' . $accepted_proposal->freelancer_id . '/' . $user_image : 'images/user-login.png';
        $employer_name = Helper::getUserName($job->user_id);
        $project_status = Helper::getProjectStatusNew();
        $duration = !empty($job->duration) ? Helper::getJobDurationList($job->duration) : '';
        $review_options = DB::table('review_options')->get()->all();
        $user_slug = User::find($accepted_proposal->freelancer_id)->slug;
        $feedbacks = Review::select('feedback')->where('receiver_id', $accepted_proposal->freelancer_id)->count();
        $avg_rating = Review::where('receiver_id', $accepted_proposal->freelancer_id)->sum('avg_rating');
        $rating  = $avg_rating != 0 ? round($avg_rating/Review::count()) : 0;
        $reviews = Review::where('receiver_id', $accepted_proposal->freelancer_id)->get();
        $stars  = $reviews->sum('avg_rating') != 0 ? (($reviews->sum('avg_rating')/$feedbacks)/5)*100 : 0;
        $average_rating_count = !empty($feedbacks) ? $reviews->sum('avg_rating')/$feedbacks : 0;
        $currency   = SiteManagement::getMetaValue('commision');
        $symbol = !empty($currency) && !empty($currency[0]['currency']) ? Helper::currencyList($currency[0]['currency']) : array();
        $cancel_proposal_text = trans('lang.cancel_proposal_text');
        $cancel_proposal_button = trans('lang.send_request');
        $validation_error_text = trans('lang.field_required');
        $cancel_popup_title = trans('lang.reason');
        $request_to_close = $accepted_proposal->request_to_close;
        $request_to_cancel = $accepted_proposal->request_to_cancel;
        if (file_exists(resource_path('views/extend/back-end/employer/proposals/show.blade.php'))) {
            return view(
                'extend.back-end.employer.proposals.show',
                compact(
                    'average_rating_count',
                    'job',
                    'duration',
                    'accepted_proposal',
                    'project_status',
                    'employer_name',
                    'profile_image',
                    'completion_time',
                    'freelancer_name',
                    'attachments',
                    'review_options',
                    'user_slug',
                    'stars',
                    'rating',
                    'feedbacks',
                    'symbol',
                    'cancel_proposal_text',
                    'cancel_proposal_button',
                    'validation_error_text',
                    'cancel_popup_title','Milestones',
                    'request_to_close',
                    'request_to_cancel',
                )
            );
        } else {
            return view(
                'back-end.employer.proposals.show',
                compact(
                    'average_rating_count',
                    'job',
                    'duration',
                    'accepted_proposal',
                    'project_status',
                    'employer_name',
                    'profile_image',
                    'completion_time',
                    'freelancer_name',
                    'attachments',
                    'review_options',
                    'user_slug',
                    'stars',
                    'rating',
                    'feedbacks',
                    'symbol',
                    'cancel_proposal_text',
                    'cancel_proposal_button',
                    'validation_error_text',
                    'cancel_popup_title','Milestones',
                    'request_to_close',
                    'request_to_cancel',
                )
            );
        }
    }

    public function changecloseprojecttatusfreelancer($slug)
    {
        $id = Auth::user()->id;
        $freelancer = User::where('id',$id)->first();
        $status = 1;
        $job = Job::where('slug', $slug)->first();
        $request_to_close = DB::table('proposals')->where('job_id', $job->id)->where('freelancer_id', $id)->update(['request_to_close' => $status]);
        $proposal = Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
        
        //send email
             
        $userE = User::where('id',$job->user_id)->first();

        $data = array();
        
        $data['freelancer_name'] = $freelancer->first_name;
        $data['job_id'] = $job->id;
        $data['job_name'] = $job->title;
        $data['amount'] = $proposal->amount;
        $data['slug'] = $job->slug;
    
        \Notification::send($userE, new RequestToCloseProject($data));

        return \Redirect::back()->withErrors([trans('Request has been sent')]);
    }

    public function changecancelprojecttatusfreelancer($slug)
    {
        $id = Auth::user()->id;
        $freelancer = User::where('id',$id)->first();
        $status = 1;
        $job = Job::where('slug', $slug)->first();
        $request_to_cancel = DB::table('proposals')->where('job_id', $job->id)->where('freelancer_id', $id)->update(['request_to_cancel' => $status]);
        $proposal = Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
        
        //send email
             
        $userE = User::where('id',$job->user_id)->first();

        $data = array();
        
        $data['freelancer_name'] = $freelancer->first_name;
        $data['job_id'] = $job->id;
        $data['job_name'] = $job->title;
        $data['amount'] = $proposal->amount;
        $data['slug'] = $job->slug;
    
        \Notification::send($userE, new RequestToCancelProject($data));

        return \Redirect::back()->withErrors([trans('Request has been sent')]);
    }

    public function changecloseprojecttatusemployer($slug,$id)
    {
        
        $status = 0;
        $job = Job::where('slug', $slug)->first();
        $request_to_close = DB::table('proposals')->where('job_id', $job->id)->where('freelancer_id', $id)->update(['request_to_close' => $status]);
        $proposal = Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
        $employer = User::where('id',$job->user_id)->first();

        //send email
             
        $userF = User::where('id',$id)->first();

        $data = array();
        
        $data['employer_name'] = $employer->first_name;
        $data['job_id'] = $job->id;
        $data['job_name'] = $job->title;
        $data['amount'] = $proposal->amount;
        $data['slug'] = $job->slug;
    
        \Notification::send($userF, new ResponseToCloseProject($data));

        return \Redirect::back()->withErrors([trans('Request has been rejected')]);
    }

    public function changecancelprojecttatusemployer($slug,$id)
    {
        
        $status = 0;
        $job = Job::where('slug', $slug)->first();
        $request_to_cancel = DB::table('proposals')->where('job_id', $job->id)->where('freelancer_id', $id)->update(['request_to_cancel' => $status]);
        $proposal = Proposal::where('job_id', $job->id)->where('freelancer_id', $id)->first();
        $employer = User::where('id',$job->user_id)->first();

        //send email
             
        $userF = User::where('id',$id)->first();

        $data = array();
        
        $data['employer_name'] = $employer->first_name;
        $data['job_id'] = $job->id;
        $data['job_name'] = $job->title;
        $data['amount'] = $proposal->amount;
        $data['slug'] = $job->slug;
    
        \Notification::send($userF, new ResponseToCancelProject($data));

        return \Redirect::back()->withErrors([trans('Request has been rejected')]);
    }

    public function completejobonrequest($id) {
        $job = Proposal::where('job_id',$id)->first();
        // $job_details = Job::where('id', $id)->first();
        $job_status = DB::table('jobs')->where('id', $id)->update(['status'=>"cancelled"]);
        $job_cancel = DB::table('proposals')->where('job_id', $id)->update(['status'=>"cancelled"]);
        $job_details = Job::where('id', $id)->first();
        $employer = User::where('id',$job_details->user_id)->first();
        $job_details = Job::where('id', $id)->first();

         //send email

        $userF = User::where('id',$job->freelancer_id)->first();

        $data = array();

        $data['employer_name'] = $employer->first_name;
        $data['job_id'] = $job->job_id;
        $data['job_name'] = $job_details->title;
        $data['amount'] = $job->amount;
        $data['slug'] = $job_details->slug;

        $cancelled_proposals = Proposal::where('freelancer_id', $job->freelancer_id)->where('status', 'cancelled')->count();
        $freelancer = User::where('id', $job->freelancer_id)->first();
        if ($freelancer) {
            $current_level = $freelancer->level;
            if ($current_level >= 2 && $cancelled_proposals >= 5 && $cancelled_proposals % 5 == 0) {
                $level_toDowngrade = ($cancelled_proposals / 5);
                $level = $current_level - $level_toDowngrade;
                if ($level < 1) {
                    $level = 1;
                }
                $freelancer->update(['level' => $level]);
            }
        }

        \Notification::send($userF, new ResponseToCancelProjectAccept($data));

        return redirect('employer/dashboard/manage-jobs');
        // dd($job);
    }

    public function sendProposalMessage(Request $request)
    {
        
        // dd($request);
        $message = $request->input('message');
        $sender_id = $request->input('sender_id');
        $receiver_id = $request->input('receiver_id');
        $proposal_id = $request->input('proposal_id');
        $send_message = DB::table('proposals_negotiations')->insert([
            'sender_id' => $sender_id, 
            'receiver_id' => $receiver_id, 
            'proposal_id' => $proposal_id, 
            'message' => $message
        ]);
        //send notification
        $role_id =  Helper::getRoleByUserID($receiver_id);
        if ($role_id == 3) {
            $user = User::where('id', $receiver_id)->first();
            $sender = User::where('id', $sender_id)->first();
            $job = Proposal::where('id', $proposal_id)->first();
            $job_proposal = Job::where('id', $job->job_id)->first();
            $job_name = $job_proposal->title;

            $data = array();
            $data['job_id'] = $job->job_id;
            $data['sender_name'] = $sender->first_name;
            $data['job_name'] = $job_name;
            $data['message'] = $message;
    
            \Notification::send($user, new ProposalNegotiationsEmployer($data));
        }
        else {
            $user = User::where('id', $receiver_id)->first();
            $sender = User::where('id', $sender_id)->first();
            $job = Proposal::where('id', $proposal_id)->first();
            $job_proposal = Job::where('id', $job->job_id)->first();
            $job_name = $job_proposal->title;

            $data = array();
            $data['job_id'] = $job->job_id;
            $data['sender_name'] = $sender->first_name;
            $data['job_name'] = $job_name;
            $data['message'] = $message;
    
            \Notification::send($user, new ProposalNegotiationsFreelancer($data));
        }
        return \Redirect::back()->withErrors([trans('Message has been sent')]);
        
    }
}
