<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MinutesOfMeeting;
use App\Notifications\MinutesMeetingNotification;
use Illuminate\Support\Facades\Session;
use App\User;
use App\CommitteeMember;
use App\Job;
use App\MeetingAccept;
use App\Notifications\MinutesOfMeetingAccept;
use App\Notifications\MinutesOfMeetingReject;

use Auth;

class MinutesOfMeetingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
    } public function show($id)
    {
        $MeetingAccept = '';
        $AcceptanceList = '';
        $MinutesOfMeeting = MinutesOfMeeting::find($id);
        $attachments = !empty($MinutesOfMeeting->attachments) ? unserialize($MinutesOfMeeting->attachments) : '';

        $userId = Auth::id();
        if($userId != $MinutesOfMeeting->user_id){

            $CommitteeMembers = CommitteeMember::where('job_id',$MinutesOfMeeting->job_id)->where('member_id',$userId)->first();
            if($CommitteeMembers){
                }else{
                return abort('403');
            }
        }
        
        $MeetingAccept = MeetingAccept::where('user_id',$userId)->where('meeting_minutes_id',$id)->first();
        $AcceptanceList = MeetingAccept::where('meeting_minutes_id',$id)->get();
        return view('back-end.employer.jobs.MinutesOfMeetingShow', compact('MinutesOfMeeting','attachments','MeetingAccept','AcceptanceList'));
    }

    public function list($id)
    {
        $userId = Auth::id();
        
        // $AcceptanceList = array();
        $MinutesOfMeetings = MinutesOfMeeting::where('job_id', $id)->get();
        $CommitteeMembers = CommitteeMember::where('job_id',$id)->get();
        // foreach($MinutesOfMeetings as $MinutesOfMeeting){
        //     $AcceptanceList = MeetingAccept::where('meeting_minutes_id',$MinutesOfMeeting->id)->get();
        //     foreach($CommitteeMembers as $CommitteeMember){
        //         if(MeetingAccept::where('meeting_minutes_id',$MinutesOfMeeting->id)->where('user_id', $CommitteeMember->id)->where('status', '1')->exists()){
        //             $ok = 1;
        //         }
        //         else{
        //             $ok = 0;
        //         }
        //     }
        // }
        $job_id = $id;
        return view('back-end.employer.jobs.ListMinutesOfMeeting', compact('MinutesOfMeetings','job_id', 'CommitteeMembers'));
    }
    public function createMinutes($id)
    {
         $job_id = $id;


        return view('back-end.employer.jobs.MinutesOfMeetingAdd', compact('job_id'));
    }

    public function create($id)
    { 
        
    }


    public function store(Request $request)
    {
         $request->validate([
            'title' => 'required',
            'body' => 'required',
            'job_id' => 'required',
            'location' => 'required',
            'date' => 'required',
            
        ]);
        $user_id = Auth::id();

        $MinutesOfMeeting = new MinutesOfMeeting([
            'title' => $request->get('title'),
            'location' => $request->get('location'),
            'body' => $request->get('body'),
            'date' => $request->get('date'),
            'job_id' => $request->get('job_id'),
            'user_id' => $user_id,
        ]);

        $userId = Auth::id();




        $attach_file = $request['attachments'];
        $Meeting_attachments = array();
        if (!empty($request['attachments'])) {
           foreach ($attach_file as $key => $value) {

            $destinationPath = 'uploads/jobs';
            $filename = time().'-'.$value->getClientOriginalName();
            $value->move($destinationPath, $filename);

            $Meeting_attachments[] = $filename;
           
         

           }
           $MinutesOfMeeting->attachments = serialize($Meeting_attachments);
       }
     

       $MinutesOfMeeting->save();
       $jobDetails = Job::where('id',$request->get('job_id'))->first();
       $jobDetails->MinutesOfMeeting = $MinutesOfMeeting;
       $CommitteeMembers = CommitteeMember::where('job_id',$request->get('job_id'))->pluck('member_id');
       foreach ($CommitteeMembers as $key => $value) {
        $user = User::where('id', $value)->first();
        $user->notify(new MinutesMeetingNotification($jobDetails));
    }
       $job_id = $request->get('job_id');

       return \Redirect::route('add-minutes-of-meeting', $job_id)->with('success', 'Saved!');

    }


    public function edit($id)
    {
        $userId = Auth::id();
     

        $MinutesOfMeeting = MinutesOfMeeting::where('id',$id)->first();
        $attachments = !empty($MinutesOfMeeting->attachments) ? unserialize($MinutesOfMeeting->attachments) : '';
        return view('back-end.employer.jobs.MinutesOfMeetingEdit',compact('MinutesOfMeeting','attachments'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'location' => 'required',
            'date' => 'required',
            
        ]);

         $MinutesOfMeeting = MinutesOfMeeting::find($id);

        $attach_file = $request['attachments'];
        $Meeting_attachments = array();

        if (!empty($request['attachments'])) {
            
            foreach ($attach_file as $key => $value) {
                $Meeting_attachments[] = $value;
            }
           

        }

        $attach_file_new = $request['attachmentsNew'];
     
        if (!empty($request['attachmentsNew'])) {
           foreach ($attach_file_new as $key => $value) {

            $destinationPath = 'uploads/jobs';
            $filename = time().'-'.$value->getClientOriginalName();
            $value->move($destinationPath, $filename);

            $Meeting_attachments[] = $filename;
           
         

           }
       }

       $MinutesOfMeeting->attachments = serialize($Meeting_attachments);

            // $zone = zone::find($id);
        $MinutesOfMeeting->title =  $request->get('title');
        $MinutesOfMeeting->body =  $request->get('body');
        $MinutesOfMeeting->location =  $request->get('location');
        $MinutesOfMeeting->date =  $request->get('date');

        $MinutesOfMeeting->save();
        return \Redirect::route('add-minutes-of-meeting', $MinutesOfMeeting->job_id)->with('success', 'Saved!');

    }

    public function publish(Request $request, $id)
    {
        $MinutesOfMeeting = MinutesOfMeeting::find($id);

        $MinutesOfMeeting->status = !$MinutesOfMeeting->status;
        $MinutesOfMeeting->save();
    
        return \Redirect::back()->withErrors(['success', 'Update']);

    }

    public function destroy($id)
    {
       
            $MinutesOfMeeting = MinutesOfMeeting::find($id);
            $MinutesOfMeeting->delete();
    
            return \Redirect::back()->withErrors(['success', 'Deleted']);

    }
public function acceptmeeting(Request $request){
    // dd($request);
    $minutesofmeetinginfo = MinutesofMeeting::where('id', $request->get('meeting_minutes_id'))->first();
    // dd($minutesofmeetinginfo->job_id);
    $user_job = Job::where('id', $minutesofmeetinginfo->job_id)->first();
    $user_info = User::where('id', $user_job->user_id)->first();
    // dd($user_info);
    //MeetingAccept
    $userId = Auth::id();
    $authuser = Auth::user();

    if($_POST['acceptvalue'] == 'Accept'){
        $status = 1;

        $MeetingAccept = new MeetingAccept([
                'status' => $status,
                'meeting_minutes_id' => $request->get('meeting_minutes_id'),
                'user_id' => $userId,
            ]);
        $MeetingAccept->save();

        $data = array();
        $data['first_name'] = $authuser->first_name;
        $data['last_name'] = $authuser->last_name;
        $data['job'] = $user_job->title;
        $data['job_id'] = $user_job->id;
        $data['minutesofmeeting'] = $minutesofmeetinginfo->title;

        \Notification::send($user_info, new MinutesOfMeetingAccept($data));

    }else{
        $status = 0;

        $MeetingAccept = new MeetingAccept([
            'status' => $status,
            'meeting_minutes_id' => $request->get('meeting_minutes_id'),
            'user_id' => $userId,
        ]);
        $MeetingAccept->save();

        $data = array();
        $data['first_name'] = $authuser->first_name;
        $data['last_name'] = $authuser->last_name;
        $data['job'] = $user_job->title;
        $data['minutesofmeeting'] = $minutesofmeetinginfo->title;

        \Notification::send($user_info, new MinutesOfMeetingReject($data));

    }

    //echo $status; exit;
    // echo "<pre>"; print_r($MeetingAccept); exit;
    

    $response_array['status'] = 'success'; 
    echo json_encode($response_array);
}
   
    
}
