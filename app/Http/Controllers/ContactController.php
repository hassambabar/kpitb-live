<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use View;
use App\Helper;
use Auth;
use App\Contact;
use App\User;
use App\Notifications\ReplytoContactUsQuery;
use App\Notifications\AdministrationsMessage;
use App\Notifications\ContactUsQuery;
use DB;


class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

   public function contactView()
   {
      return view('front-end.contact.show');

   }
    public function index()
    {

        if (!empty($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
           // $skills = $this->skill::where('title', 'like', '%' . $keyword . '%')->paginate(10)->setPath('');
            $Contacts = Contact::where('from', 'like', '%' . $keyword . '%')->paginate(50);
            $pagination = $Contacts->appends(
                array(
                    'keyword' => Input::get('keyword')
                )
            );
        } else {
         $Contacts = Contact::paginate(50);
        }
         return view('back-end.admin.Contacts.index', compact('Contacts'));
    }

    public function create()
    {
      
    }


    public function store(Request $request)
    {
         $request->validate([
            'g-recaptcha-response'=>'required|recaptcha',
            'from' => 'required',
            'email' => 'required',
            'body' => 'required',   
            'subject' => 'required', 
            ]);
        $userId = Auth::id();
         if($userId){
             $user_id = $userId;
          }else{
            $user_id = '';
          }  

        $Contact = new Contact([
            'from' => $request->get('from'),
            'order_type' => $request->get('order_type'),
            'email' => $request->get('email'),
            'body' => $request->get('body'),
            'subject' => $request->get('subject'),
         
            'user_id' => $user_id,
        ]);
       $Contact->save();

        //  send email
       $admin_mail = User::role('admin')->select('*')->first();
      

       $data = array();

       $data['user'] = $request->get('from');
       $data['email'] = $request->get('email');
       $data['subject'] = $request->get('subject');
       $data['body'] = $request->get('body');
       
       \Notification::send($admin_mail, new ContactUsQuery($data));


       $sucessMessage = 'Thank you for getting in touch. We appreciate you contacting us. One of our colleagues will get back in touch with you soon!'; 
       return redirect('/contactus')->with('success', $sucessMessage);
    }


    public function edit($id)
    {

    }

    public function show($id)
    {
        $Contact = Contact::where('id',$id)->first();
        $Contact->status =  1;
        $Contact->save();
        return view('back-end.admin.Contacts.show', compact('Contact'));

    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'contract_id' => 'required',
            'project_id' => 'required',
            'program_id' => 'required',
            'contact_id' => 'required',           
            'order_type' => 'required',           
            'start_date' => 'required',           
            'finish_date' => 'required',           

            ]);

        $Performance = Performance::find($id);
        $Performance->order_type =  $request->get('order_type');
        $Performance->first_remarks =  $request->get('first_remarks');
        $Performance->start_date =  $request->get('start_date');
        $Performance->finish_date =  $request->get('finish_date');
        $Performance->project_id =  $request->get('project_id');
        $Performance->program_id =  $request->get('program_id');
        $Performance->contact_id =  $request->get('contact_id');
        $Performance->contract_id =  $request->get('contract_id');

        $Performance->save();
        return redirect('/performance')->with('success', 'updated!');
    }


    public function destroy($id)
    {
        $userId = Auth::id();
        $Contact = Contact::where('id', $id)->first();

        if($Contact){
            $ContacteDelete = Contact::find($Contact->id);
            $ContacteDelete->delete();
    
        }
   
        return redirect('/admin/contacts')->with('message', 'Deleted successfully!');
    }

    public function reply(Request $request)
    {
        // dd($request);
        $request->validate([
            'reply' => 'required',   
            ]);         

        $reply = DB::table('contacts')->where('id', $request->input('id'))->update(['reply'=>$request->input('reply')]);

        $contact_details = Contact::where('id', $request->input('id'))->first();
         
             //send email
             
        $User = $contact_details->email;
        $user = DB::table('users')->where('email', $User)->pluck('id');
        $user = User::find($user);

        $data = array();
        
        $data['Name'] = $contact_details->from;
        $data['body'] = $contact_details->body;
        $data['reply'] = $contact_details->reply;

        \Notification::send($user, new ReplytoContactUsQuery($data));
            
            return redirect('/admin/contacts')->with('message', 'Replied successfully!');
    }

    public function message($id)
    {
        // dd($id);
        $user = User::where('id',$id)->first(); 
        $user_messages = DB::table('admin_messages')->where('user_id', $id)->get();

        return view('back-end.admin.Contacts.Directory', compact('user','user_messages'));
    }

    public function sendmessage(Request $request)
    {
            // dd($request);
            $contact_details = User::where('id', $request->input('id'))->first();
            $request->validate([
            'message' => 'required',   
            ]);
        
            $message = DB::table('admin_messages')->insert(['user_id'=>$request->input('id'), 'message'=>$request->input('message')]);

             //send email
             
            $User = $contact_details->email;
            $user = DB::table('users')->where('email', $User)->pluck('id');
            $user = User::find($user);

            $data = array();
        
            $data['Name'] = $contact_details->first_name;
            $data['message'] = $request->input('message');
            \Notification::send($user, new AdministrationsMessage($data));
            
            return redirect('/users')->with('message', 'Message sent successfully!');
    }

   
    
}
