<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Facades\Redirect;

class EnsureUserEnable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       // dd(Auth::user()->is_disabled);
        if (Auth::user()->is_disabled == 'true') {
            Session::flash('error', trans('Unfortunately you have been logged out due to Account Disable, For Further details contact to administrator'));
            Auth::logout();
            return Redirect::to('/');
         }

        return $next($request);   
     }
}
