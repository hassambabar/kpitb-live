/* CUSTOM FUNCTION WRITE HERE*/
"use strict";
jQuery(document).on('ready', function() {
    $('.registeration_box input[type="radio"]').click(function() {
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".freelance_box").not(targetBox).hide();
        $(targetBox).show();
    });
    //masking on inout field
    $('.kapra-no').mask('9999999-9');
    //contact number masking
    $('.contact-number').mask('9999-9999999');


    $(".word").fancybox({
        'width': 900, // or whatever
        'height': 600,
        'type': 'iframe'
    });

});
